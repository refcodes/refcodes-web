// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.refcodes.exception.MarshalException;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.runtime.SystemProperty;

public class FormMediaTypeFactoryTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	private MediaTypeFactory _facotory;

	// /////////////////////////////////////////////////////////////////////////
	// SETUP:
	// /////////////////////////////////////////////////////////////////////////
	@BeforeEach
	public void beforeEach() {
		_facotory = new FormMediaTypeFactory();
	}

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testFormFields() throws MarshalException, UnmarshalException {
		final FormFields theInputFields = new FormFields().withAddTo( "numbers", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" ).withAddTo( "token", "1234567890" );
		final String theHttpInputFields = _facotory.toMarshaled( theInputFields );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theHttpInputFields );
		}
		final FormFields theHttpOutput = _facotory.toUnmarshaled( theHttpInputFields, FormFields.class );
		final String theHttpOutputFields = theHttpOutput.toBodyFormFields();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theHttpOutputFields );
		}
		assertEquals( theHttpInputFields, theHttpOutputFields );
	}
}
