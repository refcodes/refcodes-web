// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import static org.junit.jupiter.api.Assertions.*;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import org.refcodes.data.Delimiter;
import org.refcodes.runtime.SystemProperty;

public class RequestHeaderFieldsTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testHeaderFieldCookie() {
		final RequestHeaderFields theHeaderFields = new RequestHeaderFields();
		final String theKey = "REFCODES";
		final String theValue = "5161";
		theHeaderFields.addTo( HeaderField.COOKIE.getName(), theKey + Delimiter.COOKIE_TUPEL.getChar() + theValue );
		assertEquals( theHeaderFields.getAllCookies().size(), 1 );
		final RequestCookie theCookie = theHeaderFields.getAllCookies().get( 0 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theHeaderFields.toHttpCookies() ) );
		}
		assertEquals( theKey, theCookie.getKey() );
		assertEquals( theValue, theCookie.getValue() );
	}

	@Test
	public void testHeaderFieldCookies() {
		final RequestHeaderFields theHeaderFields = new RequestHeaderFields();
		String theKey = "REFCODES";
		String theValue = "5161";
		theHeaderFields.addTo( HeaderField.COOKIE.getName(), theKey + Delimiter.COOKIE_TUPEL.getChar() + theValue );
		assertEquals( theHeaderFields.getAllCookies().size(), 1 );
		RequestCookie theCookie = theHeaderFields.getAllCookies().get( 0 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theHeaderFields.toHttpCookies() ) );
		}
		assertEquals( theKey, theCookie.getKey() );
		assertEquals( theValue, theCookie.getValue() );
		theKey = "COMCODES";
		theValue = "5162";
		theHeaderFields.addTo( HeaderField.COOKIE.getName(), theKey + Delimiter.COOKIE_TUPEL.getChar() + theValue );
		assertEquals( theHeaderFields.getAllCookies().size(), 2 );
		theCookie = theHeaderFields.getAllCookies().get( 1 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theHeaderFields.toHttpCookies() ) );
		}
		assertEquals( theKey, theCookie.getKey() );
		assertEquals( theValue, theCookie.getValue() );
	}

	@Test
	public void testMixedCookies() {
		final RequestHeaderFields theHeaderFields = new RequestHeaderFields();
		String theKey = "REFCODES";
		String theValue = "5161";
		theHeaderFields.addTo( HeaderField.COOKIE.getName(), theKey + Delimiter.COOKIE_TUPEL.getChar() + theValue );
		assertEquals( theHeaderFields.getAllCookies().size(), 1 );
		RequestCookie theCookie = theHeaderFields.getAllCookies().get( 0 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theHeaderFields.toHttpCookies() ) );
		}
		assertEquals( theKey, theCookie.getKey() );
		assertEquals( theValue, theCookie.getValue() );
		theKey = "COMCODES";
		theValue = "5162";
		theHeaderFields.addTo( HeaderField.COOKIE.getName(), theKey + Delimiter.COOKIE_TUPEL.getChar() + theValue );
		assertEquals( theHeaderFields.getAllCookies().size(), 2 );
		theCookie = theHeaderFields.getAllCookies().get( 1 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theHeaderFields.toHttpCookies() ) );
		}
		assertEquals( theKey, theCookie.getKey() );
		assertEquals( theValue, theCookie.getValue() );
		theKey = "FUNCODES";
		theValue = "5163";
		theHeaderFields.addCookie( theKey, theValue );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theHeaderFields.toHttpCookies() ) );
		}
		assertEquals( theHeaderFields.getAllCookies().size(), 3 );
		theCookie = theHeaderFields.getAllCookies().get( 2 );
		assertEquals( theKey, theCookie.getKey() );
		assertEquals( theValue, theCookie.getValue() );
	}
}
