// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.textual.VerboseTextBuilder;

/**
 * @author steiner
 */
public class MediaTypeFactoriesTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Disabled
	@Test
	public void testXmlMediaTypes() {
		final XmlMediaTypeFactory theFactory = new XmlMediaTypeFactory();
		System.out.println( new VerboseTextBuilder().toString( theFactory.getMediaTypes() ) );
	}

	@Disabled
	@Test
	public void testYamlMediaTypes() {
		final YamlMediaTypeFactory theFactory = new YamlMediaTypeFactory();
		System.out.println( new VerboseTextBuilder().toString( theFactory.getMediaTypes() ) );
	}

	@Disabled
	@Test
	public void testJsonMediaTypes() {
		final JsonMediaTypeFactory theFactory = new JsonMediaTypeFactory();
		System.out.println( new VerboseTextBuilder().toString( theFactory.getMediaTypes() ) );
	}

	@Disabled
	@Test
	public void testTextMediaTypes() {
		final TextMediaTypeFactory theFactory = new TextMediaTypeFactory();
		System.out.println( new VerboseTextBuilder().toString( theFactory.getMediaTypes() ) );
	}

	@Disabled
	@Test
	public void testDetermineYamlMediaTypes() {
		final List<MediaType> theMediaTypes = new ArrayList<>();
		for ( MediaType eMediaType : MediaType.values() ) {
			if ( eMediaType.getName().toLowerCase().endsWith( "yaml" ) || eMediaType.getName().toLowerCase().endsWith( "yml" ) ) {
				if ( !theMediaTypes.contains( eMediaType ) ) {
					theMediaTypes.add( eMediaType );
				}
			}
		}
		for ( MediaType eMediaType : theMediaTypes ) {
			System.out.println( eMediaType );
		}
	}

	@Disabled
	@Test
	public void testDetermineXmlMediaTypes() {
		final List<MediaType> theMediaTypes = new ArrayList<>();
		for ( MediaType eMediaType : MediaType.values() ) {
			if ( eMediaType.getName().toLowerCase().endsWith( "xml" ) ) {
				if ( !theMediaTypes.contains( eMediaType ) ) {
					theMediaTypes.add( eMediaType );
				}
			}
		}
		for ( MediaType eMediaType : theMediaTypes ) {
			System.out.println( eMediaType.name() );
		}
	}

	@Disabled
	@Test
	public void testDetermineJsonMediaTypes() {
		final List<MediaType> theMediaTypes = new ArrayList<>();
		for ( MediaType eMediaType : MediaType.values() ) {
			if ( eMediaType.getName().toLowerCase().endsWith( "json" ) ) {
				if ( !theMediaTypes.contains( eMediaType ) ) {
					theMediaTypes.add( eMediaType );
				}
			}
		}
		for ( MediaType eMediaType : theMediaTypes ) {
			System.out.println( eMediaType.name() );
		}
	}

	@Disabled
	@Test
	public void testDetermineTextMediaTypes() {
		final List<MediaType> theMediaTypes = new ArrayList<>();
		for ( MediaType eMediaType : MediaType.values() ) {
			if ( eMediaType.getTopLevelType() == TopLevelType.TEXT || eMediaType.getName().toLowerCase().endsWith( "plain" ) || eMediaType.getName().toLowerCase().endsWith( "text" ) || eMediaType.getName().toLowerCase().endsWith( "txt" ) ) {
				if ( !theMediaTypes.contains( eMediaType ) ) {
					theMediaTypes.add( eMediaType );
				}
			}
		}
		for ( MediaType eMediaType : theMediaTypes ) {
			System.out.println( eMediaType.name() );
		}
	}
}
