// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import static org.junit.jupiter.api.Assertions.*;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.textual.VerboseTextBuilder;

public class FormFieldsTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testToQueryString() {
		final FormFields theFields = new FormFields();
		theFields.addTo( "elements", "1" );
		theFields.addTo( "elements", "2" );
		theFields.addTo( "elements", "3" );
		theFields.addTo( "order", "ascending" );
		assertEquals( "?elements=1&elements=2&elements=3&order=ascending", theFields.toUrlQueryString() );
	}

	@Test
	public void testFromQueryString() {
		final FormFields theFields = new FormFields();
		theFields.fromUrlQueryString( "?elements=1&elements=2&elements=3&order=ascending&guest" );
		List<String> elements = theFields.get( "elements" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( new VerboseTextBuilder().withElements( elements ) );
		}
		assertEquals( 3, elements.size() );
		assertEquals( "1", elements.get( 0 ) );
		assertEquals( "2", elements.get( 1 ) );
		assertEquals( "3", elements.get( 2 ) );
		elements = theFields.get( "order" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( new VerboseTextBuilder().withElements( elements ) );
		}
		assertEquals( 1, elements.size() );
		assertEquals( "ascending", elements.get( 0 ) );
		elements = theFields.get( "guest" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( new VerboseTextBuilder().withElements( elements ) );
		}
		assertEquals( 1, elements.size() );
		assertEquals( "", elements.get( 0 ) );
	}

	@Test
	public void testFromUrl() {
		final FormFields theFields = new FormFields();
		theFields.fromUrl( "https://www.refcodes.org/sources?elements=1&elements=2&elements=3&order=ascending&guest" );
		List<String> elements = theFields.get( "elements" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( new VerboseTextBuilder().withElements( elements ) );
		}
		assertEquals( 3, elements.size() );
		assertEquals( "1", elements.get( 0 ) );
		assertEquals( "2", elements.get( 1 ) );
		assertEquals( "3", elements.get( 2 ) );
		elements = theFields.get( "order" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( new VerboseTextBuilder().withElements( elements ) );
		}
		assertEquals( 1, elements.size() );
		assertEquals( "ascending", elements.get( 0 ) );
		elements = theFields.get( "guest" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( new VerboseTextBuilder().withElements( elements ) );
		}
		assertEquals( 1, elements.size() );
		assertEquals( "", elements.get( 0 ) );
	}
}
