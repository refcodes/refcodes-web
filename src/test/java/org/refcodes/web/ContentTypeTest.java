// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import org.junit.jupiter.api.Test;
import org.refcodes.data.Encoding;
import org.refcodes.runtime.SystemProperty;

public class ContentTypeTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testContentType() {
		final ContentType theContentType = new ContentType( MediaType.APPLICATION_JSON );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Content type := " + theContentType.toHttpMediaType() );
		}
		final String theHttpMediaType = theContentType.toHttpMediaType();
		final ContentType theHttpContentType = new ContentType( theHttpMediaType );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "HTTP content type := " + theHttpContentType.toHttpMediaType() );
		}
	}

	@Test
	public void testContentTypeParameters() {
		final ContentType theContentType = new ContentType( MediaType.APPLICATION_JSON ).withPut( MediaTypeParameter.CHARSET, Encoding.UTF_8.getCode() ).withPut( "dummy", "fu" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Content type := " + theContentType.toHttpMediaType() );
		}
		final String theHttpMediaType = theContentType.toHttpMediaType();
		final ContentType theHttpContentType = new ContentType( theHttpMediaType );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "HTTP content type := " + theHttpContentType.toHttpMediaType() );
		}
	}
}
