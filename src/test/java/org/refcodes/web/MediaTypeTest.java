// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class MediaTypeTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * Test media types.
	 */
	@Test
	public void testMediaTypes() {
		assertEquals( MediaType.APPLICATION_JSON.toHttpMediaType(), "application/json" );
		assertEquals( MediaType.APPLICATION_XML.toHttpMediaType(), "application/xml" );
		assertEquals( MediaType.APPLICATION_JAVASCRIPT.toHttpMediaType(), "application/javascript" );
		assertEquals( MediaType.APPLICATION_POSTSCRIPT.toHttpMediaType(), "application/postscript" );
		assertEquals( MediaType.IMAGE_BMP.toHttpMediaType(), "image/bmp" );
		assertEquals( MediaType.TEXT_CSS.toHttpMediaType(), "text/css" );
		assertEquals( MediaType.TEXT_HTML.toHttpMediaType(), "text/html" );
	}
}
