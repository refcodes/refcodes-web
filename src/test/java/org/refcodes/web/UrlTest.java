// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import static org.junit.jupiter.api.Assertions.*;
import java.net.MalformedURLException;
import org.junit.jupiter.api.Test;
import org.refcodes.data.Scheme;
import org.refcodes.runtime.SystemProperty;

public class UrlTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testUrl1() throws MalformedURLException {
		final String theCidrNotation = "http://identity:secret@host/path?a=1&b=2#fragment";
		final Url theUrl = new Url( theCidrNotation );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theUrl.toHttpUrl() );
		}
		assertEquals( theUrl.getScheme(), Scheme.HTTP );
		assertEquals( theUrl.getIdentity(), "identity" );
		assertEquals( theUrl.getSecret(), "secret" );
		assertEquals( theUrl.getHost(), "host" );
		assertEquals( theUrl.getPort(), -1 );
		assertEquals( theUrl.getPath(), "/path" );
		assertEquals( theUrl.getQueryFields().toBodyFormFields(), "a=1&b=2" );
		assertEquals( theUrl.getFragment(), "fragment" );
		assertEquals( theCidrNotation, theUrl.toHttpUrl() );
	}

	@Test
	public void testUrl1Port() throws MalformedURLException {
		final String theCidrNotation = "http://identity:secret@host:80/path?a=1&b=2#fragment";
		final Url theUrl = new Url( theCidrNotation );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theUrl.toHttpUrl() );
		}
		assertEquals( theUrl.getScheme(), Scheme.HTTP );
		assertEquals( theUrl.getIdentity(), "identity" );
		assertEquals( theUrl.getSecret(), "secret" );
		assertEquals( theUrl.getHost(), "host" );
		assertEquals( theUrl.getPort(), 80 );
		assertEquals( theUrl.getPath(), "/path" );
		assertEquals( theUrl.getQueryFields().toBodyFormFields(), "a=1&b=2" );
		assertEquals( theUrl.getFragment(), "fragment" );
		assertEquals( theCidrNotation, theUrl.toHttpUrl() );
	}

	@Test
	public void testUrl2() throws MalformedURLException {
		final String theCidrNotation = "http://identity@host/path?a=1&b=2#fragment";
		final Url theUrl = new Url( theCidrNotation );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theUrl.toHttpUrl() );
		}
		assertEquals( theUrl.getScheme(), Scheme.HTTP );
		assertEquals( theUrl.getIdentity(), "identity" );
		assertEquals( theUrl.getSecret(), null );
		assertEquals( theUrl.getHost(), "host" );
		assertEquals( theUrl.getPort(), -1 );
		assertEquals( theUrl.getPath(), "/path" );
		assertEquals( theUrl.getQueryFields().toBodyFormFields(), "a=1&b=2" );
		assertEquals( theUrl.getFragment(), "fragment" );
		assertEquals( theCidrNotation, theUrl.toHttpUrl() );
	}

	@Test
	public void testUrl2Port() throws MalformedURLException {
		final String theCidrNotation = "http://identity@host:80/path?a=1&b=2#fragment";
		final Url theUrl = new Url( theCidrNotation );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theUrl.toHttpUrl() );
		}
		assertEquals( theUrl.getScheme(), Scheme.HTTP );
		assertEquals( theUrl.getIdentity(), "identity" );
		assertEquals( theUrl.getSecret(), null );
		assertEquals( theUrl.getHost(), "host" );
		assertEquals( theUrl.getPort(), 80 );
		assertEquals( theUrl.getPath(), "/path" );
		assertEquals( theUrl.getQueryFields().toBodyFormFields(), "a=1&b=2" );
		assertEquals( theUrl.getFragment(), "fragment" );
		assertEquals( theCidrNotation, theUrl.toHttpUrl() );
	}

	@Test
	public void testUrl3() throws MalformedURLException {
		final String theCidrNotation = "http://host/path?a=1&b=2#fragment";
		final Url theUrl = new Url( theCidrNotation );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theUrl.toHttpUrl() );
		}
		assertEquals( theUrl.getScheme(), Scheme.HTTP );
		assertEquals( theUrl.getIdentity(), null );
		assertEquals( theUrl.getSecret(), null );
		assertEquals( theUrl.getHost(), "host" );
		assertEquals( theUrl.getPort(), -1 );
		assertEquals( theUrl.getPath(), "/path" );
		assertEquals( theUrl.getQueryFields().toBodyFormFields(), "a=1&b=2" );
		assertEquals( theUrl.getFragment(), "fragment" );
		assertEquals( theCidrNotation, theUrl.toHttpUrl() );
	}

	@Test
	public void testUrl3Port() throws MalformedURLException {
		final String theCidrNotation = "http://host:80/path?a=1&b=2#fragment";
		final Url theUrl = new Url( theCidrNotation );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theUrl.toHttpUrl() );
		}
		assertEquals( theUrl.getScheme(), Scheme.HTTP );
		assertEquals( theUrl.getIdentity(), null );
		assertEquals( theUrl.getSecret(), null );
		assertEquals( theUrl.getHost(), "host" );
		assertEquals( theUrl.getPort(), 80 );
		assertEquals( theUrl.getPath(), "/path" );
		assertEquals( theUrl.getQueryFields().toBodyFormFields(), "a=1&b=2" );
		assertEquals( theUrl.getFragment(), "fragment" );
		assertEquals( theCidrNotation, theUrl.toHttpUrl() );
	}

	@Test
	public void testAppendPathElements1() throws MalformedURLException {
		final UrlBuilder theUrl = new UrlBuilder( "http://www.western.local" );
		theUrl.appendToPath( "good" );
		theUrl.appendToPath( "ugly" );
		theUrl.appendToPath( "bad" );
		assertEquals( "http://www.western.local/good/ugly/bad", theUrl.toHttpUrl() );
	}

	@Test
	public void testAppendPathElements2() throws MalformedURLException {
		final UrlBuilder theUrl = new UrlBuilder( "http://www.western.local" );
		theUrl.appendToPath( "/good/" );
		theUrl.appendToPath( "/ugly/" );
		theUrl.appendToPath( "/bad/" );
		assertEquals( "http://www.western.local/good/ugly/bad", theUrl.toHttpUrl() );
	}

	@Test
	public void testAppendPathElements3() throws MalformedURLException {
		final UrlBuilder theUrl = new UrlBuilder( "http://www.western.local" );
		theUrl.appendToPath( "//good//" );
		theUrl.appendToPath( "//ugly//" );
		theUrl.appendToPath( "//bad//" );
		assertEquals( "http://www.western.local/good/ugly/bad", theUrl.toHttpUrl() );
	}

	@Test
	public void testAppendPathElements4() throws MalformedURLException {
		final UrlBuilder theUrl = new UrlBuilder( "http://www.western.local//" );
		theUrl.appendToPath( "/good/ugly" );
		theUrl.appendToPath( "//bad//" );
		assertEquals( "http://www.western.local/good/ugly/bad", theUrl.toHttpUrl() );
	}
}
