// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.exception.MarshalException;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.runtime.SystemProperty;

public class JsonMediaTypeFactoryTest extends AbstractMediaFactoryTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	// @formatter:off
	private static final String WIKIPEDIA_EN_JSON = 
		"{\n" + 
		"  \"firstName\": \"John\",\n" + 
		"  \"lastName\": \"Smith\",\n" + 
		"  \"isAlive\": true,\n" + 
		"  \"age\": 25,\n" + 
		"  \"address\": {\n" + 
		"    \"streetAddress\": \"21 2nd Street\",\n" + 
		"    \"city\": \"New York\",\n" + 
		"    \"state\": \"NY\",\n" + 
		"    \"postalCode\": \"10021-3100\"\n" + 
		"  },\n" + 
		"  \"phoneNumbers\": [\n" + 
		"    {\n" + 
		"      \"type\": \"home\",\n" + 
		"      \"number\": \"212 555-1234\"\n" + 
		"    },\n" + 
		"    {\n" + 
		"      \"type\": \"office\",\n" + 
		"      \"number\": \"646 555-4567\"\n" + 
		"    },\n" + 
		"    {\n" + 
		"      \"type\": \"mobile\",\n" + 
		"      \"number\": \"123 456-7890\"\n" + 
		"    }\n" + 
		"  ],\n" + 
		"  \"children\": [],\n" + // Empty arrays are ignored by GSON
		"  \"spouse\": null\n" + 
		"}";
	private static final String WIKIPEDIA_DE_JSON = 
		"{\n" + 
		"  \"Herausgeber\": \"Xema\",\n" + 
		"  \"Nummer\": \"1234-5678-9012-3456\",\n" + 
		"  \"Deckung\": 2e+6,\n" + 
		"  \"Waehrung\": \"EURO\",\n" + 
		"  \"Inhaber\":\n" + 
		"  {\n" + 
		"    \"Name\": \"Mustermann\",\n" + 
		"    \"Vorname\": \"Max\",\n" + 
		"    \"maennlich\": true,\n" + 
		"    \"Hobbys\": [ \"Reiten\", \"Golfen\", \"Lesen\" ],\n" + 
		"    \"Alter\": 42,\n" + 
		"    \"Kinder\": [],\n" +  // Empty arrays are ignored by GSON
		"    \"Partner\": null\n" + 
		"  }\n" + 
		"}";
	// @formatter:on

	// /////////////////////////////////////////////////////////////////////////
	// SETUP:
	// /////////////////////////////////////////////////////////////////////////
	@BeforeEach
	public void beforeEach() {
		_factory = new JsonMediaTypeFactory();
	}

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testFromJsonAsEnWikipeda() throws MarshalException, UnmarshalException {
		Map<?, ?> theMap = _factory.toUnmarshaled( WIKIPEDIA_EN_JSON, Map.class );
		final HttpBodyMap theBodyMapA = new HttpBodyMap( theMap );
		final String theJson = _factory.toMarshaled( theBodyMapA.toDataStructure() );
		theMap = _factory.toUnmarshaled( theJson, Map.class );
		final HttpBodyMap theBodyMapB = new HttpBodyMap( theMap );
		List<String> theKeys = new ArrayList<>( theBodyMapA.keySet() );
		Collections.sort( theKeys );
		for ( String eKey : theKeys ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eKey + " := " + theBodyMapA.get( eKey ) );
			}
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println();
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theJson );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println();
		}
		theKeys = new ArrayList<>( theBodyMapB.keySet() );
		Collections.sort( theKeys );
		for ( String eKey : theKeys ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eKey + " := " + theBodyMapB.get( eKey ) );
			}
		}
		assertEquals( theBodyMapA, theBodyMapB );
	}

	@Test
	public void testFromJsonAsDeWikipeda() throws MarshalException, UnmarshalException {
		Map<?, ?> theMap = _factory.toUnmarshaled( WIKIPEDIA_DE_JSON, Map.class );
		final HttpBodyMap theBodyMapA = new HttpBodyMap( theMap );
		final String theJson = _factory.toMarshaled( theBodyMapA.toDataStructure() );
		theMap = _factory.toUnmarshaled( theJson, Map.class );
		final HttpBodyMap theBodyMapB = new HttpBodyMap( theMap );
		List<String> theKeys = new ArrayList<>( theBodyMapA.keySet() );
		Collections.sort( theKeys );
		for ( String eKey : theKeys ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eKey + " := " + theBodyMapA.get( eKey ) );
			}
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println();
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theJson );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println();
		}
		theKeys = new ArrayList<>( theBodyMapB.keySet() );
		Collections.sort( theKeys );
		for ( String eKey : theKeys ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eKey + " := " + theBodyMapB.get( eKey ) );
			}
		}
		assertEquals( theBodyMapA, theBodyMapB );
	}

	@Disabled("Test some edge case")
	@Test
	public void testEdgeCase() throws MarshalException, UnmarshalException {
		final String theJson = _factory.toMarshaled( "Hallo Welt!" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theJson );
		}
	}
}
