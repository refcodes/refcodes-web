// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.exception.MarshalException;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.struct.CanonicalMap;
import org.refcodes.struct.CanonicalMap.CanonicalMapBuilder;
import org.refcodes.struct.CanonicalMapBuilderImpl;
import org.refcodes.struct.CanonicalMapImpl;

public class HtmlMediaTypeFactoryTest extends AbstractMediaFactoryTest {

	// /////////////////////////////////////////////////////////////////////////
	// SETUP:
	// /////////////////////////////////////////////////////////////////////////

	@BeforeEach
	public void beforeEach() {
		_factory = new HtmlMediaTypeFactory();
	}

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Disabled
	@Test
	public void testEdgeCase() throws MarshalException, UnmarshalException {
		final HtmlMediaTypeFactory theFactory = new HtmlMediaTypeFactory();
		final CanonicalMapBuilder theDataStructure = new CanonicalMapBuilderImpl();
		theDataStructure.put( "html/body/aaa/aaa_111", "aaa_111" );
		theDataStructure.put( "html/body/aaa/aaa_222", "aaa_222" );
		theDataStructure.put( "html/body/aaa/aaa_333", "aaa_333" );
		theDataStructure.put( "html/body/bbb/bbb_111", "bbb_111" );
		theDataStructure.put( "html/body/bbb/bbb_222", "bbb_222" );
		theDataStructure.put( "html/body/bbb/bbb_333", "bbb_333" );
		theDataStructure.put( "html/body/ccc/ccc_111", "ccc_111" );
		theDataStructure.put( "html/body/ccc/ccc_222", "ccc_222" );
		theDataStructure.put( "html/body/ccc/ccc_333", "ccc_333" );
		final String theMarshaled = theFactory.toMarshaled( theDataStructure );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theMarshaled );
		}
		final HttpBodyMap theUnmarshaled = theFactory.toUnmarshaled( theMarshaled, HttpBodyMap.class );
		final CanonicalMap theUnmarshaledStructure = new CanonicalMapImpl( theUnmarshaled );
		for ( String eKey : theDataStructure.sortedKeys() ) {
			System.out.println( eKey + ": " + theUnmarshaledStructure.get( eKey ) );
		}
		System.out.println( theFactory.toMarshaled( theUnmarshaled ) );
		assertEquals( theDataStructure.size(), theUnmarshaled.size() );
		for ( String eKey : theDataStructure.sortedKeys() ) {
			assertEquals( theDataStructure.get( eKey ), theUnmarshaled.get( eKey ) );
		}
	}
}