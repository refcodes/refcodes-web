// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import static org.junit.jupiter.api.Assertions.*;
import java.util.List;
import java.util.Locale;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.textual.VerboseTextBuilder;

public class HeaderFieldsTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testAcceptLanguage() {
		final Locale[] theLanguages = new Locale[] { new Locale.Builder().setLanguage( "de" ).build(), new Locale.Builder().setLanguage( "de" ).setRegion( "DE" ).build(), new Locale.Builder().setLanguage( "en" ).build(), new Locale.Builder().setLanguage( "en" ).setRegion( "GB" ).build() };
		final String theExpected = new VerboseTextBuilder().withElements( theLanguages ).toString();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "The expected := " + theExpected );
		}
		final HeaderField theHeaderField = HeaderField.ACCEPT_LANGUAGE;
		final HeaderFields<?, ?> theHeaderFields = new RequestHeaderFields();
		theHeaderFields.putAcceptLanguages( theLanguages );
		final List<Locale> theFields = theHeaderFields.getAcceptLanguages();
		final String theResult = new VerboseTextBuilder().withElements( theFields ).toString();
		assertEquals( theExpected, theResult );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Resulting fields := " + theResult );
		}
		assertEquals( theFields.size(), 4 );
		List<String> theStrings = theHeaderFields.get( theHeaderField );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Retrieved raw fields := " + new VerboseTextBuilder().withElements( theStrings ).toString() );
		}
		assertEquals( theStrings.size(), 1 );
		theHeaderFields.putAcceptLanguages( theFields );
		final List<Locale> theElements = theHeaderFields.getAcceptLanguages();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Re-Retrieved elements := " + new VerboseTextBuilder().withElements( theElements ).toString() );
		}
		assertEquals( theElements.size(), 4 );
		theStrings = theHeaderFields.get( theHeaderField );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Re-Retrieved raw fields := " + new VerboseTextBuilder().withElements( theStrings ).toString() );
		}
		assertEquals( theStrings.size(), 1 );
	}

	@Test
	public void testAcceptTypes() {
		final HeaderField theHeaderField = HeaderField.ACCEPT;
		final HeaderFields<?, ?> theHeaderFields = new RequestHeaderFields();
		theHeaderFields.putAcceptTypes( MediaType.APPLICATION_JSON, MediaType.TEXT_XML );
		final List<ContentType> theFields = theHeaderFields.getAcceptTypes();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Retrieved fields := " + new VerboseTextBuilder().withElements( theFields ).toString() );
		}
		assertEquals( theFields.size(), 2 );
		List<String> theStrings = theHeaderFields.get( theHeaderField );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Retrieved raw fields := " + new VerboseTextBuilder().withElements( theStrings ).toString() );
		}
		assertEquals( theStrings.size(), 1 );
		theHeaderFields.putAcceptTypes( theFields );
		final List<ContentType> theElements = theHeaderFields.getAcceptTypes();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Re-Retrieved elements := " + new VerboseTextBuilder().withElements( theElements ).toString() );
		}
		assertEquals( theElements.size(), 2 );
		theStrings = theHeaderFields.get( theHeaderField );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Re-Retrieved raw fields := " + new VerboseTextBuilder().withElements( theStrings ).toString() );
		}
		assertEquals( theStrings.size(), 1 );
	}

	@Test
	public void testContentEncoding() {
		final HeaderField theHeaderField = HeaderField.ACCEPT_ENCODING;
		final HeaderFields<?, ?> theHeaderFields = new RequestHeaderFields();
		theHeaderFields.putAcceptEncodings( ContentEncoding.GZIP, ContentEncoding.IDENTITY );
		final List<ContentEncoding> theFields = theHeaderFields.getAcceptEncodings();
		List<ContentEncoding> theElements = theHeaderFields.getAcceptEncodings();
		assertEquals( theElements.size(), 2 );
		assertEquals( "{ GZIP, IDENTITY }", new VerboseTextBuilder().withElements( theElements ).toString() );
		List<String> theStrings = theHeaderFields.get( theHeaderField );
		assertEquals( theStrings.size(), 1 );
		assertEquals( "{ \"gzip,identity\" }", new VerboseTextBuilder().withElements( theStrings ).toString() );
		theHeaderFields.putAcceptEncodings( theFields );
		theElements = theHeaderFields.getAcceptEncodings();
		assertEquals( theElements.size(), 2 );
		theStrings = theHeaderFields.get( theHeaderField );
		assertEquals( theStrings.size(), 1 );
		assertEquals( "{ GZIP, IDENTITY }", new VerboseTextBuilder().withElements( theElements ).toString() );
		assertEquals( "{ \"gzip,identity\" }", new VerboseTextBuilder().withElements( theStrings ).toString() );
	}

	@Test
	public void testBasicAuthenticateRealm() {
		final HeaderFields<?, ?> theHeaderFields = new RequestHeaderFields();
		String theRealm = "REALM";
		theHeaderFields.putBasicAuthRequired( theRealm );
		String toRealm = theHeaderFields.toBasicAuthRealm();
		assertEquals( theRealm, toRealm );
		theRealm = "";
		theHeaderFields.putBasicAuthRequired( theRealm );
		toRealm = theHeaderFields.toBasicAuthRealm();
		assertEquals( theRealm, toRealm );
	}
}
