// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class HttpStatusCodeTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testHttpStatusCodeExceptions() {
		Exception eException;
		final int len = "Exception".length();
		String eStatusName;
		String eExceptionName;
		for ( HttpStatusCode eStatusCode : HttpStatusCode.values() ) {
			try {
				eException = eStatusCode.toHttpStatusException( "Test" );
				if ( eStatusCode.isErrorStatus() || eStatusCode.isRedirectStatus() ) {
					assertEquals( "Test", eException.getMessage() );
					if ( !eStatusCode.name().toLowerCase().contains( "unassigned" ) ) {
						eStatusName = eStatusCode.name().replaceAll( "_", "" ).toLowerCase();
						eExceptionName = eException.getClass().getSimpleName().toLowerCase();
						eExceptionName = eExceptionName.substring( 0, eExceptionName.length() - len );
						if ( SystemProperty.LOG_TESTS.isEnabled() ) {
							System.out.println( eStatusCode + ": " + eStatusName + " := " + eExceptionName );
						}
						assertEquals( eExceptionName, eStatusName );
					}
				}
				else {
					assertEquals( null, eException );
				}
			}
			catch ( Exception e ) {
				fail( "Failed on status <" + eStatusCode + ">", e );
			}
		}
	}

	@Test
	public void testHttpStatusCodeRuntimeExceptions() {
		Exception eException;
		final int len = "RuntimeException".length();
		String eStatusName;
		String eExceptionName;
		for ( HttpStatusCode eStatusCode : HttpStatusCode.values() ) {
			try {
				eException = eStatusCode.toHttpStatusRuntimeException( "Test" );
				if ( eStatusCode.isErrorStatus() || eStatusCode.isRedirectStatus() ) {
					assertEquals( "Test", eException.getMessage() );
					if ( !eStatusCode.name().toLowerCase().contains( "unassigned" ) ) {
						eStatusName = eStatusCode.name().replaceAll( "_", "" ).toLowerCase();
						eExceptionName = eException.getClass().getSimpleName().toLowerCase();
						eExceptionName = eExceptionName.substring( 0, eExceptionName.length() - len );
						if ( SystemProperty.LOG_TESTS.isEnabled() ) {
							System.out.println( eStatusName + " := " + eExceptionName );
						}
						assertEquals( eExceptionName, eStatusName );
					}
				}
				else {
					assertEquals( null, eException );
				}
			}
			catch ( Exception e ) {
				fail( "Failed on status <" + eStatusCode + ">", e );
			}
		}
	}

	@Test
	public void testEdgeCase() {
		final HttpStatusException theException = HttpStatusCode.TEMPORARY_REDIRECT.toHttpStatusException( "Test" );
		assertNotNull( theException );
		assertEquals( HttpStatusCode.TEMPORARY_REDIRECT, theException.getStatusCode() );
	}

	@Test
	public void TestExceptionFactory() {
		for ( HttpStatusCode eCode : HttpStatusCode.values() ) {
			if ( eCode.isErrorStatus() ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eCode );
				}
				assertEquals( eCode, eCode.toHttpStatusException( "" ).getStatusCode() );
				assertEquals( eCode, eCode.toHttpStatusRuntimeException( "" ).getStatusCode() );
			}
		}
	}

	@Disabled
	@Test
	public void generateUnknownStatusCodes() {
		HttpStatusCode eCode;
		String eCategory;
		for ( int i = 100; i <= 999; i++ ) {
			eCategory = null;
			if ( i >= 100 && i <= 199 ) {
				eCategory = "Unassigned informational";
			}
			if ( i >= 200 && i <= 299 ) {
				eCategory = "Unassigned success";
			}
			if ( i >= 300 && i <= 399 ) {
				eCategory = "Unassigned redirection";
			}
			if ( i >= 400 && i <= 499 ) {
				eCategory = "Unassigned client error";
			}
			if ( i >= 500 && i <= 599 ) {
				eCategory = "Unassigned server error";
			}
			if ( i >= 600 ) {
				eCategory = "Unassigned legacy";
			}
			eCode = HttpStatusCode.toHttpStatusCode( i );
			if ( eCode == null ) {
				final String eEnum = eCategory.toUpperCase().replaceAll( " ", "_" ) + "_" + i;
				if ( i < 300 ) {
					System.out.println( "/**" );
					System.out.println( " * " + i + " " + eCategory + " (unknown or undefined) HTTP-Status-Code." );
					System.out.println( " */" );
					System.out.println( eEnum + "(" + i + ")," );
					System.out.println();
				}
				else {
					System.out.println( "/**" );
					System.out.println( " * " + i + " " + eCategory + " (unknown or undefined) HTTP-Status-Code." );
					System.out.println( " */" );
					System.out.println( eEnum + "(" + i + ", aMessage -> new UnassignedStatusCodeException( " + i + ", aMessage ), aMessage -> new UnassignedStatusCodeRuntimeException( " + i + ", aMessage ) )," );
					System.out.println();
				}
			}
		}
	}
}
