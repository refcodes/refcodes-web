// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import static org.junit.jupiter.api.Assertions.*;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import org.junit.jupiter.api.Test;
import org.refcodes.data.Delimiter;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.time.DateFormat;

public class ResponseHeaderFieldsTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testHeaderFieldCookie() {
		final ResponseHeaderFields theHeaderFields = new ResponseHeaderFields();
		final String theKey = "REFCODES";
		final String theValue = "5161";
		theHeaderFields.addTo( HeaderField.SET_COOKIE.getName(), theKey + Delimiter.COOKIE_TUPEL.getChar() + theValue );
		assertEquals( theHeaderFields.getAllCookies().size(), 1 );
		final ResponseCookie theCookie = theHeaderFields.getAllCookies().get( 0 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theHeaderFields.toHttpCookies() ) );
		}
		assertEquals( theKey, theCookie.getKey() );
		assertEquals( theValue, theCookie.getValue() );
	}

	@Test
	public void testHeaderFieldCookies() {
		final ResponseHeaderFields theHeaderFields = new ResponseHeaderFields();
		String theKey = "REFCODES";
		String theValue = "5161";
		theHeaderFields.addTo( HeaderField.SET_COOKIE.getName(), theKey + Delimiter.COOKIE_TUPEL.getChar() + theValue );
		assertEquals( theHeaderFields.getAllCookies().size(), 1 );
		ResponseCookie theCookie = theHeaderFields.getAllCookies().get( 0 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theHeaderFields.toHttpCookies() ) );
		}
		assertEquals( theKey, theCookie.getKey() );
		assertEquals( theValue, theCookie.getValue() );
		theKey = "COMCODES";
		theValue = "5162";
		theHeaderFields.addTo( HeaderField.SET_COOKIE.getName(), theKey + Delimiter.COOKIE_TUPEL.getChar() + theValue );
		assertEquals( theHeaderFields.getAllCookies().size(), 2 );
		theCookie = theHeaderFields.getAllCookies().get( 1 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theHeaderFields.toHttpCookies() ) );
		}
		assertEquals( theKey, theCookie.getKey() );
		assertEquals( theValue, theCookie.getValue() );
	}

	@Test
	public void testMixedCookies() {
		final ResponseHeaderFields theHeaderFields = new ResponseHeaderFields();
		String theKey = "REFCODES";
		String theValue = "5161";
		theHeaderFields.addTo( HeaderField.SET_COOKIE.getName(), theKey + Delimiter.COOKIE_TUPEL.getChar() + theValue );
		assertEquals( theHeaderFields.getAllCookies().size(), 1 );
		ResponseCookie theCookie = theHeaderFields.getAllCookies().get( 0 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theHeaderFields.toHttpCookies() ) );
		}
		assertEquals( theKey, theCookie.getKey() );
		assertEquals( theValue, theCookie.getValue() );
		theKey = "COMCODES";
		theValue = "5162";
		theHeaderFields.addTo( HeaderField.SET_COOKIE.getName(), theKey + Delimiter.COOKIE_TUPEL.getChar() + theValue );
		assertEquals( theHeaderFields.getAllCookies().size(), 2 );
		theCookie = theHeaderFields.getAllCookies().get( 1 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theHeaderFields.toHttpCookies() ) );
		}
		assertEquals( theKey, theCookie.getKey() );
		assertEquals( theValue, theCookie.getValue() );
		theKey = "FUNCODES";
		theValue = "5163";
		theHeaderFields.addCookie( theKey, theValue );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theHeaderFields.toHttpCookies() ) );
		}
		assertEquals( theHeaderFields.getAllCookies().size(), 3 );
		theCookie = theHeaderFields.getAllCookies().get( 2 );
		assertEquals( theKey, theCookie.getKey() );
		assertEquals( theValue, theCookie.getValue() );
	}

	@Test
	public void testCookieAttributes() {
		final ResponseHeaderFields theHeaderFields = new ResponseHeaderFields();
		final String theKey = "REFCODES";
		final String theValue = "5161";
		final String theDomain = "refcodes.org";
		final String thePath = "/rest";
		final Date theExpiresDate = new Date();
		final boolean isHttpOnly = true;
		final boolean isSecure = true;
		theHeaderFields.addCookie( theKey, theValue ).withExpiresDate( theExpiresDate ).withHttpOnly( isHttpOnly ).withSecure( isSecure ).withDomain( theDomain ).withPath( thePath );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theHeaderFields.toHttpCookies() ) );
		}
		assertEquals( theHeaderFields.getAllCookies().size(), 1 );
		final ResponseCookie theCookie = theHeaderFields.getAllCookies().get( 0 );
		assertEquals( theKey, theCookie.getKey() );
		assertEquals( theValue, theCookie.getValue() );
		assertEquals( theDomain, theCookie.getDomain() );
		assertEquals( thePath, theCookie.getPath() );
		assertEquals( theExpiresDate, theCookie.getExpiresDate() );
	}

	@Test
	public void testParseHttpCookie() {
		final ResponseHeaderFields theHeaderFields = new ResponseHeaderFields();
		final String theKey = "REFCODES";
		final String theValue = "5161";
		final String theDomain = "refcodes.org";
		final String thePath = "/rest";
		final Date theExpiresDate = new Date();
		final boolean isHttpOnly = false;
		final boolean isSecure = true;
		theHeaderFields.addCookie( theKey, theValue ).withExpiresDate( theExpiresDate ).withHttpOnly( isHttpOnly ).withSecure( isSecure ).withDomain( theDomain ).withPath( thePath );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theHeaderFields.toHttpCookies() ) );
		}
		assertEquals( theHeaderFields.getAllCookies().size(), 1 );
		final ResponseCookie theCookie = theHeaderFields.getAllCookies().get( 0 );
		final ResponseCookie theClone = new ResponseCookie( theCookie.toHttpCookie() );
		assertEquals( theKey, theClone.getKey() );
		assertEquals( theValue, theClone.getValue() );
		assertEquals( theDomain, theClone.getDomain() );
		assertEquals( DateFormat.NETSCAPE_COOKIE_DATE_FORMAT.getFormatter().format( Instant.ofEpochMilli( theExpiresDate.getTime() ) ), DateFormat.NETSCAPE_COOKIE_DATE_FORMAT.getFormatter().format( Instant.ofEpochMilli( theClone.getExpiresDate().getTime() ) ) );
		assertEquals( thePath, theClone.getPath() );
		assertEquals( isSecure, theClone.isSecure() );
		assertEquals( isHttpOnly, theClone.isHttpOnly() );
		assertEquals( theCookie.toHttpCookie(), theClone.toHttpCookie() );
	}

	@Test
	public void testMaxAge() {
		final int theMaxAgeInSeconds = 60;
		final ResponseCookie theCookie = new ResponseCookie( "REFCODES=5161;MAX-AGE=" + theMaxAgeInSeconds + ";DOMAIN=refcodes.org;PATH=/rest;SECURE" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theCookie.toHttpCookie() );
		}
		assertEquals( theMaxAgeInSeconds, theCookie.getMaxAge() );
	}
}
