// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides an accessor for a virtual host property.
 */
public interface VirtualHostAccessor {

	/**
	 * Retrieves the virtual host from the virtual host property.
	 * 
	 * @return The virtual host stored by the virtual host property.
	 */
	String getVirtualHost();

	/**
	 * Provides a mutator for a virtual host property.
	 */
	public interface VirtualHostMutator {

		/**
		 * Sets the virtual host for the virtual host property.
		 * 
		 * @param aVirtualHost The virtual host to be stored by the virtual host
		 *        property.
		 */
		void setVirtualHost( String aVirtualHost );
	}

	/**
	 * Provides a mutator for an virtual host property.
	 * 
	 * @param <B> The builder which implements the {@link VirtualHostBuilder}.
	 */
	public interface VirtualHostBuilder<B extends VirtualHostBuilder<?>> {

		/**
		 * Sets the virtual host to use and returns this builder as of the
		 * builder pattern.
		 * 
		 * @param aVirtualHost The virtual host to be stored by the virtual host
		 *        property.
		 * 
		 * @return This {@link VirtualHostBuilder} instance to continue
		 *         configuration.
		 */
		B withVirtualHost( String aVirtualHost );
	}

	/**
	 * Provides a virtual host property.
	 */
	public interface VirtualHostProperty extends VirtualHostAccessor, VirtualHostMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setVirtualHost(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aVirtualHost The {@link String} to set (via
		 *        {@link #setVirtualHost(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letVirtualHost( String aVirtualHost ) {
			setVirtualHost( aVirtualHost );
			return aVirtualHost;
		}
	}
}
