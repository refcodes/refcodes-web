// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.refcodes.data.Delimiter;
import org.refcodes.web.HttpMediaTypeAccessor.HttpMediaTypeProvider;

/**
 * The {@link HeaderFields} reflect the structure of a HTTP header and may be
 * used to represent a HTTP header.
 *
 * @param <C> the generic type
 * @param <B> the generic type
 */
public interface HeaderFields<C extends Cookie, B extends HeaderFields<C, B>> extends HttpFields<B> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	String BASIC_REALM = "Basic realm";

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	// -------------------------------------------------------------------------
	// ALLOWS:
	// -------------------------------------------------------------------------

	/**
	 * Gets the Allow entity-Header-Field {@link HeaderField#ALLOW}: "... The
	 * Allow entity-Header-Field lists the set of methods supported by the
	 * resource ... The purpose of this field is strictly to inform the
	 * recipient of valid methods associated with the resource. An Allow header
	 * field MUST be present in a 405 (Method Not Allowed) response ..."
	 *
	 * @return The HTTP-Methods allowed for the resource.
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default List<HttpMethod> getAllowMethods() {
		final List<String> theAllows = get( HeaderField.ALLOW );
		if ( theAllows != null && theAllows.size() != 0 ) {
			final List<HttpMethod> theHttpMethods = new ArrayList<>();
			HttpMethod eHttpMethod;
			for ( String eAllow : theAllows ) {
				eHttpMethod = HttpMethod.fromHttpMethod( eAllow );
				if ( eHttpMethod != null ) {
					theHttpMethods.add( eHttpMethod );
				}
			}
			if ( theHttpMethods.size() != 0 ) {
				return theHttpMethods;
			}
		}
		return null;
	}

	/**
	 * Sets the Allow entity-Header-Field {@link HeaderField#ALLOW}: "... The
	 * Allow entity-Header-Field lists the set of methods supported by the
	 * resource ... The purpose of this field is strictly to inform the
	 * recipient of valid methods associated with the resource. An Allow header
	 * field MUST be present in a 405 (Method Not Allowed) response ..."
	 *
	 * @param aHttpMethods The HTTP-Methods allowed for the resource.
	 * 
	 * @return The replaced element (if any, else null).
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default List<HttpMethod> putAllowMethods( HttpMethod... aHttpMethods ) {
		if ( aHttpMethods == null || aHttpMethods.length == 0 ) {
			return removeAllowMethods();
		}
		final List<String> theAllows = new ArrayList<>();
		for ( HttpMethod eHttpMethod : aHttpMethods ) {
			if ( eHttpMethod != null ) {
				theAllows.add( eHttpMethod.name() );
			}
		}
		if ( theAllows.size() == 0 ) {
			return removeAllowMethods();
		}
		final List<HttpMethod> theReturn = getAllowMethods();
		put( HeaderField.ALLOW, theAllows );
		return theReturn;
	}

	/**
	 * Sets the Allow entity-Header-Field {@link HeaderField#ALLOW}: "... The
	 * Allow entity-Header-Field lists the set of methods supported by the
	 * resource ... The purpose of this field is strictly to inform the
	 * recipient of valid methods associated with the resource. An Allow header
	 * field MUST be present in a 405 (Method Not Allowed) response ..."
	 *
	 * @param aHttpMethods The HTTP-Methods allowed for the resource.
	 * 
	 * @return The replaced element (if any, else null).
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default List<HttpMethod> putAllowMethods( List<HttpMethod> aHttpMethods ) {
		if ( aHttpMethods == null || aHttpMethods.size() == 0 ) {
			return removeAllowMethods();
		}
		return putAllowMethods( aHttpMethods.toArray( new HttpMethod[aHttpMethods.size()] ) );
	}

	/**
	 * Sets the Allow entity-Header-Field {@link HeaderField#ALLOW}: "... The
	 * Allow entity-Header-Field lists the set of methods supported by the
	 * resource ... The purpose of this field is strictly to inform the
	 * recipient of valid methods associated with the resource. An Allow header
	 * field MUST be present in a 405 (Method Not Allowed) response ..."
	 *
	 * @param aHttpMethods The HTTP-Methods allowed for the resource.
	 * 
	 * @return This object as of the Builder-Pattern.
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	@SuppressWarnings("unchecked")
	default B withAllowMethods( HttpMethod... aHttpMethods ) {
		putAllowMethods( aHttpMethods );
		return (B) this;
	}

	/**
	 * Sets the Allow entity-Header-Field {@link HeaderField#ALLOW}: "... The
	 * Allow entity-Header-Field lists the set of methods supported by the
	 * resource ... The purpose of this field is strictly to inform the
	 * recipient of valid methods associated with the resource. An Allow header
	 * field MUST be present in a 405 (Method Not Allowed) response ..."
	 *
	 * @param aHttpMethods The HTTP-Methods allowed for the resource.
	 * 
	 * @return This object as of the Builder-Pattern.
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	@SuppressWarnings("unchecked")
	default B withAllowMethods( List<HttpMethod> aHttpMethods ) {
		putAllowMethods( aHttpMethods );
		return (B) this;
	}

	/**
	 * Removes the Allow entity-Header-Field {@link HeaderField#ALLOW}: "... The
	 * Allow entity-Header-Field lists the set of methods supported by the
	 * resource ... The purpose of this field is strictly to inform the
	 * recipient of valid methods associated with the resource. An Allow header
	 * field MUST be present in a 405 (Method Not Allowed) response ..."
	 *
	 * @return The value being removed (or null if none was set).
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default List<HttpMethod> removeAllowMethods() {
		final List<HttpMethod> theReturn = getAllowMethods();
		remove( HeaderField.ALLOW );
		return theReturn;
	}

	// -------------------------------------------------------------------------
	// CONTENT LENGTH:
	// -------------------------------------------------------------------------

	/**
	 * Gets the Content-Length request-Header-Field
	 * {@link HeaderField#CONTENT_LENGTH}: "... The Content-Length
	 * request-Header-Field contains information about the user agent
	 * originating the request. This is for statistical purposes, the tracing of
	 * protocol violations, and automated recognition of user agents for the
	 * sake of tailoring responses to avoid particular user agent limitations.
	 * User agents SHOULD include this field with requests ..."
	 *
	 * @return The according user agent.
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default Integer getContentLength() {
		final String theContentLength = getFirst( HeaderField.CONTENT_LENGTH );
		if ( theContentLength != null ) {
			try {
				return Integer.parseInt( theContentLength );
			}
			catch ( IllegalArgumentException ignore ) { /* ignore */ }
		}
		return null;
	}

	/**
	 * Sets the Content-Length request-Header-Field
	 * {@link HeaderField#CONTENT_LENGTH}: "... The Content-Length
	 * request-Header-Field contains information about the user agent
	 * originating the request. This is for statistical purposes, the tracing of
	 * protocol violations, and automated recognition of user agents for the
	 * sake of tailoring responses to avoid particular user agent limitations.
	 * User agents SHOULD include this field with requests ..."
	 *
	 * @param aContentLength The according user agent.
	 * 
	 * @return the string
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default Integer putContentLength( Integer aContentLength ) {
		if ( aContentLength == null ) {
			return removeContentLength();
		}
		final Integer theReturn = getContentLength();
		put( HeaderField.CONTENT_LENGTH, aContentLength.toString() );
		return theReturn;
	}

	/**
	 * Sets he Content-Length request-Header-Field
	 * {@link HeaderField#CONTENT_LENGTH}: "... The Content-Length
	 * request-Header-Field contains information about the user agent
	 * originating the request. This is for statistical purposes, the tracing of
	 * protocol violations, and automated recognition of user agents for the
	 * sake of tailoring responses to avoid particular user agent limitations.
	 * User agents SHOULD include this field with requests ..."
	 *
	 * @param aContentLength The according user agent.
	 * 
	 * @return This object as of the Builder-Pattern.
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	@SuppressWarnings("unchecked")
	default B withContentLength( Integer aContentLength ) {
		putContentLength( aContentLength );
		return (B) this;
	}

	/**
	 * Removes he Content-Length request-Header-Field
	 * {@link HeaderField#CONTENT_LENGTH} : "... The Content-Length
	 * request-Header-Field contains information about the user agent
	 * originating the request. This is for statistical purposes, the tracing of
	 * protocol violations, and automated recognition of user agents for the
	 * sake of tailoring responses to avoid particular user agent limitations.
	 * User agents SHOULD include this field with requests ..."
	 *
	 * @return The value being removed (or null if none was set).
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default Integer removeContentLength() {
		final Integer theReturn = getContentLength();
		remove( HeaderField.CONTENT_LENGTH );
		return theReturn;
	}

	// -------------------------------------------------------------------------
	// CONTENT TYPE:
	// -------------------------------------------------------------------------

	/**
	 * Gets the Content-Type entity-Header-Field
	 * {@link HeaderField#CONTENT_TYPE}: "... The Content-Type
	 * entity-Header-Field indicates the media type of the entity-body sent to
	 * the recipient ..."
	 *
	 * @return The media type being used for the body.
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default ContentType getContentType() {
		final String theContentType = getFirst( HeaderField.CONTENT_TYPE );
		if ( theContentType != null ) {
			try {
				return new ContentType( theContentType );
			}
			catch ( IllegalArgumentException ignore ) { /* ignore */ }
		}
		return null;
	}

	/**
	 * Gets the unresolvable (unknown) Content-Type request-Header-Field
	 * {@link HeaderField#CONTENT_TYPE}. E.g. the ones which cannot be mapped by
	 * the enumeration {@link MediaType}.
	 *
	 * @return the unknown content types
	 * 
	 * @see #getContentType()
	 */
	default List<String> getUnknownContentTypes() {
		final List<String> theAcceptTypes = get( HeaderField.CONTENT_TYPE );
		if ( theAcceptTypes != null && theAcceptTypes.size() != 0 ) {
			final List<String> theUnknwonContentTypes = new ArrayList<>();
			String[] eSplitMediaTypes;
			for ( String eAcceptType : theAcceptTypes ) {
				eSplitMediaTypes = eAcceptType.split( "" + Delimiter.HTTP_HEADER_ELEMENTS.getChar() );
				for ( String eMediaType : eSplitMediaTypes ) {
					try {
						new ContentType( eMediaType );
					}
					catch ( IllegalArgumentException ignore ) {
						theUnknwonContentTypes.add( eMediaType );
					}
				}
			}
			if ( theUnknwonContentTypes.size() != 0 ) {
				return theUnknwonContentTypes;
			}
		}
		return null;
	}

	/**
	 * Sets the Content-Type entity-Header-Field
	 * {@link HeaderField#CONTENT_TYPE}. You may pass a {@link MediaType} or a
	 * {@link ContentType} instance as them both implement the
	 * {@link HttpMediaTypeProvider} interface: "... The Content-Type
	 * entity-Header-Field indicates the media type of the entity-body sent to
	 * the recipient ..."
	 *
	 * @param aHttpMediaType The media type used for the body.
	 * 
	 * @return The replaced element (if any, else null).
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default ContentType putContentType( HttpMediaType aHttpMediaType ) {
		if ( aHttpMediaType == null ) {
			return removeContentType();
		}
		final ContentType theReturn = getContentType();
		put( HeaderField.CONTENT_TYPE, aHttpMediaType.toHttpMediaType() );
		return theReturn;
	}

	/**
	 * Sets the Content-Type entity-Header-Field
	 * {@link HeaderField#CONTENT_TYPE}. : "... The Content-Type
	 * entity-Header-Field indicates the media type of the entity-body sent to
	 * the recipient ..."
	 *
	 * @param aMediaType The media type used for the body.
	 * @param aParameters The Content-Type's parameters.
	 * 
	 * @return The replaced element (if any, else null).
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default ContentType putContentType( MediaType aMediaType, Map<String, String> aParameters ) {
		if ( aMediaType == null ) {
			return removeContentType();
		}
		return putContentType( new ContentType( aMediaType, aParameters ) );
	}

	/**
	 * Sets the Content-Type entity-Header-Field
	 * {@link HeaderField#CONTENT_TYPE}. You may pass a {@link MediaType} or a
	 * {@link ContentType} instance as them both implement the
	 * {@link HttpMediaTypeProvider} interface: "... The Content-Type
	 * entity-Header-Field indicates the media type of the entity-body sent to
	 * the recipient ..."
	 *
	 * @param aHttpMediaType The media type used for the body.
	 * 
	 * @return This object as of the Builder-Pattern.
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	@SuppressWarnings("unchecked")
	default B withContentType( HttpMediaType aHttpMediaType ) {
		putContentType( aHttpMediaType );
		return (B) this;
	}

	/**
	 * Sets the Content-Type's {@link MediaType} and the parameters for the HTTP
	 * body.
	 * 
	 * @param aMediaType The media type used for the body.
	 * @param aParameters The Content-Type's parameters.
	 * 
	 * @return This object as of the Builder-Pattern.
	 */
	@SuppressWarnings("unchecked")
	default B withContentType( MediaType aMediaType, Map<String, String> aParameters ) {
		putContentType( aMediaType, aParameters );
		return (B) this;
	}

	/**
	 * Removes the Content-Type entity-Header-Field
	 * {@link HeaderField#CONTENT_TYPE}. : "... The Content-Type
	 * entity-Header-Field indicates the media type of the entity-body sent to
	 * the recipient ..."
	 *
	 * @return The value being removed (or null if none was set).
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default ContentType removeContentType() {
		final ContentType theReturn = getContentType();
		remove( HeaderField.CONTENT_TYPE );
		return theReturn;
	}

	// -------------------------------------------------------------------------
	// ACCEPT TYPES:
	// -------------------------------------------------------------------------

	/**
	 * Gets the Accept request-Header-Field {@link HeaderField#ACCEPT}: "... The
	 * Accept request-Header-Field can be used to specify certain media types
	 * which are acceptable for the response. Accept headers can be used to
	 * indicate that the request is specifically limited to a small set of
	 * desired ..."
	 *
	 * @return The expected (supported) kinds of {@link MediaType} and the
	 *         according parameters (being actually instances of the
	 *         {@link ContentType} type).
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default List<ContentType> getAcceptTypes() {
		final List<String> theAcceptTypes = get( HeaderField.ACCEPT );
		if ( theAcceptTypes != null && theAcceptTypes.size() != 0 ) {
			final List<ContentType> theContentTypes = new ArrayList<>();
			String[] eSplitMediaTypes;
			for ( String eAcceptType : theAcceptTypes ) {
				eSplitMediaTypes = eAcceptType.split( "" + Delimiter.HTTP_HEADER_ELEMENTS.getChar() );
				for ( String eMediaType : eSplitMediaTypes ) {
					try {
						theContentTypes.add( new ContentType( eMediaType ) );
					}
					catch ( IllegalArgumentException ignore ) { /* ignore */ }
				}
			}
			if ( theContentTypes.size() != 0 ) {
				return theContentTypes;
			}
		}
		return null;
	}

	/**
	 * Gets the unresolvable (unknown) Accept request-Header-Field
	 * {@link HeaderField#ACCEPT}. E.g. the ones which cannot be mapped by the
	 * enumeration {@link MediaType}.
	 *
	 * @return the unknown accept types
	 * 
	 * @see #getAcceptTypes()
	 */
	default List<String> getUnknownAcceptTypes() {
		final List<String> theAcceptTypes = get( HeaderField.ACCEPT );
		if ( theAcceptTypes != null && theAcceptTypes.size() != 0 ) {
			final List<String> theUnknwonContentTypes = new ArrayList<>();
			String[] eSplitMediaTypes;
			for ( String eAcceptType : theAcceptTypes ) {
				eSplitMediaTypes = eAcceptType.split( "" + Delimiter.HTTP_HEADER_ELEMENTS.getChar() );
				for ( String eMediaType : eSplitMediaTypes ) {
					try {
						new ContentType( eMediaType );
					}
					catch ( IllegalArgumentException ignore ) {
						theUnknwonContentTypes.add( eMediaType );
					}
				}
			}
			if ( theUnknwonContentTypes.size() != 0 ) {
				return theUnknwonContentTypes;
			}
		}
		return null;
	}

	/**
	 * Sets the Accept request-Header-Field {@link HeaderField#ACCEPT}. You may
	 * pass a {@link MediaType} or a {@link ContentType} instance as them both
	 * implement the {@link HttpMediaTypeProvider} interface: "... The Accept
	 * request-Header-Field can be used to specify certain media types which are
	 * acceptable for the response. Accept headers can be used to indicate that
	 * the request is specifically limited to a small set of desired types ..."
	 *
	 * @param aHttpMediaType The according {@link HttpMediaTypeProvider}.
	 * 
	 * @return The replaced element (if any, else null).
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default List<ContentType> putAcceptTypes( HttpMediaType... aHttpMediaType ) {
		if ( aHttpMediaType == null || aHttpMediaType.length == 0 ) {
			return removeAcceptTypes();
		}
		String theAccept = "";
		for ( int i = 0; i < aHttpMediaType.length; i++ ) {
			if ( aHttpMediaType[i] != null ) {
				theAccept += aHttpMediaType[i].toHttpMediaType();
				if ( i < aHttpMediaType.length - 1 ) {
					theAccept += Delimiter.HTTP_HEADER_ELEMENTS.getChar();
				}
			}
		}
		if ( theAccept.isEmpty() ) {
			return removeAcceptTypes();
		}
		final List<ContentType> theReturn = getAcceptTypes();
		final List<String> theAccepts = new ArrayList<>();
		theAccepts.add( theAccept );
		put( HeaderField.ACCEPT, theAccepts );
		return theReturn;
	}

	/**
	 * Sets the Accept request-Header-Field {@link HeaderField#ACCEPT}. You may
	 * pass a {@link MediaType} or a {@link ContentType} instance as them both
	 * implement the {@link HttpMediaTypeProvider} interface: "... The Accept
	 * request-Header-Field can be used to specify certain media types which are
	 * acceptable for the response. Accept headers can be used to indicate that
	 * the request is specifically limited to a small set of desired types ..."
	 *
	 * @param aHttpMediaTypes The according {@link HttpMediaTypeProvider}.
	 * 
	 * @return The replaced element (if any, else null).
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default List<ContentType> putAcceptTypes( List<? extends HttpMediaType> aHttpMediaTypes ) {
		if ( aHttpMediaTypes == null || aHttpMediaTypes.size() == 0 ) {
			return removeAcceptTypes();
		}
		return putAcceptTypes( aHttpMediaTypes.toArray( new HttpMediaType[aHttpMediaTypes.size()] ) );
	}

	/**
	 * Sets the Accept request-Header-Field {@link HeaderField#ACCEPT}. You may
	 * pass a {@link MediaType} or a {@link ContentType} instance as them both
	 * implement the {@link HttpMediaTypeProvider} interface: "... The Accept
	 * request-Header-Field can be used to specify certain media types which are
	 * acceptable for the response. Accept headers can be used to indicate that
	 * the request is specifically limited to a small set of desired types ..."
	 *
	 * @param aHttpMediaType The according {@link HttpMediaTypeProvider}.
	 * 
	 * @return This object as of the Builder-Pattern.
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	@SuppressWarnings("unchecked")
	default B withAcceptTypes( HttpMediaType... aHttpMediaType ) {
		putAcceptTypes( aHttpMediaType );
		return (B) this;
	}

	/**
	 * Sets the Accept request-Header-Field {@link HeaderField#ACCEPT}. You may
	 * pass a {@link MediaType} or a {@link ContentType} instance as them both
	 * implement the {@link HttpMediaTypeProvider} interface: "... The Accept
	 * request-Header-Field can be used to specify certain media types which are
	 * acceptable for the response. Accept headers can be used to indicate that
	 * the request is specifically limited to a small set of desired types ..."
	 *
	 * @param aHttpMediaType The according {@link HttpMediaTypeProvider}.
	 * 
	 * @return This object as of the Builder-Pattern.
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	@SuppressWarnings("unchecked")
	default B withAcceptTypes( List<? extends HttpMediaType> aHttpMediaType ) {
		putAcceptTypes( aHttpMediaType );
		return (B) this;
	}

	/**
	 * Removes the Accept request-Header-Field {@link HeaderField#ACCEPT}: "...
	 * The Accept request-Header-Field can be used to specify certain media
	 * types which are acceptable for the response. Accept headers can be used
	 * to indicate that the request is specifically limited to a small set of
	 * desired types ..."
	 *
	 * @return The value being removed (or null if none was set).
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default List<ContentType> removeAcceptTypes() {
		final List<ContentType> theReturn = getAcceptTypes();
		remove( HeaderField.ACCEPT );
		return theReturn;
	}

	// -------------------------------------------------------------------------
	// ACCEPT CHARSET:
	// -------------------------------------------------------------------------

	/**
	 * Gets the Accept-Charset request-Header-Field
	 * {@link HeaderField#ACCEPT_CHARSET}. "... The Accept-Charset
	 * request-Header-Field can be used to indicate what character sets are
	 * acceptable for the response. This field allows clients capable of
	 * understanding more comprehensive or https://www.metacodes.pro- purpose
	 * character sets to signal that capability to a server which is capable of
	 * representing documents in those character sets ..."
	 *
	 * @return The Accept-Charset expected by a client from a server.
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default List<String> getAcceptCharsets() {
		final List<String> theAcceptCharsets = get( HeaderField.ACCEPT_CHARSET );
		if ( theAcceptCharsets != null && theAcceptCharsets.size() != 0 ) {
			final List<String> theCharsets = new ArrayList<>();
			String[] eSplitCharsets;
			for ( String eAcceptCharset : theAcceptCharsets ) {
				eSplitCharsets = eAcceptCharset.split( "" + Delimiter.HTTP_HEADER_ELEMENTS.getChar() );
				for ( String eCharset : eSplitCharsets ) {
					if ( eCharset != null && eCharset.length() > 0 ) {
						theCharsets.add( eCharset );
					}
				}
			}
			if ( theCharsets.size() != 0 ) {
				return theCharsets;
			}
		}
		return null;
	}

	/**
	 * Sets the Accept-Charset request-Header-Field
	 * {@link HeaderField#ACCEPT_CHARSET}. "... The Accept-Charset
	 * request-Header-Field can be used to indicate what character sets are
	 * acceptable for the response. This field allows clients capable of
	 * understanding more comprehensive or https://www.metacodes.pro- purpose
	 * character sets to signal that capability to a server which is capable of
	 * representing documents in those character sets ..."
	 *
	 * @param aAcceptCharsets The Accept-Charset expected by a client from a
	 *        server.
	 * 
	 * @return The replaced elements (if any, else null).
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default List<String> putAcceptCharsets( String... aAcceptCharsets ) {
		if ( aAcceptCharsets == null || aAcceptCharsets.length == 0 ) {
			return removeAcceptCharsets();
		}
		String theAcceptCharset = "";
		for ( int i = 0; i < aAcceptCharsets.length; i++ ) {
			if ( aAcceptCharsets[i] != null && aAcceptCharsets[i].length() != 0 ) {
				theAcceptCharset += aAcceptCharsets[i];
				if ( i < aAcceptCharsets.length - 1 ) {
					theAcceptCharset += Delimiter.HTTP_HEADER_ELEMENTS.getChar();
				}
			}
		}
		if ( theAcceptCharset.isEmpty() ) {
			return removeAcceptCharsets();
		}
		final List<String> theReturn = getAcceptCharsets();
		final List<String> theAcceptCharsets = new ArrayList<>();
		theAcceptCharsets.add( theAcceptCharset );
		put( HeaderField.ACCEPT_CHARSET, theAcceptCharsets );
		return theReturn;
	}

	/**
	 * Sets the Accept-Charset request-Header-Field
	 * {@link HeaderField#ACCEPT_CHARSET}. "... The Accept-Charset
	 * request-Header-Field can be used to indicate what character sets are
	 * acceptable for the response. This field allows clients capable of
	 * understanding more comprehensive or https://www.metacodes.pro- purpose
	 * character sets to signal that capability to a server which is capable of
	 * representing documents in those character sets ..."
	 *
	 * @param aAcceptCharsets The Accept-Charset expected by a client from a
	 *        server.
	 * 
	 * @return The replaced elements (if any, else null).
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default List<String> putAcceptCharsets( List<String> aAcceptCharsets ) {
		if ( aAcceptCharsets == null || aAcceptCharsets.size() == 0 ) {
			return removeAcceptCharsets();
		}
		return putAcceptCharsets( aAcceptCharsets.toArray( new String[aAcceptCharsets.size()] ) );
	}

	/**
	 * Sets the the Accept-Charset request-Header-Field
	 * {@link HeaderField#ACCEPT_CHARSET}. "... The Accept-Charset
	 * request-Header-Field can be used to indicate what character sets are
	 * acceptable for the response. This field allows clients capable of
	 * understanding more comprehensive or https://www.metacodes.pro- purpose
	 * character sets to signal that capability to a server which is capable of
	 * representing documents in those character sets ..."
	 *
	 * @param aAcceptCharsets The Accept-Charset expected by a client from a
	 *        server.
	 * 
	 * @return This object as of the Builder-Pattern.
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	@SuppressWarnings("unchecked")
	default B withAcceptCharsets( List<String> aAcceptCharsets ) {
		putAcceptCharsets( aAcceptCharsets );
		return (B) this;
	}

	/**
	 * Sets the Accept-Charset request-Header-Field
	 * {@link HeaderField#ACCEPT_CHARSET}. "... The Accept-Charset
	 * request-Header-Field can be used to indicate what character sets are
	 * acceptable for the response. This field allows clients capable of
	 * understanding more comprehensive or https://www.metacodes.pro- purpose
	 * character sets to signal that capability to a server which is capable of
	 * representing documents in those character sets ..."
	 *
	 * @param aAcceptCharsets The Accept-Charset expected by a client from a
	 *        server.
	 * 
	 * @return This object as of the Builder-Pattern.
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	@SuppressWarnings("unchecked")
	default B withAcceptCharsets( String... aAcceptCharsets ) {
		putAcceptCharsets( aAcceptCharsets );
		return (B) this;
	}

	/**
	 * Removes the Accept-Charset request-Header-Field
	 * {@link HeaderField#ACCEPT_CHARSET}. "... The Accept-Charset
	 * request-Header-Field can be used to indicate what character sets are
	 * acceptable for the response. This field allows clients capable of
	 * understanding more comprehensive or https://www.metacodes.pro- purpose
	 * character sets to signal that capability to a server which is capable of
	 * representing documents in those character sets ..."
	 *
	 * @return The value being removed (or null if none was set).
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default List<String> removeAcceptCharsets() {
		final List<String> theReturn = getAcceptCharsets();
		remove( HeaderField.ACCEPT_CHARSET );
		return theReturn;
	}

	// -------------------------------------------------------------------------
	// ACCEPT ENCODINGS:
	// -------------------------------------------------------------------------

	/**
	 * Gets the the Content-Encoding entity-Header-Field
	 * {@link HeaderField#CONTENT_ENCODING}: "... The Content-Encoding
	 * entity-Header-Field is used as a modifier to the media-type. When
	 * present, its value indicates what additional content codings have been
	 * applied to the entity-body, and thus what decoding mechanisms must be
	 * applied in order to obtain the media-type referenced by the Content-Type
	 * Header-Field. Content-Encoding is primarily used to allow a document to
	 * be compressed without losing the identity of its underlying media type
	 * ..."
	 *
	 * @return The expected (supported) kinds of encodings.
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 * @see "https://developer.mozilla.org/de/docs/Web/HTTP/Headers/Content-Encoding"
	 */
	default List<ContentEncoding> getAcceptEncodings() {
		final List<String> theAcceptEncodings = get( HeaderField.ACCEPT_ENCODING );
		if ( theAcceptEncodings != null && theAcceptEncodings.size() != 0 ) {
			final List<ContentEncoding> theContentEncodings = new ArrayList<>();
			String[] eSplitEncodings;
			for ( String eAcceptEncoding : theAcceptEncodings ) {
				eSplitEncodings = eAcceptEncoding.split( "" + Delimiter.HTTP_HEADER_ELEMENTS.getChar() );
				for ( String eContentEncoding : eSplitEncodings ) {
					try {
						theContentEncodings.add( ContentEncoding.fromHttpEncoding( eContentEncoding ) );
					}
					catch ( IllegalArgumentException ignore ) { /* ignore */ }
				}
			}
			if ( theContentEncodings.size() != 0 ) {
				return theContentEncodings;
			}
		}
		return null;
	}

	/**
	 * Sets the the Content-Encoding entity-Header-Field
	 * {@link HeaderField#CONTENT_ENCODING}: "... The Content-Encoding
	 * entity-Header-Field is used as a modifier to the media-type. When
	 * present, its value indicates what additional content codings have been
	 * applied to the entity-body, and thus what decoding mechanisms must be
	 * applied in order to obtain the media-type referenced by the Content-Type
	 * Header-Field. Content-Encoding is primarily used to allow a document to
	 * be compressed without losing the identity of its underlying media type
	 * ..."
	 *
	 * @param aEncodings The according encodings.
	 * 
	 * @return the list
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 * @see "https://developer.mozilla.org/de/docs/Web/HTTP/Headers/Content-Encoding"
	 */
	default List<ContentEncoding> putAcceptEncodings( ContentEncoding... aEncodings ) {
		if ( aEncodings == null || aEncodings.length == 0 ) {
			return removeAcceptEncodings();
		}
		String theAcceptEncoding = "";
		for ( int i = 0; i < aEncodings.length; i++ ) {
			if ( aEncodings[i] != null ) {
				theAcceptEncoding += aEncodings[i].getName();
				if ( i < aEncodings.length - 1 ) {
					theAcceptEncoding += Delimiter.HTTP_HEADER_ELEMENTS.getChar();
				}
			}
		}
		if ( theAcceptEncoding.isEmpty() ) {
			return removeAcceptEncodings();
		}
		final List<ContentEncoding> theReturn = getAcceptEncodings();
		final List<String> theAcceptEncodings = new ArrayList<>();
		theAcceptEncodings.add( theAcceptEncoding );
		put( HeaderField.ACCEPT_ENCODING, theAcceptEncodings );
		return theReturn;
	}

	/**
	 * Sets the the Content-Encoding entity-Header-Field
	 * {@link HeaderField#CONTENT_ENCODING}: "... The Content-Encoding
	 * entity-Header-Field is used as a modifier to the media-type. When
	 * present, its value indicates what additional content codings have been
	 * applied to the entity-body, and thus what decoding mechanisms must be
	 * applied in order to obtain the media-type referenced by the Content-Type
	 * Header-Field. Content-Encoding is primarily used to allow a document to
	 * be compressed without losing the identity of its underlying media type
	 * ..."
	 *
	 * @param aEncodings The according encodings.
	 * 
	 * @return the list
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 * @see "https://developer.mozilla.org/de/docs/Web/HTTP/Headers/Content-Encoding"
	 */
	default List<ContentEncoding> putAcceptEncodings( List<ContentEncoding> aEncodings ) {
		if ( aEncodings == null || aEncodings.size() == 0 ) {
			return removeAcceptEncodings();
		}
		return putAcceptEncodings( aEncodings.toArray( new ContentEncoding[aEncodings.size()] ) );
	}

	/**
	 * Sets the the Content-Encoding entity-Header-Field
	 * {@link HeaderField#CONTENT_ENCODING}: "... The Content-Encoding
	 * entity-Header-Field is used as a modifier to the media-type. When
	 * present, its value indicates what additional content codings have been
	 * applied to the entity-body, and thus what decoding mechanisms must be
	 * applied in order to obtain the media-type referenced by the Content-Type
	 * Header-Field. Content-Encoding is primarily used to allow a document to
	 * be compressed without losing the identity of its underlying media type
	 * ..."
	 *
	 * @param aEncodings The according encodings.
	 * 
	 * @return This object as of the Builder-Pattern.
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 * @see "https://developer.mozilla.org/de/docs/Web/HTTP/Headers/Content-Encoding"
	 */
	@SuppressWarnings("unchecked")
	default B withAcceptEncodings( ContentEncoding... aEncodings ) {
		putAcceptEncodings( aEncodings );
		return (B) this;
	}

	/**
	 * Sets the the Content-Encoding entity-Header-Field
	 * {@link HeaderField#CONTENT_ENCODING}: "... The Content-Encoding
	 * entity-Header-Field is used as a modifier to the media-type. When
	 * present, its value indicates what additional content codings have been
	 * applied to the entity-body, and thus what decoding mechanisms must be
	 * applied in order to obtain the media-type referenced by the Content-Type
	 * Header-Field. Content-Encoding is primarily used to allow a document to
	 * be compressed without losing the identity of its underlying media type
	 * ..."
	 *
	 * @param aEncodings The according encodings.
	 * 
	 * @return This object as of the Builder-Pattern.
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 * @see "https://developer.mozilla.org/de/docs/Web/HTTP/Headers/Content-Encoding"
	 */
	@SuppressWarnings("unchecked")
	default B withAcceptEncodings( List<ContentEncoding> aEncodings ) {
		putAcceptEncodings( aEncodings );
		return (B) this;
	}

	/**
	 * Removes the the Content-Encoding entity-Header-Field
	 * {@link HeaderField#CONTENT_ENCODING}: "... The Content-Encoding
	 * entity-Header-Field is used as a modifier to the media-type. When
	 * present, its value indicates what additional content codings have been
	 * applied to the entity-body, and thus what decoding mechanisms must be
	 * applied in order to obtain the media-type referenced by the Content-Type
	 * Header-Field. Content-Encoding is primarily used to allow a document to
	 * be compressed without losing the identity of its underlying media type
	 * ..."
	 *
	 * @return The value being removed (or null if none was set).
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default List<ContentEncoding> removeAcceptEncodings() {
		final List<ContentEncoding> theReturn = getAcceptEncodings();
		remove( HeaderField.ACCEPT_ENCODING );
		return theReturn;
	}

	// -------------------------------------------------------------------------
	// ACCEPT LANGAUGES:
	// -------------------------------------------------------------------------

	/**
	 * Gets the the Content-Encoding entity-Header-Field
	 * {@link HeaderField#ACCEPT_LANGUAGE}:
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 *
	 * @return The expected (supported) kinds of languages.
	 */
	default List<Locale> getAcceptLanguages() {
		final List<String> theAcceptLanguages = get( HeaderField.ACCEPT_LANGUAGE );
		if ( theAcceptLanguages != null && theAcceptLanguages.size() != 0 ) {
			final List<Locale> theLocales = new ArrayList<>();
			String[] eSplitEncodings;
			for ( String eAcceptLanguage : theAcceptLanguages ) {
				eSplitEncodings = eAcceptLanguage.split( "" + Delimiter.HTTP_HEADER_ELEMENTS.getChar() );
				for ( String eLocale : eSplitEncodings ) {
					try {
						theLocales.add( Locale.forLanguageTag( eLocale ) );
					}
					catch ( IllegalArgumentException ignore ) { /* ignore */ }
				}
			}
			if ( theLocales.size() != 0 ) {
				return theLocales;
			}
		}
		return null;
	}

	/**
	 * Sets the the Content-Encoding entity-Header-Field
	 * {@link HeaderField#ACCEPT_LANGUAGE}:
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 * 
	 * @param aLanguages The according languages.
	 * 
	 * @return the list
	 */
	default List<Locale> putAcceptLanguages( Locale... aLanguages ) {
		if ( aLanguages == null || aLanguages.length == 0 ) {
			return removeAcceptLanguages();
		}
		String theAcceptLanguage = "";
		for ( int i = 0; i < aLanguages.length; i++ ) {
			if ( aLanguages[i] != null ) {
				theAcceptLanguage += aLanguages[i].getLanguage() != null ? aLanguages[i].getLanguage() : "";
				theAcceptLanguage += aLanguages[i].getLanguage() != null && aLanguages[i].getCountry() != null && aLanguages[i].getCountry().length() != 0 ? "-" : "";
				theAcceptLanguage += aLanguages[i].getCountry() != null ? aLanguages[i].getCountry() : "";
				if ( i < aLanguages.length - 1 ) {
					theAcceptLanguage += Delimiter.HTTP_HEADER_ELEMENTS.getChar();
				}
			}
		}
		if ( theAcceptLanguage.isEmpty() ) {
			return removeAcceptLanguages();
		}
		final List<Locale> theReturn = getAcceptLanguages();
		final List<String> theAcceptLanguages = new ArrayList<>();
		theAcceptLanguages.add( theAcceptLanguage );
		put( HeaderField.ACCEPT_LANGUAGE, theAcceptLanguages );
		return theReturn;
	}

	/**
	 * Sets the the Content-Encoding entity-Header-Field
	 * {@link HeaderField#ACCEPT_LANGUAGE}:
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 * 
	 * @param aLanguages The according languages.
	 * 
	 * @return the list
	 */
	default List<Locale> putAcceptLanguages( List<Locale> aLanguages ) {
		if ( aLanguages == null || aLanguages.size() == 0 ) {
			return removeAcceptLanguages();
		}
		return putAcceptLanguages( aLanguages.toArray( new Locale[aLanguages.size()] ) );
	}

	/**
	 * Sets the the Content-Encoding entity-Header-Field
	 * {@link HeaderField#ACCEPT_LANGUAGE}:
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 *
	 * @param aLanguages The according languages.
	 * 
	 * @return This object as of the Builder-Pattern.
	 */
	@SuppressWarnings("unchecked")
	default B withAcceptLanguages( Locale... aLanguages ) {
		putAcceptLanguages( aLanguages );
		return (B) this;
	}

	/**
	 * Sets the the Content-Encoding entity-Header-Field
	 * {@link HeaderField#ACCEPT_LANGUAGE}: "... The Accept-Language request
	 * HTTP header advertises which languages the client is able to understand,
	 * and which locale variant is preferred ..."
	 * 
	 * @see "https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Accept-Language"
	 *
	 * @param aLanguages The according languages.
	 * 
	 * @return This object as of the Builder-Pattern.
	 */
	@SuppressWarnings("unchecked")
	default B withAcceptLanguages( List<Locale> aLanguages ) {
		putAcceptLanguages( aLanguages );
		return (B) this;
	}

	/**
	 * Removes the the Content-Encoding entity-Header-Field
	 * {@link HeaderField#ACCEPT_LANGUAGE}: "... The Accept-Language request
	 * HTTP header advertises which languages the client is able to understand,
	 * and which locale variant is preferred ..."
	 * 
	 * @see "https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Accept-Language"
	 *
	 * @return The value being removed (or null if none was set).
	 */
	default List<Locale> removeAcceptLanguages() {
		final List<Locale> theReturn = getAcceptLanguages();
		remove( HeaderField.ACCEPT_LANGUAGE );
		return theReturn;
	}

	// -------------------------------------------------------------------------
	// CONTENT ENCODING:
	// -------------------------------------------------------------------------

	/**
	 * Gets the Content-Encoding entity-Header-Field
	 * {@link HeaderField#CONTENT_ENCODING}: "... The Content-Encoding
	 * entity-Header-Field is used as a modifier to the media-type. When
	 * present, its value indicates what additional content codings have been
	 * applied to the entity-body, and thus what decoding mechanisms must be
	 * applied in order to obtain the media-type referenced by the Content-Type
	 * Header-Field. Content-Encoding is primarily used to allow a document to
	 * be compressed without losing the identity of its underlying media type
	 * ..."
	 *
	 * @return The communication partner's HTTP body's encoding.
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default ContentEncoding getContentEncoding() {
		final String theContentType = getFirst( HeaderField.CONTENT_ENCODING );
		if ( theContentType != null ) {
			return ContentEncoding.fromHttpEncoding( theContentType );
		}
		return null;
	}

	/**
	 * Sets the Content-Encoding entity-Header-Field
	 * {@link HeaderField#CONTENT_ENCODING}: "... The Content-Encoding
	 * entity-Header-Field is used as a modifier to the media-type. When
	 * present, its value indicates what additional content codings have been
	 * applied to the entity-body, and thus what decoding mechanisms must be
	 * applied in order to obtain the media-type referenced by the Content-Type
	 * Header-Field. Content-Encoding is primarily used to allow a document to
	 * be compressed without losing the identity of its underlying media type
	 * ..."
	 *
	 * @param aEncoding The HTTP body's content encoding.
	 * 
	 * @return the content encoding
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default ContentEncoding putContentEncoding( ContentEncoding aEncoding ) {
		if ( aEncoding == null ) {
			return removeContentEncoding();
		}
		final ContentEncoding theReturn = getContentEncoding();
		put( HeaderField.CONTENT_ENCODING, aEncoding.getName() );
		return theReturn;
	}

	/**
	 * Sets the Content-Encoding entity-Header-Field
	 * {@link HeaderField#CONTENT_ENCODING}: "... The Content-Encoding
	 * entity-Header-Field is used as a modifier to the media-type. When
	 * present, its value indicates what additional content codings have been
	 * applied to the entity-body, and thus what decoding mechanisms must be
	 * applied in order to obtain the media-type referenced by the Content-Type
	 * Header-Field. Content-Encoding is primarily used to allow a document to
	 * be compressed without losing the identity of its underlying media type
	 * ..."
	 *
	 * @param aEncoding The HTTP body's content encoding.
	 * 
	 * @return This object as of the Builder-Pattern.
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	@SuppressWarnings("unchecked")
	default B withContentEncoding( ContentEncoding aEncoding ) {
		putContentEncoding( aEncoding );
		return (B) this;
	}

	/**
	 * Removes the Content-Encoding entity-Header-Field
	 * {@link HeaderField#CONTENT_ENCODING}: "... The Content-Encoding
	 * entity-Header-Field is used as a modifier to the media-type. When
	 * present, its value indicates what additional content codings have been
	 * applied to the entity-body, and thus what decoding mechanisms must be
	 * applied in order to obtain the media-type referenced by the Content-Type
	 * Header-Field. Content-Encoding is primarily used to allow a document to
	 * be compressed without losing the identity of its underlying media type
	 * ..."
	 *
	 * @return The value being removed (or null if none was set).
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default ContentEncoding removeContentEncoding() {
		final ContentEncoding theReturn = getContentEncoding();
		remove( HeaderField.CONTENT_ENCODING );
		return theReturn;
	}

	// -------------------------------------------------------------------------
	// USER_HOME AGENT:
	// -------------------------------------------------------------------------

	/**
	 * Gets the User-Agent request-Header-Field {@link HeaderField#USER_AGENT}:
	 * "... The User-Agent request-Header-Field contains information about the
	 * user agent originating the request. This is for statistical purposes, the
	 * tracing of protocol violations, and automated recognition of user agents
	 * for the sake of tailoring responses to avoid particular user agent
	 * limitations. User agents SHOULD include this field with requests ..."
	 *
	 * @return The according user agent.
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default String getUserAgent() {
		return getFirst( HeaderField.USER_AGENT );
	}

	/**
	 * Sets the User-Agent request-Header-Field {@link HeaderField#USER_AGENT}:
	 * "... The User-Agent request-Header-Field contains information about the
	 * user agent originating the request. This is for statistical purposes, the
	 * tracing of protocol violations, and automated recognition of user agents
	 * for the sake of tailoring responses to avoid particular user agent
	 * limitations. User agents SHOULD include this field with requests ..."
	 *
	 * @param aUserAgent The according user agent.
	 * 
	 * @return the string
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default String putUserAgent( String aUserAgent ) {
		if ( aUserAgent == null || aUserAgent.isEmpty() ) {
			return removeUserAgent();
		}
		final String theReturn = getUserAgent();
		put( HeaderField.USER_AGENT, aUserAgent );
		return theReturn;
	}

	/**
	 * Sets he User-Agent request-Header-Field {@link HeaderField#USER_AGENT}:
	 * "... The User-Agent request-Header-Field contains information about the
	 * user agent originating the request. This is for statistical purposes, the
	 * tracing of protocol violations, and automated recognition of user agents
	 * for the sake of tailoring responses to avoid particular user agent
	 * limitations. User agents SHOULD include this field with requests ..."
	 *
	 * @param aUserAgent The according user agent.
	 * 
	 * @return This object as of the Builder-Pattern.
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	@SuppressWarnings("unchecked")
	default B withUserAgent( String aUserAgent ) {
		putUserAgent( aUserAgent );
		return (B) this;
	}

	/**
	 * Removes he User-Agent request-Header-Field {@link HeaderField#USER_AGENT}
	 * : "... The User-Agent request-Header-Field contains information about the
	 * user agent originating the request. This is for statistical purposes, the
	 * tracing of protocol violations, and automated recognition of user agents
	 * for the sake of tailoring responses to avoid particular user agent
	 * limitations. User agents SHOULD include this field with requests ..."
	 *
	 * @return The value being removed (or null if none was set).
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default String removeUserAgent() {
		final String theReturn = getUserAgent();
		remove( HeaderField.USER_AGENT );
		return theReturn;
	}

	// -------------------------------------------------------------------------
	// HOST:
	// -------------------------------------------------------------------------

	/**
	 * Gets the Host request-Header-Field {@link HeaderField#HOST}: "... The
	 * Host request-Header-Field specifies the Internet host and port number of
	 * the resource being requested, as obtained from the original URI given by
	 * the user or referring resource ..."
	 *
	 * @return The according host.
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default String getHost() {
		return getFirst( HeaderField.HOST );
	}

	/**
	 * Sets the Host request-Header-Field {@link HeaderField#HOST}: "... The
	 * Host request-Header-Field specifies the Internet host and port number of
	 * the resource being requested, as obtained from the original URI given by
	 * the user or referring resource ..."
	 *
	 * @param aHost The according host.
	 * 
	 * @return the string
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default String putHost( String aHost ) {
		if ( aHost == null || aHost.isEmpty() ) {
			return removeHost();
		}
		final String theReturn = getHost();
		put( HeaderField.HOST, aHost );
		return theReturn;
	}

	/**
	 * Sets the Host request-Header-Field {@link HeaderField#HOST}: "... The
	 * Host request-Header-Field specifies the Internet host and port number of
	 * the resource being requested, as obtained from the original URI given by
	 * the user or referring resource ..."
	 *
	 * @param aHost The according host.
	 * 
	 * @return This object as of the Builder-Pattern.
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	@SuppressWarnings("unchecked")
	default B withHost( String aHost ) {
		putHost( aHost );
		return (B) this;
	}

	/**
	 * Removes the Host request-Header-Field {@link HeaderField#HOST}: "... The
	 * Host request-Header-Field specifies the Internet host and port number of
	 * the resource being requested, as obtained from the original URI given by
	 * the user or referring resource ..."
	 *
	 * @return The value being removed (or null if none was set).
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default String removeHost() {
		final String theReturn = getHost();
		remove( HeaderField.HOST );
		return theReturn;
	}

	// -------------------------------------------------------------------------
	// CORRELATION TID:
	// -------------------------------------------------------------------------

	/**
	 * Gets the (interprocess) Request-Correlation TID field
	 * {@link HeaderField#REQUEST_ID}: A Request-Correlation TID is an TID to
	 * uniquely identify an entity (request) across multiple systems.
	 * 
	 * @return The according Request-Correlation TID.
	 */
	default String getRequestId() {
		return getFirst( HeaderField.REQUEST_ID );
	}

	/**
	 * Sets the (interprocess) Request-Correlation TID field
	 * {@link HeaderField#REQUEST_ID}: A Request-Correlation TID is an TID to
	 * uniquely identify an entity (request) across multiple systems.
	 * 
	 * @param aRequestId The according Request-Correlation TID.
	 * 
	 * @return The value being removed (or null if none was set).
	 */
	default String putRequestId( String aRequestId ) {
		if ( aRequestId == null || aRequestId.isEmpty() ) {
			return removeRequestId();
		}
		final String theReturn = getRequestId();
		put( HeaderField.REQUEST_ID, aRequestId );
		return theReturn;
	}

	/**
	 * Sets the (interprocess) Request-Correlation TID field
	 * {@link HeaderField#REQUEST_ID}: A Request-Correlation TID is an TID to
	 * uniquely identify an entity (request) across multiple systems.
	 * 
	 * @param aRequestId The according Request-Correlation TID.
	 * 
	 * @return This object as of the Builder-Pattern.
	 */
	@SuppressWarnings("unchecked")
	default B withRequestId( String aRequestId ) {
		putRequestId( aRequestId );
		return (B) this;
	}

	/**
	 * Removes the (interprocess) Request-Correlation TID field
	 * {@link HeaderField#REQUEST_ID}: A Request-Correlation TID is an TID to
	 * uniquely identify an entity (request) across multiple systems.
	 * 
	 * @return The value being removed (or null if none was set).
	 */
	default String removeRequestId() {
		final String theReturn = getRequestId();
		remove( HeaderField.REQUEST_ID );
		return theReturn;
	}

	/**
	 * Gets the (interprocess) Session-Correlation TID field
	 * {@link HeaderField#SESSION_ID}: A Session-Correlation TID is an TID to
	 * uniquely identify an entity (session) across multiple systems.
	 * 
	 * @return The according Session-Correlation TID.
	 */
	default String getSessionId() {
		return getFirst( HeaderField.SESSION_ID );
	}

	/**
	 * Sets the (interprocess) Session-Correlation TID field
	 * {@link HeaderField#SESSION_ID}: A Session-Correlation TID is an TID to
	 * uniquely identify an entity (session) across multiple systems.
	 * 
	 * @param aSessionId The according Session-Correlation TID.
	 * 
	 * @return The value being removed (or null if none was set).
	 */
	default String putSessionId( String aSessionId ) {
		if ( aSessionId == null || aSessionId.isEmpty() ) {
			return removeSessionId();
		}
		final String theReturn = getSessionId();
		put( HeaderField.SESSION_ID, aSessionId );
		return theReturn;
	}

	/**
	 * Sets the (interprocess) Session-Correlation TID field
	 * {@link HeaderField#SESSION_ID}: A Session-Correlation TID is an TID to
	 * uniquely identify an entity (session) across multiple systems.
	 * 
	 * @param aSessionId The according Session-Correlation TID.
	 * 
	 * @return This object as of the Builder-Pattern.
	 */
	@SuppressWarnings("unchecked")
	default B withSessionId( String aSessionId ) {
		putSessionId( aSessionId );
		return (B) this;
	}

	/**
	 * Removes the (interprocess) Session-Correlation TID field
	 * {@link HeaderField#SESSION_ID}: A Session-Correlation TID is an TID to
	 * uniquely identify an entity (session) across multiple systems.
	 * 
	 * @return The value being removed (or null if none was set).
	 */
	default String removeSessionId() {
		final String theReturn = getSessionId();
		remove( HeaderField.SESSION_ID );
		return theReturn;
	}

	// -------------------------------------------------------------------------
	// AUTHENTICATE:
	// -------------------------------------------------------------------------

	/**
	 * Gets the WWW-Authenticate Response-Header-Field
	 * {@link HeaderField#WWW_AUTHENTICATE}: "... The WWW-Authenticate
	 * Response-Header-Field MUST be included in 401 (Unauthorized) response
	 * messages. The field value consists of at least one challenge that
	 * indicates the authentication scheme(s) and parameters applicable to the
	 * Request-URI ..."
	 *
	 * @return The according WWW-Authenticate field.
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default String getAuthenticate() {
		return getFirst( HeaderField.WWW_AUTHENTICATE );
	}

	/**
	 * Sets the WWW-Authenticate Response-Header-Field
	 * {@link HeaderField#WWW_AUTHENTICATE}: "... The WWW-Authenticate
	 * Response-Header-Field MUST be included in 401 (Unauthorized) response
	 * messages. The field value consists of at least one challenge that
	 * indicates the authentication scheme(s) and parameters applicable to the
	 * Request-URI ..."
	 *
	 * @param aAuthenticate The according WWW-Authenticate field.
	 * 
	 * @return The value being removed (or null if none was set).
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default String putAuthenticate( String aAuthenticate ) {
		if ( aAuthenticate == null || aAuthenticate.isEmpty() ) {
			return removeAuthenticate();
		}
		final String theReturn = getAuthenticate();
		put( HeaderField.WWW_AUTHENTICATE, aAuthenticate );
		return theReturn;
	}

	/**
	 * Sets the WWW-Authenticate Response-Header-Field
	 * {@link HeaderField#WWW_AUTHENTICATE}: "... The WWW-Authenticate
	 * Response-Header-Field MUST be included in 401 (Unauthorized) response
	 * messages. The field value consists of at least one challenge that
	 * indicates the authentication scheme(s) and parameters applicable to the
	 * Request-URI ..."
	 *
	 * @param aAuthenticate The according WWW-Authenticate field.
	 * 
	 * @return This object as of the Builder-Pattern.
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	@SuppressWarnings("unchecked")
	default B withAuthenticate( String aAuthenticate ) {
		putAuthenticate( aAuthenticate );
		return (B) this;
	}

	/**
	 * Removes the WWW-Authenticate Response-Header-Field
	 * {@link HeaderField#WWW_AUTHENTICATE}: "... The WWW-Authenticate
	 * Response-Header-Field MUST be included in 401 (Unauthorized) response
	 * messages. The field value consists of at least one challenge that
	 * indicates the authentication scheme(s) and parameters applicable to the
	 * Request-URI ..."
	 *
	 * @return The value being removed (or null if none was set).
	 * 
	 * @see "https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html"
	 */
	default String removeAuthenticate() {
		final String theReturn = getAuthenticate();
		remove( HeaderField.WWW_AUTHENTICATE );
		return theReturn;
	}

	// -------------------------------------------------------------------------
	// BASIC AUTH AUTHENTICATE:
	// -------------------------------------------------------------------------

	/**
	 * Retrieves the WWW-Authenticate field for Basic-Authentication. If not set
	 * or another kind of authorization is set, then null is returned. Use
	 * {@link #getAuthenticate()} to get the field's value no matter which kind
	 * of authenticate is used.
	 * 
	 * @return The WWW-Authenticate field for Basic-Authentication or null if
	 *         there is no value for the given field or it does not declare
	 *         Basic-Authentication.
	 */
	default String getBasicAuth() {
		final String theReturn = getFirst( HeaderField.WWW_AUTHENTICATE );
		if ( theReturn != null && theReturn.startsWith( BASIC_REALM ) ) {
			return theReturn;
		}
		return null;
	}

	/**
	 * Sets the WWW-Authenticate field for Basic-Authentication with the given
	 * realm.
	 * 
	 * @param aRealm The realm to be set for Basic-Authentication of the
	 *        WWW-authenticate field.
	 * 
	 * @return The value being removed (or null if none was set).
	 */
	default String putBasicAuthRequired( String aRealm ) {
		if ( aRealm == null ) {
			return removeBasicAuthenticate();
		}
		final String theReturn = getBasicAuth();
		put( HeaderField.WWW_AUTHENTICATE.getName(), BASIC_REALM + "=\"" + aRealm + "\"" );
		return theReturn;
	}

	/**
	 * Sets the WWW-Authenticate field for Basic-Authentication with the given
	 * realm.
	 * 
	 * @param aRealm The realm to be set for Basic-Authentication of the
	 *        WWW-authenticate field.
	 * 
	 * @return This object as of the Builder-Pattern.
	 */
	@SuppressWarnings("unchecked")
	default B withBasicAuthRequired( String aRealm ) {
		putBasicAuthRequired( aRealm );
		return (B) this;
	}

	/**
	 * Removes the WWW-Authenticate field for Basic-Authentication. If not set
	 * or another kind of authorization is set, then null is returned and the
	 * field is not removed. Use {@link #removeAuthenticate()} to remove the
	 * field's value no matter which kind of authenticate is used.
	 * 
	 * @return The value being removed (or null if none or none for basic
	 *         authentication was set).
	 */
	default String removeBasicAuthenticate() {
		final String theReturn = getBasicAuth();
		if ( theReturn != null ) {
			remove( HeaderField.WWW_AUTHENTICATE );
		}
		return theReturn;
	}

	/**
	 * Extracts the real (if any) from the {@link HeaderField#WWW_AUTHENTICATE}
	 * Header-Field ({@link #getAuthenticate()}).
	 * 
	 * @return The basic authenticate's realm or null if there is none such.
	 */
	default String toBasicAuthRealm() {
		String theReturn = getAuthenticate();
		if ( theReturn != null ) {
			final String theRealmPrefix = HeaderFields.BASIC_REALM + "=\"";
			if ( theReturn.startsWith( theRealmPrefix ) ) {
				theReturn = theReturn.substring( theRealmPrefix.length() );
				if ( theReturn.endsWith( "\"" ) ) {
					theReturn = theReturn.substring( 0, theReturn.length() - 1 );
				}
			}
		}
		return theReturn;
	}

	// -------------------------------------------------------------------------
	// AUTHORIZATION:
	// -------------------------------------------------------------------------

	// -------------------------------------------------------------------------
	// AUTHORIZATION:
	// -------------------------------------------------------------------------

	/**
	 * Gets the Authorize Request-Header-Field
	 * {@link HeaderField#AUTHORIZATION}: "... The Authorize
	 * Request-Header-Field MUST be included in 401 (Unauthorized) response
	 * messages. The field value consists of at least one challenge that
	 * indicates the authentication scheme(s) and parameters applicable to the
	 * Request-URI ..."
	 *
	 * @return The according Authorize field.
	 */
	default String getAuthorization() {
		return getFirst( HeaderField.AUTHORIZATION );
	}

	/**
	 * Sets the Authorize Request-Header-Field
	 * {@link HeaderField#AUTHORIZATION}: "... The Authorize
	 * Request-Header-Field MUST be included in 401 (Unauthorized) response
	 * messages. The field value consists of at least one challenge that
	 * indicates the authentication scheme(s) and parameters applicable to the
	 * Request-URI ..."
	 *
	 * @param aAuthType The {@link AuthType} to be used.
	 * @param aAuthorize The according Authorize field.
	 * 
	 * @return The value being removed (or null if none was set).
	 */
	default String putAuthorization( String aAuthType, String aAuthorize ) {
		if ( ( aAuthorize == null || aAuthorize.isEmpty() ) && ( aAuthType == null || aAuthType.isEmpty() ) ) {
			return removeAuthorization();
		}
		final String theReturn = getAuthorization();
		put( HeaderField.AUTHORIZATION, aAuthType + " " + aAuthorize );
		return theReturn;
	}

	/**
	 * Sets the Authorize Request-Header-Field
	 * {@link HeaderField#AUTHORIZATION}: "... The Authorize
	 * Request-Header-Field MUST be included in 401 (Unauthorized) response
	 * messages. The field value consists of at least one challenge that
	 * indicates the authentication scheme(s) and parameters applicable to the
	 * Request-URI ..."
	 *
	 * @param aAuthorize The according Authorize field.
	 * @param aAuthType The {@link AuthType} to be used.
	 * 
	 * @return This object as of the Builder-Pattern.
	 */
	@SuppressWarnings("unchecked")
	default B withAuthorization( String aAuthType, String aAuthorize ) {
		putAuthorization( aAuthType, aAuthorize );
		return (B) this;
	}

	/**
	 * Sets the Authorize Request-Header-Field
	 * {@link HeaderField#AUTHORIZATION}: "... The Authorize
	 * Request-Header-Field MUST be included in 401 (Unauthorized) response
	 * messages. The field value consists of at least one challenge that
	 * indicates the authentication scheme(s) and parameters applicable to the
	 * Request-URI ..."
	 *
	 * @param aAuthType The {@link AuthType} to be used.
	 * @param aAuthorize The according Authorize field.
	 * 
	 * @return The value being removed (or null if none was set).
	 */
	default String putAuthorization( AuthType aAuthType, String aAuthorize ) {
		if ( ( aAuthorize == null || aAuthorize.isEmpty() ) && ( aAuthType == null ) ) {
			return removeAuthorization();
		}
		final String theReturn = getAuthorization();
		put( HeaderField.AUTHORIZATION, aAuthType.getName() + " " + aAuthorize );
		return theReturn;
	}

	/**
	 * Sets the Authorize Request-Header-Field
	 * {@link HeaderField#AUTHORIZATION}: "... The Authorize
	 * Request-Header-Field MUST be included in 401 (Unauthorized) response
	 * messages. The field value consists of at least one challenge that
	 * indicates the authentication scheme(s) and parameters applicable to the
	 * Request-URI ..."
	 *
	 * @param aAuthorize The according Authorize field.
	 * @param aAuthType The {@link AuthType} to be used.
	 * 
	 * @return This object as of the Builder-Pattern.
	 */
	@SuppressWarnings("unchecked")
	default B withAuthorization( AuthType aAuthType, String aAuthorize ) {
		putAuthorization( aAuthType, aAuthorize );
		return (B) this;
	}

	/**
	 * Sets the Authorize Request-Header-Field
	 * {@link HeaderField#AUTHORIZATION}: "... The Authorize
	 * Request-Header-Field MUST be included in 401 (Unauthorized) response
	 * messages. The field value consists of at least one challenge that
	 * indicates the authentication scheme(s) and parameters applicable to the
	 * Request-URI ..."
	 *
	 * @param aAuthorize The according Authorize field.
	 * 
	 * @return The value being removed (or null if none was set).
	 */
	default String putAuthorization( String aAuthorize ) {
		if ( aAuthorize == null || aAuthorize.isEmpty() ) {
			return removeAuthorization();
		}
		final String theReturn = getAuthorization();
		put( HeaderField.AUTHORIZATION, aAuthorize );
		return theReturn;
	}

	/**
	 * Sets the Authorize Request-Header-Field
	 * {@link HeaderField#AUTHORIZATION}: "... The Authorize
	 * Request-Header-Field MUST be included in 401 (Unauthorized) response
	 * messages. The field value consists of at least one challenge that
	 * indicates the authentication scheme(s) and parameters applicable to the
	 * Request-URI ..."
	 *
	 * @param aAuthorize The according Authorize field.
	 * 
	 * @return This object as of the Builder-Pattern.
	 */
	@SuppressWarnings("unchecked")
	default B withAuthorization( String aAuthorize ) {
		putAuthorization( aAuthorize );
		return (B) this;
	}

	/**
	 * Removes the Authorize Request-Header-Field
	 * {@link HeaderField#AUTHORIZATION}: "... The Authorize
	 * Request-Header-Field MUST be included in 401 (Unauthorized) response
	 * messages. The field value consists of at least one challenge that
	 * indicates the authentication scheme(s) and parameters applicable to the
	 * Request-URI ..."
	 *
	 * @return The value being removed (or null if none was set).
	 */
	default String removeAuthorization() {
		final String theReturn = getAuthorization();
		remove( HeaderField.AUTHORIZATION );
		return theReturn;
	}

	// -------------------------------------------------------------------------
	// BEARER AUTH AUTHORIZATION:
	// -------------------------------------------------------------------------

	/**
	 * Retrieves the {@link BearerAuthCredentials} from the request (or null if
	 * there are none such credentials). Throw an {@link UnauthorizedException}
	 * in case you do not agree with the credentials you find there.
	 * 
	 * @return The {@link BearerAuthCredentials} stored by the bearer
	 *         authentication credentials property (or null if there are none
	 *         such credentials).
	 */
	default BearerAuthCredentials getBearerAuthCredentials() {
		final String theValue = getFirst( HeaderField.AUTHORIZATION );
		if ( theValue != null ) {
			try {
				return new BearerAuthCredentials().withHttpAuthorization( theValue );
			}
			catch ( IllegalArgumentException ignore ) { /* ignore */ }
		}
		return null;
	}

	/**
	 * Sets the {@link BearerAuthCredentials} from the token for the bearer auth
	 * credentials property.
	 * 
	 * @param aBearerAuthCredentials The password to be stored by the
	 *        {@link BearerAuthCredentials} property.
	 */
	default BearerAuthCredentials putBearerAuthCredentials( String aBearerAuthCredentials ) {
		return putBearerAuthCredentials( new BearerAuthCredentials( aBearerAuthCredentials ) );
	}

	/**
	 * Sets the {@link BearerAuthCredentials} (token) for HTTP bearer
	 * authentication.
	 *
	 * @param aBearerAuthCredentials The credentials (token).
	 */
	default BearerAuthCredentials putBearerAuthCredentials( BearerAuthCredentials aBearerAuthCredentials ) {
		if ( aBearerAuthCredentials == null ) {
			return removeBearerAuthCredentials();
		}
		final BearerAuthCredentials theReturn = getBearerAuthCredentials();
		put( HeaderField.AUTHORIZATION, aBearerAuthCredentials.toHttpAuthorization() );
		return theReturn;

	}

	/**
	 * Sets the {@link BearerAuthCredentials} (token) for HTTP bearer
	 * authentication.
	 * 
	 * @param aBearerAuthCredentials The credentials (token).
	 * 
	 * @return This object as of the Builder-Pattern.
	 */

	@SuppressWarnings("unchecked")
	default B withBearerAuthCredentials( BearerAuthCredentials aBearerAuthCredentials ) {
		putBearerAuthCredentials( aBearerAuthCredentials );
		return (B) this;
	}

	/**
	 * Sets the token (secret) for HTTP bearer authentication.
	 * 
	 * @param aToken The password part of the credentials.
	 * 
	 * @return This object as of the Builder-Pattern.
	 */
	@SuppressWarnings("unchecked")
	default B withBearerAuthCredentials( String aToken ) {
		putBearerAuthCredentials( aToken );
		return (B) this;
	}

	/**
	 * Removes the {@link BearerAuthCredentials} (token) for HTTP bearer
	 * authentication.
	 * 
	 * @return The value being removed (or null if none was set).
	 */
	default BearerAuthCredentials removeBearerAuthCredentials() {
		final BearerAuthCredentials theReturn = getBearerAuthCredentials();
		if ( theReturn != null ) {
			remove( HeaderField.AUTHORIZATION );
		}
		return theReturn;
	}

	// -------------------------------------------------------------------------
	// BASIC AUTH AUTHORIZATION:
	// -------------------------------------------------------------------------

	/**
	 * Retrieves the {@link BasicAuthCredentials} from the request (or null if
	 * there are none such credentials). Throw an {@link UnauthorizedException}
	 * in case you do not agree with the credentials you find there.
	 * 
	 * @return The {@link BasicAuthCredentials} stored by the basic
	 *         authentication credentials property (or null if there are none
	 *         such credentials).
	 */
	default BasicAuthCredentials getBasicAuthCredentials() {
		final String theValue = getFirst( HeaderField.AUTHORIZATION );
		if ( theValue != null ) {
			try {
				return new BasicAuthCredentials().withHttpAuthorization( theValue );
			}
			catch ( IllegalArgumentException ignore ) { /* ignore */ }
		}
		return null;
	}

	/**
	 * Sets the {@link BasicAuthCredentials} from the user name and the secret
	 * for the Basic-Authentication credentials property.
	 * 
	 * @param aUserName The user name to be stored by the
	 *        {@link BasicAuthCredentials} property.
	 * @param aSecret The password to be stored by the
	 *        {@link BasicAuthCredentials} property.
	 */
	default BasicAuthCredentials putBasicAuthCredentials( String aUserName, String aSecret ) {
		return putBasicAuthCredentials( new BasicAuthCredentials( aUserName, aSecret ) );
	}

	/**
	 * Sets the {@link BasicAuthCredentials} (user name and secret) for HTTP
	 * Basic-Authentication.
	 *
	 * @param aBasicAuthCredentials The credentials (user name and secret).
	 */
	default BasicAuthCredentials putBasicAuthCredentials( BasicAuthCredentials aBasicAuthCredentials ) {
		if ( aBasicAuthCredentials == null ) {
			return removeBasicAuthCredentials();
		}
		final BasicAuthCredentials theReturn = getBasicAuthCredentials();
		put( HeaderField.AUTHORIZATION, aBasicAuthCredentials.toHttpAuthorization() );
		return theReturn;

	}

	/**
	 * Sets the {@link BasicAuthCredentials} (user name and secret) for HTTP
	 * Basic-Authentication.
	 * 
	 * @param aBasicAuthCredentials The credentials (user name and secret).
	 * 
	 * @return This object as of the Builder-Pattern.
	 */

	@SuppressWarnings("unchecked")
	default B withBasicAuthCredentials( BasicAuthCredentials aBasicAuthCredentials ) {
		putBasicAuthCredentials( aBasicAuthCredentials );
		return (B) this;
	}

	/**
	 * Sets the user name and secret (password) for HTTP Basic-Authentication.
	 * 
	 * @param aUserName The user name part of the credentials.
	 * @param aSecret The password part of the credentials.
	 * 
	 * @return This object as of the Builder-Pattern.
	 */
	@SuppressWarnings("unchecked")
	default B withBasicAuthCredentials( String aUserName, String aSecret ) {
		putBasicAuthCredentials( aUserName, aSecret );
		return (B) this;
	}

	/**
	 * Removes the {@link BasicAuthCredentials} (user name and secret) for HTTP
	 * Basic-Authentication.
	 * 
	 * @return The value being removed (or null if none was set).
	 */
	default BasicAuthCredentials removeBasicAuthCredentials() {
		final BasicAuthCredentials theReturn = getBasicAuthCredentials();
		if ( theReturn != null ) {
			remove( HeaderField.AUTHORIZATION );
		}
		return theReturn;
	}

	// -------------------------------------------------------------------------
	// COMMONS:
	// -------------------------------------------------------------------------

	/**
	 * Gets the values for the according Header-Field.
	 *
	 * @param aHeaderField The Header-Field
	 * 
	 * @return The list of values.
	 */
	default List<String> get( HeaderField aHeaderField ) {
		return get( aHeaderField.getName() );
	}

	/**
	 * Gets the first value of the according Header-Field.
	 *
	 * @param aHeaderField The a Header-Field
	 * 
	 * @return The first value.
	 */
	default String getFirst( HeaderField aHeaderField ) {
		return getFirst( aHeaderField.getName() );
	}

	/**
	 * Adds the value to the given Header-Field.
	 *
	 * @param aHeaderField The Header-Field.
	 * @param aValue The a value to be added.
	 */
	default void addTo( HeaderField aHeaderField, String aValue ) {
		addTo( aHeaderField.getName(), aValue );
	}

	/**
	 * Builder method for {@link #addTo(HeaderField, String)}.
	 *
	 * @param aHeaderField the Header-Field
	 * @param aValue the value
	 * 
	 * @return This instance as of the Builder-Pattern to chain operations.
	 */
	default HeaderFields<C, B> withAddTo( HeaderField aHeaderField, String aValue ) {
		return withAddTo( aHeaderField.getName(), aValue );
	}

	/**
	 * Adds the given values to the according Header-Field.
	 *
	 * @param aHeaderField The Header-Field.
	 * @param aValues The a values to be added.
	 */
	default void addTo( HeaderField aHeaderField, String... aValues ) {
		addTo( aHeaderField.getName(), aValues );
	}

	/**
	 * Adds the given values to the according Header-Field.
	 *
	 * @param aHeaderField The Header-Field.
	 * @param aValues The a values to be added.
	 */
	default void addTo( HeaderField aHeaderField, List<String> aValues ) {
		addTo( aHeaderField.getName(), aValues );
	}

	/**
	 * Builder method for {@link #addTo(HeaderField, String ...)}.
	 *
	 * @param aHeaderField the Header-Field
	 * @param aValues the values
	 * 
	 * @return This instance as of the Builder-Pattern to chain operations.
	 */
	default HeaderFields<C, B> withAddTo( HeaderField aHeaderField, String... aValues ) {
		return withAddTo( aHeaderField.getName(), aValues );
	}

	/**
	 * Builder method for {@link #addTo(HeaderField, List)}.
	 *
	 * @param aHeaderField the Header-Field
	 * @param aValues the values
	 * 
	 * @return This instance as of the Builder-Pattern to chain operations.
	 */
	default HeaderFields<C, B> withAddTo( HeaderField aHeaderField, List<String> aValues ) {
		return withAddTo( aHeaderField.getName(), aValues );
	}

	/**
	 * Puts (replaces) the currently set Header-Field values with the provided
	 * one.
	 *
	 * @param aHeaderField The a Header-Field.
	 * @param aValue The value with which to replace the currently set
	 *        Header-Field values.
	 *
	 * @return The {@link List} with the previously set Header-Field values.
	 */
	default List<String> put( HeaderField aHeaderField, String aValue ) {
		return put( aHeaderField.getName(), aValue );
	}

	/**
	 * Builder method for {@link #put(HeaderField, String)}.
	 *
	 * @param aHeaderField The Header-Field.
	 * @param aValue The value.
	 * 
	 * @return This instance as of the Builder-Pattern to chain operations.
	 */
	default HeaderFields<C, B> withPut( HeaderField aHeaderField, String aValue ) {
		return withPut( aHeaderField.getName(), aValue );
	}

	/**
	 * Puts (replaces) the currently set Header-Field values with the provided
	 * ones.
	 *
	 * @param aHeaderField The a Header-Field.
	 * @param aValues The values with which to replace the currently set
	 *        Header-Field values.
	 *
	 * @return The {@link List} with the previously set Header-Field values.
	 */
	default List<String> put( HeaderField aHeaderField, String... aValues ) {
		return put( aHeaderField.getName(), aValues );
	}

	/**
	 * Puts (replaces) the currently set Header-Field values with the provided
	 * ones.
	 *
	 * @param aHeaderField The a Header-Field.
	 * @param aValues The values with which to replace the currently set
	 *        Header-Field values.
	 *
	 * @return The {@link List} with the previously set Header-Field values.
	 */
	default List<String> put( HeaderField aHeaderField, List<String> aValues ) {
		return put( aHeaderField.getName(), aValues );
	}

	/**
	 * Builder method for {@link #put(HeaderField, String ...)}.
	 *
	 * @param aHeaderField The Header-Field.
	 * @param aValues The values.
	 * 
	 * @return This instance as of the Builder-Pattern to chain operations.
	 */
	default HeaderFields<C, B> withPut( HeaderField aHeaderField, String... aValues ) {
		return withPut( aHeaderField.getName(), aValues );
	}

	/**
	 * Builder method for {@link #put(HeaderField, List)}.
	 *
	 * @param aHeaderField The Header-Field.
	 * @param aValues The values.
	 * 
	 * @return This instance as of the Builder-Pattern to chain operations.
	 */
	default HeaderFields<C, B> withPut( HeaderField aHeaderField, List<String> aValues ) {
		return withPut( aHeaderField.getName(), aValues );
	}

	/**
	 * Removes the values from the given Header-Field.
	 * 
	 * @param aHeaderField The a Header-Field for which to remove the values.
	 * 
	 * @return The {@link List} with the previously set Header-Field values.
	 */
	default List<String> remove( HeaderField aHeaderField ) {
		return remove( aHeaderField.getName() );
	}

	// -------------------------------------------------------------------------
	// COOKIES:
	// -------------------------------------------------------------------------

	/**
	 * Adds an individual server-side cookie to be sent to the client to this
	 * {@link ResponseHeaderFields} instance. The server-side cookies are
	 * retrieved from the {@link HeaderField#SET_COOKIE} Header-Field. WITH THE
	 * METHOD {@link #getAllCookies()} and {@link #getCookies(String)} as well
	 * as {@link #getFirstCookie(String)} ONLY THE COOKIES SENT BY THE CLIENT
	 * CAN BE RETRIEVED (E.G. COOKIES RESIDING IN THE HEADER FIELD
	 * {@link HeaderField#COOKIE}). COOKIES SET VIA
	 * {@link #addCookie(String, String)} OR
	 * {@link #withAddCookie(String, String)} CANNOT BE RETRIEVED, AS THE
	 * COOKIES BEING SET SERVER-SIDE ARE PUT IN THE
	 * {@link HeaderField#SET_COOKIE} Header-Field.
	 * 
	 * @param aHttpCookie The HTTP-Cookie {@link String} to be paresd as a
	 *        cookie.
	 * 
	 * @return The resulting cookie builder being added which can be modified
	 *         affecting(!) this {@link ResponseHeaderFields} instance!
	 * 
	 * @throws IllegalArgumentException thrown in case the HTTP-Cookie
	 *         {@link String} cannot be parsed as a cookie.
	 */
	C addCookie( String aHttpCookie );

	/**
	 * Adds an individual server-side cookie to be sent to the client to this
	 * {@link ResponseHeaderFields} instance. The server-side cookies are
	 * retrieved from the {@link HeaderField#SET_COOKIE} Header-Field. WITH THE
	 * METHOD {@link #getAllCookies()} and {@link #getCookies(String)} as well
	 * as {@link #getFirstCookie(String)} ONLY THE COOKIES SENT BY THE CLIENT
	 * CAN BE RETRIEVED (E.G. COOKIES RESIDING IN THE HEADER FIELD
	 * {@link HeaderField#COOKIE}). COOKIES SET VIA
	 * {@link #addCookie(String, String)} OR
	 * {@link #withAddCookie(String, String)} CANNOT BE RETRIEVED, AS THE
	 * COOKIES BEING SET SERVER-SIDE ARE PUT IN THE
	 * {@link HeaderField#SET_COOKIE} Header-Field.
	 * 
	 * @param aCookieName The name for the cookie to be added.
	 * @param aValue The value of the cookie to be added.
	 * 
	 * @return The resulting cookie builder being added which can be modified
	 *         affecting(!) this {@link ResponseHeaderFields} instance!
	 */
	C addCookie( String aCookieName, String aValue );

	/**
	 * Adds an individual server-side cookie to be sent to the client to this
	 * {@link ResponseHeaderFields} instance. The server-side cookies are
	 * retrieved from the {@link HeaderField#SET_COOKIE} Header-Field. WITH THE
	 * METHOD {@link #getAllCookies()} and {@link #getCookies(String)} as well
	 * as {@link #getFirstCookie(String)} ONLY THE COOKIES SENT BY THE CLIENT
	 * CAN BE RETRIEVED (E.G. COOKIES RESIDING IN THE HEADER FIELD
	 * {@link HeaderField#COOKIE}). COOKIES SET VIA
	 * {@link #addCookie(String, String)} OR
	 * {@link #withAddCookie(String, String)} CANNOT BE RETRIEVED, AS THE
	 * COOKIES BEING SET SERVER-SIDE ARE PUT IN THE
	 * {@link HeaderField#SET_COOKIE} Header-Field.
	 * 
	 * @param aCookie The cookie to be added.
	 * 
	 * @return The resulting cookie builder being added which can be modified
	 *         affecting(!) this {@link ResponseHeaderFields} instance!
	 */
	C addCookie( C aCookie );

	/**
	 * Builder method for the {@link #addCookie(String, String)} method. WITH
	 * THE METHOD {@link #getAllCookies()} and {@link #getCookies(String)} as
	 * well as {@link #getFirstCookie(String)} ONLY THE COOKIES SENT BY THE
	 * CLIENT CAN BE RETRIEVED (E.G. COOKIES RESIDING IN THE HEADER FIELD
	 * {@link HeaderField#COOKIE}). COOKIES SET VIA
	 * {@link #addCookie(String, String)} OR
	 * {@link #withAddCookie(String, String)} CANNOT BE RETRIEVED, AS THE
	 * COOKIES BEING SET SERVER-SIDE ARE PUT IN THE
	 * {@link HeaderField#SET_COOKIE} Header-Field.
	 * 
	 * @param aCookieName The name for the cookie to be added.
	 * @param aValue The value of the cookie to be added.
	 * 
	 * @return This {@link ResponseHeaderFields} instance to continue building
	 *         up the Header-Fields.
	 */
	default HeaderFields<C, B> withAddCookie( String aCookieName, String aValue ) {
		addCookie( aCookieName, aValue );
		return this;
	}

	/**
	 * Builder method for the {@link #addCookie(Cookie)} method. WITH THE METHOD
	 * {@link #getAllCookies()} and {@link #getCookies(String)} as well as
	 * {@link #getFirstCookie(String)} ONLY THE COOKIES SENT BY THE CLIENT CAN
	 * BE RETRIEVED (E.G. COOKIES RESIDING IN THE HEADER FIELD
	 * {@link HeaderField#COOKIE}). COOKIES SET VIA
	 * {@link #addCookie(String, String)} OR
	 * {@link #withAddCookie(String, String)} CANNOT BE RETRIEVED, AS THE
	 * COOKIES BEING SET SERVER-SIDE ARE PUT IN THE
	 * {@link HeaderField#SET_COOKIE} Header-Field.
	 * 
	 * @param aCookie The cookie to be added.
	 * 
	 * @return This {@link ResponseHeaderFields} instance to continue building
	 *         up the Header-Fields.
	 */
	default HeaderFields<C, B> withAddCookie( C aCookie ) {
		addCookie( aCookie );
		return this;
	}

	/**
	 * Retrieves an array of cookies sent by the client stored in this
	 * {@link RequestHeaderFields} instance. If no cookie has been found, then
	 * an empty array is returned. The client-side cookies are retrieved from
	 * the {@link HeaderField#COOKIE} Header-Field. WITH THE METHOD
	 * {@link #getAllCookies()} and {@link #getCookies(String)} as well as
	 * {@link #getFirstCookie(String)} ONLY THE COOKIES SENT BY THE CLIENT CAN
	 * BE RETRIEVED (E.G. COOKIES RESIDING IN THE HEADER FIELD
	 * {@link HeaderField#COOKIE}). COOKIES SET VIA
	 * {@link #addCookie(String, String)} OR
	 * {@link #withAddCookie(String, String)} CANNOT BE RETRIEVED, AS THE
	 * COOKIES BEING SET SERVER-SIDE ARE PUT IN THE
	 * {@link HeaderField#SET_COOKIE} Header-Field. According to the Netscape
	 * cookie_spec (https://curl.haxx.se/rfc/cookie_spec.html) the entire
	 * FILE=VALUE string of a cookie is a sequence of characters excluding
	 * semi-colon, comma and white space.
	 * 
	 * @return An array containing the cookies (key/value) stored in this
	 *         {@link RequestHeaderFields} instance.
	 */
	List<C> getAllCookies();

	/**
	 * Retrieves an individual cookie sent by the client stored in this
	 * {@link RequestHeaderFields} instance. The client-side cookies are
	 * retrieved from the {@link HeaderField#COOKIE} Header-Field. WITH THE
	 * METHOD {@link #getAllCookies()} and {@link #getCookies(String)} as well
	 * as {@link #getFirstCookie(String)} ONLY THE COOKIES SENT BY THE CLIENT
	 * CAN BE RETRIEVED (E.G. COOKIES RESIDING IN THE HEADER FIELD
	 * {@link HeaderField#COOKIE}). COOKIES SET VIA
	 * {@link #addCookie(String, String)} OR
	 * {@link #withAddCookie(String, String)} CANNOT BE RETRIEVED, AS THE
	 * COOKIES BEING SET SERVER-SIDE ARE PUT IN THE
	 * {@link HeaderField#SET_COOKIE} Header-Field.
	 * 
	 * @param aCookieName The name of the cookie to be retrieved.
	 * 
	 * @return The values of the cookie found for the according name.
	 */
	default List<C> getCookies( String aCookieName ) {
		final List<C> theCookies = getAllCookies();
		final List<C> theResult = new ArrayList<>();
		for ( C eCookie : theCookies ) {
			if ( aCookieName.equals( eCookie.getKey() ) ) {
				theResult.add( eCookie );
			}
		}
		return theResult;
	}

	/**
	 * Same as {@link #getAllCookies()} though just returning the first cookie.
	 * WITH THE METHOD {@link #getAllCookies()} and {@link #getCookies(String)}
	 * as well as {@link #getFirstCookie(String)} ONLY THE COOKIES SENT BY THE
	 * CLIENT CAN BE RETRIEVED (E.G. COOKIES RESIDING IN THE HEADER FIELD
	 * {@link HeaderField#COOKIE}). COOKIES SET VIA
	 * {@link #addCookie(String, String)} OR
	 * {@link #withAddCookie(String, String)} CANNOT BE RETRIEVED, AS THE
	 * COOKIES BEING SET SERVER-SIDE ARE PUT IN THE
	 * {@link HeaderField#SET_COOKIE} Header-Field.
	 * 
	 * @param aCookieName The name of the cookie to be retrieved.
	 * 
	 * @return The first cookie found for the according name or null if none
	 *         such cookies exist.
	 */
	default C getFirstCookie( String aCookieName ) {
		final List<C> theCookies = getCookies( aCookieName );
		if ( theCookies != null && !theCookies.isEmpty() ) {
			return theCookies.get( 0 );
		}
		return null;
	}

	/**
	 * Creates a HTTP Header-Field value from the herein stored {@link Cookie}
	 * instances.
	 * 
	 * @return The text representing the HTTP Header-Field for the herein stored
	 *         cookies.
	 */
	default String[] toHttpCookies() {
		final List<String> theHttpCookies = new ArrayList<>();
		final List<C> theCookies = getAllCookies();
		if ( theCookies == null ) {
			return null;
		}
		for ( Cookie eCookie : theCookies ) {
			theHttpCookies.add( eCookie.toHttpCookie() );
		}
		return theHttpCookies.toArray( new String[theHttpCookies.size()] );
	}

	// /////////////////////////////////////////////////////////////////////////
	// COMMON:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Copies the Header-Fields into the provided {@link Map}. This is useful
	 * when some underlying system provides an own instance of Header-Fields to
	 * be filled.
	 * 
	 * @param aToFields The {@link Map} where to copy the Header-Fields to.
	 */
	default void toHeaderFields( Map<String, List<String>> aToFields ) {
		for ( String eKey : keySet() ) {
			aToFields.put( eKey, get( eKey ) );
		}
	}
}
