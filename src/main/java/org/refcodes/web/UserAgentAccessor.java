// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides an accessor for a user agent property.
 */
public interface UserAgentAccessor {

	/**
	 * Retrieves the user agent from the user agent property.
	 * 
	 * @return The user agent stored by the user agent property.
	 */
	String getUserAgent();

	/**
	 * Provides a mutator for a user agent property.
	 */
	public interface UserAgentMutator {

		/**
		 * Sets the user agent for the user agent property.
		 * 
		 * @param aUserAgent The user agent to be stored by the user agent
		 *        property.
		 */
		void setUserAgent( String aUserAgent );
	}

	/**
	 * Provides a builder method for a user agent property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface UserAgentBuilder<B extends UserAgentBuilder<B>> {

		/**
		 * Sets the user agent for the user agent property.
		 * 
		 * @param aUserAgent The user agent to be stored by the user agent
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withUserAgent( String aUserAgent );
	}

	/**
	 * Provides a user agent property.
	 */
	public interface UserAgentProperty extends UserAgentAccessor, UserAgentMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setUserAgent(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aUserAgent The {@link String} to set (via
		 *        {@link #setUserAgent(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letUserAgent( String aUserAgent ) {
			setUserAgent( aUserAgent );
			return aUserAgent;
		}
	}
}
