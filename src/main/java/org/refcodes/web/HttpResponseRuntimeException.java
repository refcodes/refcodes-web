// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Thrown by a HTTP-Response handling system in case of some unexpected
 * response.
 */
public class HttpResponseRuntimeException extends HttpStatusRuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * {@inheritDoc}
	 */
	public HttpResponseRuntimeException( String aMessage, HttpStatusCode aStatusCode, String aErrorCode ) {
		super( aMessage, aStatusCode, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public HttpResponseRuntimeException( String aMessage, HttpStatusCode aStatusCode, Throwable aCause, String aErrorCode ) {
		super( aMessage, aStatusCode, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public HttpResponseRuntimeException( String aMessage, HttpStatusCode aStatusCode, Throwable aCause ) {
		super( aMessage, aStatusCode, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public HttpResponseRuntimeException( String aMessage, HttpStatusCode aStatusCode ) {
		super( aMessage, aStatusCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public HttpResponseRuntimeException( Throwable aCause, HttpStatusCode aStatusCode, String aErrorCode ) {
		super( aStatusCode, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public HttpResponseRuntimeException( Throwable aCause, HttpStatusCode aStatusCode ) {
		super( aStatusCode, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public HttpResponseRuntimeException( HttpStatusCode aStatusCode, Throwable aCause, String aErrorCode ) {
		super( aStatusCode, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public HttpResponseRuntimeException( HttpStatusCode aStatusCode, Throwable aCause ) {
		super( aStatusCode, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public HttpResponseRuntimeException( HttpStatusCode aStatusCode, Url aUrl, Throwable aCause, String aErrorCode ) {
		super( aStatusCode, aUrl, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public HttpResponseRuntimeException( HttpStatusCode aStatusCode, Url aUrl, Throwable aCause ) {
		super( aStatusCode, aUrl, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public HttpResponseRuntimeException( String aMessage, HttpStatusCode aStatusCode, Url aUrl, String aErrorCode ) {
		super( aMessage, aStatusCode, aUrl, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public HttpResponseRuntimeException( String aMessage, HttpStatusCode aStatusCode, Url aUrl, Throwable aCause, String aErrorCode ) {
		super( aMessage, aStatusCode, aUrl, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public HttpResponseRuntimeException( String aMessage, HttpStatusCode aStatusCode, Url aUrl, Throwable aCause ) {
		super( aMessage, aStatusCode, aUrl, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public HttpResponseRuntimeException( String aMessage, HttpStatusCode aStatusCode, Url aUrl ) {
		super( aMessage, aStatusCode, aUrl );
	}
}
