// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Abstract implementation of the {@link HeaderFields} type.
 * 
 * @param <T> The type of the sub-class, required for the builder methods such
 *        as {@link #withAddTo(String, String)}.
 */
@SuppressWarnings("serial")
public abstract class AbstractHttpFields<T extends HttpFields<T>> extends HashMap<String, List<String>> implements HttpFields<T> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates an empty instance.
	 */
	public AbstractHttpFields() {}

	/**
	 * Initializes the instance with the fields provided by the given
	 * {@link Map}.
	 * 
	 * @param aHttpFields The {@link Map} from which to get the keys and values
	 *        for initializing this instance.
	 */
	public AbstractHttpFields( Map<String, List<String>> aHttpFields ) {
		copyHttpFields( aHttpFields, this );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Copies the provided "from" fields into the provided "to" fields. Omits
	 * empty or null values.
	 * 
	 * @param aFromFields The fields from which to copy.
	 * @param aToFields The fields to which to copy.
	 */
	protected static void copyHttpFields( Map<String, List<String>> aFromFields, HttpFields<?> aToFields ) {
		List<String> eValues;
		String eElement;
		for ( String eKey : aFromFields.keySet() ) {
			eValues = aFromFields.get( eKey );
			if ( eValues != null ) {
				final Iterator<String> e = eValues.iterator();
				while ( e.hasNext() ) {
					eElement = e.next();
					if ( eElement != null && eElement.trim().length() != 0 ) {
						aToFields.addTo( eKey, eElement );
					}
				}
			}
		}
	}
}
