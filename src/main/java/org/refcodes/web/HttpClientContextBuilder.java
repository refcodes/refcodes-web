// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import java.net.MalformedURLException;
import java.net.URL;

import org.refcodes.data.Scheme;
import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.security.TrustStoreDescriptorAccessor.TrustStoreDescriptorBuilder;
import org.refcodes.security.TrustStoreDescriptorAccessor.TrustStoreDescriptorProperty;
import org.refcodes.web.BaseUrlAccessor.BaseUrlBuilder;
import org.refcodes.web.BaseUrlAccessor.BaseUrlProperty;

/**
 * The {@link HttpClientContextBuilder} implements the
 * {@link HttpClientContextBuilder} interface.
 */
public class HttpClientContextBuilder implements HttpClientContext, BaseUrlProperty, BaseUrlBuilder<HttpClientContextBuilder>, TrustStoreDescriptorProperty, TrustStoreDescriptorBuilder<HttpClientContextBuilder> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Url _baseUrl = null;
	private TrustStoreDescriptor _trustStoreDescriptor = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link HttpClientContext}.
	 */
	public HttpClientContextBuilder() {}

	/**
	 * Constructs a {@link HttpClientContext} with the given data.
	 * 
	 * @param aBaseUrl The {@link Url} to be used as a base {@link Url}.
	 * @param aTrustStoreDescriptor The descriptor describing the truststore to
	 *        be used.
	 */
	public HttpClientContextBuilder( Url aBaseUrl, TrustStoreDescriptor aTrustStoreDescriptor ) {
		_baseUrl = aBaseUrl;
		_trustStoreDescriptor = aTrustStoreDescriptor;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Url getBaseUrl() {
		return _baseUrl;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBaseUrl( Url aBaseUrl ) {
		_baseUrl = aBaseUrl;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBaseUrl( URL aBaseUrl ) {
		_baseUrl = new Url( aBaseUrl );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TrustStoreDescriptor getTrustStoreDescriptor() {
		return _trustStoreDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setTrustStoreDescriptor( TrustStoreDescriptor aTrustStoreDescriptor ) {
		_trustStoreDescriptor = aTrustStoreDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpClientContextBuilder withBaseUrl( Url aBaseUrl ) {
		setBaseUrl( aBaseUrl );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpClientContextBuilder withBaseUrl( URL aBaseURL ) {
		setBaseUrl( aBaseURL );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpClientContextBuilder withBaseUrl( String aBaseUrl ) throws MalformedURLException {
		setBaseUrl( aBaseUrl );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpClientContextBuilder withBaseUrl( String aProtocol, String aHost ) throws MalformedURLException {
		setBaseUrl( aProtocol, aHost );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpClientContextBuilder withBaseUrl( Scheme aScheme, String aHost ) throws MalformedURLException {
		setBaseUrl( aScheme, aHost );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpClientContextBuilder withBaseUrl( String aProtocol, String aHost, String aPath ) throws MalformedURLException {
		setBaseUrl( aProtocol, aHost, aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpClientContextBuilder withBaseUrl( Scheme aScheme, String aHost, String aPath ) throws MalformedURLException {
		setBaseUrl( aScheme, aHost, aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpClientContextBuilder withBaseUrl( String aProtocol, String aHost, int aPort ) throws MalformedURLException {
		setBaseUrl( aProtocol, aHost, aPort );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpClientContextBuilder withBaseUrl( Scheme aScheme, String aHost, int aPort ) throws MalformedURLException {
		setBaseUrl( aScheme, aHost, aPort );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpClientContextBuilder withBaseUrl( String aProtocol, String aHost, int aPort, String aPath ) throws MalformedURLException {
		setBaseUrl( aProtocol, aHost, aPort, aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpClientContextBuilder withBaseUrl( Scheme aScheme, String aHost, int aPort, String aPath ) throws MalformedURLException {
		setBaseUrl( aScheme, aHost, aPort, aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpClientContextBuilder withTrustStoreDescriptor( TrustStoreDescriptor aStoreDescriptor ) {
		setTrustStoreDescriptor( aStoreDescriptor );
		return this;
	}
}
