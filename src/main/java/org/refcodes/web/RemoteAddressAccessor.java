// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import java.net.InetSocketAddress;

/**
 * Provides an accessor for a remote address property.
 */
public interface RemoteAddressAccessor {

	/**
	 * Retrieves the remote address from the remote address property.
	 * 
	 * @return The remote address stored by the remote address property.
	 */
	InetSocketAddress getRemoteAddress();

	/**
	 * Provides a mutator for a remote address property.
	 */
	public interface RemoteAddressMutator {

		/**
		 * Sets the remote address for the remote address property.
		 * 
		 * @param aRemoteAddress The remote address to be stored by the remote
		 *        address property.
		 */
		void setRemoteAddress( InetSocketAddress aRemoteAddress );
	}

	/**
	 * Provides a builder method for a remote address property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface RemoteAddressBuilder<B extends RemoteAddressBuilder<B>> {

		/**
		 * Sets the remote address for the remote address property.
		 * 
		 * @param aRemoteAddress The remote address to be stored by the remote
		 *        address property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withRemoteAddress( InetSocketAddress aRemoteAddress );
	}

	/**
	 * Provides a remote address property.
	 */
	public interface RemoteAddressProperty extends RemoteAddressAccessor, RemoteAddressMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link InetSocketAddress} (setter) as of
		 * {@link #setRemoteAddress(InetSocketAddress)} and returns the very
		 * same value (getter).
		 * 
		 * @param aRemoteAddress The {@link InetSocketAddress} to set (via
		 *        {@link #setRemoteAddress(InetSocketAddress)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default InetSocketAddress letRemoteAddress( InetSocketAddress aRemoteAddress ) {
			setRemoteAddress( aRemoteAddress );
			return aRemoteAddress;
		}
	}
}
