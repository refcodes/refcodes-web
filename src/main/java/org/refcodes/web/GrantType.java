/////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import org.refcodes.mixin.ValueAccessor;

/**
 * The {@link GrantType} enumerations.
 * 
 * See also:
 * https://auth0.com/docs/get-started/applications/application-grant-types
 * https://fusionauth.io/learn/expert-advice/oauth/complete-list-oauth-grants
 */
public enum GrantType implements ValueAccessor<String> {

	/**
	 * Implicit "<code>implicit</code>" Grant (specification conforming grant).
	 */
	IMPLICIT("implicit"),

	/**
	 * Authorization Code "<code>authorization_code</code>" Grant (specification
	 * conforming grant).
	 */
	AUTHORIZATION_CODE("authorization_code"),

	/**
	 * Client Credentials "<code>client_credentials</code>" Grant (specification
	 * conforming grant).
	 */
	CLIENT_CREDENTIALS("client_credentials"),

	/**
	 * Resource Owner Password "<code>password</code>" Grant (specification
	 * conforming grant).
	 */
	PASSWORD("password"),

	/**
	 * Use Refresh Tokens (specification conforming grant).
	 */
	REFRESH_TOKEN("refresh_token"),

	/**
	 * Device Authorization
	 * "<code>urn:ietf:params:oauth:grant-type:device_code</code>" Grant
	 * (specification conforming grant).
	 */
	DEVICE_CODE("urn:ietf:params:oauth:grant-type:device_code"),

	/**
	 * Use JWT Bearer (JWT Bearer
	 * "<code>urn:ietf:params:oauth:grant-type:jwt-bearer</code>" Grant)
	 */
	JWT_BEARER("urn:ietf:params:oauth:grant-type:jwt-bearer"),

	/**
	 * Use UMA "<code>urn:ietf:params:oauth:grant-type:uma-ticket</code>" Grant
	 * (flow for UMA Grant).
	 */
	UMA_TICKET("urn:ietf:params:oauth:grant-type:uma-ticket"),

	/**
	 * Use SAML 2.0 Bearer
	 * "<code>urn:ietf:params:oauth:grant-type:saml2-bearer</code>" Grant (flow
	 * for SAML 2.0 Bearer Grant).
	 */
	SAML2_BEARER("urn:ietf:params:oauth:grant-type:saml2-bearer"),

	/**
	 * Use Token Exchange (flow for OAuth 2.0 Token Exchange
	 * "<code>urn:ietf:params:oauth:grant-type:token-exchange</code>" Grant).
	 */
	TOKEN_EXCHANGE("urn:ietf:params:oauth:grant-type:token-exchange"),

	/**
	 * Use an extension grant
	 * "<code>http://auth0.com/oauth/grant-type/password-realm</code>" similar
	 * to the Resource Owner Password Grant that includes the ability to
	 * indicate a specific realm (Auth0 extension grant).
	 */
	AUTH0_PASSWORD_REALM("http://auth0.com/oauth/grant-type/password-realm"),

	/**
	 * Multi-factor Authentication OOB
	 * "<code>http://auth0.com/oauth/grant-type/mfa-oob</code>" Grant Request
	 * (Auth0 extension grant).
	 */
	AUTH0_MFA_OOB("http://auth0.com/oauth/grant-type/mfa-oob"),

	/**
	 * Multi-factor Authentication OTP
	 * "<code>http://auth0.com/oauth/grant-type/mfa-otp</code>" Grant Request
	 * (Auth0 extension grant).
	 */
	AUTH0_MFA_OTP("http://auth0.com/oauth/grant-type/mfa-otp"),

	/**
	 * Multi-factor Authentication Recovery
	 * "<code>http://auth0.com/oauth/grant-type/mfa-recovery-code</code>" Grant
	 * Request (Auth0 extension grant).
	 */
	AUTH0_MFA_RECOVERY_CODE("http://auth0.com/oauth/grant-type/mfa-recovery-code"),

	/**
	 * Embedded Passwordless Login
	 * "<code>http://auth0.com/oauth/grant-type/passwordless/otp</code>" Grant
	 * Request (Auth0 extension grant).
	 */
	AUTH0_PASSWORDLESS_OTP("http://auth0.com/oauth/grant-type/passwordless/otp"),

	/**
	 * Use legacy RO "<code>http://auth0.com/oauth/legacy/grant-type/ro</code>"
	 * Grant (Auth0 legacy grant).
	 */
	AUTH0_LEGACY_RO("http://auth0.com/oauth/legacy/grant-type/ro"),

	/**
	 * Use legacy JWT-Bearer
	 * "<code>http://auth0.com/oauth/legacy/grant-type/ro/jwt-bearer</code>"
	 * Grant (Auth0 legacy grant).
	 */
	AUTH0_LEGACY_RO_JWT_BEARER("http://auth0.com/oauth/legacy/grant-type/ro/jwt-bearer"),

	/**
	 * Use Legacy Refresh Tokens
	 * "<code>http://auth0.com/oauth/legacy/grant-type/delegation/refresh_token</code>"
	 * Grant (Auth0 legacy grant).
	 */
	AUTH0_LEGACY_DELEGATION_REFRESH_TOKEN("http://auth0.com/oauth/legacy/grant-type/delegation/refresh_token"),

	/**
	 * Use legacy ID Token
	 * "<code>http://auth0.com/oauth/legacy/grant-type/delegation/id_token</code>"
	 * Grant (Auth0 legacy grant).
	 */
	AUTH0_LEGACY_DELEGATION_ID_TOKEN("http://auth0.com/oauth/legacy/grant-type/delegation/id_token"),

	/**
	 * Use legacy Access Token
	 * "<code>http://auth0.com/oauth/legacy/grant-type/access_token</code>"
	 * Grant (Auth0 legacy grant).
	 */
	AUTH0_LEGACY_ACCESS_TOKEN("http://auth0.com/oauth/legacy/grant-type/access_token");

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _name;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new grant type.
	 *
	 * @param aGrantType the grant type
	 */
	private GrantType( String aGrantType ) {
		_name = aGrantType;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the value representing the given {@link GrantType}.
	 *
	 * @return the according value.
	 */
	@Override
	public String getValue() {
		return _name;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns that {@link GrantType} represented by the given name.
	 * 
	 * @param aName The name for which to determine the {@link GrantType}.
	 * 
	 * @return The determined {@link GrantType} or null if none was
	 *         determinable.
	 */
	public static GrantType fromName( String aName ) {
		for ( GrantType eElement : values() ) {
			if ( eElement.getValue().equalsIgnoreCase( aName ) ) {
				return eElement;
			}
		}
		return null;
	}
}
