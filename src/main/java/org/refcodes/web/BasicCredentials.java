// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import org.refcodes.mixin.CredentialsAccessor;
import org.refcodes.mixin.Validatable;

/**
 * The {@link BasicCredentials} describes the attributes required by a
 * Basic-Auth authentication over HTTP(S).
 */
public class BasicCredentials implements CredentialsAccessor, Validatable<BasicCredentials> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected String _identity = null;
	protected String _secret = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new basic credentials impl.
	 */
	protected BasicCredentials() {}

	/**
	 * Instantiates a new basic credentials impl.
	 *
	 * @param aIdentity the user name
	 * @param aSecret the secret
	 */
	public BasicCredentials( String aIdentity, String aSecret ) {
		_identity = aIdentity;
		_secret = aSecret;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Validates the provided {@link BasicCredentials} against this
	 * {@link BasicCredentials} user-name and secret. If this
	 * {@link BasicCredentials} instance's user-name or password are null, then
	 * false is returned.
	 * 
	 * @param aCredentials The {@link BasicCredentials} to be tested if them fit
	 *        with the this {@link BasicCredentials} instance.
	 * 
	 * @return True if the {@link BasicCredentials} match with the this
	 *         {@link BasicCredentials} instance and this
	 *         {@link BasicAuthCredentials} instance's user-name and secret are
	 *         not null.
	 */
	@Override
	public boolean isValid( BasicCredentials aCredentials ) {
		if ( getIdentity() == null || getSecret() == null ) {
			return false;
		}
		return getIdentity().equals( aCredentials.getIdentity() ) && getSecret().equals( aCredentials.getSecret() );
	}

	/**
	 * Validates the provided user-name and secret against this
	 * {@link BasicCredentials} user-name and secret. If this
	 * {@link BasicCredentials} instance's user-name or password are null, then
	 * false is returned.
	 * 
	 * @param aUserName The user-name part to be tested if it fits with the this
	 *        {@link BasicCredentials} instance.
	 * @param aSecret The secret part to be tested if it fits with the this
	 *        {@link BasicCredentials} instance.
	 * 
	 * @return True if the user-name and secret match with the this
	 *         {@link BasicCredentials} instance and this
	 *         {@link BasicAuthCredentials} instance's user-name and secret are
	 *         not null.
	 */
	public boolean isValid( String aUserName, String aSecret ) {
		if ( getIdentity() == null || getSecret() == null ) {
			return false;
		}
		return getIdentity().equals( aUserName ) && getSecret().equals( aSecret );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getIdentity() {
		return _identity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getSecret() {
		return _secret;
	}
}
