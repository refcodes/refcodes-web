// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides access to a HTTP success code property including just the 2xx HTTP
 * success codes as of {@link HttpSuccessCode} excluding all HTTP error codes.
 */
public interface HttpSuccessCodeAccessor {

	/**
	 * Retrieves the HTTP success code from the HTTP success code property.
	 * 
	 * @return The HTTP success code stored by the HTTP success code property.
	 */
	HttpSuccessCode getHttpSuccessCode();

	/**
	 * Extends the {@link HttpSuccessCodeAccessor} with a setter method.
	 */
	public interface HttpSuccessCodeMutator {

		/**
		 * Sets the HTTP success code for the HTTP success code property.
		 * 
		 * @param aSuccessCode The HTTP success code to be stored by the HTTP
		 *        success code property.
		 */
		void setHttpSuccessCode( HttpSuccessCode aSuccessCode );
	}

	/**
	 * Provides a builder method for a HTTP success code property returning the
	 * builder for applying multiple build operations.
	 *
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface HttpSuccessCodeBuilder<B extends HttpSuccessCodeBuilder<B>> {

		/**
		 * Sets the HTTP success code for the HTTP success code property.
		 * 
		 * @param aSuccessCode The HTTP success code to be stored by the HTTP
		 *        success code property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withHttpSuccessCode( HttpSuccessCode aSuccessCode );
	}

	/**
	 * Extends the {@link HttpSuccessCodeAccessor} with a setter method.
	 */
	public interface HttpSuccessCodeProperty extends HttpSuccessCodeAccessor, HttpSuccessCodeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link HttpSuccessCode}
		 * (setter) as of {@link #setHttpSuccessCode(HttpSuccessCode)} and
		 * returns the very same value (getter).
		 * 
		 * @param aHttpSuccessCode The {@link HttpSuccessCode} to set (via
		 *        {@link #setHttpSuccessCode(HttpSuccessCode)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default HttpSuccessCode letHttpSuccessCode( HttpSuccessCode aHttpSuccessCode ) {
			setHttpSuccessCode( aHttpSuccessCode );
			return aHttpSuccessCode;
		}
	}
}
