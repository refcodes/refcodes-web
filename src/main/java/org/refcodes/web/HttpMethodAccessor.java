// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides an accessor for a HTTP-Method property.
 */
public interface HttpMethodAccessor {

	/**
	 * Retrieves the HTTP-Method from the HTTP-Method property.
	 * 
	 * @return The HTTP-Method stored by the HTTP-Method property.
	 */
	HttpMethod getHttpMethod();

	/**
	 * Provides a mutator for a HTTP-Method property.
	 */
	public interface HttpMethodMutator {

		/**
		 * Sets the HTTP-Method for the HTTP-Method property.
		 * 
		 * @param aHttpMethod The HTTP-Method to be stored by the HTTP-Method
		 *        property.
		 */
		void setHttpMethod( HttpMethod aHttpMethod );
	}

	/**
	 * Provides a builder method for a HTTP-Method property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface HttpMethodBuilder<B extends HttpMethodBuilder<B>> {

		/**
		 * Sets the HTTP-Method for the HTTP-Method property.
		 * 
		 * @param aHttpMethod The HTTP-Method to be stored by the HTTP-Method
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withHttpMethod( HttpMethod aHttpMethod );
	}

	/**
	 * Provides a HTTP-Method property.
	 */
	public interface HttpMethodProperty extends HttpMethodAccessor, HttpMethodMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link HttpMethod}
		 * (setter) as of {@link #setHttpMethod(HttpMethod)} and returns the
		 * very same value (getter).
		 * 
		 * @param aHttpMethod The {@link HttpMethod} to set (via
		 *        {@link #setHttpMethod(HttpMethod)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default HttpMethod letHttpMethod( HttpMethod aHttpMethod ) {
			setHttpMethod( aHttpMethod );
			return aHttpMethod;
		}
	}
}
