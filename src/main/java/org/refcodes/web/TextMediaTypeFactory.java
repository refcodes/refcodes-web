// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.web;

import org.refcodes.exception.MarshalException;
import org.refcodes.exception.UnmarshalException;

/**
 * Implements the {@link MediaTypeFactory} for Media-Type "text/plain" (
 * {@link MediaType#TEXT_PLAIN}).
 */
public class TextMediaTypeFactory implements MediaTypeFactory {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	// static {
	// List<MediaType> theMediaTypes = new ArrayList<>();
	// theMediaTypes.add( MediaType.TEXT_PLAIN );
	// for ( MediaType eMediaType : MediaType.values() ) {
	// if ( eMediaType.getTopLevelType() == TopLevelType.TEXT ||
	// eMediaType.getName().toLowerCase().endsWith( "plain" ) ||
	// eMediaType.getName().toLowerCase().endsWith( "text" ) ||
	// eMediaType.getName().toLowerCase().endsWith( "txt" ) ) {
	// if ( !theMediaTypes.contains( eMediaType ) ) theMediaTypes.add(
	// eMediaType );
	// }
	// }
	// MEDIA_TYPES = theMediaTypes.toArray( new MediaType[theMediaTypes.size()]
	// );
	// }

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final MediaType[] MEDIA_TYPES = new MediaType[] { MediaType.TEXT_PLAIN };

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MediaType[] getMediaTypes() {
		return MEDIA_TYPES;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toMarshaled( Object aObject ) throws MarshalException {
		if ( aObject == null ) {
			return null;
		}
		return aObject instanceof String ? (String) aObject : aObject.toString();
	}

	/**
	 * To unmarshaled.
	 *
	 * @param <T> the generic type
	 * @param aHttpBody the http body
	 * @param aType the type
	 * 
	 * @return the t
	 * 
	 * @throws UnmarshalException the unmarshal exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> T toUnmarshaled( String aHttpBody, Class<T> aType ) throws UnmarshalException {
		if ( String.class.isAssignableFrom( aType ) ) {
			return (T) aHttpBody;
		}
		throw new UnmarshalException( "Unable to unmarshal the HTTP body \"" + aHttpBody + "\" to type  <" + aType.getName() + ">. This factory solely supports the type <String>." );

	}
}
