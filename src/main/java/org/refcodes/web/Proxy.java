// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import org.refcodes.data.Delimiter;
import org.refcodes.data.Scheme;
import org.refcodes.runtime.EnvironmentVariable;
import org.refcodes.runtime.SystemProperty;

/**
 * The Enum Proxy.
 */
public enum Proxy {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	// @formatter:off
	HTTP(EnvironmentVariable.HTTP_PROXY, SystemProperty.HTTP_PROXY_HOST, SystemProperty.HTTP_PROXY_PORT, Scheme.HTTP ),
	
	HTTPS(EnvironmentVariable.HTTPS_PROXY, SystemProperty.HTTPS_PROXY_HOST, SystemProperty.HTTPS_PROXY_PORT, Scheme.HTTPS),
	
	SOCKS(EnvironmentVariable.HTTP_PROXY, SystemProperty.SOCKS_PROXY_HOST, SystemProperty.SOCKS_PROXY_PORT, Scheme.SOCKS ),
	
	SOCKS4(EnvironmentVariable.HTTP_PROXY, SystemProperty.SOCKS_PROXY_HOST, SystemProperty.SOCKS_PROXY_PORT, Scheme.SOCKS4),
	
	SOCKS5(EnvironmentVariable.HTTP_PROXY, SystemProperty.SOCKS_PROXY_HOST, SystemProperty.SOCKS_PROXY_PORT, Scheme.SOCKS5);
	// @formatter:on

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private EnvironmentVariable _envProxy;

	private SystemProperty _sysHost;

	private SystemProperty _sysPort;

	private Scheme _protocol;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new proxy.
	 *
	 * @param aEnvProxy the env proxy
	 * @param aSysHost the sys host
	 * @param aSysPort the sys port
	 * @param aProtocol the protocol
	 */
	private Proxy( EnvironmentVariable aEnvProxy, SystemProperty aSysHost, SystemProperty aSysPort, Scheme aProtocol ) {
		_envProxy = aEnvProxy;
		_protocol = aProtocol;
		_sysHost = aSysHost;
		_sysPort = aSysPort;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Initializes the proxy from the settings of the environment variables if
	 * non according proxy settings are found in the system properties.
	 */
	public void initialize() {
		toProxySettings( _envProxy, _sysHost, _sysPort, _protocol );
	}

	/**
	 * Initializes the proxy for all proxy enumeration elements, e.g. calls
	 * {@link #initialize()} on each element in this enumeration.
	 */
	public static void initializeAll() {
		for ( Proxy eProxy : values() ) {
			eProxy.initialize();
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * To proxy settings.
	 *
	 * @param aFromEnvProxy the from env proxy
	 * @param aToSysHost the to sys host
	 * @param aToSysPort the to sys port
	 * @param aProtocol the protocol
	 */
	protected static void toProxySettings( EnvironmentVariable aFromEnvProxy, SystemProperty aToSysHost, SystemProperty aToSysPort, Scheme aProtocol ) {
		if ( aToSysHost.getValue() == null || aToSysHost.getValue().isEmpty() ) {
			final String theHttpProxy = aFromEnvProxy.getValue();
			if ( theHttpProxy != null ) {
				if ( theHttpProxy.toLowerCase().startsWith( aProtocol.getMarker().toLowerCase() ) ) {
					final int index = theHttpProxy.lastIndexOf( Delimiter.URL_PORT.getChar() );
					if ( index != -1 ) {
						try {
							final int ePort = Integer.valueOf( theHttpProxy.substring( index + 1 ) );
							final String eHost = theHttpProxy.substring( 0, index );
							aToSysHost.setValue( eHost );
							aToSysPort.setValue( Integer.toString( ePort ) );

						}
						catch ( Exception e ) {
							aToSysHost.setValue( theHttpProxy );
						}
					}
					else {
						aToSysHost.setValue( theHttpProxy );
					}
				}
			}
		}
	}
}
