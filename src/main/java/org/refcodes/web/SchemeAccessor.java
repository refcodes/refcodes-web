// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import org.refcodes.data.Scheme;

/**
 * Provides an accessor for a URL scheme.
 */
public interface SchemeAccessor {

	/**
	 * Retrieves the UrlScheme from the URL scheme.
	 * 
	 * @return The UrlScheme stored by the URL scheme.
	 */
	Scheme getScheme();

	/**
	 * Retrieves the protocol representation from the {@link Scheme}. In case of
	 * a scheme unknown by the {@link Scheme} enumeration, then
	 * {@link #getScheme()} might return null whilst {@link #toProtocol()} still
	 * retrieves the unknown scheme's protocol representation (as of
	 * {@link SchemeMutator#setProtocol(String)}).
	 * 
	 * @return The protocol representation for the URL.
	 */
	String toProtocol();

	/**
	 * Provides a mutator for a URL scheme.
	 */
	public interface SchemeMutator {

		/**
		 * Sets the UrlScheme for the URL scheme.
		 * 
		 * @param aScheme The UrlScheme to be stored by the URL scheme.
		 */
		void setScheme( Scheme aScheme );

		/**
		 * Sets the protocol {@link String} representation for the scheme
		 * property. You must provide the scheme's specific part as well. In
		 * case of HTTP, provide "http://". In case your provided protocol is
		 * unknown by the {@link Scheme} enumeration, then the scheme property
		 * will not be set, though the {@link SchemeAccessor#toProtocol()} still
		 * returns your protocol.
		 * 
		 * @param aProtocol The UrlScheme's {@link String} representation to be
		 *        stored by the URL scheme.
		 */
		void setProtocol( String aProtocol );
	}

	/**
	 * Provides a builder method for a URL scheme returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface SchemeBuilder<B extends SchemeBuilder<B>> {

		/**
		 * Sets the UrlScheme for the URL scheme.
		 * 
		 * @param aScheme The UrlScheme to be stored by the URL scheme.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withScheme( Scheme aScheme );

		/**
		 * Sets the protocol for the URL scheme.
		 * 
		 * @param aProtocol The protocol to be stored by the URL scheme.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withProtocol( String aProtocol );
	}

	/**
	 * Provides a URL scheme.
	 */
	public interface SchemeProperty extends SchemeAccessor, SchemeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Scheme} (setter)
		 * as of {@link #setScheme(Scheme)} and returns the very same value
		 * (getter).
		 * 
		 * @param aScheme The {@link Scheme} to set (via
		 *        {@link #setScheme(Scheme)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Scheme letScheme( Scheme aScheme ) {
			setScheme( aScheme );
			return aScheme;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setProtocol(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aProtocol The {@link String} to set (via
		 *        {@link #setProtocol(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letProtocol( String aProtocol ) {
			setProtocol( aProtocol );
			return aProtocol;
		}
	}
}
