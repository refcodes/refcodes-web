// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classfragment ("http://www.gnu.org/software/classfragment/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides an accessor for a fragment property. A fragment is the hash ("#")
 * separated suffix of an {@link Url}.
 */
public interface FragmentAccessor {

	/**
	 * Retrieves the fragment from the fragment property.
	 * 
	 * @return The fragment stored by the fragment property.
	 */
	String getFragment();

	/**
	 * Provides a mutator for a fragment property.
	 */
	public interface FragmentMutator {

		/**
		 * Sets the fragment for the fragment property.
		 * 
		 * @param aFragment The fragment to be stored by the fragment property.
		 */
		void setFragment( String aFragment );
	}

	/**
	 * Provides a mutator for an fragment property.
	 * 
	 * @param <B> The builder which implements the {@link FragmentBuilder}.
	 */
	public interface FragmentBuilder<B extends FragmentBuilder<?>> {

		/**
		 * Sets the fragment to use and returns this builder as of the builder
		 * pattern.
		 * 
		 * @param aFragment The fragment to be stored by the fragment property.
		 * 
		 * @return This {@link FragmentBuilder} instance to continue
		 *         configuration.
		 */
		B withFragment( String aFragment );
	}

	/**
	 * Provides a fragment property.
	 */
	public interface FragmentProperty extends FragmentAccessor, FragmentMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setFragment(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aFragment The {@link String} to set (via
		 *        {@link #setFragment(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letFragment( String aFragment ) {
			setFragment( aFragment );
			return aFragment;
		}
	}
}
