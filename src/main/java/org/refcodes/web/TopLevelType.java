// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;
///////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
///////////////////////////////////////////////////////////////////////////////

import org.refcodes.data.Delimiter;
import org.refcodes.mixin.NameAccessor;

/**
 * The {@link TopLevelType} is the top-level part of a HTTP Media-Type. See
 * {@link MediaType} for a full list of the so called HTTP Media-Types. Given
 * the HTTP Media-Type "application/json", "application" is considered to be the
 * top-level Media-Type and "json" to be the Sub-Media-Type. As of common
 * speaking, the Sub-Media-Type is usually called Media-Type. Therefore the
 * Sub-Media-Types are gathered in the {@link MediaType} enumeration to avoid
 * misunderstanding.
 */
public enum TopLevelType implements NameAccessor {

	WILDCARD("*"),

	NONE("NONE"),

	UNKNOWN("UNKNOWN"),

	APPLICATION("application"),

	AUDIO("audio"),

	IMAGE("image"),

	MESSAGE("message"),

	MODEL("model"),

	TEXT("text"),

	MULTIPART("multipart"),

	VIDEO("video");

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _topLevelTypeName;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new top level type.
	 *
	 * @param aTopLevelType the top level type
	 */
	private TopLevelType( String aTopLevelType ) {
		_topLevelTypeName = aTopLevelType;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	@Override
	public String getName() {
		return _topLevelTypeName;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns that {@link TopLevelType} represented by the given HTTP
	 * Top-Level-Media-Type. If the HTTP Media-Type contains the Media-Sub-Type
	 * portion (in terms of {@link MediaType}), then the provided HTTP
	 * Media-Type till the "/" slash is evaluated. For example
	 * "application/json" as well as "application" will return
	 * {@link TopLevelType#APPLICATION}.
	 * 
	 * @param aHttpTopLevelType The HTTP Media-Type for which to determine the
	 *        {@link TopLevelType}.
	 * 
	 * @return The determined {@link TopLevelType} or null if none was
	 *         determinable.
	 */
	public static TopLevelType fromHttpTopLevelType( String aHttpTopLevelType ) {
		for ( TopLevelType eHttpTopLevelType : values() ) {
			if ( aHttpTopLevelType.toLowerCase().startsWith( eHttpTopLevelType.getName() + Delimiter.PATH ) ) {
				return eHttpTopLevelType;
			}
			if ( aHttpTopLevelType.equalsIgnoreCase( eHttpTopLevelType.getName() ) ) {
				return eHttpTopLevelType;
			}
		}
		return null;
	}
}
