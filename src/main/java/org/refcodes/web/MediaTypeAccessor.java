// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides an accessor for a Media-Type property.
 */
public interface MediaTypeAccessor {

	/**
	 * Retrieves the Media-Type from the Media-Type property.
	 * 
	 * @return The Media-Type stored by the Media-Type property.
	 */
	MediaType getMediaType();

	/**
	 * Provides a mutator for a Media-Type property.
	 */
	public interface MediaTypeMutator {

		/**
		 * Sets the Media-Type for the Media-Type property.
		 * 
		 * @param aMediaType The Media-Type to be stored by the Media-Type
		 *        property.
		 */
		void setMediaType( MediaType aMediaType );
	}

	/**
	 * Provides a builder method for a Media-Type property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface MediaTypeBuilder<B extends MediaTypeBuilder<B>> {

		/**
		 * Sets the Media-Type for the Media-Type property.
		 * 
		 * @param aMediaType The Media-Type to be stored by the Media-Type
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMediaType( MediaType aMediaType );
	}

	/**
	 * Provides a Media-Type property.
	 */
	public interface MediaTypeProperty extends MediaTypeAccessor, MediaTypeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link MediaType}
		 * (setter) as of {@link #setMediaType(MediaType)} and returns the very
		 * same value (getter).
		 * 
		 * @param aMediaType The {@link MediaType} to set (via
		 *        {@link #setMediaType(MediaType)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default MediaType letMediaType( MediaType aMediaType ) {
			setMediaType( aMediaType );
			return aMediaType;
		}
	}
}
