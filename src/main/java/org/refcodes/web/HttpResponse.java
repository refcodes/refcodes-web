package org.refcodes.web;

/**
 * Defines a {@link HttpResponse} being the response Header-Fields and the
 * response body.
 */
public interface HttpResponse extends HeaderFieldsAccessor<ResponseHeaderFields>, HttpStatusCodeAccessor {

}
