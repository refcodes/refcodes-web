// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import java.net.MalformedURLException;
import java.net.URL;

import org.refcodes.data.Port;
import org.refcodes.data.Scheme;

/**
 * Provides an accessor for a base URL (protocol, host, port, locator) property.
 */
public interface BaseUrlAccessor {

	/**
	 * Retrieves the base URL (protocol, host, port, locator) from the base URL
	 * (protocol, host, port, locator) property.
	 * 
	 * @return The base URL (protocol, host, port, locator) stored by the base
	 *         URL (protocol, host, port, locator) property.
	 */
	Url getBaseUrl();

	/**
	 * Provides a mutator for a base URL (protocol, host, port, locator)
	 * property.
	 */
	public interface BaseUrlMutator {

		/**
		 * Sets the base {@link Url} (protocol, host, port, path) for the base
		 * URL (protocol, host, port, path) property.
		 * 
		 * @param aBaseUrl The base URL (protocol, host, port, path) to be
		 *        stored by the local address property.
		 */
		void setBaseUrl( Url aBaseUrl );

		/**
		 * Sets the base URL (protocol, host, port, path) for the base URL
		 * (protocol, host, port, path) property.
		 * 
		 * @param aBaseUrl The base URL (protocol, host, port, path) to be
		 *        stored by the local address property.
		 */
		void setBaseUrl( URL aBaseUrl );

		/**
		 * Same as {@link #setBaseUrl(URL)} except that a
		 * {@link MalformedURLException} may occur when creating the {@link URL}
		 * instance from the given {@link String}.
		 * 
		 * @param aBaseUrl The {@link String} representing the {@link URL}.
		 * 
		 * @throws MalformedURLException in case the {@link String} cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default void setBaseUrl( String aBaseUrl ) throws MalformedURLException {
			setBaseUrl( new URL( aBaseUrl ) );
		}

		/**
		 * Same as {@link #setBaseUrl(URL)} except that a
		 * {@link MalformedURLException} may occur when creating the {@link URL}
		 * instance from the given parameters.
		 * 
		 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
		 *        base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default void setBaseUrl( String aProtocol, String aHost ) throws MalformedURLException {
			int thePort = -1;
			if ( Scheme.HTTP.getName().equalsIgnoreCase( aProtocol ) ) {
				thePort = Port.HTTP.getPort();
			}
			if ( Scheme.HTTPS.getName().equalsIgnoreCase( aProtocol ) ) {
				thePort = Port.HTTPS.getPort();
			}
			setBaseUrl( new URL( aProtocol, aHost, thePort, "" ) );
		}

		/**
		 * Same as {@link #setBaseUrl(URL)} except that a
		 * {@link MalformedURLException} may occur when creating the {@link URL}
		 * instance from the given parameters.
		 * 
		 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
		 *        base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * @param aPort The port to be used when connecting to the host.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default void setBaseUrl( String aProtocol, String aHost, int aPort ) throws MalformedURLException {
			setBaseUrl( new URL( aProtocol, aHost, aPort, "" ) );
		}

		/**
		 * Same as {@link #setBaseUrl(URL)} except that a
		 * {@link MalformedURLException} may occur when creating the {@link URL}
		 * instance from the given parameters.
		 * 
		 * @param aScheme The {@link Scheme} (e.g. {@link Scheme#HTTP} or
		 *        {@link Scheme#HTTPS}) to be used for the base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default void setBaseUrl( Scheme aScheme, String aHost ) throws MalformedURLException {
			int thePort = -1;
			if ( Scheme.HTTP == aScheme ) {
				thePort = Port.HTTP.getPort();
			}
			if ( Scheme.HTTPS == aScheme ) {
				thePort = Port.HTTPS.getPort();
			}
			setBaseUrl( new URL( aScheme.getName(), aHost, thePort, "" ) );
		}

		/**
		 * Same as {@link #setBaseUrl(URL)} except that a
		 * {@link MalformedURLException} may occur when creating the {@link URL}
		 * instance from the given parameters.
		 * 
		 * @param aScheme The {@link Scheme} (e.g. {@link Scheme#HTTP} or
		 *        {@link Scheme#HTTPS}) to be used for the base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * @param aPort The port to be used when connecting to the host.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default void setBaseUrl( Scheme aScheme, String aHost, int aPort ) throws MalformedURLException {
			setBaseUrl( new URL( aScheme.getName(), aHost, aPort, "" ) );
		}

		/**
		 * Same as {@link #setBaseUrl(URL)} except that a
		 * {@link MalformedURLException} may occur when creating the {@link URL}
		 * instance from the given parameters.
		 * 
		 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
		 *        base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * @param aPath The path on the host to which the base {@link URL} is to
		 *        point to.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default void setBaseUrl( String aProtocol, String aHost, String aPath ) throws MalformedURLException {
			int thePort = -1;
			if ( Scheme.HTTP.getName().equalsIgnoreCase( aProtocol ) ) {
				thePort = Port.HTTP.getPort();
			}
			if ( Scheme.HTTPS.getName().equalsIgnoreCase( aProtocol ) ) {
				thePort = Port.HTTPS.getPort();
			}
			setBaseUrl( new URL( aProtocol, aHost, thePort, "" ) );
		}

		/**
		 * Same as {@link #setBaseUrl(URL)} except that a
		 * {@link MalformedURLException} may occur when creating the {@link URL}
		 * instance from the given parameters.
		 * 
		 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
		 *        base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * @param aPort The port to be used when connecting to the host.
		 * @param aPath The path on the host to which the base {@link URL} is to
		 *        point to.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default void setBaseUrl( String aProtocol, String aHost, int aPort, String aPath ) throws MalformedURLException {
			setBaseUrl( new URL( aProtocol, aHost, aPort, aPath ) );
		}

		/**
		 * Same as {@link #setBaseUrl(URL)} except that a
		 * {@link MalformedURLException} may occur when creating the {@link URL}
		 * instance from the given parameters.
		 * 
		 * @param aScheme The {@link Scheme} (e.g. {@link Scheme#HTTP} or
		 *        {@link Scheme#HTTPS}) to be used for the base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * @param aPath The path on the host to which the base {@link URL} is to
		 *        point to.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default void setBaseUrl( Scheme aScheme, String aHost, String aPath ) throws MalformedURLException {
			int thePort = -1;
			if ( Scheme.HTTP == aScheme ) {
				thePort = Port.HTTP.getPort();
			}
			if ( Scheme.HTTPS == aScheme ) {
				thePort = Port.HTTPS.getPort();
			}
			setBaseUrl( new URL( aScheme.getName(), aHost, thePort, "" ) );
		}

		/**
		 * Same as {@link #setBaseUrl(URL)} except that a
		 * {@link MalformedURLException} may occur when creating the {@link URL}
		 * instance from the given parameters.
		 * 
		 * @param aScheme The {@link Scheme} (e.g. {@link Scheme#HTTP} or
		 *        {@link Scheme#HTTPS}) to be used for the base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * @param aPort The port to be used when connecting to the host.
		 * @param aPath The path on the host to which the base {@link URL} is to
		 *        point to.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default void setBaseUrl( Scheme aScheme, String aHost, int aPort, String aPath ) throws MalformedURLException {
			setBaseUrl( new URL( aScheme.getName(), aHost, aPort, aPath ) );
		}
	}

	/**
	 * Provides a builder method for a base URL (protocol, host, port, path)
	 * property returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface BaseUrlBuilder<B extends BaseUrlBuilder<B>> {

		/**
		 * Sets the base {@link Url} (protocol, host, port, path) for the base
		 * URL (protocol, host, port, path) property.
		 * 
		 * @param aBaseUrl The base URL (protocol, host, port, path) to be
		 *        stored by the local address property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withBaseUrl( Url aBaseUrl );

		/**
		 * Sets the base URL (protocol, host, port, path) for the base URL
		 * (protocol, host, port, path) property.
		 * 
		 * @param aBaseURL The base URL (protocol, host, port, path) to be
		 *        stored by the local address property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withBaseUrl( URL aBaseURL );

		/**
		 * Same as {@link #withBaseUrl(URL)} except that a
		 * {@link MalformedURLException} may occur when creating the {@link URL}
		 * instance from the given {@link String}.
		 * 
		 * @param aBaseUrl The {@link String} representing the {@link URL}.
		 * 
		 * @return The builder for applying multiple build operations.
		 * 
		 * @throws MalformedURLException in case the {@link String} cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default B withBaseUrl( String aBaseUrl ) throws MalformedURLException {
			return withBaseUrl( new URL( aBaseUrl ) );
		}

		/**
		 * Same as {@link #withBaseUrl(URL)} except that a
		 * {@link MalformedURLException} may occur when creating the {@link URL}
		 * instance from the given parameters.
		 * 
		 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
		 *        base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * 
		 * @return The builder for applying multiple build operations.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default B withBaseUrl( String aProtocol, String aHost ) throws MalformedURLException {
			int thePort = -1;
			if ( Scheme.HTTP.getName().equalsIgnoreCase( aProtocol ) ) {
				thePort = Port.HTTP.getPort();
			}
			if ( Scheme.HTTPS.getName().equalsIgnoreCase( aProtocol ) ) {
				thePort = Port.HTTPS.getPort();
			}
			return withBaseUrl( new URL( aProtocol, aHost, thePort, "" ) );
		}

		/**
		 * Same as {@link #withBaseUrl(URL)} except that a
		 * {@link MalformedURLException} may occur when creating the {@link URL}
		 * instance from the given parameters.
		 * 
		 * @param aScheme The {@link Scheme} (e.g. {@link Scheme#HTTP} or
		 *        {@link Scheme#HTTPS}) to be used for the base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * 
		 * @return The builder for applying multiple build operations.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default B withBaseUrl( Scheme aScheme, String aHost ) throws MalformedURLException {
			int thePort = -1;
			if ( Scheme.HTTP == aScheme ) {
				thePort = Port.HTTP.getPort();
			}
			if ( Scheme.HTTPS == aScheme ) {
				thePort = Port.HTTPS.getPort();
			}
			return withBaseUrl( new URL( aScheme.getName(), aHost, thePort, "" ) );
		}

		/**
		 * Same as {@link #withBaseUrl(URL)} except that a
		 * {@link MalformedURLException} may occur when creating the {@link URL}
		 * instance from the given parameters.
		 * 
		 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
		 *        base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * @param aPath The path on the host to which the base {@link URL} is to
		 *        point to.
		 * 
		 * @return The builder for applying multiple build operations.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default B withBaseUrl( String aProtocol, String aHost, String aPath ) throws MalformedURLException {
			int thePort = -1;
			if ( Scheme.HTTP.getName().equalsIgnoreCase( aProtocol ) ) {
				thePort = Port.HTTP.getPort();
			}
			if ( Scheme.HTTPS.getName().equalsIgnoreCase( aProtocol ) ) {
				thePort = Port.HTTPS.getPort();
			}
			return withBaseUrl( new URL( aProtocol, aHost, thePort, aPath ) );
		}

		/**
		 * Same as {@link #withBaseUrl(URL)} except that a
		 * {@link MalformedURLException} may occur when creating the {@link URL}
		 * instance from the given parameters.
		 * 
		 * @param aScheme The {@link Scheme} (e.g. {@link Scheme#HTTP} or
		 *        {@link Scheme#HTTPS}) to be used for the base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * @param aPath The path on the host to which the base {@link URL} is to
		 *        point to.
		 * 
		 * @return The builder for applying multiple build operations.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default B withBaseUrl( Scheme aScheme, String aHost, String aPath ) throws MalformedURLException {
			int thePort = -1;
			if ( Scheme.HTTP == aScheme ) {
				thePort = Port.HTTP.getPort();
			}
			if ( Scheme.HTTPS == aScheme ) {
				thePort = Port.HTTPS.getPort();
			}
			return withBaseUrl( new URL( aScheme.getName(), aHost, thePort, aPath ) );
		}

		/**
		 * Builder method for
		 * {@link BaseUrlMutator#setBaseUrl(String, String, int)}.
		 * 
		 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
		 *        base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * @param aPort The port to be used when connecting to the host.
		 * 
		 * @return The builder for applying multiple build operations.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default B withBaseUrl( String aProtocol, String aHost, int aPort ) throws MalformedURLException {
			return withBaseUrl( new URL( aProtocol, aHost, aPort, "" ) );
		}

		/**
		 * Builder method for
		 * {@link BaseUrlMutator#setBaseUrl(Scheme, String, int)}.
		 * 
		 * @param aScheme The {@link Scheme} (e.g. {@link Scheme#HTTP} or
		 *        {@link Scheme#HTTPS}) to be used for the base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * @param aPort The port to be used when connecting to the host.
		 * 
		 * @return The builder for applying multiple build operations.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default B withBaseUrl( Scheme aScheme, String aHost, int aPort ) throws MalformedURLException {
			return withBaseUrl( new URL( aScheme.getName(), aHost, aPort, "" ) );
		}

		/**
		 * Builder method for
		 * {@link BaseUrlMutator#setBaseUrl(String, String, int, String)}.
		 * 
		 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
		 *        base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * @param aPort The port to be used when connecting to the host.
		 * @param aPath The path on the host to which the base {@link URL} is to
		 *        point to.
		 * 
		 * @return The builder for applying multiple build operations.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default B withBaseUrl( String aProtocol, String aHost, int aPort, String aPath ) throws MalformedURLException {
			return withBaseUrl( new URL( aProtocol, aHost, aPort, aPath ) );
		}

		/**
		 * Builder method for
		 * {@link BaseUrlMutator#setBaseUrl(Scheme, String, int, String)}.
		 * 
		 * @param aScheme The {@link Scheme} (e.g. {@link Scheme#HTTP} or
		 *        {@link Scheme#HTTPS}) to be used for the base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * @param aPort The port to be used when connecting to the host.
		 * @param aPath The path on the host to which the base {@link URL} is to
		 *        point to.
		 * 
		 * @return The builder for applying multiple build operations.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default B withBaseUrl( Scheme aScheme, String aHost, int aPort, String aPath ) throws MalformedURLException {
			return withBaseUrl( new URL( aScheme.getName(), aHost, aPort, aPath ) );
		}
	}

	/**
	 * Provides a base URL (protocol, host, port, path) property.
	 */
	public interface BaseUrlProperty extends BaseUrlAccessor, BaseUrlMutator {

		/**
		 * This method stores and passes through (being produced to an
		 * {@link Url}) the given arguments, which is very useful for builder
		 * APIs.
		 * 
		 * @param aScheme The {@link Scheme} (e.g. {@link Scheme#HTTP} or
		 *        {@link Scheme#HTTPS}) to be used for the base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 * 
		 * @return The produced value to be used in conclusive processing steps.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default Url letBaseUrl( Scheme aScheme, String aHost ) throws MalformedURLException {
			setBaseUrl( aScheme, aHost );
			return getBaseUrl();
		}

		/**
		 * This method stores and passes through (being produced to an
		 * {@link Url}) the given arguments, which is very useful for builder
		 * APIs.
		 * 
		 * @param aScheme The {@link Scheme} (e.g. {@link Scheme#HTTP} or
		 *        {@link Scheme#HTTPS}) to be used for the base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * @param aPort The port to be used when connecting to the host.
		 * 
		 * @return The produced value to be used in conclusive processing steps.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default Url letBaseUrl( Scheme aScheme, String aHost, int aPort ) throws MalformedURLException {
			setBaseUrl( aScheme, aHost, aPort );
			return getBaseUrl();
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs.
		 * 
		 * @param aBaseUrl The {@link String} representing the {@link URL}.
		 * 
		 * @return The value to be used in conclusive processing steps.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default String letBaseUrl( String aBaseUrl ) throws MalformedURLException {
			setBaseUrl( aBaseUrl );
			return aBaseUrl;
		}

		/**
		 * This method stores and passes through (being produced to an
		 * {@link Url}) the given arguments, which is very useful for builder
		 * APIs.
		 * 
		 * @param aScheme The {@link Scheme} (e.g. {@link Scheme#HTTP} or
		 *        {@link Scheme#HTTPS}) to be used for the base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * @param aPath The path on the host to which the base {@link URL} is to
		 *        point to.
		 * 
		 * @return The produced value to be used in conclusive processing steps.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default Url letBaseUrl( Scheme aScheme, String aHost, String aPath ) throws MalformedURLException {
			setBaseUrl( aScheme, aHost, aPath );
			return getBaseUrl();
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs.
		 * 
		 * @param aBaseUrl The base URL (protocol, host, port, path) to be
		 *        stored by the local address property.
		 * 
		 * @return The value to be used in conclusive processing steps.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default Url letBaseUrl( Url aBaseUrl ) throws MalformedURLException {
			setBaseUrl( aBaseUrl );
			return aBaseUrl;
		}

		/**
		 * This method stores and passes through the given arguments, which is
		 * very useful for builder APIs.
		 * 
		 * @param aBaseUrl The base URL (protocol, host, port, path) to be
		 *        stored by the local address property.
		 * 
		 * @return The passed value to be used in conclusive processing steps.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default URL letBaseUrl( URL aBaseUrl ) throws MalformedURLException {
			setBaseUrl( aBaseUrl );
			return aBaseUrl;
		}

		/**
		 * This method stores and passes through (being produced to an
		 * {@link URL}) the given arguments, which is very useful for builder
		 * APIs.
		 * 
		 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
		 *        base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * 
		 * @return The produced value to be used in conclusive processing steps.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default Url letBaseUrl( String aProtocol, String aHost ) throws MalformedURLException {
			setBaseUrl( aProtocol, aHost );
			return getBaseUrl();
		}

		/**
		 * This method stores and passes through (being produced to an
		 * {@link URL}) the given arguments, which is very useful for builder
		 * APIs.
		 * 
		 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
		 *        base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * @param aPort The port to be used when connecting to the host.
		 * 
		 * @return The produced value to be used in conclusive processing steps.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default Url letBaseUrl( String aProtocol, String aHost, int aPort ) throws MalformedURLException {
			setBaseUrl( aProtocol, aHost, aPort );
			return getBaseUrl();
		}

		/**
		 * This method stores and passes through (being produced to an
		 * {@link URL}) the given arguments, which is very useful for builder
		 * APIs.
		 * 
		 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
		 *        base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * @param aPath The path on the host to which the base {@link URL} is to
		 *        point to.
		 * 
		 * @return The produced value to be used in conclusive processing steps.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default Url letBaseUrl( String aProtocol, String aHost, String aPath ) throws MalformedURLException {
			setBaseUrl( aProtocol, aHost, aPath );
			return getBaseUrl();
		}

		/**
		 * This method stores and passes through (being produced to an
		 * {@link URL}) the given arguments, which is very useful for builder
		 * APIs.
		 * 
		 * @param aProtocol The protocol (e.g. HTTP or HTTPS) to be used for the
		 *        base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * @param aPort The port to be used when connecting to the host.
		 * @param aPath The path on the host to which the base {@link URL} is to
		 *        point to.
		 * 
		 * @return The produced value to be used in conclusive processing steps.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default Url letBaseUrl( String aProtocol, String aHost, int aPort, String aPath ) throws MalformedURLException {
			setBaseUrl( aProtocol, aHost, aPort, aPath );
			return getBaseUrl();
		}

		/**
		 * This method stores and passes through (being produced to an
		 * {@link URL}) the given arguments, which is very useful for builder
		 * APIs.
		 * 
		 * @param aScheme The {@link Scheme} (e.g. {@link Scheme#HTTP} or
		 *        {@link Scheme#HTTPS}) to be used for the base {@link URL}.
		 * @param aHost The host to which the base {@link URL} is to point to.
		 * @param aPort The port to be used when connecting to the host.
		 * @param aPath The path on the host to which the base {@link URL} is to
		 *        point to.
		 * 
		 * @return The produced value to be used in conclusive processing steps.
		 * 
		 * @throws MalformedURLException in case the parameters cannot be
		 *         converted to a valid (accepted) {@link URL}.
		 */
		default Url letBaseUrl( Scheme aScheme, String aHost, int aPort, String aPath ) throws MalformedURLException {
			setBaseUrl( aScheme, aHost, aPort, aPath );
			return getBaseUrl();
		}
	}
}
