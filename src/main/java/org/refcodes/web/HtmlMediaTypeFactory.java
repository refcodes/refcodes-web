package org.refcodes.web;

import java.io.InputStream;
import java.util.Collection;
import java.util.Map;

import org.refcodes.exception.MarshalException;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.struct.CanonicalMap;
import org.refcodes.struct.CanonicalMap.CanonicalMapBuilder;
import org.refcodes.struct.CanonicalMapBuilderImpl;
import org.refcodes.struct.ext.factory.HtmlCanonicalMapFactory;
import org.refcodes.struct.ext.factory.HtmlCanonicalMapFactorySingleton;

/**
 * Implements the {@link MediaTypeFactory} for Media-Type "application/YAML" (
 * {@link MediaType#APPLICATION_YAML}). CAUTION: This implementation uses field
 * resolution instead of getter/setter property resolution as of the used
 * marshaling API.
 */
public class HtmlMediaTypeFactory implements MediaTypeFactory {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final MediaType[] MEDIA_TYPES = new MediaType[] { MediaType.TEXT_HTML, MediaType.APPLICATION_XHTML_XML };

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MediaType[] getMediaTypes() {
		return MEDIA_TYPES;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <SRC> String toMarshaled( SRC aContext ) throws MarshalException {
		return toMarshaled( aContext, null );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <SRC> String toMarshaled( SRC aObject, Map<String, String> aProperties ) throws MarshalException {
		CanonicalMapBuilder theCanoniocalMap = new CanonicalMapBuilderImpl( aObject );
		if ( aObject.getClass().isArray() ) {
			theCanoniocalMap = new CanonicalMapBuilderImpl( theCanoniocalMap.retrieveTo( HtmlCanonicalMapFactory.HTML, HtmlCanonicalMapFactory.BODY, VALUE_SELECTOR ) );
			theCanoniocalMap.put( new String[] { HtmlCanonicalMapFactory.HTML, HtmlCanonicalMapFactory.BODY, TYPE_ANNOTATION }, aObject.getClass().getName() );
		}
		else if ( Collection.class.isAssignableFrom( aObject.getClass() ) ) {
			theCanoniocalMap = new CanonicalMapBuilderImpl( theCanoniocalMap.retrieveTo( HtmlCanonicalMapFactory.HTML, HtmlCanonicalMapFactory.BODY, VALUE_SELECTOR ) );
			theCanoniocalMap.put( new String[] { HtmlCanonicalMapFactory.HTML, HtmlCanonicalMapFactory.BODY, TYPE_ANNOTATION }, aObject.getClass().getName() );
		}
		else if ( Map.class.isAssignableFrom( aObject.getClass() ) && theCanoniocalMap.dirs().size() == 0 ) {
			theCanoniocalMap = new CanonicalMapBuilderImpl( theCanoniocalMap.retrieveTo( HtmlCanonicalMapFactory.HTML, HtmlCanonicalMapFactory.BODY, VALUE_SELECTOR ) );
			theCanoniocalMap.put( new String[] { HtmlCanonicalMapFactory.HTML, HtmlCanonicalMapFactory.BODY, TYPE_ANNOTATION }, aObject.getClass().getName() );
		}
		//	else if ( theCanoniocalMap.children().size() > 1 || SimpleType.isSimpleType( aObject.getClass() ) ) {
		//		String theRootElement = aProperties != null ? aProperties.get( MarshalParameter.ROOT_ELEMENT.getName() ) : null;
		//		if ( theRootElement == null || theRootElement.isEmpty() ) {
		//			theRootElement = VALUE_SELECTOR;
		//		}
		//		theCanoniocalMap = new CanonicalMapBuilderImpl( theCanoniocalMap.retrieveTo( theRootElement ) );
		//	}
		return HtmlCanonicalMapFactorySingleton.getInstance().toMarshaled( theCanoniocalMap, aProperties );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> T toUnmarshaled( String aContext, Class<T> aType ) throws UnmarshalException {
		return toUnmarshaled( aContext, aType, null );
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> T toUnmarshaled( String aHttpBody, Class<T> aType, Map<String, String> aProperties ) throws UnmarshalException {
		CanonicalMap theCanoniocalMap = HtmlCanonicalMapFactorySingleton.getInstance().toUnmarshaled( aHttpBody, aProperties );
		if ( aType.isArray() ) {
			theCanoniocalMap = theCanoniocalMap.retrieveFrom( HtmlCanonicalMapFactory.HTML, HtmlCanonicalMapFactory.BODY, VALUE_SELECTOR );
		}
		else if ( Collection.class.isAssignableFrom( aType ) ) {
			theCanoniocalMap = theCanoniocalMap.retrieveFrom( HtmlCanonicalMapFactory.HTML, HtmlCanonicalMapFactory.BODY, VALUE_SELECTOR );
		}
		else if ( Map.class.isAssignableFrom( aType ) && theCanoniocalMap.dirs().size() == 1 && theCanoniocalMap.leaves().size() == 0 && theCanoniocalMap.isDir( HtmlCanonicalMapFactory.HTML, HtmlCanonicalMapFactory.BODY, VALUE_SELECTOR ) && theCanoniocalMap.dirs( HtmlCanonicalMapFactory.HTML, HtmlCanonicalMapFactory.BODY ).size() == 1 ) {
			theCanoniocalMap = theCanoniocalMap.retrieveFrom( HtmlCanonicalMapFactory.HTML, HtmlCanonicalMapFactory.BODY, VALUE_SELECTOR );
		}
		else if ( aType.isAssignableFrom( HttpBodyMap.class ) ) {
			return (T) new HttpBodyMap( theCanoniocalMap );
		}
		else if ( ( theCanoniocalMap.hasValue( HtmlCanonicalMapFactory.HTML, HtmlCanonicalMapFactory.BODY, VALUE_SELECTOR ) && theCanoniocalMap.leaves().size() == 1 && theCanoniocalMap.dirs().size() == 0 ) || ( theCanoniocalMap.isDir( HtmlCanonicalMapFactory.HTML, HtmlCanonicalMapFactory.BODY, VALUE_SELECTOR ) && theCanoniocalMap.leaves().size() == 0 && theCanoniocalMap.dirs().size() == 1 ) ) {
			theCanoniocalMap = theCanoniocalMap.retrieveFrom( HtmlCanonicalMapFactory.HTML, HtmlCanonicalMapFactory.BODY, VALUE_SELECTOR );
		}
		//	else if ( ( theCanoniocalMap.hasValue( HtmlCanonicalMapFactory.HTML, HtmlCanonicalMapFactory.BODY, VALUE_SELECTOR ) && theCanoniocalMap.leaves().size() == 0 && theCanoniocalMap.dirs().size() == 1 ) || ( !theCanoniocalMap.isDir( HtmlCanonicalMapFactory.HTML, HtmlCanonicalMapFactory.BODY, VALUE_SELECTOR ) && theCanoniocalMap.leaves().size() == 0 && theCanoniocalMap.dirs().size() == 1 ) ) {
		//		theCanoniocalMap = theCanoniocalMap.retrieveFrom( HtmlCanonicalMapFactory.HTML, HtmlCanonicalMapFactory.BODY, VALUE_SELECTOR );
		//	}
		else {
			theCanoniocalMap = theCanoniocalMap.retrieveFrom( HtmlCanonicalMapFactory.HTML, HtmlCanonicalMapFactory.BODY );
		}
		return theCanoniocalMap.toType( aType );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> T fromMarshaled( InputStream aContentInputStream, Class<T> aType ) throws UnmarshalException {
		return fromMarshaled( aContentInputStream, aType, null );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object fromMarshaled( InputStream aContentInputStream, Map<String, String> aProperties, Class<?>... aTypes ) throws UnmarshalException {
		final CanonicalMapBuilder theBuilder = HtmlCanonicalMapFactorySingleton.getInstance().fromMarshaled( aContentInputStream, aProperties );
		for ( Class<?> eType : aTypes ) {
			try {
				return theBuilder.toType( eType );
			}
			catch ( Exception ignore ) { /* ignore */ }
		}
		throw new UnmarshalException( "Unable to unmarshal the input stream to fit into one of the provided types." );

	}
}