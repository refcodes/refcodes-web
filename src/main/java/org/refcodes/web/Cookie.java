// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.web;

import org.refcodes.struct.Property.PropertyBuilder;

/**
 * The {@link Cookie} represents a HTTP cookie. As the cookies differ between an
 * HTTP-Request and an HTTP-Response, we have the sub-interfaces
 * {@link RequestCookie} and {@link ResponseCookie}. As of "HTTP cookies
 * explained - NCZOnline": "... There is some confusion over encoding of a
 * cookie value. The commonly held belief is that cookie values must be
 * URL-encoded, but this is a fallacy even though it is the de facto
 * implementation. The original specification indicates that only three types of
 * characters must be encoded: semicolon, comma, and white space. The
 * specification indicates that URL encoding may be used but stops short of
 * requiring it. The RFC makes no mention of encoding whatsoever. Still, almost
 * all implementations perform some sort of URL encoding on cookie values. In
 * the case of name=value formats, the name and value are typically encoded
 * separately while the equals sign is left as is. ..." Therefore we use URL
 * encoding / decoding for the cookie value (regarding
 * {@link #fromHttpCookie(String)} and {@link #toHttpCookie()}) to make life
 * easier and not fall into the trap of unescaped values.
 * 
 * @see "https://www.nczonline.net/blog/2009/05/05/http-cookies-explained"
 */
public interface Cookie extends PropertyBuilder {

	/**
	 * Returns the cookie to be assigned to a cookie Header-Field.
	 * 
	 * @return The cookie value for a Header-Field.
	 */
	String toHttpCookie();

	/**
	 * Sets the cookie according to the provided HTTP cookie text via
	 * {@link #fromHttpCookie(String)}.
	 * 
	 * @param aHttpCookie The HTTP cookie text.
	 * 
	 * @return A {@link Cookie} instance as of the Builder-Pattern.
	 */
	Cookie withHttpCookie( String aHttpCookie );

	/**
	 * Sets the cookie according to the provided HTTP cookie text.
	 * 
	 * @param aHttpCookie The HTTP cookie text.
	 */
	void fromHttpCookie( String aHttpCookie );
}
