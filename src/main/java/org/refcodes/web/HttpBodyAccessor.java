// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import org.refcodes.web.ContentTypeAccessor.ContentTypeProvider;

/**
 * Provides an accessor for a HTTP body property.
 */
public interface HttpBodyAccessor {

	/**
	 * Retrieves the HTTP body from the HTTP body property.
	 * 
	 * @return The HTTP body stored by the HTTP body property.
	 */
	String getHttpBody();

	/**
	 * A provider interface provides a "toSomething(?)" method which converts a
	 * given instance into something else. The {@link ContentTypeProvider}
	 * converts an implementing instance's state into a {@link ContentType}
	 * instance.
	 * 
	 * @param <EXC> The exception type which may be thrown upon converting to
	 *        the desired type.
	 */
	public interface HttpBodyProvider<EXC extends Exception> {

		/**
		 * Converts an object representing the HTTP body to a {@link String}
		 * instance. Automatically determines the {@link ContentType} from any
		 * headers when applicable.
		 * 
		 * @return The converted {@link String} instance represented by the
		 *         according object.
		 * 
		 * @throws EXC Thrown in case providing the type as {@link String}
		 *         failed.
		 */
		String toHttpBody() throws EXC;

	}

	/**
	 * Provides a mutator for a HTTP body property.
	 */
	public interface HttpBodyMutator {

		/**
		 * Sets the HTTP body for the HTTP body property.
		 * 
		 * @param aHttpBody The HTTP body to be stored by the HTTP body
		 *        property.
		 */
		void setHttpBody( String aHttpBody );
	}

	/**
	 * Provides a builder body for a HTTP body property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface HttpBodyBuilder<B extends HttpBodyBuilder<B>> {

		/**
		 * Sets the HTTP body for the HTTP body property.
		 * 
		 * @param aHttpBody The HTTP body to be stored by the HTTP body
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withHttpBody( String aHttpBody );
	}

	/**
	 * Provides a HTTP body property.
	 */
	public interface HttpBodyProperty extends HttpBodyAccessor, HttpBodyMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setHttpBody(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aHttpBody The {@link String} to set (via
		 *        {@link #setHttpBody(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letHttpBody( String aHttpBody ) {
			setHttpBody( aHttpBody );
			return aHttpBody;
		}
	}
}
