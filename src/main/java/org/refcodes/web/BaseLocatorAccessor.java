// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides an accessor for a base locator property.
 */
public interface BaseLocatorAccessor {

	/**
	 * Retrieves the base locator from the base locator property.
	 * 
	 * @return The base locator stored by the base locator property.
	 */
	String getBaseLocator();

	/**
	 * Provides a mutator for a base locator property.
	 */
	public interface BaseLocatorMutator {

		/**
		 * Sets the base locator for the base locator property.
		 * 
		 * @param aBaseLocator The base locator to be stored by the local
		 *        address property.
		 */
		void setBaseLocator( String aBaseLocator );
	}

	/**
	 * Provides a builder method for a base locator property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface BaseLocatorBuilder<B extends BaseLocatorBuilder<B>> {

		/**
		 * Sets the base locator for the base locator property.
		 * 
		 * @param aBaseLocator The base locator to be stored by the local
		 *        address property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withBaseLocator( String aBaseLocator );
	}

	/**
	 * Provides a base locator property.
	 */
	public interface BaseLocatorProperty extends BaseLocatorAccessor, BaseLocatorMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setBaseLocator(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aBaseLocator The {@link String} to set (via
		 *        {@link #setBaseLocator(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letBaseLocator( String aBaseLocator ) {
			setBaseLocator( aBaseLocator );
			return aBaseLocator;
		}
	}
}
