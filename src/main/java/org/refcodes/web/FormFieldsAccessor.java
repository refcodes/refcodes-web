// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides an accessor for a request Form-Fields property.
 */
public interface FormFieldsAccessor {

	/**
	 * Retrieves the request Form-Fields from the request Form-Fields property.
	 * 
	 * @return The request Form-Fields stored by the request Form-Fields
	 *         property.
	 */
	FormFields getFormFields();

	/**
	 * Provides a mutator for a request Form-Fields property.
	 */
	public interface FormFieldsMutator {

		/**
		 * Sets the request Form-Fields for the request Form-Fields property.
		 * 
		 * @param aFormFields The request Form-Fields to be stored by the form
		 *        fields property.
		 */
		void setFormFields( FormFields aFormFields );
	}

	/**
	 * Provides a mutator for a request Form-Fields property.
	 * 
	 * @param <B> The builder which implements the {@link FormFieldsBuilder}.
	 */
	public interface FormFieldsBuilder<B extends FormFieldsBuilder<?>> {

		/**
		 * Sets the request Form-Fields to use and returns this builder as of
		 * the Builder-Pattern.
		 * 
		 * @param aFormFields The request Form-Fields to be stored by the form
		 *        fields property.
		 * 
		 * @return This {@link FormFieldsBuilder} instance to continue
		 *         configuration.
		 */
		B withFormFields( FormFields aFormFields );
	}

	/**
	 * Provides a request Form-Fields property.
	 */
	public interface FormFieldsProperty extends FormFieldsAccessor, FormFieldsMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link FormFields}
		 * (setter) as of {@link #setFormFields(FormFields)} and returns the
		 * very same value (getter).
		 * 
		 * @param aFormFields The {@link FormFields} to set (via
		 *        {@link #setFormFields(FormFields)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default FormFields letFormFields( FormFields aFormFields ) {
			setFormFields( aFormFields );
			return aFormFields;
		}
	}
}
