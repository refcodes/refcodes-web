package org.refcodes.web;

/**
 * Implementation of the {@link HttpResponse} interface.
 */
public abstract class AbstractHttpResponse implements HttpResponse {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ResponseHeaderFields _headerFields;
	protected HttpStatusCode _httpStatusCode;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link HttpResponse} with {@link HttpStatusCode#OK}.
	 */
	public AbstractHttpResponse() {
		this( null, new ResponseHeaderFields() );
	}

	/**
	 * Constructs a {@link HttpResponse} with all required attributes.
	 * 
	 * @param aHttpStatusCode The {@link HttpStatusCode} of the response.
	 */
	public AbstractHttpResponse( HttpStatusCode aHttpStatusCode ) {
		this( aHttpStatusCode, new ResponseHeaderFields() );
	}

	/**
	 * Constructs a {@link HttpResponse} with with {@link HttpStatusCode#OK}.
	 *
	 * @param aResponseHeaderFields the response Header-Fields
	 */
	public AbstractHttpResponse( ResponseHeaderFields aResponseHeaderFields ) {
		this( null, aResponseHeaderFields );
	}

	/**
	 * Constructs a {@link HttpResponse} with all required attributes.
	 * 
	 * @param aHttpStatusCode The {@link HttpStatusCode} of the response.
	 * @param aHeaderFields The {@link ResponseHeaderFields} sent by the
	 *        response.
	 */
	public AbstractHttpResponse( HttpStatusCode aHttpStatusCode, ResponseHeaderFields aHeaderFields ) {
		_headerFields = aHeaderFields;
		_httpStatusCode = aHttpStatusCode;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ResponseHeaderFields getHeaderFields() {
		return _headerFields;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpStatusCode getHttpStatusCode() {
		return _httpStatusCode;
	}
}
