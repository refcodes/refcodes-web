package org.refcodes.web;

/**
 * The Enum HttpMethod.
 */
public enum HttpMethod {

	GET, PUT, POST, DELETE, HEAD, PATCH, TRACE, OPTIONS, CONNECT;

	/**
	 * From http method.
	 *
	 * @param aHttpMethod the http method
	 * 
	 * @return the http method
	 */
	public static HttpMethod fromHttpMethod( String aHttpMethod ) {
		for ( HttpMethod eMethod : values() ) {
			if ( eMethod.name().equalsIgnoreCase( aHttpMethod ) ) {
				return eMethod;
			}
		}
		return null;
	}
}
