// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import java.util.ArrayList;
import java.util.List;

/**
 * The {@link ResponseHeaderFields} reflect the structure of a HTTP header and
 * may be used to represent a HTTP header.
 */
public class ResponseHeaderFields extends AbstractHeaderFields<ResponseCookie, ResponseHeaderFields> {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ResponseCookie addCookie( String aHttpCookie ) {
		return addCookie( new ResponseCookie( aHttpCookie ) );
	}

	/**
	 * Builder method for {@link #withAddCookie(ResponseCookie)}
	 *
	 * @param aHttpCookie The cookie to be added.
	 *
	 * @return The implementing instance as of the builder pattern.
	 */
	public ResponseHeaderFields witAddCookie( String aHttpCookie ) {
		addCookie( aHttpCookie );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ResponseHeaderFields withAddCookie( ResponseCookie aCookie ) {
		addCookie( aCookie );
		return this;
	}

	// -------------------------------------------------------------------------
	// LOCATION:
	// -------------------------------------------------------------------------

	/**
	 * Gets the Location Response-Header-Field {@link HeaderField#LOCATION}:
	 * "... The Location response-header field is used to redirect the recipient
	 * to a location other than the Request-URI for completion of the request or
	 * identification of a new resource. For 201 (Created) responses, the
	 * Location is that of the new resource which was created by the request.
	 * For 3xx responses, the location SHOULD indicate the server's preferred
	 * URI for automatic redirection to the resource. The field value consists
	 * of a single absolute URI. ..."
	 *
	 * @return The expected (supported) kinds of {@link MediaType} and the
	 *         according parameters (being actually instances of the
	 *         {@link String} type).
	 * 
	 * @see "https://tools.ietf.org/html/rfc2616#page-135"
	 */
	public String getLocation() {
		final List<String> theLocations = get( HeaderField.LOCATION );
		if ( theLocations != null && theLocations.size() != 0 ) {
			return theLocations.get( 0 );
		}
		return null;
	}

	/**
	 * Sets the Location Response-Header-Field {@link HeaderField#LOCATION}:
	 * "... The Location response-header field is used to redirect the recipient
	 * to a location other than the Request-URI for completion of the request or
	 * identification of a new resource. For 201 (Created) responses, the
	 * Location is that of the new resource which was created by the request.
	 * For 3xx responses, the location SHOULD indicate the server's preferred
	 * URI for automatic redirection to the resource. The field value consists
	 * of a single absolute URI. types ..."
	 *
	 * @param aLocation The according location.
	 * 
	 * @return The replaced element (if any, else null).
	 * 
	 * @see "https://tools.ietf.org/html/rfc2616#page-135"
	 */
	public String putLocation( String aLocation ) {
		if ( aLocation == null || aLocation.isEmpty() ) {
			return removeLocation();
		}
		final String theReturn = getLocation();
		final List<String> theLocations = new ArrayList<>();
		theLocations.add( aLocation );
		put( HeaderField.LOCATION, theLocations );
		return theReturn;
	}

	/**
	 * Sets the Location Response-Header-Field {@link HeaderField#LOCATION}:
	 * "... The Location response-header field is used to redirect the recipient
	 * to a location other than the Request-URI for completion of the request or
	 * identification of a new resource. For 201 (Created) responses, the
	 * Location is that of the new resource which was created by the request.
	 * For 3xx responses, the location SHOULD indicate the server's preferred
	 * URI for automatic redirection to the resource. The field value consists
	 * of a single absolute URI. types ..."
	 *
	 * @param aLocation The according location.
	 * 
	 * @return This object as of the Builder-Pattern.
	 * 
	 * @see "https://tools.ietf.org/html/rfc2616#page-135"
	 */
	public ResponseHeaderFields withLocation( String aLocation ) {
		putLocation( aLocation );
		return this;
	}

	/**
	 * Removes the Location Response-Header-Field {@link HeaderField#LOCATION}:
	 * "... The Location response-header field is used to redirect the recipient
	 * to a location other than the Request-URI for completion of the request or
	 * identification of a new resource. For 201 (Created) responses, the
	 * Location is that of the new resource which was created by the request.
	 * For 3xx responses, the location SHOULD indicate the server's preferred
	 * URI for automatic redirection to the resource. The field value consists
	 * of a single absolute URI. types ..."
	 *
	 * @return The value being removed (or null if none was set).
	 * 
	 * @see "https://tools.ietf.org/html/rfc2616#page-135"
	 */
	public String removeLocation() {
		final String theReturn = getLocation();
		remove( HeaderField.ACCEPT );
		return theReturn;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getCookieFieldName() {
		return HeaderField.SET_COOKIE.getName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ResponseCookie createCookie( String aCookieName, String aValue ) {
		return new ResponseCookie( aCookieName, aValue );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ResponseCookie createCookie( String aHttpCookie ) {
		return new ResponseCookie( aHttpCookie );
	}
}
