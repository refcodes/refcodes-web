package org.refcodes.web;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.refcodes.exception.MarshalException;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.struct.TypeUtility;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Implements the {@link MediaTypeFactory} for Media-Type "application/json" (
 * {@link MediaType#APPLICATION_JSON}). CAUTION: This implementation uses field
 * resolution instead of getter/setter property resolution as of the used
 * marshaling API.
 */
public class JsonMediaTypeFactory implements MediaTypeFactory {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final MediaType[] MEDIA_TYPES = new MediaType[] { MediaType.APPLICATION_JSON, MediaType.APPLICATION_HAL_JSON, MediaType.APPLICATION_SCHEMA_JSON };

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final ObjectMapper _mapper = new ObjectMapper();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link JsonMediaTypeFactory}.
	 */
	public JsonMediaTypeFactory() {
		_mapper.configure( DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MediaType[] getMediaTypes() {
		return MEDIA_TYPES;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toMarshaled( Object aObject ) throws MarshalException {
		// if ( aObject instanceof PathMap<?> ) {
		//	aObject = ((PathMap<?>) aObject).toDataStructure();
		// }
		try {
			return _mapper.writeValueAsString( aObject );
		}
		catch ( Exception e ) {
			throw new MarshalException( "A problem occurred marshaling an object of type <" + aObject.getClass().getName() + ">.", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> T toUnmarshaled( String aHttpBody, Class<T> aType ) throws UnmarshalException {
		try {
			if ( aHttpBody == null ) {
				return null;
			}
			else if ( aHttpBody.isEmpty() ) {
				return aType.getConstructor().newInstance();
			}
			//	if ( aType.isArray() ) {
			//		final Map<String, ?> theMap = _mapper.readValue( aHttpBody, Map.class );
			//		return TypeUtility.toArrayType( theMap, aType );
			//	}
			return _mapper.readValue( aHttpBody, aType );
		}
		catch ( JsonMappingException | JsonParseException e ) {
			if ( aType.isArray() ) {
				try {
					final Map<?, ?> theMap = _mapper.readValue( aHttpBody, Map.class );
					return TypeUtility.toArrayType( theMap, aType );
				}
				catch ( JsonProcessingException e2 ) {
					throw new UnmarshalException( "Unable to unmarshal the HTTP body \"" + aHttpBody + "\" to type  <" + aType.getName() + "> at column <" + e.getLocation().getColumnNr() + ">!", e );
				}
			}
			if ( Map.class.isAssignableFrom( aType ) ) {
				try {
					final Map<Object, Object> theMap;
					if ( Map.class.equals( aType ) ) {
						theMap = new HashMap<>();
					}
					else {
						theMap = (Map<Object, Object>) aType.getDeclaredConstructor().newInstance();
					}
					final Object[] theArray = _mapper.readValue( aHttpBody, Object[].class );
					if ( theArray != null ) {
						for ( int i = 0; i < theArray.length; i++ ) {
							if ( theArray[i] != null ) {
								theMap.put( Integer.toString( i ), theArray[i] );
							}
						}
					}
					return (T) theMap;

				}
				catch ( JsonProcessingException | IllegalAccessException | InstantiationException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e2 ) {
					throw new UnmarshalException( "Unable to unmarshal the HTTP body \"" + aHttpBody + "\" to type  <" + aType.getName() + "> at column <" + e.getLocation().getColumnNr() + ">!", e );
				}
			}
			throw new UnmarshalException( "Unable to unmarshal the HTTP body \"" + aHttpBody + "\" to type  <" + aType.getName() + "> at column <" + e.getLocation().getColumnNr() + ">!", e );
		}
		catch ( Exception e ) {
			throw new UnmarshalException( "Unable to unmarshal the HTTP body \"" + aHttpBody + "\" to type  <" + aType.getName() + ">!", e );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Removes any prefixed variable declaration.
	 * 
	 * @param aJson The JSON from which to remove any prefixed variable
	 *        declaration.
	 * 
	 * @return The JSON without any prefixed variable declaration.
	 */
	protected String toPlainJson( String aJson ) {
		return aJson.replaceFirst( "^\"[A-Za-z][A-Za-z0-9]+\"\\s*:", "" );
	}

	/**
	 * Unboxes any enveloped JSON.
	 * 
	 * @param aJson The JSON to be unboxed.
	 * 
	 * @return The unboxed JSON.
	 */
	protected String toUnboxed( String aJson ) {
		if ( aJson.startsWith( "{" ) && aJson.endsWith( "}" ) ) {
			aJson = aJson.substring( 1, aJson.length() - 1 );
			aJson = toPlainJson( aJson );
		}
		return aJson;
	}

	/**
	 * Creates an element name for the class.
	 * 
	 * @param aClass The class from which to retrieve the element name.
	 * 
	 * @return The element name for the class.
	 */
	protected String toElementName( Class<?> aClass ) {
		String theElementName = aClass.getSimpleName();
		if ( aClass.isArray() ) {
			theElementName = theElementName.substring( 0, theElementName.length() - 2 ) + "Array";
		}
		final String theFirstLetter = ( theElementName.charAt( 0 ) + "" ).toLowerCase();
		theElementName = theFirstLetter + theElementName.substring( 1 );
		return theElementName;
	}

	/**
	 * Returns the JSON specific assignment declaration for the given class.
	 * 
	 * @param aClass The class for which to construct the assignment
	 *        declaration.
	 * 
	 * @return The according assignment declaration.
	 */
	protected String toVariableDeclaration( Class<?> aClass ) {
		return "\"" + toElementName( aClass ) + "\": ";
	}
}