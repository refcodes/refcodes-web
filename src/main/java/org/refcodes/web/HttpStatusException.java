// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "HTTP://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("HTTP://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("HTTP://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("HTTP://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import org.refcodes.mixin.StatusCodeAccessor;

/**
 * Base HTTP exception, thrown in case of HTTP status code related exceptions
 * (e.g. as of a HTTP response was of an erroneous status).
 */
public class HttpStatusException extends HttpException implements StatusCodeAccessor<HttpStatusCode>, UrlAccessor {

	private static final long serialVersionUID = 1L;

	protected HttpStatusCode _httpStatusCode;
	private Url _url = null;

	/**
	 * {@inheritDoc}
	 * 
	 * @param aStatusCode The status code involved in this exception.
	 */
	public HttpStatusException( String aMessage, HttpStatusCode aStatusCode, String aErrorCode ) {
		super( aMessage, aErrorCode );
		_httpStatusCode = aStatusCode;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aStatusCode The status code involved in this exception.
	 */
	public HttpStatusException( String aMessage, HttpStatusCode aStatusCode, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
		_httpStatusCode = aStatusCode;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aStatusCode The status code involved in this exception.
	 */
	public HttpStatusException( String aMessage, HttpStatusCode aStatusCode, Throwable aCause ) {
		super( aMessage, aCause );
		_httpStatusCode = aStatusCode;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aStatusCode The status code involved in this exception.
	 */
	public HttpStatusException( String aMessage, HttpStatusCode aStatusCode ) {
		super( aMessage );
		_httpStatusCode = aStatusCode;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aStatusCode The status code involved in this exception.
	 */
	public HttpStatusException( HttpStatusCode aStatusCode, Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
		_httpStatusCode = aStatusCode;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param aStatusCode The status code involved in this exception.
	 */
	public HttpStatusException( HttpStatusCode aStatusCode, Throwable aCause ) {
		super( aCause );
		_httpStatusCode = aStatusCode;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aStatusCode The status code involved in this exception.
	 * @param aUrl The URL involved in this exception.
	 */
	public HttpStatusException( String aMessage, HttpStatusCode aStatusCode, Url aUrl, String aErrorCode ) {
		super( aMessage, aErrorCode );
		_httpStatusCode = aStatusCode;
		_url = aUrl;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aStatusCode The status code involved in this exception.
	 * @param aUrl The URL involved in this exception.
	 */
	public HttpStatusException( String aMessage, HttpStatusCode aStatusCode, Url aUrl, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
		_httpStatusCode = aStatusCode;
		_url = aUrl;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aStatusCode The status code involved in this exception.
	 * @param aUrl The URL involved in this exception.
	 */
	public HttpStatusException( String aMessage, HttpStatusCode aStatusCode, Url aUrl, Throwable aCause ) {
		super( aMessage, aCause );
		_httpStatusCode = aStatusCode;
		_url = aUrl;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aStatusCode The status code involved in this exception.
	 * @param aUrl The URL involved in this exception.
	 */
	public HttpStatusException( String aMessage, HttpStatusCode aStatusCode, Url aUrl ) {
		super( aMessage );
		_httpStatusCode = aStatusCode;
		_url = aUrl;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aStatusCode The status code involved in this exception.
	 * @param aUrl The URL involved in this exception.
	 */
	public HttpStatusException( HttpStatusCode aStatusCode, Url aUrl, Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
		_httpStatusCode = aStatusCode;
		_url = aUrl;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param aStatusCode The status code involved in this exception.
	 * @param aUrl The URL involved in this exception.
	 */
	public HttpStatusException( HttpStatusCode aStatusCode, Url aUrl, Throwable aCause ) {
		super( aCause );
		_httpStatusCode = aStatusCode;
		_url = aUrl;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpStatusCode getStatusCode() {
		return _httpStatusCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Url getUrl() {
		return _url;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] getPatternArguments() {
		return new Object[] { _httpStatusCode, _url };
	}
}
