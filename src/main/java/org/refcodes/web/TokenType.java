package org.refcodes.web;

/**
 * The Enum TokenType.
 */
public enum TokenType {

	/**
	 * Any party in possession of a bearer token (a "bearer") can use it to get
	 * access to the associated resources (without demonstrating possession of a
	 * cryptographic key). To prevent misuse, bearer tokens need to be protected
	 * from disclosure in storage and in transport. Provide this
	 * {@link TokenType} in an HTTP-Header {@link HeaderField#TOKEN_TYPE} in
	 * case you want to use the {@link #BEARER} token type. See also
	 * "https://tools.ietf.org/html/rfc6750"
	 */
	BEARER("bearer"),

	/**
	 * Use MAC Tokens in HTTP requests to access OAuth 2.0 protected resources.
	 * An OAuth client willing to access a protected resource needs to
	 * demonstrate possession of a cryptographic key by using it with a keyed
	 * aMessage digest function to the request. See also
	 * "https://tools.ietf.org/html/draft-ietf-oauth-v2-http-mac-05" Provide
	 * this {@link TokenType} in an HTTP-Header {@link HeaderField#TOKEN_TYPE}
	 * in case you want to use the {@link #MAC} token type.
	 */
	MAC("mac");

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _name;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new token type.
	 *
	 * @param aTokenType the grant type
	 */
	private TokenType( String aTokenType ) {
		_name = aTokenType;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return _name;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns that {@link TokenType} represented by the given name.
	 * 
	 * @param aName The name for which to determine the {@link TokenType}.
	 * 
	 * @return The determined {@link TokenType} or null if none was
	 *         determinable.
	 */
	public static TokenType fromName( String aName ) {
		for ( TokenType eElement : values() ) {
			if ( eElement.getName().equalsIgnoreCase( aName ) ) {
				return eElement;
			}
		}
		return null;
	}
}
