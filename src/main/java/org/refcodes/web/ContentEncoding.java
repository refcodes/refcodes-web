// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;
///////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
///////////////////////////////////////////////////////////////////////////////

import org.refcodes.mixin.NameAccessor;

/**
 * {@link ContentEncoding}s are specified an an HTTP Header-Fields
 * {@link HeaderField#ACCEPT_ENCODING} or {@link HeaderField#CONTENT_ENCODING}
 * and how to encode a HTTP body or how a HTTP body is encoded.
 * 
 * @see "https://developer.mozilla.org/de/docs/Web/HTTP/Headers/Content-Encoding"
 */
public enum ContentEncoding implements NameAccessor {

	GZIP("gzip"), COMPRESS("compress"), DEFLATE("deflate"), IDENTITY("identity"), BR("br");

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _encodingName;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new content encoding.
	 *
	 * @param aEncodingName the encoding name
	 */
	private ContentEncoding( String aEncodingName ) {
		_encodingName = aEncodingName;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	@Override
	public String getName() {
		return _encodingName;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns that {@link ContentEncoding} represented by the given HTTP
	 * header's encoding.
	 * 
	 * @param aHttpEncoding The HTTP header encoding for which to determine the
	 *        {@link ContentEncoding}.
	 * 
	 * @return The determined {@link ContentEncoding} or null if none was
	 *         determinable.
	 */
	public static ContentEncoding fromHttpEncoding( String aHttpEncoding ) {
		for ( ContentEncoding eElement : values() ) {
			if ( eElement.getName().equalsIgnoreCase( aHttpEncoding ) ) {
				return eElement;
			}
		}
		return null;
	}
}
