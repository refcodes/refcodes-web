// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * The {@link PreHttpInterceptable} provides base functionality for working with
 * {@link PreHttpInterceptor} instances.
 *
 * @param <I> The type of {@link PreHttpInterceptor} to be managed.
 */
public interface PreHttpInterceptable<I extends PreHttpInterceptor<?, ?>> {

	/**
	 * Tests whether the given {@link PreHttpInterceptor} instance has been
	 * added.
	 * 
	 * @param aPreInterceptor The {@link PreHttpInterceptor} instance for which
	 *        to test if it has been added.
	 * 
	 * @return True if the given {@link PreHttpInterceptor} instance has been
	 *         added already.
	 */
	boolean hasPreHttpInterceptor( I aPreInterceptor );

	/**
	 * Adds the given {@link PreHttpInterceptor} instance. The
	 * {@link PreHttpInterceptor} instance itself acts as the handle which is
	 * used when removing the given {@link PreHttpInterceptor} instance later.
	 * 
	 * @param aPreInterceptor The {@link PreHttpInterceptor} instance which is
	 *        to be added.
	 * 
	 * @return True if the {@link PreHttpInterceptor} instance has been added
	 *         successfully. If the {@link PreHttpInterceptor} instance has
	 *         already been added, false is returned.
	 */
	boolean addPreHttpInterceptor( I aPreInterceptor );

	/**
	 * Removes the {@link PreHttpInterceptor} instance. In case the
	 * {@link PreHttpInterceptor} instance has not been added before, then false
	 * is returned.
	 * 
	 * @param aPreInterceptor The {@link PreHttpInterceptor} instance which is
	 *        to be removed.
	 * 
	 * @return True if the {@link PreHttpInterceptor} instance has been removed
	 *         successfully. If there was none such {@link PreHttpInterceptor}
	 *         instance or if the {@link PreHttpInterceptor} instance has
	 *         already been removed, then false is returned.
	 */
	boolean removePreHttpInterceptor( I aPreInterceptor );

}