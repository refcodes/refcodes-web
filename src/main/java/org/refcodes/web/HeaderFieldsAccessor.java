// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides an accessor for an Header-Fields property.
 *
 * @param <T> the generic type
 */
public interface HeaderFieldsAccessor<T extends HeaderFields<?, ?>> {

	/**
	 * Retrieves the Header-Fields from the Header-Fields property.
	 * 
	 * @return The Header-Fields stored by the Header-Fields property.
	 */
	T getHeaderFields();

	/**
	 * Provides a mutator for an Header-Fields property.
	 *
	 * @param <T> the generic type
	 */
	public interface HeaderFieldsMutator<T extends HeaderFields<?, ?>> {

		/**
		 * Sets the Header-Fields for the Header-Fields property.
		 * 
		 * @param aHeaderFields The Header-Fields to be stored by the header
		 *        fields property.
		 */
		void setHeaderFields( T aHeaderFields );
	}

	/**
	 * Provides a mutator for an Header-Fields property.
	 *
	 * @param <T> the generic type
	 * @param <B> The builder which implements the {@link HeaderFieldsBuilder}.
	 */
	public interface HeaderFieldsBuilder<T extends HeaderFields<?, ?>, B extends HeaderFieldsBuilder<T, B>> {

		/**
		 * Sets the Header-Fields to use and returns this builder as of the
		 * Builder-Pattern.
		 * 
		 * @param aHeaderFields The Header-Fields to be stored by the header
		 *        fields property.
		 * 
		 * @return This {@link HeaderFieldsBuilder} instance to continue
		 *         configuration.
		 */
		B withHeaderFields( T aHeaderFields );
	}

	/**
	 * Provides an Header-Fields property.
	 *
	 * @param <T> the generic type
	 */
	public interface HeaderFieldsProperty<T extends HeaderFields<?, ?>> extends HeaderFieldsAccessor<T>, HeaderFieldsMutator<T> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setHeaderFields(HeaderFields)} and returns the very same
		 * value (getter).
		 * 
		 * @param aHeaderFields The value to set (via
		 *        {@link #setHeaderFields(HeaderFields)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default T letHeaderFields( T aHeaderFields ) {
			setHeaderFields( aHeaderFields );
			return aHeaderFields;
		}
	}
}
