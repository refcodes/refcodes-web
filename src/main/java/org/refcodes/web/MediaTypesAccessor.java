// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides an accessor for a Media-Types property.
 */
public interface MediaTypesAccessor {

	/**
	 * Retrieves the Media-Types from the Media-Types property.
	 * 
	 * @return The Media-Types stored by the Media-Types property.
	 */
	MediaType[] getMediaTypes();

	/**
	 * Provides a mutator for a Media-Types property.
	 */
	public interface MediaTypesMutator {

		/**
		 * Sets the Media-Types for the Media-Types property.
		 * 
		 * @param aMediaTypes The Media-Types to be stored by the Media-Types
		 *        property.
		 */
		void setMediaTypes( MediaType[] aMediaTypes );
	}

	/**
	 * Provides a builder method for a Media-Types property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface MediaTypesBuilder<B extends MediaTypesBuilder<B>> {

		/**
		 * Sets the Media-Types for the Media-Types property.
		 * 
		 * @param aMediaTypes The Media-Types to be stored by the Media-Types
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMediaTypes( MediaType[] aMediaTypes );
	}

	/**
	 * Provides a Media-Types property.
	 */
	public interface MediaTypesProperty extends MediaTypesAccessor, MediaTypesMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link MediaType} array
		 * (setter) as of {@link #setMediaTypes(MediaType[])} and returns the
		 * very same value (getter).
		 * 
		 * @param aMediaTypes The {@link MediaType} array to set (via
		 *        {@link #setMediaTypes(MediaType[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default MediaType[] letMediaTypes( MediaType[] aMediaTypes ) {
			setMediaTypes( aMediaTypes );
			return aMediaTypes;
		}
	}
}
