// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides an accessor for a HTTP Media-Type property.
 */
public interface HttpMediaTypeAccessor {

	/**
	 * Retrieves the HTTP Media-Type from the HTTP Media-Type property.
	 * 
	 * @return The HTTP Media-Type stored by the HTTP Media-Type property.
	 */
	String getHttpMediaType();

	/**
	 * A provider interface provides a "toSomething(?)" method which converts a
	 * given instance into something else. The {@link HttpMediaTypeProvider}
	 * converts something like a {@link MediaType} to a Media-Type's
	 * {@link String} representation.
	 *
	 * @param <EXC> The {@link Exception} to be thrown in case conversion
	 *        failed. Throw a {@link RuntimeException} in order to make this
	 *        method unchecked.
	 */
	public interface HttpMediaTypeProvider<EXC extends Exception> {

		/**
		 * A provider interface provides a "toSomething(?)" method which
		 * converts a given instance into something else. Returns the HTTP media
		 * type: It consists of the Media-Top-Level-Type and the Media-Subtype
		 * as well as some (optional) suffixed parameters all being concatenated
		 * in an HTTP valid manner, such as: "application/json;charset=UTF-8".
		 * 
		 * @return The HTTP header Media-Type as of the IANA specification.
		 * 
		 * @throws EXC Thrown in case providing the type as {@link String}
		 *         failed.
		 */
		String toHttpMediaType() throws EXC;

	}

	/**
	 * Provides a mutator for a HTTP Media-Type property.
	 */
	public interface HttpMediaTypeMutator {

		/**
		 * Sets the HTTP Media-Type for the HTTP Media-Type property.
		 * 
		 * @param aString The HTTP Media-Type to be stored by the HTTP
		 *        Media-Type property.
		 */
		void setHttpMediaType( String aString );
	}

	/**
	 * Provides a builder body for a HTTP Media-Type property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface HttpMediaTypeBuilder<B extends HttpMediaTypeBuilder<B>> {

		/**
		 * Sets the HTTP Media-Type for the HTTP Media-Type property.
		 * 
		 * @param aString The HTTP Media-Type to be stored by the HTTP
		 *        Media-Type property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMediaType( String aString );
	}

	/**
	 * Provides a HTTP Media-Type property.
	 */
	public interface HttpMediaTypeProperty extends HttpMediaTypeAccessor, HttpMediaTypeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setHttpMediaType(String)} and returns the very same
		 * value (getter).
		 * 
		 * @param aHttpMediaType The {@link String} to set (via
		 *        {@link #setHttpMediaType(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letHttpMediaType( String aHttpMediaType ) {
			setHttpMediaType( aHttpMediaType );
			return aHttpMediaType;
		}
	}
}
