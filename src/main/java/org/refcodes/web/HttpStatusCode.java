// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import org.refcodes.factory.ContextTypeFactory;
import org.refcodes.mixin.StatusCodeAccessor;

/**
 * The org.apache.commons.httpclient.HttpStatus constants as enumeration.
 */
public enum HttpStatusCode implements StatusCodeAccessor<Integer> {

	// /////////////////////////////////////////////////////////////////////////
	// 1XX INFORMATIONAL:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * 100 Continue (HTTP/1.1 - RFC 2616)
	 */
	CONTINUE(100),

	/**
	 * 101 Switching Protocols (HTTP/1.1 - RFC 2616)
	 */
	SWITCHING_PROTOCOLS(101),

	/**
	 * 102 Processing (WebDAV - RFC 2518).
	 */
	PROCESSING(102),

	// /////////////////////////////////////////////////////////////////////////
	// 2XX SUCCESS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * 200 OK (HTTP/1.0 - RFC 1945)
	 */
	OK(200),

	/**
	 * 201 Created (HTTP/1.0 - RFC 1945)
	 */
	CREATED(201),

	/**
	 * 202 Accepted (HTTP/1.0 - RFC 1945)
	 */
	ACCEPTED(202),

	/**
	 * 203 Non Authoritative Information (HTTP/1.1 - RFC 2616)
	 */
	NON_AUTHORITATIVE_INFORMATION(203),

	/**
	 * 204 No Content (HTTP/1.0 - RFC 1945)
	 */
	NO_CONTENT(204),

	/**
	 * 205 Reset Content (HTTP/1.1 - RFC 2616)
	 */
	RESET_CONTENT(205),

	/**
	 * 206 Partial Content (HTTP/1.1 - RFC 2616)
	 */
	PARTIAL_CONTENT(206),

	/**
	 * 207 Multi-Status (WebDAV - RFC 2518) or 207 Partial Update OK (HTTP/1.1 -
	 * draft-ietf-http-v11-spec-rev-01?)
	 */
	MULTI_STATUS(207),

	// /////////////////////////////////////////////////////////////////////////
	// 3XX REDIRECTION:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * 300 Mutliple Choices (HTTP/1.1 - RFC 2616)
	 */
	MULTIPLE_CHOICES(300, aMessage -> new MultipleChoicesException( aMessage ), aMessage -> new MultipleChoicesRuntimeException( aMessage )),

	/**
	 * 301 Moved Permanently (HTTP/1.0 - RFC 1945)
	 */
	MOVED_PERMANENTLY(301, aMessage -> new MovedPermanentlyException( aMessage ), aMessage -> new MovedPermanentlyRuntimeException( aMessage )),

	/**
	 * 302 Moved Temporarily (Sometimes Found) (HTTP/1.0 - RFC 1945)
	 */
	MOVED_TEMPORARILY(302, aMessage -> new MovedTemporarilyException( aMessage ), aMessage -> new MovedTemporarilyRuntimeException( aMessage )),

	/**
	 * 303 See Other (HTTP/1.1 - RFC 2616)
	 */
	SEE_OTHER(303, aMessage -> new SeeOtherException( aMessage ), aMessage -> new SeeOtherRuntimeException( aMessage )),

	/**
	 * 304 Not Modified (HTTP/1.0 - RFC 1945)
	 */
	NOT_MODIFIED(304, aMessage -> new NotModifiedException( aMessage ), aMessage -> new NotModifiedRuntimeException( aMessage )),

	/**
	 * 305 Use Proxy (HTTP/1.1 - RFC 2616)
	 */
	USE_PROXY(305, aMessage -> new UseProxyException( aMessage ), aMessage -> new UseProxyRuntimeException( aMessage )),

	/**
	 * 307 Temporary Redirect (HTTP/1.1 - RFC 2616)
	 */
	TEMPORARY_REDIRECT(307, aMessage -> new TemporaryRedirectException( aMessage ), aMessage -> new TemporaryRedirectRuntimeException( aMessage )),

	// /////////////////////////////////////////////////////////////////////////
	// 4XX CLIENT ERROR:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * 400 Bad Request (HTTP/1.1 - RFC 2616)
	 */
	BAD_REQUEST(400, aMessage -> new BadRequestException( aMessage ), aMessage -> new BadRequestRuntimeException( aMessage )),

	/**
	 * 401 Unauthorized (HTTP/1.0 - RFC 1945)
	 */
	UNAUTHORIZED(401, aMessage -> new UnauthorizedException( aMessage ), aMessage -> new UnauthorizedRuntimeException( aMessage )),

	/**
	 * 402 Payment Required (HTTP/1.1 - RFC 2616)
	 */
	PAYMENT_REQUIRED(402, aMessage -> new PaymentRequiredException( aMessage ), aMessage -> new PaymentRequiredRuntimeException( aMessage )),

	/**
	 * 403 Forbidden (HTTP/1.0 - RFC 1945)
	 */
	FORBIDDEN(403, aMessage -> new ForbiddenException( aMessage ), aMessage -> new ForbiddenRuntimeException( aMessage )),

	/**
	 * 404 Not Found (HTTP/1.0 - RFC 1945)
	 */
	NOT_FOUND(404, aMessage -> new NotFoundException( aMessage ), aMessage -> new NotFoundRuntimeException( aMessage )),

	/**
	 * 405 Method Not Allowed (HTTP/1.1 - RFC 2616)
	 */
	METHOD_NOT_ALLOWED(405, aMessage -> new MethodNotAllowedException( aMessage ), aMessage -> new MethodNotAllowedRuntimeException( aMessage )),

	/**
	 * 406 Not Acceptable (HTTP/1.1 - RFC 2616)
	 */
	NOT_ACCEPTABLE(406, aMessage -> new NotAcceptableException( aMessage ), aMessage -> new NotAcceptableRuntimeException( aMessage )),

	/**
	 * 407 Proxy Authentication Required (HTTP/1.1 - RFC 2616)
	 */
	PROXY_AUTHENTICATION_REQUIRED(407, aMessage -> new ProxyAuthenticationRequiredException( aMessage ), aMessage -> new ProxyAuthenticationRequiredRuntimeException( aMessage )),

	/**
	 * 408 Request Timeout (HTTP/1.1 - RFC 2616)
	 */
	REQUEST_TIMEOUT(408, aMessage -> new RequestTimeoutException( aMessage ), aMessage -> new RequestTimeoutRuntimeException( aMessage )),

	/**
	 * 409 Conflict (HTTP/1.1 - RFC 2616)
	 */
	CONFLICT(409, aMessage -> new ConflictException( aMessage ), aMessage -> new ConflictRuntimeException( aMessage )),

	/**
	 * 410 Gone (HTTP/1.1 - RFC 2616)
	 */
	GONE(410, aMessage -> new GoneException( aMessage ), aMessage -> new GoneRuntimeException( aMessage )),

	/**
	 * 411 Length Required (HTTP/1.1 - RFC 2616)
	 */
	LENGTH_REQUIRED(411, aMessage -> new LengthRequiredException( aMessage ), aMessage -> new LengthRequiredRuntimeException( aMessage )),

	/**
	 * 412 Precondition Failed (HTTP/1.1 - RFC 2616)
	 */
	PRECONDITION_FAILED(412, aMessage -> new PreconditionFailedException( aMessage ), aMessage -> new PreconditionFailedRuntimeException( aMessage )),

	/**
	 * 413 Request Entity Too Large (HTTP/1.1 - RFC 2616)
	 */
	REQUEST_TOO_LONG(413, aMessage -> new RequestTooLongException( aMessage ), aMessage -> new RequestTooLongRuntimeException( aMessage )),

	/**
	 * 414 Request-URI Too Long (HTTP/1.1 - RFC 2616)
	 */
	REQUEST_URI_TOO_LONG(414, aMessage -> new RequestUriTooLongException( aMessage ), aMessage -> new RequestUriTooLongRuntimeException( aMessage )),

	/**
	 * 415 Unsupported Media Type (HTTP/1.1 - RFC 2616)
	 */
	UNSUPPORTED_MEDIA_TYPE(415, aMessage -> new UnsupportedMediaTypeException( aMessage ), aMessage -> new UnsupportedMediaTypeRuntimeException( aMessage )),

	/**
	 * 416 Requested Range Not Satisfiable (HTTP/1.1 - RFC 2616)
	 */
	REQUESTED_RANGE_NOT_SATISFIABLE(416, aMessage -> new RequestedRangeNotSatisfiableException( aMessage ), aMessage -> new RequestedRangeNotSatisfiableRuntimeException( aMessage )),

	/**
	 * 417 Expectation Failed (HTTP/1.1 - RFC 2616)
	 */
	EXPECTATION_FAILED(417, aMessage -> new ExpectationFailedException( aMessage ), aMessage -> new ExpectationFailedRuntimeException( aMessage )),

	/**
	 * 418 Unprocessable Entity (WebDAV drafts?) or 418 Reauthentication
	 * Required (HTTP/1.1 drafts?)
	 */
	REAUTHENTICATION_REQUIRED(418, aMessage -> new ReauthenticationRequiredException( aMessage ), aMessage -> new ReauthenticationRequiredRuntimeException( aMessage )),

	/**
	 * 419 Insufficient Space on Resource (WebDAV -
	 * draft-ietf-webdav-protocol-05?) or 419 Proxy Reauthentication Required
	 * (HTTP/1.1 drafts?)
	 */
	INSUFFICIENT_SPACE_ON_RESOURCE(419, aMessage -> new InsufficientSpaceOnResourceException( aMessage ), aMessage -> new InsufficientSpaceOnResourceRuntimeException( aMessage )),

	/**
	 * 420 Method Failure (WebDAV - draft-ietf-webdav-protocol-05?).
	 */
	METHOD_FAILURE(420, aMessage -> new MethodFailureException( aMessage ), aMessage -> new MethodFailureRuntimeException( aMessage )),

	/**
	 * 422 Unprocessable Entity (WebDAV - RFC 2518).
	 */
	UNPROCESSABLE_ENTITY(422, aMessage -> new UnprocessableEntityException( aMessage ), aMessage -> new UnprocessableEntityRuntimeException( aMessage )),

	/**
	 * 423 Locked (WebDAV - RFC 2518).
	 */
	LOCKED(423, aMessage -> new LockedException( aMessage ), aMessage -> new LockedRuntimeException( aMessage )),

	/**
	 * 424 Failed Dependency (WebDAV - RFC 2518).
	 */
	FAILED_DEPENDENCY(424, aMessage -> new FailedDependencyException( aMessage ), aMessage -> new FailedDependencyRuntimeException( aMessage )),

	/**
	 * 451 Unavailable For Legal Reasons (HTTP - RFC 7725).
	 */
	UNAVAILABLE_FOR_LEGAL_REASONS(451, aMessage -> new UnavailableForLegalReasonsException( aMessage ), aMessage -> new UnavailableForLegalReasonsRuntimeException( aMessage )),

	// /////////////////////////////////////////////////////////////////////////
	// 5XX SERVER ERROR:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * 500 Server Error (HTTP/1.0 - RFC 1945)
	 */
	INTERNAL_SERVER_ERROR(500, aMessage -> new InternalServerErrorException( aMessage ), aMessage -> new InternalServerErrorRuntimeException( aMessage )),

	/**
	 * 501 Not Implemented (HTTP/1.0 - RFC 1945)
	 */
	NOT_IMPLEMENTED(501, aMessage -> new NotImplementedException( aMessage ), aMessage -> new NotImplementedRuntimeException( aMessage )),

	/**
	 * 502 Bad Gateway (HTTP/1.0 - RFC 1945)
	 */
	BAD_GATEWAY(502, aMessage -> new BadGatewayException( aMessage ), aMessage -> new BadGatewayRuntimeException( aMessage )),

	/**
	 * 503 Service Unavailable (HTTP/1.0 - RFC 1945)
	 */
	SERVICE_UNAVAILABLE(503, aMessage -> new ServiceUnavailableException( aMessage ), aMessage -> new ServiceUnavailableRuntimeException( aMessage )),

	/**
	 * 504 Gateway Timeout (HTTP/1.1 - RFC 2616)
	 */
	GATEWAY_TIMEOUT(504, aMessage -> new GatewayTimeoutException( aMessage ), aMessage -> new GatewayTimeoutRuntimeException( aMessage )),

	/**
	 * 505 HTTP Version Not Supported (HTTP/1.1 - RFC 2616)
	 */
	HTTP_VERSION_NOT_SUPPORTED(505, aMessage -> new HttpVersionNotSupportedException( aMessage ), aMessage -> new HttpVersionNotSupportedRuntimeException( aMessage )),

	/** 507 Insufficient Storage (WebDAV - RFC 2518). */
	INSUFFICIENT_STORAGE(507, aMessage -> new InsufficientStorageException( aMessage ), aMessage -> new InsufficientStorageRuntimeException( aMessage )),

	// /////////////////////////////////////////////////////////////////////////
	// 9XX LOCAL ERRORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Proprietary code and only used inside a REFCODES.ORG client not
	 * trespassing any networking border (the error is generated by the client
	 * for the client locally). This code is used is used when a client signals
	 * the business logic that internal client processing failed even though the
	 * response was received fine.
	 */
	INTERNAL_CLIENT_ERROR(900, aMessage -> new InternalClientErrorException( aMessage ), aMessage -> new InternalClientErrorRuntimeException( aMessage )),

	/**
	 * Proprietary code and only used inside a REFCODES.ORG client not
	 * trespassing any networking border (the error is generated by the client
	 * for the client locally). This code is used when a client signals a bad
	 * response being received by a server, e.g. the response cannot be
	 * processed by the client and does not meet the client's expectations.
	 */
	BAD_RESPONSE(901, aMessage -> new BadResponseException( aMessage ), aMessage -> new BadResponseRuntimeException( aMessage )),

	/**
	 * Proprietary code and only used inside a REFCODES.ORG client not
	 * trespassing any networking border (the error is generated by the client
	 * for the client locally). This code is used when a client signals a bad
	 * invocation of the client by business logic, e.g. by providing a malformed
	 * URL.
	 */
	BAD_INVOCATION(902, aMessage -> new BadInvocationException( aMessage ), aMessage -> new BadInvocationRuntimeException( aMessage )),

	// /////////////////////////////////////////////////////////////////////////
	// UNASSIGNED 100 - 999:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * 103 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_103(103),

	/**
	 * 104 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_104(104),

	/**
	 * 105 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_105(105),

	/**
	 * 106 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_106(106),

	/**
	 * 107 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_107(107),

	/**
	 * 108 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_108(108),

	/**
	 * 109 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_109(109),

	/**
	 * 110 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_110(110),

	/**
	 * 111 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_111(111),

	/**
	 * 112 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_112(112),

	/**
	 * 113 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_113(113),

	/**
	 * 114 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_114(114),

	/**
	 * 115 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_115(115),

	/**
	 * 116 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_116(116),

	/**
	 * 117 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_117(117),

	/**
	 * 118 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_118(118),

	/**
	 * 119 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_119(119),

	/**
	 * 120 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_120(120),

	/**
	 * 121 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_121(121),

	/**
	 * 122 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_122(122),

	/**
	 * 123 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_123(123),

	/**
	 * 124 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_124(124),

	/**
	 * 125 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_125(125),

	/**
	 * 126 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_126(126),

	/**
	 * 127 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_127(127),

	/**
	 * 128 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_128(128),

	/**
	 * 129 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_129(129),

	/**
	 * 130 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_130(130),

	/**
	 * 131 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_131(131),

	/**
	 * 132 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_132(132),

	/**
	 * 133 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_133(133),

	/**
	 * 134 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_134(134),

	/**
	 * 135 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_135(135),

	/**
	 * 136 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_136(136),

	/**
	 * 137 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_137(137),

	/**
	 * 138 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_138(138),

	/**
	 * 139 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_139(139),

	/**
	 * 140 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_140(140),

	/**
	 * 141 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_141(141),

	/**
	 * 142 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_142(142),

	/**
	 * 143 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_143(143),

	/**
	 * 144 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_144(144),

	/**
	 * 145 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_145(145),

	/**
	 * 146 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_146(146),

	/**
	 * 147 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_147(147),

	/**
	 * 148 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_148(148),

	/**
	 * 149 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_149(149),

	/**
	 * 150 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_150(150),

	/**
	 * 151 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_151(151),

	/**
	 * 152 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_152(152),

	/**
	 * 153 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_153(153),

	/**
	 * 154 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_154(154),

	/**
	 * 155 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_155(155),

	/**
	 * 156 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_156(156),

	/**
	 * 157 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_157(157),

	/**
	 * 158 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_158(158),

	/**
	 * 159 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_159(159),

	/**
	 * 160 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_160(160),

	/**
	 * 161 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_161(161),

	/**
	 * 162 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_162(162),

	/**
	 * 163 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_163(163),

	/**
	 * 164 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_164(164),

	/**
	 * 165 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_165(165),

	/**
	 * 166 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_166(166),

	/**
	 * 167 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_167(167),

	/**
	 * 168 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_168(168),

	/**
	 * 169 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_169(169),

	/**
	 * 170 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_170(170),

	/**
	 * 171 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_171(171),

	/**
	 * 172 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_172(172),

	/**
	 * 173 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_173(173),

	/**
	 * 174 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_174(174),

	/**
	 * 175 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_175(175),

	/**
	 * 176 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_176(176),

	/**
	 * 177 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_177(177),

	/**
	 * 178 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_178(178),

	/**
	 * 179 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_179(179),

	/**
	 * 180 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_180(180),

	/**
	 * 181 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_181(181),

	/**
	 * 182 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_182(182),

	/**
	 * 183 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_183(183),

	/**
	 * 184 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_184(184),

	/**
	 * 185 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_185(185),

	/**
	 * 186 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_186(186),

	/**
	 * 187 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_187(187),

	/**
	 * 188 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_188(188),

	/**
	 * 189 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_189(189),

	/**
	 * 190 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_190(190),

	/**
	 * 191 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_191(191),

	/**
	 * 192 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_192(192),

	/**
	 * 193 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_193(193),

	/**
	 * 194 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_194(194),

	/**
	 * 195 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_195(195),

	/**
	 * 196 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_196(196),

	/**
	 * 197 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_197(197),

	/**
	 * 198 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_198(198),

	/**
	 * 199 Unassigned informational (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_INFORMATIONAL_199(199),

	/**
	 * 208 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_208(208),

	/**
	 * 209 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_209(209),

	/**
	 * 210 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_210(210),

	/**
	 * 211 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_211(211),

	/**
	 * 212 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_212(212),

	/**
	 * 213 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_213(213),

	/**
	 * 214 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_214(214),

	/**
	 * 215 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_215(215),

	/**
	 * 216 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_216(216),

	/**
	 * 217 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_217(217),

	/**
	 * 218 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_218(218),

	/**
	 * 219 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_219(219),

	/**
	 * 220 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_220(220),

	/**
	 * 221 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_221(221),

	/**
	 * 222 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_222(222),

	/**
	 * 223 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_223(223),

	/**
	 * 224 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_224(224),

	/**
	 * 225 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_225(225),

	/**
	 * 226 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_226(226),

	/**
	 * 227 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_227(227),

	/**
	 * 228 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_228(228),

	/**
	 * 229 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_229(229),

	/**
	 * 230 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_230(230),

	/**
	 * 231 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_231(231),

	/**
	 * 232 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_232(232),

	/**
	 * 233 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_233(233),

	/**
	 * 234 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_234(234),

	/**
	 * 235 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_235(235),

	/**
	 * 236 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_236(236),

	/**
	 * 237 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_237(237),

	/**
	 * 238 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_238(238),

	/**
	 * 239 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_239(239),

	/**
	 * 240 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_240(240),

	/**
	 * 241 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_241(241),

	/**
	 * 242 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_242(242),

	/**
	 * 243 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_243(243),

	/**
	 * 244 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_244(244),

	/**
	 * 245 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_245(245),

	/**
	 * 246 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_246(246),

	/**
	 * 247 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_247(247),

	/**
	 * 248 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_248(248),

	/**
	 * 249 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_249(249),

	/**
	 * 250 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_250(250),

	/**
	 * 251 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_251(251),

	/**
	 * 252 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_252(252),

	/**
	 * 253 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_253(253),

	/**
	 * 254 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_254(254),

	/**
	 * 255 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_255(255),

	/**
	 * 256 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_256(256),

	/**
	 * 257 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_257(257),

	/**
	 * 258 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_258(258),

	/**
	 * 259 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_259(259),

	/**
	 * 260 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_260(260),

	/**
	 * 261 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_261(261),

	/**
	 * 262 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_262(262),

	/**
	 * 263 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_263(263),

	/**
	 * 264 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_264(264),

	/**
	 * 265 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_265(265),

	/**
	 * 266 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_266(266),

	/**
	 * 267 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_267(267),

	/**
	 * 268 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_268(268),

	/**
	 * 269 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_269(269),

	/**
	 * 270 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_270(270),

	/**
	 * 271 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_271(271),

	/**
	 * 272 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_272(272),

	/**
	 * 273 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_273(273),

	/**
	 * 274 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_274(274),

	/**
	 * 275 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_275(275),

	/**
	 * 276 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_276(276),

	/**
	 * 277 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_277(277),

	/**
	 * 278 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_278(278),

	/**
	 * 279 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_279(279),

	/**
	 * 280 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_280(280),

	/**
	 * 281 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_281(281),

	/**
	 * 282 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_282(282),

	/**
	 * 283 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_283(283),

	/**
	 * 284 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_284(284),

	/**
	 * 285 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_285(285),

	/**
	 * 286 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_286(286),

	/**
	 * 287 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_287(287),

	/**
	 * 288 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_288(288),

	/**
	 * 289 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_289(289),

	/**
	 * 290 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_290(290),

	/**
	 * 291 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_291(291),

	/**
	 * 292 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_292(292),

	/**
	 * 293 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_293(293),

	/**
	 * 294 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_294(294),

	/**
	 * 295 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_295(295),

	/**
	 * 296 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_296(296),

	/**
	 * 297 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_297(297),

	/**
	 * 298 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_298(298),

	/**
	 * 299 Unassigned success (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SUCCESS_299(299),

	/**
	 * 306 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_306(306, aMessage -> new UnassignedStatusCodeException( aMessage, 306 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 306 )),

	/**
	 * 308 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_308(308, aMessage -> new UnassignedStatusCodeException( aMessage, 308 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 308 )),

	/**
	 * 309 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_309(309, aMessage -> new UnassignedStatusCodeException( aMessage, 309 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 309 )),

	/**
	 * 310 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_310(310, aMessage -> new UnassignedStatusCodeException( aMessage, 310 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 310 )),

	/**
	 * 311 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_311(311, aMessage -> new UnassignedStatusCodeException( aMessage, 311 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 311 )),

	/**
	 * 312 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_312(312, aMessage -> new UnassignedStatusCodeException( aMessage, 312 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 312 )),

	/**
	 * 313 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_313(313, aMessage -> new UnassignedStatusCodeException( aMessage, 313 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 313 )),

	/**
	 * 314 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_314(314, aMessage -> new UnassignedStatusCodeException( aMessage, 314 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 314 )),

	/**
	 * 315 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_315(315, aMessage -> new UnassignedStatusCodeException( aMessage, 315 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 315 )),

	/**
	 * 316 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_316(316, aMessage -> new UnassignedStatusCodeException( aMessage, 316 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 316 )),

	/**
	 * 317 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_317(317, aMessage -> new UnassignedStatusCodeException( aMessage, 317 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 317 )),

	/**
	 * 318 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_318(318, aMessage -> new UnassignedStatusCodeException( aMessage, 318 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 318 )),

	/**
	 * 319 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_319(319, aMessage -> new UnassignedStatusCodeException( aMessage, 319 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 319 )),

	/**
	 * 320 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_320(320, aMessage -> new UnassignedStatusCodeException( aMessage, 320 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 320 )),

	/**
	 * 321 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_321(321, aMessage -> new UnassignedStatusCodeException( aMessage, 321 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 321 )),

	/**
	 * 322 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_322(322, aMessage -> new UnassignedStatusCodeException( aMessage, 322 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 322 )),

	/**
	 * 323 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_323(323, aMessage -> new UnassignedStatusCodeException( aMessage, 323 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 323 )),

	/**
	 * 324 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_324(324, aMessage -> new UnassignedStatusCodeException( aMessage, 324 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 324 )),

	/**
	 * 325 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_325(325, aMessage -> new UnassignedStatusCodeException( aMessage, 325 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 325 )),

	/**
	 * 326 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_326(326, aMessage -> new UnassignedStatusCodeException( aMessage, 326 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 326 )),

	/**
	 * 327 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_327(327, aMessage -> new UnassignedStatusCodeException( aMessage, 327 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 327 )),

	/**
	 * 328 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_328(328, aMessage -> new UnassignedStatusCodeException( aMessage, 328 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 328 )),

	/**
	 * 329 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_329(329, aMessage -> new UnassignedStatusCodeException( aMessage, 329 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 329 )),

	/**
	 * 330 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_330(330, aMessage -> new UnassignedStatusCodeException( aMessage, 330 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 330 )),

	/**
	 * 331 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_331(331, aMessage -> new UnassignedStatusCodeException( aMessage, 331 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 331 )),

	/**
	 * 332 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_332(332, aMessage -> new UnassignedStatusCodeException( aMessage, 332 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 332 )),

	/**
	 * 333 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_333(333, aMessage -> new UnassignedStatusCodeException( aMessage, 333 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 333 )),

	/**
	 * 334 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_334(334, aMessage -> new UnassignedStatusCodeException( aMessage, 334 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 334 )),

	/**
	 * 335 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_335(335, aMessage -> new UnassignedStatusCodeException( aMessage, 335 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 335 )),

	/**
	 * 336 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_336(336, aMessage -> new UnassignedStatusCodeException( aMessage, 336 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 336 )),

	/**
	 * 337 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_337(337, aMessage -> new UnassignedStatusCodeException( aMessage, 337 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 337 )),

	/**
	 * 338 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_338(338, aMessage -> new UnassignedStatusCodeException( aMessage, 338 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 338 )),

	/**
	 * 339 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_339(339, aMessage -> new UnassignedStatusCodeException( aMessage, 339 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 339 )),

	/**
	 * 340 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_340(340, aMessage -> new UnassignedStatusCodeException( aMessage, 340 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 340 )),

	/**
	 * 341 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_341(341, aMessage -> new UnassignedStatusCodeException( aMessage, 341 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 341 )),

	/**
	 * 342 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_342(342, aMessage -> new UnassignedStatusCodeException( aMessage, 342 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 342 )),

	/**
	 * 343 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_343(343, aMessage -> new UnassignedStatusCodeException( aMessage, 343 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 343 )),

	/**
	 * 344 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_344(344, aMessage -> new UnassignedStatusCodeException( aMessage, 344 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 344 )),

	/**
	 * 345 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_345(345, aMessage -> new UnassignedStatusCodeException( aMessage, 345 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 345 )),

	/**
	 * 346 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_346(346, aMessage -> new UnassignedStatusCodeException( aMessage, 346 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 346 )),

	/**
	 * 347 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_347(347, aMessage -> new UnassignedStatusCodeException( aMessage, 347 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 347 )),

	/**
	 * 348 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_348(348, aMessage -> new UnassignedStatusCodeException( aMessage, 348 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 348 )),

	/**
	 * 349 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_349(349, aMessage -> new UnassignedStatusCodeException( aMessage, 349 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 349 )),

	/**
	 * 350 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_350(350, aMessage -> new UnassignedStatusCodeException( aMessage, 350 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 350 )),

	/**
	 * 351 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_351(351, aMessage -> new UnassignedStatusCodeException( aMessage, 351 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 351 )),

	/**
	 * 352 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_352(352, aMessage -> new UnassignedStatusCodeException( aMessage, 352 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 352 )),

	/**
	 * 353 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_353(353, aMessage -> new UnassignedStatusCodeException( aMessage, 353 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 353 )),

	/**
	 * 354 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_354(354, aMessage -> new UnassignedStatusCodeException( aMessage, 354 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 354 )),

	/**
	 * 355 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_355(355, aMessage -> new UnassignedStatusCodeException( aMessage, 355 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 355 )),

	/**
	 * 356 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_356(356, aMessage -> new UnassignedStatusCodeException( aMessage, 356 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 356 )),

	/**
	 * 357 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_357(357, aMessage -> new UnassignedStatusCodeException( aMessage, 357 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 357 )),

	/**
	 * 358 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_358(358, aMessage -> new UnassignedStatusCodeException( aMessage, 358 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 358 )),

	/**
	 * 359 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_359(359, aMessage -> new UnassignedStatusCodeException( aMessage, 359 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 359 )),

	/**
	 * 360 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_360(360, aMessage -> new UnassignedStatusCodeException( aMessage, 360 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 360 )),

	/**
	 * 361 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_361(361, aMessage -> new UnassignedStatusCodeException( aMessage, 361 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 361 )),

	/**
	 * 362 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_362(362, aMessage -> new UnassignedStatusCodeException( aMessage, 362 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 362 )),

	/**
	 * 363 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_363(363, aMessage -> new UnassignedStatusCodeException( aMessage, 363 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 363 )),

	/**
	 * 364 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_364(364, aMessage -> new UnassignedStatusCodeException( aMessage, 364 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 364 )),

	/**
	 * 365 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_365(365, aMessage -> new UnassignedStatusCodeException( aMessage, 365 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 365 )),

	/**
	 * 366 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_366(366, aMessage -> new UnassignedStatusCodeException( aMessage, 366 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 366 )),

	/**
	 * 367 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_367(367, aMessage -> new UnassignedStatusCodeException( aMessage, 367 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 367 )),

	/**
	 * 368 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_368(368, aMessage -> new UnassignedStatusCodeException( aMessage, 368 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 368 )),

	/**
	 * 369 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_369(369, aMessage -> new UnassignedStatusCodeException( aMessage, 369 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 369 )),

	/**
	 * 370 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_370(370, aMessage -> new UnassignedStatusCodeException( aMessage, 370 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 370 )),

	/**
	 * 371 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_371(371, aMessage -> new UnassignedStatusCodeException( aMessage, 371 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 371 )),

	/**
	 * 372 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_372(372, aMessage -> new UnassignedStatusCodeException( aMessage, 372 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 372 )),

	/**
	 * 373 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_373(373, aMessage -> new UnassignedStatusCodeException( aMessage, 373 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 373 )),

	/**
	 * 374 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_374(374, aMessage -> new UnassignedStatusCodeException( aMessage, 374 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 374 )),

	/**
	 * 375 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_375(375, aMessage -> new UnassignedStatusCodeException( aMessage, 375 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 375 )),

	/**
	 * 376 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_376(376, aMessage -> new UnassignedStatusCodeException( aMessage, 376 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 376 )),

	/**
	 * 377 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_377(377, aMessage -> new UnassignedStatusCodeException( aMessage, 377 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 377 )),

	/**
	 * 378 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_378(378, aMessage -> new UnassignedStatusCodeException( aMessage, 378 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 378 )),

	/**
	 * 379 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_379(379, aMessage -> new UnassignedStatusCodeException( aMessage, 379 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 379 )),

	/**
	 * 380 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_380(380, aMessage -> new UnassignedStatusCodeException( aMessage, 380 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 380 )),

	/**
	 * 381 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_381(381, aMessage -> new UnassignedStatusCodeException( aMessage, 381 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 381 )),

	/**
	 * 382 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_382(382, aMessage -> new UnassignedStatusCodeException( aMessage, 382 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 382 )),

	/**
	 * 383 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_383(383, aMessage -> new UnassignedStatusCodeException( aMessage, 383 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 383 )),

	/**
	 * 384 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_384(384, aMessage -> new UnassignedStatusCodeException( aMessage, 384 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 384 )),

	/**
	 * 385 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_385(385, aMessage -> new UnassignedStatusCodeException( aMessage, 385 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 385 )),

	/**
	 * 386 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_386(386, aMessage -> new UnassignedStatusCodeException( aMessage, 386 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 386 )),

	/**
	 * 387 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_387(387, aMessage -> new UnassignedStatusCodeException( aMessage, 387 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 387 )),

	/**
	 * 388 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_388(388, aMessage -> new UnassignedStatusCodeException( aMessage, 388 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 388 )),

	/**
	 * 389 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_389(389, aMessage -> new UnassignedStatusCodeException( aMessage, 389 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 389 )),

	/**
	 * 390 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_390(390, aMessage -> new UnassignedStatusCodeException( aMessage, 390 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 390 )),

	/**
	 * 391 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_391(391, aMessage -> new UnassignedStatusCodeException( aMessage, 391 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 391 )),

	/**
	 * 392 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_392(392, aMessage -> new UnassignedStatusCodeException( aMessage, 392 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 392 )),

	/**
	 * 393 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_393(393, aMessage -> new UnassignedStatusCodeException( aMessage, 393 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 393 )),

	/**
	 * 394 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_394(394, aMessage -> new UnassignedStatusCodeException( aMessage, 394 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 394 )),

	/**
	 * 395 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_395(395, aMessage -> new UnassignedStatusCodeException( aMessage, 395 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 395 )),

	/**
	 * 396 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_396(396, aMessage -> new UnassignedStatusCodeException( aMessage, 396 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 396 )),

	/**
	 * 397 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_397(397, aMessage -> new UnassignedStatusCodeException( aMessage, 397 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 397 )),

	/**
	 * 398 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_398(398, aMessage -> new UnassignedStatusCodeException( aMessage, 398 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 398 )),

	/**
	 * 399 Unassigned redirection (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_REDIRECTION_399(399, aMessage -> new UnassignedStatusCodeException( aMessage, 399 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 399 )),

	/**
	 * 421 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_421(421, aMessage -> new UnassignedStatusCodeException( aMessage, 421 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 421 )),

	/**
	 * 425 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_425(425, aMessage -> new UnassignedStatusCodeException( aMessage, 425 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 425 )),

	/**
	 * 426 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_426(426, aMessage -> new UnassignedStatusCodeException( aMessage, 426 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 426 )),

	/**
	 * 427 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_427(427, aMessage -> new UnassignedStatusCodeException( aMessage, 427 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 427 )),

	/**
	 * 428 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_428(428, aMessage -> new UnassignedStatusCodeException( aMessage, 428 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 428 )),

	/**
	 * 429 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_429(429, aMessage -> new UnassignedStatusCodeException( aMessage, 429 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 429 )),

	/**
	 * 430 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_430(430, aMessage -> new UnassignedStatusCodeException( aMessage, 430 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 430 )),

	/**
	 * 431 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_431(431, aMessage -> new UnassignedStatusCodeException( aMessage, 431 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 431 )),

	/**
	 * 432 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_432(432, aMessage -> new UnassignedStatusCodeException( aMessage, 432 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 432 )),

	/**
	 * 433 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_433(433, aMessage -> new UnassignedStatusCodeException( aMessage, 433 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 433 )),

	/**
	 * 434 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_434(434, aMessage -> new UnassignedStatusCodeException( aMessage, 434 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 434 )),

	/**
	 * 435 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_435(435, aMessage -> new UnassignedStatusCodeException( aMessage, 435 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 435 )),

	/**
	 * 436 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_436(436, aMessage -> new UnassignedStatusCodeException( aMessage, 436 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 436 )),

	/**
	 * 437 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_437(437, aMessage -> new UnassignedStatusCodeException( aMessage, 437 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 437 )),

	/**
	 * 438 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_438(438, aMessage -> new UnassignedStatusCodeException( aMessage, 438 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 438 )),

	/**
	 * 439 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_439(439, aMessage -> new UnassignedStatusCodeException( aMessage, 439 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 439 )),

	/**
	 * 440 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_440(440, aMessage -> new UnassignedStatusCodeException( aMessage, 440 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 440 )),

	/**
	 * 441 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_441(441, aMessage -> new UnassignedStatusCodeException( aMessage, 441 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 441 )),

	/**
	 * 442 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_442(442, aMessage -> new UnassignedStatusCodeException( aMessage, 442 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 442 )),

	/**
	 * 443 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_443(443, aMessage -> new UnassignedStatusCodeException( aMessage, 443 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 443 )),

	/**
	 * 444 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_444(444, aMessage -> new UnassignedStatusCodeException( aMessage, 444 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 444 )),

	/**
	 * 445 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_445(445, aMessage -> new UnassignedStatusCodeException( aMessage, 445 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 445 )),

	/**
	 * 446 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_446(446, aMessage -> new UnassignedStatusCodeException( aMessage, 446 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 446 )),

	/**
	 * 447 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_447(447, aMessage -> new UnassignedStatusCodeException( aMessage, 447 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 447 )),

	/**
	 * 448 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_448(448, aMessage -> new UnassignedStatusCodeException( aMessage, 448 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 448 )),

	/**
	 * 449 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_449(449, aMessage -> new UnassignedStatusCodeException( aMessage, 449 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 449 )),

	/**
	 * 450 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_450(450, aMessage -> new UnassignedStatusCodeException( aMessage, 450 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 450 )),

	/**
	 * 452 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_452(452, aMessage -> new UnassignedStatusCodeException( aMessage, 452 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 452 )),

	/**
	 * 453 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_453(453, aMessage -> new UnassignedStatusCodeException( aMessage, 453 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 453 )),

	/**
	 * 454 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_454(454, aMessage -> new UnassignedStatusCodeException( aMessage, 454 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 454 )),

	/**
	 * 455 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_455(455, aMessage -> new UnassignedStatusCodeException( aMessage, 455 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 455 )),

	/**
	 * 456 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_456(456, aMessage -> new UnassignedStatusCodeException( aMessage, 456 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 456 )),

	/**
	 * 457 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_457(457, aMessage -> new UnassignedStatusCodeException( aMessage, 457 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 457 )),

	/**
	 * 458 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_458(458, aMessage -> new UnassignedStatusCodeException( aMessage, 458 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 458 )),

	/**
	 * 459 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_459(459, aMessage -> new UnassignedStatusCodeException( aMessage, 459 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 459 )),

	/**
	 * 460 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_460(460, aMessage -> new UnassignedStatusCodeException( aMessage, 460 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 460 )),

	/**
	 * 461 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_461(461, aMessage -> new UnassignedStatusCodeException( aMessage, 461 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 461 )),

	/**
	 * 462 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_462(462, aMessage -> new UnassignedStatusCodeException( aMessage, 462 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 462 )),

	/**
	 * 463 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_463(463, aMessage -> new UnassignedStatusCodeException( aMessage, 463 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 463 )),

	/**
	 * 464 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_464(464, aMessage -> new UnassignedStatusCodeException( aMessage, 464 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 464 )),

	/**
	 * 465 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_465(465, aMessage -> new UnassignedStatusCodeException( aMessage, 465 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 465 )),

	/**
	 * 466 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_466(466, aMessage -> new UnassignedStatusCodeException( aMessage, 466 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 466 )),

	/**
	 * 467 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_467(467, aMessage -> new UnassignedStatusCodeException( aMessage, 467 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 467 )),

	/**
	 * 468 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_468(468, aMessage -> new UnassignedStatusCodeException( aMessage, 468 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 468 )),

	/**
	 * 469 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_469(469, aMessage -> new UnassignedStatusCodeException( aMessage, 469 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 469 )),

	/**
	 * 470 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_470(470, aMessage -> new UnassignedStatusCodeException( aMessage, 470 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 470 )),

	/**
	 * 471 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_471(471, aMessage -> new UnassignedStatusCodeException( aMessage, 471 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 471 )),

	/**
	 * 472 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_472(472, aMessage -> new UnassignedStatusCodeException( aMessage, 472 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 472 )),

	/**
	 * 473 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_473(473, aMessage -> new UnassignedStatusCodeException( aMessage, 473 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 473 )),

	/**
	 * 474 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_474(474, aMessage -> new UnassignedStatusCodeException( aMessage, 474 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 474 )),

	/**
	 * 475 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_475(475, aMessage -> new UnassignedStatusCodeException( aMessage, 475 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 475 )),

	/**
	 * 476 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_476(476, aMessage -> new UnassignedStatusCodeException( aMessage, 476 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 476 )),

	/**
	 * 477 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_477(477, aMessage -> new UnassignedStatusCodeException( aMessage, 477 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 477 )),

	/**
	 * 478 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_478(478, aMessage -> new UnassignedStatusCodeException( aMessage, 478 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 478 )),

	/**
	 * 479 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_479(479, aMessage -> new UnassignedStatusCodeException( aMessage, 479 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 479 )),

	/**
	 * 480 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_480(480, aMessage -> new UnassignedStatusCodeException( aMessage, 480 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 480 )),

	/**
	 * 481 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_481(481, aMessage -> new UnassignedStatusCodeException( aMessage, 481 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 481 )),

	/**
	 * 482 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_482(482, aMessage -> new UnassignedStatusCodeException( aMessage, 482 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 482 )),

	/**
	 * 483 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_483(483, aMessage -> new UnassignedStatusCodeException( aMessage, 483 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 483 )),

	/**
	 * 484 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_484(484, aMessage -> new UnassignedStatusCodeException( aMessage, 484 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 484 )),

	/**
	 * 485 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_485(485, aMessage -> new UnassignedStatusCodeException( aMessage, 485 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 485 )),

	/**
	 * 486 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_486(486, aMessage -> new UnassignedStatusCodeException( aMessage, 486 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 486 )),

	/**
	 * 487 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_487(487, aMessage -> new UnassignedStatusCodeException( aMessage, 487 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 487 )),

	/**
	 * 488 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_488(488, aMessage -> new UnassignedStatusCodeException( aMessage, 488 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 488 )),

	/**
	 * 489 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_489(489, aMessage -> new UnassignedStatusCodeException( aMessage, 489 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 489 )),

	/**
	 * 490 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_490(490, aMessage -> new UnassignedStatusCodeException( aMessage, 490 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 490 )),

	/**
	 * 491 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_491(491, aMessage -> new UnassignedStatusCodeException( aMessage, 491 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 491 )),

	/**
	 * 492 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_492(492, aMessage -> new UnassignedStatusCodeException( aMessage, 492 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 492 )),

	/**
	 * 493 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_493(493, aMessage -> new UnassignedStatusCodeException( aMessage, 493 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 493 )),

	/**
	 * 494 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_494(494, aMessage -> new UnassignedStatusCodeException( aMessage, 494 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 494 )),

	/**
	 * 495 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_495(495, aMessage -> new UnassignedStatusCodeException( aMessage, 495 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 495 )),

	/**
	 * 496 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_496(496, aMessage -> new UnassignedStatusCodeException( aMessage, 496 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 496 )),

	/**
	 * 497 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_497(497, aMessage -> new UnassignedStatusCodeException( aMessage, 497 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 497 )),

	/**
	 * 498 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_498(498, aMessage -> new UnassignedStatusCodeException( aMessage, 498 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 498 )),

	/**
	 * 499 Unassigned client error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_CLIENT_ERROR_499(499, aMessage -> new UnassignedStatusCodeException( aMessage, 499 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 499 )),

	/**
	 * 506 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_506(506, aMessage -> new UnassignedStatusCodeException( aMessage, 506 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 506 )),

	/**
	 * 508 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_508(508, aMessage -> new UnassignedStatusCodeException( aMessage, 508 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 508 )),

	/**
	 * 509 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_509(509, aMessage -> new UnassignedStatusCodeException( aMessage, 509 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 509 )),

	/**
	 * 510 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_510(510, aMessage -> new UnassignedStatusCodeException( aMessage, 510 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 510 )),

	/**
	 * 511 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_511(511, aMessage -> new UnassignedStatusCodeException( aMessage, 511 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 511 )),

	/**
	 * 512 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_512(512, aMessage -> new UnassignedStatusCodeException( aMessage, 512 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 512 )),

	/**
	 * 513 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_513(513, aMessage -> new UnassignedStatusCodeException( aMessage, 513 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 513 )),

	/**
	 * 514 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_514(514, aMessage -> new UnassignedStatusCodeException( aMessage, 514 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 514 )),

	/**
	 * 515 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_515(515, aMessage -> new UnassignedStatusCodeException( aMessage, 515 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 515 )),

	/**
	 * 516 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_516(516, aMessage -> new UnassignedStatusCodeException( aMessage, 516 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 516 )),

	/**
	 * 517 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_517(517, aMessage -> new UnassignedStatusCodeException( aMessage, 517 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 517 )),

	/**
	 * 518 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_518(518, aMessage -> new UnassignedStatusCodeException( aMessage, 518 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 518 )),

	/**
	 * 519 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_519(519, aMessage -> new UnassignedStatusCodeException( aMessage, 519 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 519 )),

	/**
	 * 520 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_520(520, aMessage -> new UnassignedStatusCodeException( aMessage, 520 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 520 )),

	/**
	 * 521 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_521(521, aMessage -> new UnassignedStatusCodeException( aMessage, 521 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 521 )),

	/**
	 * 522 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_522(522, aMessage -> new UnassignedStatusCodeException( aMessage, 522 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 522 )),

	/**
	 * 523 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_523(523, aMessage -> new UnassignedStatusCodeException( aMessage, 523 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 523 )),

	/**
	 * 524 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_524(524, aMessage -> new UnassignedStatusCodeException( aMessage, 524 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 524 )),

	/**
	 * 525 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_525(525, aMessage -> new UnassignedStatusCodeException( aMessage, 525 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 525 )),

	/**
	 * 526 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_526(526, aMessage -> new UnassignedStatusCodeException( aMessage, 526 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 526 )),

	/**
	 * 527 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_527(527, aMessage -> new UnassignedStatusCodeException( aMessage, 527 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 527 )),

	/**
	 * 528 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_528(528, aMessage -> new UnassignedStatusCodeException( aMessage, 528 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 528 )),

	/**
	 * 529 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_529(529, aMessage -> new UnassignedStatusCodeException( aMessage, 529 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 529 )),

	/**
	 * 530 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_530(530, aMessage -> new UnassignedStatusCodeException( aMessage, 530 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 530 )),

	/**
	 * 531 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_531(531, aMessage -> new UnassignedStatusCodeException( aMessage, 531 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 531 )),

	/**
	 * 532 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_532(532, aMessage -> new UnassignedStatusCodeException( aMessage, 532 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 532 )),

	/**
	 * 533 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_533(533, aMessage -> new UnassignedStatusCodeException( aMessage, 533 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 533 )),

	/**
	 * 534 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_534(534, aMessage -> new UnassignedStatusCodeException( aMessage, 534 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 534 )),

	/**
	 * 535 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_535(535, aMessage -> new UnassignedStatusCodeException( aMessage, 535 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 535 )),

	/**
	 * 536 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_536(536, aMessage -> new UnassignedStatusCodeException( aMessage, 536 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 536 )),

	/**
	 * 537 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_537(537, aMessage -> new UnassignedStatusCodeException( aMessage, 537 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 537 )),

	/**
	 * 538 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_538(538, aMessage -> new UnassignedStatusCodeException( aMessage, 538 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 538 )),

	/**
	 * 539 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_539(539, aMessage -> new UnassignedStatusCodeException( aMessage, 539 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 539 )),

	/**
	 * 540 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_540(540, aMessage -> new UnassignedStatusCodeException( aMessage, 540 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 540 )),

	/**
	 * 541 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_541(541, aMessage -> new UnassignedStatusCodeException( aMessage, 541 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 541 )),

	/**
	 * 542 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_542(542, aMessage -> new UnassignedStatusCodeException( aMessage, 542 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 542 )),

	/**
	 * 543 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_543(543, aMessage -> new UnassignedStatusCodeException( aMessage, 543 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 543 )),

	/**
	 * 544 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_544(544, aMessage -> new UnassignedStatusCodeException( aMessage, 544 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 544 )),

	/**
	 * 545 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_545(545, aMessage -> new UnassignedStatusCodeException( aMessage, 545 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 545 )),

	/**
	 * 546 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_546(546, aMessage -> new UnassignedStatusCodeException( aMessage, 546 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 546 )),

	/**
	 * 547 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_547(547, aMessage -> new UnassignedStatusCodeException( aMessage, 547 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 547 )),

	/**
	 * 548 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_548(548, aMessage -> new UnassignedStatusCodeException( aMessage, 548 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 548 )),

	/**
	 * 549 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_549(549, aMessage -> new UnassignedStatusCodeException( aMessage, 549 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 549 )),

	/**
	 * 550 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_550(550, aMessage -> new UnassignedStatusCodeException( aMessage, 550 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 550 )),

	/**
	 * 551 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_551(551, aMessage -> new UnassignedStatusCodeException( aMessage, 551 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 551 )),

	/**
	 * 552 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_552(552, aMessage -> new UnassignedStatusCodeException( aMessage, 552 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 552 )),

	/**
	 * 553 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_553(553, aMessage -> new UnassignedStatusCodeException( aMessage, 553 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 553 )),

	/**
	 * 554 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_554(554, aMessage -> new UnassignedStatusCodeException( aMessage, 554 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 554 )),

	/**
	 * 555 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_555(555, aMessage -> new UnassignedStatusCodeException( aMessage, 555 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 555 )),

	/**
	 * 556 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_556(556, aMessage -> new UnassignedStatusCodeException( aMessage, 556 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 556 )),

	/**
	 * 557 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_557(557, aMessage -> new UnassignedStatusCodeException( aMessage, 557 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 557 )),

	/**
	 * 558 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_558(558, aMessage -> new UnassignedStatusCodeException( aMessage, 558 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 558 )),

	/**
	 * 559 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_559(559, aMessage -> new UnassignedStatusCodeException( aMessage, 559 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 559 )),

	/**
	 * 560 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_560(560, aMessage -> new UnassignedStatusCodeException( aMessage, 560 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 560 )),

	/**
	 * 561 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_561(561, aMessage -> new UnassignedStatusCodeException( aMessage, 561 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 561 )),

	/**
	 * 562 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_562(562, aMessage -> new UnassignedStatusCodeException( aMessage, 562 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 562 )),

	/**
	 * 563 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_563(563, aMessage -> new UnassignedStatusCodeException( aMessage, 563 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 563 )),

	/**
	 * 564 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_564(564, aMessage -> new UnassignedStatusCodeException( aMessage, 564 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 564 )),

	/**
	 * 565 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_565(565, aMessage -> new UnassignedStatusCodeException( aMessage, 565 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 565 )),

	/**
	 * 566 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_566(566, aMessage -> new UnassignedStatusCodeException( aMessage, 566 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 566 )),

	/**
	 * 567 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_567(567, aMessage -> new UnassignedStatusCodeException( aMessage, 567 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 567 )),

	/**
	 * 568 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_568(568, aMessage -> new UnassignedStatusCodeException( aMessage, 568 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 568 )),

	/**
	 * 569 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_569(569, aMessage -> new UnassignedStatusCodeException( aMessage, 569 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 569 )),

	/**
	 * 570 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_570(570, aMessage -> new UnassignedStatusCodeException( aMessage, 570 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 570 )),

	/**
	 * 571 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_571(571, aMessage -> new UnassignedStatusCodeException( aMessage, 571 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 571 )),

	/**
	 * 572 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_572(572, aMessage -> new UnassignedStatusCodeException( aMessage, 572 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 572 )),

	/**
	 * 573 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_573(573, aMessage -> new UnassignedStatusCodeException( aMessage, 573 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 573 )),

	/**
	 * 574 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_574(574, aMessage -> new UnassignedStatusCodeException( aMessage, 574 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 574 )),

	/**
	 * 575 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_575(575, aMessage -> new UnassignedStatusCodeException( aMessage, 575 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 575 )),

	/**
	 * 576 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_576(576, aMessage -> new UnassignedStatusCodeException( aMessage, 576 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 576 )),

	/**
	 * 577 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_577(577, aMessage -> new UnassignedStatusCodeException( aMessage, 577 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 577 )),

	/**
	 * 578 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_578(578, aMessage -> new UnassignedStatusCodeException( aMessage, 578 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 578 )),

	/**
	 * 579 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_579(579, aMessage -> new UnassignedStatusCodeException( aMessage, 579 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 579 )),

	/**
	 * 580 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_580(580, aMessage -> new UnassignedStatusCodeException( aMessage, 580 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 580 )),

	/**
	 * 581 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_581(581, aMessage -> new UnassignedStatusCodeException( aMessage, 581 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 581 )),

	/**
	 * 582 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_582(582, aMessage -> new UnassignedStatusCodeException( aMessage, 582 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 582 )),

	/**
	 * 583 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_583(583, aMessage -> new UnassignedStatusCodeException( aMessage, 583 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 583 )),

	/**
	 * 584 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_584(584, aMessage -> new UnassignedStatusCodeException( aMessage, 584 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 584 )),

	/**
	 * 585 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_585(585, aMessage -> new UnassignedStatusCodeException( aMessage, 585 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 585 )),

	/**
	 * 586 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_586(586, aMessage -> new UnassignedStatusCodeException( aMessage, 586 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 586 )),

	/**
	 * 587 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_587(587, aMessage -> new UnassignedStatusCodeException( aMessage, 587 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 587 )),

	/**
	 * 588 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_588(588, aMessage -> new UnassignedStatusCodeException( aMessage, 588 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 588 )),

	/**
	 * 589 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_589(589, aMessage -> new UnassignedStatusCodeException( aMessage, 589 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 589 )),

	/**
	 * 590 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_590(590, aMessage -> new UnassignedStatusCodeException( aMessage, 590 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 590 )),

	/**
	 * 591 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_591(591, aMessage -> new UnassignedStatusCodeException( aMessage, 591 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 591 )),

	/**
	 * 592 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_592(592, aMessage -> new UnassignedStatusCodeException( aMessage, 592 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 592 )),

	/**
	 * 593 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_593(593, aMessage -> new UnassignedStatusCodeException( aMessage, 593 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 593 )),

	/**
	 * 594 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_594(594, aMessage -> new UnassignedStatusCodeException( aMessage, 594 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 594 )),

	/**
	 * 595 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_595(595, aMessage -> new UnassignedStatusCodeException( aMessage, 595 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 595 )),

	/**
	 * 596 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_596(596, aMessage -> new UnassignedStatusCodeException( aMessage, 596 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 596 )),

	/**
	 * 597 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_597(597, aMessage -> new UnassignedStatusCodeException( aMessage, 597 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 597 )),

	/**
	 * 598 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_598(598, aMessage -> new UnassignedStatusCodeException( aMessage, 598 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 598 )),

	/**
	 * 599 Unassigned server error (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_SERVER_ERROR_599(599, aMessage -> new UnassignedStatusCodeException( aMessage, 599 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 599 )),

	/**
	 * 600 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_600(600, aMessage -> new UnassignedStatusCodeException( aMessage, 600 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 600 )),

	/**
	 * 601 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_601(601, aMessage -> new UnassignedStatusCodeException( aMessage, 601 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 601 )),

	/**
	 * 602 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_602(602, aMessage -> new UnassignedStatusCodeException( aMessage, 602 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 602 )),

	/**
	 * 603 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_603(603, aMessage -> new UnassignedStatusCodeException( aMessage, 603 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 603 )),

	/**
	 * 604 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_604(604, aMessage -> new UnassignedStatusCodeException( aMessage, 604 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 604 )),

	/**
	 * 605 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_605(605, aMessage -> new UnassignedStatusCodeException( aMessage, 605 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 605 )),

	/**
	 * 606 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_606(606, aMessage -> new UnassignedStatusCodeException( aMessage, 606 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 606 )),

	/**
	 * 607 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_607(607, aMessage -> new UnassignedStatusCodeException( aMessage, 607 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 607 )),

	/**
	 * 608 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_608(608, aMessage -> new UnassignedStatusCodeException( aMessage, 608 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 608 )),

	/**
	 * 609 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_609(609, aMessage -> new UnassignedStatusCodeException( aMessage, 609 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 609 )),

	/**
	 * 610 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_610(610, aMessage -> new UnassignedStatusCodeException( aMessage, 610 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 610 )),

	/**
	 * 611 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_611(611, aMessage -> new UnassignedStatusCodeException( aMessage, 611 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 611 )),

	/**
	 * 612 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_612(612, aMessage -> new UnassignedStatusCodeException( aMessage, 612 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 612 )),

	/**
	 * 613 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_613(613, aMessage -> new UnassignedStatusCodeException( aMessage, 613 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 613 )),

	/**
	 * 614 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_614(614, aMessage -> new UnassignedStatusCodeException( aMessage, 614 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 614 )),

	/**
	 * 615 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_615(615, aMessage -> new UnassignedStatusCodeException( aMessage, 615 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 615 )),

	/**
	 * 616 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_616(616, aMessage -> new UnassignedStatusCodeException( aMessage, 616 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 616 )),

	/**
	 * 617 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_617(617, aMessage -> new UnassignedStatusCodeException( aMessage, 617 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 617 )),

	/**
	 * 618 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_618(618, aMessage -> new UnassignedStatusCodeException( aMessage, 618 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 618 )),

	/**
	 * 619 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_619(619, aMessage -> new UnassignedStatusCodeException( aMessage, 619 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 619 )),

	/**
	 * 620 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_620(620, aMessage -> new UnassignedStatusCodeException( aMessage, 620 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 620 )),

	/**
	 * 621 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_621(621, aMessage -> new UnassignedStatusCodeException( aMessage, 621 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 621 )),

	/**
	 * 622 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_622(622, aMessage -> new UnassignedStatusCodeException( aMessage, 622 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 622 )),

	/**
	 * 623 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_623(623, aMessage -> new UnassignedStatusCodeException( aMessage, 623 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 623 )),

	/**
	 * 624 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_624(624, aMessage -> new UnassignedStatusCodeException( aMessage, 624 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 624 )),

	/**
	 * 625 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_625(625, aMessage -> new UnassignedStatusCodeException( aMessage, 625 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 625 )),

	/**
	 * 626 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_626(626, aMessage -> new UnassignedStatusCodeException( aMessage, 626 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 626 )),

	/**
	 * 627 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_627(627, aMessage -> new UnassignedStatusCodeException( aMessage, 627 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 627 )),

	/**
	 * 628 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_628(628, aMessage -> new UnassignedStatusCodeException( aMessage, 628 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 628 )),

	/**
	 * 629 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_629(629, aMessage -> new UnassignedStatusCodeException( aMessage, 629 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 629 )),

	/**
	 * 630 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_630(630, aMessage -> new UnassignedStatusCodeException( aMessage, 630 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 630 )),

	/**
	 * 631 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_631(631, aMessage -> new UnassignedStatusCodeException( aMessage, 631 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 631 )),

	/**
	 * 632 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_632(632, aMessage -> new UnassignedStatusCodeException( aMessage, 632 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 632 )),

	/**
	 * 633 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_633(633, aMessage -> new UnassignedStatusCodeException( aMessage, 633 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 633 )),

	/**
	 * 634 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_634(634, aMessage -> new UnassignedStatusCodeException( aMessage, 634 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 634 )),

	/**
	 * 635 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_635(635, aMessage -> new UnassignedStatusCodeException( aMessage, 635 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 635 )),

	/**
	 * 636 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_636(636, aMessage -> new UnassignedStatusCodeException( aMessage, 636 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 636 )),

	/**
	 * 637 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_637(637, aMessage -> new UnassignedStatusCodeException( aMessage, 637 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 637 )),

	/**
	 * 638 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_638(638, aMessage -> new UnassignedStatusCodeException( aMessage, 638 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 638 )),

	/**
	 * 639 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_639(639, aMessage -> new UnassignedStatusCodeException( aMessage, 639 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 639 )),

	/**
	 * 640 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_640(640, aMessage -> new UnassignedStatusCodeException( aMessage, 640 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 640 )),

	/**
	 * 641 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_641(641, aMessage -> new UnassignedStatusCodeException( aMessage, 641 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 641 )),

	/**
	 * 642 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_642(642, aMessage -> new UnassignedStatusCodeException( aMessage, 642 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 642 )),

	/**
	 * 643 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_643(643, aMessage -> new UnassignedStatusCodeException( aMessage, 643 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 643 )),

	/**
	 * 644 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_644(644, aMessage -> new UnassignedStatusCodeException( aMessage, 644 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 644 )),

	/**
	 * 645 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_645(645, aMessage -> new UnassignedStatusCodeException( aMessage, 645 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 645 )),

	/**
	 * 646 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_646(646, aMessage -> new UnassignedStatusCodeException( aMessage, 646 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 646 )),

	/**
	 * 647 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_647(647, aMessage -> new UnassignedStatusCodeException( aMessage, 647 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 647 )),

	/**
	 * 648 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_648(648, aMessage -> new UnassignedStatusCodeException( aMessage, 648 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 648 )),

	/**
	 * 649 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_649(649, aMessage -> new UnassignedStatusCodeException( aMessage, 649 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 649 )),

	/**
	 * 650 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_650(650, aMessage -> new UnassignedStatusCodeException( aMessage, 650 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 650 )),

	/**
	 * 651 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_651(651, aMessage -> new UnassignedStatusCodeException( aMessage, 651 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 651 )),

	/**
	 * 652 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_652(652, aMessage -> new UnassignedStatusCodeException( aMessage, 652 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 652 )),

	/**
	 * 653 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_653(653, aMessage -> new UnassignedStatusCodeException( aMessage, 653 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 653 )),

	/**
	 * 654 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_654(654, aMessage -> new UnassignedStatusCodeException( aMessage, 654 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 654 )),

	/**
	 * 655 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_655(655, aMessage -> new UnassignedStatusCodeException( aMessage, 655 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 655 )),

	/**
	 * 656 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_656(656, aMessage -> new UnassignedStatusCodeException( aMessage, 656 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 656 )),

	/**
	 * 657 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_657(657, aMessage -> new UnassignedStatusCodeException( aMessage, 657 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 657 )),

	/**
	 * 658 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_658(658, aMessage -> new UnassignedStatusCodeException( aMessage, 658 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 658 )),

	/**
	 * 659 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_659(659, aMessage -> new UnassignedStatusCodeException( aMessage, 659 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 659 )),

	/**
	 * 660 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_660(660, aMessage -> new UnassignedStatusCodeException( aMessage, 660 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 660 )),

	/**
	 * 661 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_661(661, aMessage -> new UnassignedStatusCodeException( aMessage, 661 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 661 )),

	/**
	 * 662 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_662(662, aMessage -> new UnassignedStatusCodeException( aMessage, 662 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 662 )),

	/**
	 * 663 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_663(663, aMessage -> new UnassignedStatusCodeException( aMessage, 663 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 663 )),

	/**
	 * 664 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_664(664, aMessage -> new UnassignedStatusCodeException( aMessage, 664 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 664 )),

	/**
	 * 665 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_665(665, aMessage -> new UnassignedStatusCodeException( aMessage, 665 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 665 )),

	/**
	 * 666 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_666(666, aMessage -> new UnassignedStatusCodeException( aMessage, 666 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 666 )),

	/**
	 * 667 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_667(667, aMessage -> new UnassignedStatusCodeException( aMessage, 667 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 667 )),

	/**
	 * 668 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_668(668, aMessage -> new UnassignedStatusCodeException( aMessage, 668 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 668 )),

	/**
	 * 669 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_669(669, aMessage -> new UnassignedStatusCodeException( aMessage, 669 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 669 )),

	/**
	 * 670 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_670(670, aMessage -> new UnassignedStatusCodeException( aMessage, 670 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 670 )),

	/**
	 * 671 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_671(671, aMessage -> new UnassignedStatusCodeException( aMessage, 671 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 671 )),

	/**
	 * 672 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_672(672, aMessage -> new UnassignedStatusCodeException( aMessage, 672 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 672 )),

	/**
	 * 673 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_673(673, aMessage -> new UnassignedStatusCodeException( aMessage, 673 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 673 )),

	/**
	 * 674 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_674(674, aMessage -> new UnassignedStatusCodeException( aMessage, 674 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 674 )),

	/**
	 * 675 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_675(675, aMessage -> new UnassignedStatusCodeException( aMessage, 675 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 675 )),

	/**
	 * 676 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_676(676, aMessage -> new UnassignedStatusCodeException( aMessage, 676 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 676 )),

	/**
	 * 677 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_677(677, aMessage -> new UnassignedStatusCodeException( aMessage, 677 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 677 )),

	/**
	 * 678 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_678(678, aMessage -> new UnassignedStatusCodeException( aMessage, 678 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 678 )),

	/**
	 * 679 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_679(679, aMessage -> new UnassignedStatusCodeException( aMessage, 679 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 679 )),

	/**
	 * 680 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_680(680, aMessage -> new UnassignedStatusCodeException( aMessage, 680 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 680 )),

	/**
	 * 681 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_681(681, aMessage -> new UnassignedStatusCodeException( aMessage, 681 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 681 )),

	/**
	 * 682 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_682(682, aMessage -> new UnassignedStatusCodeException( aMessage, 682 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 682 )),

	/**
	 * 683 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_683(683, aMessage -> new UnassignedStatusCodeException( aMessage, 683 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 683 )),

	/**
	 * 684 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_684(684, aMessage -> new UnassignedStatusCodeException( aMessage, 684 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 684 )),

	/**
	 * 685 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_685(685, aMessage -> new UnassignedStatusCodeException( aMessage, 685 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 685 )),

	/**
	 * 686 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_686(686, aMessage -> new UnassignedStatusCodeException( aMessage, 686 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 686 )),

	/**
	 * 687 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_687(687, aMessage -> new UnassignedStatusCodeException( aMessage, 687 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 687 )),

	/**
	 * 688 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_688(688, aMessage -> new UnassignedStatusCodeException( aMessage, 688 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 688 )),

	/**
	 * 689 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_689(689, aMessage -> new UnassignedStatusCodeException( aMessage, 689 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 689 )),

	/**
	 * 690 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_690(690, aMessage -> new UnassignedStatusCodeException( aMessage, 690 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 690 )),

	/**
	 * 691 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_691(691, aMessage -> new UnassignedStatusCodeException( aMessage, 691 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 691 )),

	/**
	 * 692 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_692(692, aMessage -> new UnassignedStatusCodeException( aMessage, 692 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 692 )),

	/**
	 * 693 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_693(693, aMessage -> new UnassignedStatusCodeException( aMessage, 693 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 693 )),

	/**
	 * 694 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_694(694, aMessage -> new UnassignedStatusCodeException( aMessage, 694 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 694 )),

	/**
	 * 695 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_695(695, aMessage -> new UnassignedStatusCodeException( aMessage, 695 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 695 )),

	/**
	 * 696 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_696(696, aMessage -> new UnassignedStatusCodeException( aMessage, 696 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 696 )),

	/**
	 * 697 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_697(697, aMessage -> new UnassignedStatusCodeException( aMessage, 697 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 697 )),

	/**
	 * 698 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_698(698, aMessage -> new UnassignedStatusCodeException( aMessage, 698 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 698 )),

	/**
	 * 699 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_699(699, aMessage -> new UnassignedStatusCodeException( aMessage, 699 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 699 )),

	/**
	 * 700 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_700(700, aMessage -> new UnassignedStatusCodeException( aMessage, 700 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 700 )),

	/**
	 * 701 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_701(701, aMessage -> new UnassignedStatusCodeException( aMessage, 701 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 701 )),

	/**
	 * 702 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_702(702, aMessage -> new UnassignedStatusCodeException( aMessage, 702 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 702 )),

	/**
	 * 703 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_703(703, aMessage -> new UnassignedStatusCodeException( aMessage, 703 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 703 )),

	/**
	 * 704 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_704(704, aMessage -> new UnassignedStatusCodeException( aMessage, 704 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 704 )),

	/**
	 * 705 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_705(705, aMessage -> new UnassignedStatusCodeException( aMessage, 705 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 705 )),

	/**
	 * 706 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_706(706, aMessage -> new UnassignedStatusCodeException( aMessage, 706 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 706 )),

	/**
	 * 707 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_707(707, aMessage -> new UnassignedStatusCodeException( aMessage, 707 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 707 )),

	/**
	 * 708 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_708(708, aMessage -> new UnassignedStatusCodeException( aMessage, 708 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 708 )),

	/**
	 * 709 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_709(709, aMessage -> new UnassignedStatusCodeException( aMessage, 709 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 709 )),

	/**
	 * 710 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_710(710, aMessage -> new UnassignedStatusCodeException( aMessage, 710 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 710 )),

	/**
	 * 711 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_711(711, aMessage -> new UnassignedStatusCodeException( aMessage, 711 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 711 )),

	/**
	 * 712 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_712(712, aMessage -> new UnassignedStatusCodeException( aMessage, 712 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 712 )),

	/**
	 * 713 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_713(713, aMessage -> new UnassignedStatusCodeException( aMessage, 713 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 713 )),

	/**
	 * 714 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_714(714, aMessage -> new UnassignedStatusCodeException( aMessage, 714 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 714 )),

	/**
	 * 715 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_715(715, aMessage -> new UnassignedStatusCodeException( aMessage, 715 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 715 )),

	/**
	 * 716 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_716(716, aMessage -> new UnassignedStatusCodeException( aMessage, 716 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 716 )),

	/**
	 * 717 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_717(717, aMessage -> new UnassignedStatusCodeException( aMessage, 717 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 717 )),

	/**
	 * 718 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_718(718, aMessage -> new UnassignedStatusCodeException( aMessage, 718 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 718 )),

	/**
	 * 719 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_719(719, aMessage -> new UnassignedStatusCodeException( aMessage, 719 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 719 )),

	/**
	 * 720 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_720(720, aMessage -> new UnassignedStatusCodeException( aMessage, 720 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 720 )),

	/**
	 * 721 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_721(721, aMessage -> new UnassignedStatusCodeException( aMessage, 721 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 721 )),

	/**
	 * 722 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_722(722, aMessage -> new UnassignedStatusCodeException( aMessage, 722 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 722 )),

	/**
	 * 723 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_723(723, aMessage -> new UnassignedStatusCodeException( aMessage, 723 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 723 )),

	/**
	 * 724 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_724(724, aMessage -> new UnassignedStatusCodeException( aMessage, 724 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 724 )),

	/**
	 * 725 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_725(725, aMessage -> new UnassignedStatusCodeException( aMessage, 725 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 725 )),

	/**
	 * 726 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_726(726, aMessage -> new UnassignedStatusCodeException( aMessage, 726 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 726 )),

	/**
	 * 727 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_727(727, aMessage -> new UnassignedStatusCodeException( aMessage, 727 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 727 )),

	/**
	 * 728 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_728(728, aMessage -> new UnassignedStatusCodeException( aMessage, 728 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 728 )),

	/**
	 * 729 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_729(729, aMessage -> new UnassignedStatusCodeException( aMessage, 729 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 729 )),

	/**
	 * 730 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_730(730, aMessage -> new UnassignedStatusCodeException( aMessage, 730 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 730 )),

	/**
	 * 731 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_731(731, aMessage -> new UnassignedStatusCodeException( aMessage, 731 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 731 )),

	/**
	 * 732 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_732(732, aMessage -> new UnassignedStatusCodeException( aMessage, 732 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 732 )),

	/**
	 * 733 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_733(733, aMessage -> new UnassignedStatusCodeException( aMessage, 733 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 733 )),

	/**
	 * 734 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_734(734, aMessage -> new UnassignedStatusCodeException( aMessage, 734 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 734 )),

	/**
	 * 735 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_735(735, aMessage -> new UnassignedStatusCodeException( aMessage, 735 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 735 )),

	/**
	 * 736 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_736(736, aMessage -> new UnassignedStatusCodeException( aMessage, 736 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 736 )),

	/**
	 * 737 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_737(737, aMessage -> new UnassignedStatusCodeException( aMessage, 737 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 737 )),

	/**
	 * 738 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_738(738, aMessage -> new UnassignedStatusCodeException( aMessage, 738 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 738 )),

	/**
	 * 739 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_739(739, aMessage -> new UnassignedStatusCodeException( aMessage, 739 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 739 )),

	/**
	 * 740 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_740(740, aMessage -> new UnassignedStatusCodeException( aMessage, 740 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 740 )),

	/**
	 * 741 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_741(741, aMessage -> new UnassignedStatusCodeException( aMessage, 741 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 741 )),

	/**
	 * 742 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_742(742, aMessage -> new UnassignedStatusCodeException( aMessage, 742 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 742 )),

	/**
	 * 743 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_743(743, aMessage -> new UnassignedStatusCodeException( aMessage, 743 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 743 )),

	/**
	 * 744 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_744(744, aMessage -> new UnassignedStatusCodeException( aMessage, 744 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 744 )),

	/**
	 * 745 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_745(745, aMessage -> new UnassignedStatusCodeException( aMessage, 745 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 745 )),

	/**
	 * 746 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_746(746, aMessage -> new UnassignedStatusCodeException( aMessage, 746 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 746 )),

	/**
	 * 747 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_747(747, aMessage -> new UnassignedStatusCodeException( aMessage, 747 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 747 )),

	/**
	 * 748 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_748(748, aMessage -> new UnassignedStatusCodeException( aMessage, 748 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 748 )),

	/**
	 * 749 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_749(749, aMessage -> new UnassignedStatusCodeException( aMessage, 749 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 749 )),

	/**
	 * 750 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_750(750, aMessage -> new UnassignedStatusCodeException( aMessage, 750 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 750 )),

	/**
	 * 751 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_751(751, aMessage -> new UnassignedStatusCodeException( aMessage, 751 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 751 )),

	/**
	 * 752 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_752(752, aMessage -> new UnassignedStatusCodeException( aMessage, 752 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 752 )),

	/**
	 * 753 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_753(753, aMessage -> new UnassignedStatusCodeException( aMessage, 753 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 753 )),

	/**
	 * 754 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_754(754, aMessage -> new UnassignedStatusCodeException( aMessage, 754 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 754 )),

	/**
	 * 755 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_755(755, aMessage -> new UnassignedStatusCodeException( aMessage, 755 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 755 )),

	/**
	 * 756 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_756(756, aMessage -> new UnassignedStatusCodeException( aMessage, 756 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 756 )),

	/**
	 * 757 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_757(757, aMessage -> new UnassignedStatusCodeException( aMessage, 757 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 757 )),

	/**
	 * 758 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_758(758, aMessage -> new UnassignedStatusCodeException( aMessage, 758 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 758 )),

	/**
	 * 759 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_759(759, aMessage -> new UnassignedStatusCodeException( aMessage, 759 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 759 )),

	/**
	 * 760 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_760(760, aMessage -> new UnassignedStatusCodeException( aMessage, 760 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 760 )),

	/**
	 * 761 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_761(761, aMessage -> new UnassignedStatusCodeException( aMessage, 761 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 761 )),

	/**
	 * 762 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_762(762, aMessage -> new UnassignedStatusCodeException( aMessage, 762 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 762 )),

	/**
	 * 763 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_763(763, aMessage -> new UnassignedStatusCodeException( aMessage, 763 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 763 )),

	/**
	 * 764 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_764(764, aMessage -> new UnassignedStatusCodeException( aMessage, 764 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 764 )),

	/**
	 * 765 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_765(765, aMessage -> new UnassignedStatusCodeException( aMessage, 765 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 765 )),

	/**
	 * 766 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_766(766, aMessage -> new UnassignedStatusCodeException( aMessage, 766 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 766 )),

	/**
	 * 767 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_767(767, aMessage -> new UnassignedStatusCodeException( aMessage, 767 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 767 )),

	/**
	 * 768 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_768(768, aMessage -> new UnassignedStatusCodeException( aMessage, 768 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 768 )),

	/**
	 * 769 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_769(769, aMessage -> new UnassignedStatusCodeException( aMessage, 769 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 769 )),

	/**
	 * 770 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_770(770, aMessage -> new UnassignedStatusCodeException( aMessage, 770 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 770 )),

	/**
	 * 771 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_771(771, aMessage -> new UnassignedStatusCodeException( aMessage, 771 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 771 )),

	/**
	 * 772 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_772(772, aMessage -> new UnassignedStatusCodeException( aMessage, 772 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 772 )),

	/**
	 * 773 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_773(773, aMessage -> new UnassignedStatusCodeException( aMessage, 773 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 773 )),

	/**
	 * 774 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_774(774, aMessage -> new UnassignedStatusCodeException( aMessage, 774 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 774 )),

	/**
	 * 775 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_775(775, aMessage -> new UnassignedStatusCodeException( aMessage, 775 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 775 )),

	/**
	 * 776 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_776(776, aMessage -> new UnassignedStatusCodeException( aMessage, 776 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 776 )),

	/**
	 * 777 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_777(777, aMessage -> new UnassignedStatusCodeException( aMessage, 777 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 777 )),

	/**
	 * 778 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_778(778, aMessage -> new UnassignedStatusCodeException( aMessage, 778 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 778 )),

	/**
	 * 779 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_779(779, aMessage -> new UnassignedStatusCodeException( aMessage, 779 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 779 )),

	/**
	 * 780 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_780(780, aMessage -> new UnassignedStatusCodeException( aMessage, 780 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 780 )),

	/**
	 * 781 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_781(781, aMessage -> new UnassignedStatusCodeException( aMessage, 781 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 781 )),

	/**
	 * 782 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_782(782, aMessage -> new UnassignedStatusCodeException( aMessage, 782 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 782 )),

	/**
	 * 783 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_783(783, aMessage -> new UnassignedStatusCodeException( aMessage, 783 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 783 )),

	/**
	 * 784 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_784(784, aMessage -> new UnassignedStatusCodeException( aMessage, 784 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 784 )),

	/**
	 * 785 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_785(785, aMessage -> new UnassignedStatusCodeException( aMessage, 785 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 785 )),

	/**
	 * 786 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_786(786, aMessage -> new UnassignedStatusCodeException( aMessage, 786 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 786 )),

	/**
	 * 787 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_787(787, aMessage -> new UnassignedStatusCodeException( aMessage, 787 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 787 )),

	/**
	 * 788 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_788(788, aMessage -> new UnassignedStatusCodeException( aMessage, 788 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 788 )),

	/**
	 * 789 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_789(789, aMessage -> new UnassignedStatusCodeException( aMessage, 789 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 789 )),

	/**
	 * 790 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_790(790, aMessage -> new UnassignedStatusCodeException( aMessage, 790 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 790 )),

	/**
	 * 791 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_791(791, aMessage -> new UnassignedStatusCodeException( aMessage, 791 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 791 )),

	/**
	 * 792 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_792(792, aMessage -> new UnassignedStatusCodeException( aMessage, 792 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 792 )),

	/**
	 * 793 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_793(793, aMessage -> new UnassignedStatusCodeException( aMessage, 793 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 793 )),

	/**
	 * 794 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_794(794, aMessage -> new UnassignedStatusCodeException( aMessage, 794 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 794 )),

	/**
	 * 795 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_795(795, aMessage -> new UnassignedStatusCodeException( aMessage, 795 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 795 )),

	/**
	 * 796 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_796(796, aMessage -> new UnassignedStatusCodeException( aMessage, 796 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 796 )),

	/**
	 * 797 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_797(797, aMessage -> new UnassignedStatusCodeException( aMessage, 797 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 797 )),

	/**
	 * 798 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_798(798, aMessage -> new UnassignedStatusCodeException( aMessage, 798 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 798 )),

	/**
	 * 799 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_799(799, aMessage -> new UnassignedStatusCodeException( aMessage, 799 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 799 )),

	/**
	 * 800 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_800(800, aMessage -> new UnassignedStatusCodeException( aMessage, 800 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 800 )),

	/**
	 * 801 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_801(801, aMessage -> new UnassignedStatusCodeException( aMessage, 801 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 801 )),

	/**
	 * 802 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_802(802, aMessage -> new UnassignedStatusCodeException( aMessage, 802 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 802 )),

	/**
	 * 803 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_803(803, aMessage -> new UnassignedStatusCodeException( aMessage, 803 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 803 )),

	/**
	 * 804 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_804(804, aMessage -> new UnassignedStatusCodeException( aMessage, 804 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 804 )),

	/**
	 * 805 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_805(805, aMessage -> new UnassignedStatusCodeException( aMessage, 805 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 805 )),

	/**
	 * 806 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_806(806, aMessage -> new UnassignedStatusCodeException( aMessage, 806 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 806 )),

	/**
	 * 807 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_807(807, aMessage -> new UnassignedStatusCodeException( aMessage, 807 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 807 )),

	/**
	 * 808 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_808(808, aMessage -> new UnassignedStatusCodeException( aMessage, 808 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 808 )),

	/**
	 * 809 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_809(809, aMessage -> new UnassignedStatusCodeException( aMessage, 809 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 809 )),

	/**
	 * 810 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_810(810, aMessage -> new UnassignedStatusCodeException( aMessage, 810 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 810 )),

	/**
	 * 811 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_811(811, aMessage -> new UnassignedStatusCodeException( aMessage, 811 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 811 )),

	/**
	 * 812 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_812(812, aMessage -> new UnassignedStatusCodeException( aMessage, 812 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 812 )),

	/**
	 * 813 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_813(813, aMessage -> new UnassignedStatusCodeException( aMessage, 813 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 813 )),

	/**
	 * 814 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_814(814, aMessage -> new UnassignedStatusCodeException( aMessage, 814 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 814 )),

	/**
	 * 815 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_815(815, aMessage -> new UnassignedStatusCodeException( aMessage, 815 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 815 )),

	/**
	 * 816 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_816(816, aMessage -> new UnassignedStatusCodeException( aMessage, 816 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 816 )),

	/**
	 * 817 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_817(817, aMessage -> new UnassignedStatusCodeException( aMessage, 817 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 817 )),

	/**
	 * 818 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_818(818, aMessage -> new UnassignedStatusCodeException( aMessage, 818 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 818 )),

	/**
	 * 819 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_819(819, aMessage -> new UnassignedStatusCodeException( aMessage, 819 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 819 )),

	/**
	 * 820 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_820(820, aMessage -> new UnassignedStatusCodeException( aMessage, 820 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 820 )),

	/**
	 * 821 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_821(821, aMessage -> new UnassignedStatusCodeException( aMessage, 821 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 821 )),

	/**
	 * 822 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_822(822, aMessage -> new UnassignedStatusCodeException( aMessage, 822 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 822 )),

	/**
	 * 823 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_823(823, aMessage -> new UnassignedStatusCodeException( aMessage, 823 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 823 )),

	/**
	 * 824 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_824(824, aMessage -> new UnassignedStatusCodeException( aMessage, 824 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 824 )),

	/**
	 * 825 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_825(825, aMessage -> new UnassignedStatusCodeException( aMessage, 825 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 825 )),

	/**
	 * 826 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_826(826, aMessage -> new UnassignedStatusCodeException( aMessage, 826 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 826 )),

	/**
	 * 827 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_827(827, aMessage -> new UnassignedStatusCodeException( aMessage, 827 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 827 )),

	/**
	 * 828 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_828(828, aMessage -> new UnassignedStatusCodeException( aMessage, 828 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 828 )),

	/**
	 * 829 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_829(829, aMessage -> new UnassignedStatusCodeException( aMessage, 829 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 829 )),

	/**
	 * 830 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_830(830, aMessage -> new UnassignedStatusCodeException( aMessage, 830 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 830 )),

	/**
	 * 831 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_831(831, aMessage -> new UnassignedStatusCodeException( aMessage, 831 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 831 )),

	/**
	 * 832 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_832(832, aMessage -> new UnassignedStatusCodeException( aMessage, 832 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 832 )),

	/**
	 * 833 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_833(833, aMessage -> new UnassignedStatusCodeException( aMessage, 833 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 833 )),

	/**
	 * 834 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_834(834, aMessage -> new UnassignedStatusCodeException( aMessage, 834 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 834 )),

	/**
	 * 835 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_835(835, aMessage -> new UnassignedStatusCodeException( aMessage, 835 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 835 )),

	/**
	 * 836 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_836(836, aMessage -> new UnassignedStatusCodeException( aMessage, 836 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 836 )),

	/**
	 * 837 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_837(837, aMessage -> new UnassignedStatusCodeException( aMessage, 837 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 837 )),

	/**
	 * 838 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_838(838, aMessage -> new UnassignedStatusCodeException( aMessage, 838 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 838 )),

	/**
	 * 839 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_839(839, aMessage -> new UnassignedStatusCodeException( aMessage, 839 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 839 )),

	/**
	 * 840 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_840(840, aMessage -> new UnassignedStatusCodeException( aMessage, 840 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 840 )),

	/**
	 * 841 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_841(841, aMessage -> new UnassignedStatusCodeException( aMessage, 841 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 841 )),

	/**
	 * 842 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_842(842, aMessage -> new UnassignedStatusCodeException( aMessage, 842 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 842 )),

	/**
	 * 843 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_843(843, aMessage -> new UnassignedStatusCodeException( aMessage, 843 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 843 )),

	/**
	 * 844 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_844(844, aMessage -> new UnassignedStatusCodeException( aMessage, 844 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 844 )),

	/**
	 * 845 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_845(845, aMessage -> new UnassignedStatusCodeException( aMessage, 845 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 845 )),

	/**
	 * 846 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_846(846, aMessage -> new UnassignedStatusCodeException( aMessage, 846 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 846 )),

	/**
	 * 847 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_847(847, aMessage -> new UnassignedStatusCodeException( aMessage, 847 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 847 )),

	/**
	 * 848 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_848(848, aMessage -> new UnassignedStatusCodeException( aMessage, 848 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 848 )),

	/**
	 * 849 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_849(849, aMessage -> new UnassignedStatusCodeException( aMessage, 849 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 849 )),

	/**
	 * 850 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_850(850, aMessage -> new UnassignedStatusCodeException( aMessage, 850 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 850 )),

	/**
	 * 851 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_851(851, aMessage -> new UnassignedStatusCodeException( aMessage, 851 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 851 )),

	/**
	 * 852 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_852(852, aMessage -> new UnassignedStatusCodeException( aMessage, 852 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 852 )),

	/**
	 * 853 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_853(853, aMessage -> new UnassignedStatusCodeException( aMessage, 853 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 853 )),

	/**
	 * 854 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_854(854, aMessage -> new UnassignedStatusCodeException( aMessage, 854 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 854 )),

	/**
	 * 855 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_855(855, aMessage -> new UnassignedStatusCodeException( aMessage, 855 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 855 )),

	/**
	 * 856 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_856(856, aMessage -> new UnassignedStatusCodeException( aMessage, 856 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 856 )),

	/**
	 * 857 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_857(857, aMessage -> new UnassignedStatusCodeException( aMessage, 857 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 857 )),

	/**
	 * 858 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_858(858, aMessage -> new UnassignedStatusCodeException( aMessage, 858 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 858 )),

	/**
	 * 859 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_859(859, aMessage -> new UnassignedStatusCodeException( aMessage, 859 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 859 )),

	/**
	 * 860 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_860(860, aMessage -> new UnassignedStatusCodeException( aMessage, 860 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 860 )),

	/**
	 * 861 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_861(861, aMessage -> new UnassignedStatusCodeException( aMessage, 861 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 861 )),

	/**
	 * 862 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_862(862, aMessage -> new UnassignedStatusCodeException( aMessage, 862 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 862 )),

	/**
	 * 863 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_863(863, aMessage -> new UnassignedStatusCodeException( aMessage, 863 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 863 )),

	/**
	 * 864 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_864(864, aMessage -> new UnassignedStatusCodeException( aMessage, 864 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 864 )),

	/**
	 * 865 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_865(865, aMessage -> new UnassignedStatusCodeException( aMessage, 865 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 865 )),

	/**
	 * 866 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_866(866, aMessage -> new UnassignedStatusCodeException( aMessage, 866 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 866 )),

	/**
	 * 867 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_867(867, aMessage -> new UnassignedStatusCodeException( aMessage, 867 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 867 )),

	/**
	 * 868 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_868(868, aMessage -> new UnassignedStatusCodeException( aMessage, 868 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 868 )),

	/**
	 * 869 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_869(869, aMessage -> new UnassignedStatusCodeException( aMessage, 869 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 869 )),

	/**
	 * 870 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_870(870, aMessage -> new UnassignedStatusCodeException( aMessage, 870 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 870 )),

	/**
	 * 871 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_871(871, aMessage -> new UnassignedStatusCodeException( aMessage, 871 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 871 )),

	/**
	 * 872 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_872(872, aMessage -> new UnassignedStatusCodeException( aMessage, 872 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 872 )),

	/**
	 * 873 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_873(873, aMessage -> new UnassignedStatusCodeException( aMessage, 873 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 873 )),

	/**
	 * 874 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_874(874, aMessage -> new UnassignedStatusCodeException( aMessage, 874 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 874 )),

	/**
	 * 875 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_875(875, aMessage -> new UnassignedStatusCodeException( aMessage, 875 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 875 )),

	/**
	 * 876 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_876(876, aMessage -> new UnassignedStatusCodeException( aMessage, 876 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 876 )),

	/**
	 * 877 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_877(877, aMessage -> new UnassignedStatusCodeException( aMessage, 877 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 877 )),

	/**
	 * 878 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_878(878, aMessage -> new UnassignedStatusCodeException( aMessage, 878 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 878 )),

	/**
	 * 879 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_879(879, aMessage -> new UnassignedStatusCodeException( aMessage, 879 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 879 )),

	/**
	 * 880 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_880(880, aMessage -> new UnassignedStatusCodeException( aMessage, 880 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 880 )),

	/**
	 * 881 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_881(881, aMessage -> new UnassignedStatusCodeException( aMessage, 881 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 881 )),

	/**
	 * 882 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_882(882, aMessage -> new UnassignedStatusCodeException( aMessage, 882 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 882 )),

	/**
	 * 883 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_883(883, aMessage -> new UnassignedStatusCodeException( aMessage, 883 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 883 )),

	/**
	 * 884 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_884(884, aMessage -> new UnassignedStatusCodeException( aMessage, 884 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 884 )),

	/**
	 * 885 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_885(885, aMessage -> new UnassignedStatusCodeException( aMessage, 885 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 885 )),

	/**
	 * 886 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_886(886, aMessage -> new UnassignedStatusCodeException( aMessage, 886 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 886 )),

	/**
	 * 887 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_887(887, aMessage -> new UnassignedStatusCodeException( aMessage, 887 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 887 )),

	/**
	 * 888 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_888(888, aMessage -> new UnassignedStatusCodeException( aMessage, 888 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 888 )),

	/**
	 * 889 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_889(889, aMessage -> new UnassignedStatusCodeException( aMessage, 889 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 889 )),

	/**
	 * 890 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_890(890, aMessage -> new UnassignedStatusCodeException( aMessage, 890 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 890 )),

	/**
	 * 891 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_891(891, aMessage -> new UnassignedStatusCodeException( aMessage, 891 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 891 )),

	/**
	 * 892 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_892(892, aMessage -> new UnassignedStatusCodeException( aMessage, 892 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 892 )),

	/**
	 * 893 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_893(893, aMessage -> new UnassignedStatusCodeException( aMessage, 893 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 893 )),

	/**
	 * 894 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_894(894, aMessage -> new UnassignedStatusCodeException( aMessage, 894 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 894 )),

	/**
	 * 895 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_895(895, aMessage -> new UnassignedStatusCodeException( aMessage, 895 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 895 )),

	/**
	 * 896 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_896(896, aMessage -> new UnassignedStatusCodeException( aMessage, 896 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 896 )),

	/**
	 * 897 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_897(897, aMessage -> new UnassignedStatusCodeException( aMessage, 897 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 897 )),

	/**
	 * 898 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_898(898, aMessage -> new UnassignedStatusCodeException( aMessage, 898 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 898 )),

	/**
	 * 899 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_899(899, aMessage -> new UnassignedStatusCodeException( aMessage, 899 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 899 )),

	/**
	 * 903 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_903(903, aMessage -> new UnassignedStatusCodeException( aMessage, 903 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 903 )),

	/**
	 * 904 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_904(904, aMessage -> new UnassignedStatusCodeException( aMessage, 904 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 904 )),

	/**
	 * 905 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_905(905, aMessage -> new UnassignedStatusCodeException( aMessage, 905 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 905 )),

	/**
	 * 906 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_906(906, aMessage -> new UnassignedStatusCodeException( aMessage, 906 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 906 )),

	/**
	 * 907 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_907(907, aMessage -> new UnassignedStatusCodeException( aMessage, 907 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 907 )),

	/**
	 * 908 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_908(908, aMessage -> new UnassignedStatusCodeException( aMessage, 908 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 908 )),

	/**
	 * 909 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_909(909, aMessage -> new UnassignedStatusCodeException( aMessage, 909 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 909 )),

	/**
	 * 910 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_910(910, aMessage -> new UnassignedStatusCodeException( aMessage, 910 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 910 )),

	/**
	 * 911 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_911(911, aMessage -> new UnassignedStatusCodeException( aMessage, 911 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 911 )),

	/**
	 * 912 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_912(912, aMessage -> new UnassignedStatusCodeException( aMessage, 912 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 912 )),

	/**
	 * 913 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_913(913, aMessage -> new UnassignedStatusCodeException( aMessage, 913 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 913 )),

	/**
	 * 914 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_914(914, aMessage -> new UnassignedStatusCodeException( aMessage, 914 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 914 )),

	/**
	 * 915 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_915(915, aMessage -> new UnassignedStatusCodeException( aMessage, 915 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 915 )),

	/**
	 * 916 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_916(916, aMessage -> new UnassignedStatusCodeException( aMessage, 916 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 916 )),

	/**
	 * 917 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_917(917, aMessage -> new UnassignedStatusCodeException( aMessage, 917 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 917 )),

	/**
	 * 918 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_918(918, aMessage -> new UnassignedStatusCodeException( aMessage, 918 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 918 )),

	/**
	 * 919 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_919(919, aMessage -> new UnassignedStatusCodeException( aMessage, 919 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 919 )),

	/**
	 * 920 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_920(920, aMessage -> new UnassignedStatusCodeException( aMessage, 920 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 920 )),

	/**
	 * 921 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_921(921, aMessage -> new UnassignedStatusCodeException( aMessage, 921 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 921 )),

	/**
	 * 922 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_922(922, aMessage -> new UnassignedStatusCodeException( aMessage, 922 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 922 )),

	/**
	 * 923 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_923(923, aMessage -> new UnassignedStatusCodeException( aMessage, 923 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 923 )),

	/**
	 * 924 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_924(924, aMessage -> new UnassignedStatusCodeException( aMessage, 924 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 924 )),

	/**
	 * 925 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_925(925, aMessage -> new UnassignedStatusCodeException( aMessage, 925 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 925 )),

	/**
	 * 926 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_926(926, aMessage -> new UnassignedStatusCodeException( aMessage, 926 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 926 )),

	/**
	 * 927 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_927(927, aMessage -> new UnassignedStatusCodeException( aMessage, 927 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 927 )),

	/**
	 * 928 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_928(928, aMessage -> new UnassignedStatusCodeException( aMessage, 928 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 928 )),

	/**
	 * 929 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_929(929, aMessage -> new UnassignedStatusCodeException( aMessage, 929 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 929 )),

	/**
	 * 930 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_930(930, aMessage -> new UnassignedStatusCodeException( aMessage, 930 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 930 )),

	/**
	 * 931 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_931(931, aMessage -> new UnassignedStatusCodeException( aMessage, 931 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 931 )),

	/**
	 * 932 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_932(932, aMessage -> new UnassignedStatusCodeException( aMessage, 932 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 932 )),

	/**
	 * 933 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_933(933, aMessage -> new UnassignedStatusCodeException( aMessage, 933 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 933 )),

	/**
	 * 934 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_934(934, aMessage -> new UnassignedStatusCodeException( aMessage, 934 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 934 )),

	/**
	 * 935 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_935(935, aMessage -> new UnassignedStatusCodeException( aMessage, 935 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 935 )),

	/**
	 * 936 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_936(936, aMessage -> new UnassignedStatusCodeException( aMessage, 936 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 936 )),

	/**
	 * 937 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_937(937, aMessage -> new UnassignedStatusCodeException( aMessage, 937 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 937 )),

	/**
	 * 938 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_938(938, aMessage -> new UnassignedStatusCodeException( aMessage, 938 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 938 )),

	/**
	 * 939 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_939(939, aMessage -> new UnassignedStatusCodeException( aMessage, 939 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 939 )),

	/**
	 * 940 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_940(940, aMessage -> new UnassignedStatusCodeException( aMessage, 940 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 940 )),

	/**
	 * 941 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_941(941, aMessage -> new UnassignedStatusCodeException( aMessage, 941 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 941 )),

	/**
	 * 942 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_942(942, aMessage -> new UnassignedStatusCodeException( aMessage, 942 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 942 )),

	/**
	 * 943 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_943(943, aMessage -> new UnassignedStatusCodeException( aMessage, 943 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 943 )),

	/**
	 * 944 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_944(944, aMessage -> new UnassignedStatusCodeException( aMessage, 944 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 944 )),

	/**
	 * 945 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_945(945, aMessage -> new UnassignedStatusCodeException( aMessage, 945 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 945 )),

	/**
	 * 946 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_946(946, aMessage -> new UnassignedStatusCodeException( aMessage, 946 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 946 )),

	/**
	 * 947 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_947(947, aMessage -> new UnassignedStatusCodeException( aMessage, 947 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 947 )),

	/**
	 * 948 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_948(948, aMessage -> new UnassignedStatusCodeException( aMessage, 948 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 948 )),

	/**
	 * 949 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_949(949, aMessage -> new UnassignedStatusCodeException( aMessage, 949 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 949 )),

	/**
	 * 950 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_950(950, aMessage -> new UnassignedStatusCodeException( aMessage, 950 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 950 )),

	/**
	 * 951 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_951(951, aMessage -> new UnassignedStatusCodeException( aMessage, 951 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 951 )),

	/**
	 * 952 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_952(952, aMessage -> new UnassignedStatusCodeException( aMessage, 952 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 952 )),

	/**
	 * 953 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_953(953, aMessage -> new UnassignedStatusCodeException( aMessage, 953 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 953 )),

	/**
	 * 954 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_954(954, aMessage -> new UnassignedStatusCodeException( aMessage, 954 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 954 )),

	/**
	 * 955 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_955(955, aMessage -> new UnassignedStatusCodeException( aMessage, 955 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 955 )),

	/**
	 * 956 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_956(956, aMessage -> new UnassignedStatusCodeException( aMessage, 956 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 956 )),

	/**
	 * 957 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_957(957, aMessage -> new UnassignedStatusCodeException( aMessage, 957 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 957 )),

	/**
	 * 958 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_958(958, aMessage -> new UnassignedStatusCodeException( aMessage, 958 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 958 )),

	/**
	 * 959 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_959(959, aMessage -> new UnassignedStatusCodeException( aMessage, 959 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 959 )),

	/**
	 * 960 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_960(960, aMessage -> new UnassignedStatusCodeException( aMessage, 960 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 960 )),

	/**
	 * 961 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_961(961, aMessage -> new UnassignedStatusCodeException( aMessage, 961 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 961 )),

	/**
	 * 962 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_962(962, aMessage -> new UnassignedStatusCodeException( aMessage, 962 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 962 )),

	/**
	 * 963 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_963(963, aMessage -> new UnassignedStatusCodeException( aMessage, 963 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 963 )),

	/**
	 * 964 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_964(964, aMessage -> new UnassignedStatusCodeException( aMessage, 964 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 964 )),

	/**
	 * 965 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_965(965, aMessage -> new UnassignedStatusCodeException( aMessage, 965 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 965 )),

	/**
	 * 966 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_966(966, aMessage -> new UnassignedStatusCodeException( aMessage, 966 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 966 )),

	/**
	 * 967 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_967(967, aMessage -> new UnassignedStatusCodeException( aMessage, 967 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 967 )),

	/**
	 * 968 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_968(968, aMessage -> new UnassignedStatusCodeException( aMessage, 968 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 968 )),

	/**
	 * 969 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_969(969, aMessage -> new UnassignedStatusCodeException( aMessage, 969 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 969 )),

	/**
	 * 970 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_970(970, aMessage -> new UnassignedStatusCodeException( aMessage, 970 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 970 )),

	/**
	 * 971 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_971(971, aMessage -> new UnassignedStatusCodeException( aMessage, 971 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 971 )),

	/**
	 * 972 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_972(972, aMessage -> new UnassignedStatusCodeException( aMessage, 972 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 972 )),

	/**
	 * 973 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_973(973, aMessage -> new UnassignedStatusCodeException( aMessage, 973 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 973 )),

	/**
	 * 974 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_974(974, aMessage -> new UnassignedStatusCodeException( aMessage, 974 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 974 )),

	/**
	 * 975 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_975(975, aMessage -> new UnassignedStatusCodeException( aMessage, 975 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 975 )),

	/**
	 * 976 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_976(976, aMessage -> new UnassignedStatusCodeException( aMessage, 976 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 976 )),

	/**
	 * 977 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_977(977, aMessage -> new UnassignedStatusCodeException( aMessage, 977 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 977 )),

	/**
	 * 978 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_978(978, aMessage -> new UnassignedStatusCodeException( aMessage, 978 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 978 )),

	/**
	 * 979 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_979(979, aMessage -> new UnassignedStatusCodeException( aMessage, 979 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 979 )),

	/**
	 * 980 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_980(980, aMessage -> new UnassignedStatusCodeException( aMessage, 980 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 980 )),

	/**
	 * 981 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_981(981, aMessage -> new UnassignedStatusCodeException( aMessage, 981 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 981 )),

	/**
	 * 982 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_982(982, aMessage -> new UnassignedStatusCodeException( aMessage, 982 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 982 )),

	/**
	 * 983 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_983(983, aMessage -> new UnassignedStatusCodeException( aMessage, 983 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 983 )),

	/**
	 * 984 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_984(984, aMessage -> new UnassignedStatusCodeException( aMessage, 984 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 984 )),

	/**
	 * 985 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_985(985, aMessage -> new UnassignedStatusCodeException( aMessage, 985 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 985 )),

	/**
	 * 986 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_986(986, aMessage -> new UnassignedStatusCodeException( aMessage, 986 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 986 )),

	/**
	 * 987 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_987(987, aMessage -> new UnassignedStatusCodeException( aMessage, 987 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 987 )),

	/**
	 * 988 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_988(988, aMessage -> new UnassignedStatusCodeException( aMessage, 988 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 988 )),

	/**
	 * 989 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_989(989, aMessage -> new UnassignedStatusCodeException( aMessage, 989 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 989 )),

	/**
	 * 990 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_990(990, aMessage -> new UnassignedStatusCodeException( aMessage, 990 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 990 )),

	/**
	 * 991 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_991(991, aMessage -> new UnassignedStatusCodeException( aMessage, 991 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 991 )),

	/**
	 * 992 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_992(992, aMessage -> new UnassignedStatusCodeException( aMessage, 992 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 992 )),

	/**
	 * 993 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_993(993, aMessage -> new UnassignedStatusCodeException( aMessage, 993 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 993 )),

	/**
	 * 994 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_994(994, aMessage -> new UnassignedStatusCodeException( aMessage, 994 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 994 )),

	/**
	 * 995 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_995(995, aMessage -> new UnassignedStatusCodeException( aMessage, 995 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 995 )),

	/**
	 * 996 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_996(996, aMessage -> new UnassignedStatusCodeException( aMessage, 996 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 996 )),

	/**
	 * 997 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_997(997, aMessage -> new UnassignedStatusCodeException( aMessage, 997 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 997 )),

	/**
	 * 998 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_998(998, aMessage -> new UnassignedStatusCodeException( aMessage, 998 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 998 )),

	/**
	 * 999 Unassigned legacy (unknown or undefined) HTTP-Status-Code.
	 */
	UNASSIGNED_LEGACY_999(999, aMessage -> new UnassignedStatusCodeException( aMessage, 999 ), aMessage -> new UnassignedStatusCodeRuntimeException( aMessage, 999 ));

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Integer _statusCode;

	private ContextTypeFactory<HttpStatusException, String> _statusExceptionFactory = null;
	private ContextTypeFactory<HttpStatusRuntimeException, String> _statusRuntimeExceptionFactory = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new http status code.
	 *
	 * @param aStatusCode the status code
	 */
	private HttpStatusCode( Integer aStatusCode ) {
		_statusCode = aStatusCode;
	}

	/**
	 * Instantiates a new http status code.
	 *
	 * @param aExeceptionFactory In case of an erronous status code, here we go
	 *        with the factory creating an according exception.
	 * @param aStatusCode the status code
	 */
	private HttpStatusCode( Integer aStatusCode, ContextTypeFactory<HttpStatusException, String> aExeceptionFactory, ContextTypeFactory<HttpStatusRuntimeException, String> aRuntimeExeceptionFactory ) {
		_statusCode = aStatusCode;
		_statusExceptionFactory = aExeceptionFactory;
		_statusRuntimeExceptionFactory = aRuntimeExeceptionFactory;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer getStatusCode() {
		return _statusCode;
	}

	/**
	 * Determines whether the given HTTP Status-Code signals a success.
	 * 
	 * @return True in case the {@link HttpStatusCode} represents an OK status.
	 */
	public boolean isSuccessStatus() {
		return _statusCode >= 200 && _statusCode < 300;
	}

	/**
	 * Determines whether the given HTTP Status-Code signals a redirect. Then
	 * the {@link HeaderField#CONTENT_LOCATION}
	 * 
	 * @return True in case the {@link HttpStatusCode} represents an redirect
	 *         status.
	 */
	public boolean isRedirectStatus() {
		return _statusCode >= 300 && _statusCode < 400;
	}

	/**
	 * Determines whether the given HTTP Status-Code signals a failure.
	 *
	 * @return True in case the {@link HttpStatusCode} represents an ERROR
	 *         status.
	 */
	public boolean isErrorStatus() {
		return _statusCode >= 400;
	}

	/**
	 * Determines whether the given HTTP Status-Code signals a client side
	 * error.
	 *
	 * @return True in case the {@link HttpStatusCode} represents a an ERROR
	 *         status related to a client's HTTP-Request.
	 */
	public boolean isClientErrorStatus() {
		return _statusCode >= 400 && _statusCode < 500;
	}

	/**
	 * Determines whether the given HTTP Status-Code signals a server side
	 * error.
	 *
	 * @return True in case the {@link HttpStatusCode} represents a an ERROR
	 *         status caused by a server processing a client's HTTP-Request.
	 */
	public boolean isServerErrorStatus() {
		return _statusCode >= 500 && _statusCode < 600;
	}

	/**
	 * Determines the HTTP success code from the given value by evaluating the
	 * {@link #getStatusCode()} property.
	 * 
	 * @param aHttpStatusCode The code from which to get the
	 *        {@link HttpStatusCode} element.
	 * 
	 * @return The according {@link HttpStatusCode} element or null if none was
	 *         found.
	 */
	public static HttpStatusCode toHttpStatusCode( int aHttpStatusCode ) {
		for ( HttpStatusCode eCode : values() ) {
			if ( eCode.getStatusCode().equals( aHttpStatusCode ) ) {
				return eCode;
			}
		}
		return null;
	}

	/**
	 * Determines the HTTP success code from the given HTTP Status-Code property
	 * by evaluating the {@link #getStatusCode()} property.
	 * 
	 * @param aHttpStatusCode The code property from which
	 *        {@link StatusCodeAccessor#getStatusCode()} to get the
	 *        {@link HttpStatusCode} element.
	 * 
	 * @return The according {@link HttpStatusCode} element or null if none was
	 *         found.
	 */
	public static HttpStatusCode toHttpStatusCode( StatusCodeAccessor<Integer> aHttpStatusCode ) {
		for ( HttpStatusCode eCode : values() ) {
			if ( eCode.getStatusCode().equals( aHttpStatusCode.getStatusCode() ) ) {
				return eCode;
			}
		}
		return null;
	}

	/**
	 * If the status is an erroneous status as of {@link #isErrorStatus()} ,
	 * then this method creates the according {@link HttpStatusException}
	 * exception.
	 * 
	 * @param aMessage The aMessage to be contained in the exception.
	 * 
	 * @return The according exception or null if we do not have an erroneous
	 *         status..
	 */
	public HttpStatusException toHttpStatusException( String aMessage ) {
		if ( isErrorStatus() || isRedirectStatus() ) {
			return _statusExceptionFactory.create( aMessage );
		}
		return null;
	}

	/**
	 * If the status is an erroneous status as of {@link #isErrorStatus()}, then
	 * this method creates the according {@link HttpStatusRuntimeException}
	 * exception.
	 * 
	 * @param aMessage The aMessage to be contained in the exception.
	 * 
	 * @return The according exception or null if we do not have an erronous
	 *         status.
	 */
	public HttpStatusRuntimeException toHttpStatusRuntimeException( String aMessage ) {
		if ( isErrorStatus() || isRedirectStatus() ) {
			return _statusRuntimeExceptionFactory.create( aMessage );
		}
		return null;
	}

	/**
	 * Returns the verbose text for the according HTTP-Status-Code. The verbose
	 * text for the HTTP-Status-Code 200 would be "OK".
	 * 
	 * @return The verbose code for the {@link HttpStatusCode}.
	 */
	public String toVerbose() {
		return name().replaceAll( "_", " " );
	}
}
