// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import org.refcodes.mixin.CredentialsAccessor.CredentialsBuilder;
import org.refcodes.mixin.CredentialsAccessor.CredentialsProperty;

/**
 * The {@link BasicAuthCredentialsBuilder} compares the secret
 * ({@link #getSecret()}) case sensitive but the username (
 * {@link #getIdentity()}) case insensitive within the {@link #equals(Object)}
 * method.
 */
public class BasicAuthCredentialsBuilder extends BasicAuthCredentials implements CredentialsProperty, CredentialsBuilder<BasicAuthCredentialsBuilder> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an empty {@link BasicAuthCredentialsBuilder} type.
	 */
	public BasicAuthCredentialsBuilder() {}

	/**
	 * Instantiates a new {@link BasicAuthCredentialsBuilder} instance.
	 *
	 * @param aIdentity the user name
	 * @param aSecret the secret
	 */
	public BasicAuthCredentialsBuilder( String aIdentity, String aSecret ) {
		super( aIdentity, aSecret );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	@Override
	public void setIdentity( String aIdentity ) {
		_identity = aIdentity;

	}

	@Override
	public void setSecret( String aSecret ) {
		_secret = aSecret;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BasicAuthCredentialsBuilder withIdentity( String aIdentity ) {
		setIdentity( aIdentity );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BasicAuthCredentialsBuilder withSecret( String aSecret ) {
		setSecret( aSecret );
		return this;
	}
}
