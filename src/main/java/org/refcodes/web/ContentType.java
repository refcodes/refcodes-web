// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import java.util.Collection;
import java.util.Map;

import org.refcodes.data.Delimiter;
import org.refcodes.data.Encoding;
import org.refcodes.properties.Properties;
import org.refcodes.properties.PropertiesBuilderImpl;
import org.refcodes.struct.PathMap;
import org.refcodes.struct.Property;
import org.refcodes.struct.PropertyImpl;
import org.refcodes.struct.Relation;

/**
 * A {@link ContentType} represents a {@link MediaType} and its according
 * {@link TopLevelType} with additional (optional) suffixed parameters (as of
 * the according HTTP Header-Fields). In other words, it is a concrete
 * occurrence of a {@link MediaType} in an HTTP header including the particular
 * parameters. A {@link ContentType} with the particular parameters may look as
 * follows: "application/json;charset=UTF-8" in case of
 * {@link MediaType#APPLICATION_JSON} and the parameter'
 * {@link MediaTypeParameter#CHARSET} name set to the {@link Encoding#UTF_8}
 * code.
 */
public class ContentType extends PropertiesBuilderImpl implements TopLevelTypeAccessor, MediaTypeAccessor, HttpMediaType {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final MediaType _mediaType;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link ContentType} from the provided {@link MediaType}
	 * (and with it the {@link TopLevelType}).
	 * 
	 * @param aMediaType The {@link MediaType} from which to create the
	 *        {@link ContentType} instance.
	 */
	public ContentType( MediaType aMediaType ) {
		if ( aMediaType == null ) {
			throw new IllegalArgumentException( "You must set a <MediaType> and not <null> as a <ContentType> without <MediaType> does not make sense." );
		}
		_mediaType = aMediaType;
	}

	/**
	 * Constructs a {@link ContentType} instance form a Media-Argument
	 * {@link String} as passed in the according HTTP Header-Fields. A
	 * Media-Argument {@link String} may look as follows:
	 * "application/json;charset=UTF-8". The properties introduced by a
	 * semicolon (";") are parsed into the {@link ContentType}'s parameters map
	 * (accessible via {@link #keySet()}, {@link #values()},
	 * {@link #get(Object)} or adjustable by {@link #put(String, Object)} and
	 * the such {@link Properties} operations).
	 * 
	 * @param aHttpMediaType A HTTP Header-Fields Media-Argument {@link String}.
	 */
	public ContentType( String aHttpMediaType ) {
		if ( aHttpMediaType == null || aHttpMediaType.isEmpty() ) {
			throw new IllegalArgumentException( "You must set a HTTP content type and not <null> (or an empty >String>) as a <ContentType> without <MediaType> does not make sense." );
		}
		String theHttpContentType;
		final String[] theParameters;
		final int indexOf = aHttpMediaType.indexOf( Delimiter.MEDIA_TYPE_PARAMETERS.getChar() );
		if ( indexOf != -1 ) {
			theHttpContentType = aHttpMediaType.substring( 0, indexOf );
			if ( aHttpMediaType.length() > indexOf ) {
				theParameters = aHttpMediaType.substring( indexOf + 1 ).split( "" + Delimiter.MEDIA_TYPE_PARAMETERS.getChar() );
				Property eProperty;
				for ( String eTupel : theParameters ) {
					if ( eTupel != null ) {
						eProperty = new PropertyImpl( eTupel );
						put( eProperty.getKey().trim(), eProperty.getValue() != null ? eProperty.getValue().trim() : null );
					}
				}
			}
		}
		else {
			theHttpContentType = aHttpMediaType;
		}
		theHttpContentType = theHttpContentType.trim();
		_mediaType = MediaType.fromHttpMediaSubtype( theHttpContentType );
		if ( _mediaType == null ) {
			throw new IllegalArgumentException( "Unable to detect valid known Media-Type from argument <" + aHttpMediaType + ">." );
		}
	}

	/**
	 * Instantiates a new content type impl.
	 *
	 * @param aMediaType the media type
	 * @param aProperties the properties
	 */
	public ContentType( MediaType aMediaType, Map<String, String> aProperties ) {
		if ( aMediaType == null ) {
			throw new IllegalArgumentException( "You must set a <MediaType> and not <null> as a <ContentType> without <MediaType> does not make sense." );
		}
		_mediaType = aMediaType;
		putAll( aProperties );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TopLevelType getTopLevelType() {
		return getMediaType().getTopLevelType();
	}

	/**
	 * Put.
	 *
	 * @param aParameter the parameter
	 * @param aValue the value
	 * 
	 * @return the string
	 */
	public String put( MediaTypeParameter aParameter, String aValue ) {
		return put( aParameter.getName(), aValue );
	}

	/**
	 * With put.
	 *
	 * @param aParameter the parameter
	 * @param aValue the value
	 * 
	 * @return the content type
	 */
	public ContentType withPut( MediaTypeParameter aParameter, String aValue ) {
		put( aParameter.getName(), aValue );
		return this;
	}

	/**
	 * Retrieves the charset parameter from the properties of this
	 * {@link ContentType} instance.
	 * 
	 * @return The charset parameter or null if none is found.
	 */
	public String getCharsetParametrer() {
		return get( MediaTypeParameter.CHARSET.getName() );
	}

	/**
	 * Sets the charset parameter's charset-code.
	 * 
	 * @param aCharsetCode The code of the charset to be set (e.g. "UTF-8"), see
	 *        {@link Encoding} for some common codes.
	 * 
	 * @return The charset parameter's charset-code (if previously been set) or
	 *         null if none was set before.
	 */
	public String putCharsetParametrer( String aCharsetCode ) {
		return put( MediaTypeParameter.CHARSET.getName(), aCharsetCode );
	}

	/**
	 * Sets the charset parameter's charset-code.
	 *
	 * @param aValue the value
	 * 
	 * @return This {@link ContentType} instance as of the Builder-Pattern.
	 */
	public ContentType withCharsetParametrer( String aValue ) {
		put( MediaTypeParameter.CHARSET.getName(), aValue );
		return this;
	}

	/**
	 * Returns the HTTP media type: It consists of the Media-Top-Level-Type (
	 * {@link #getTopLevelType()} and the Media-Subtype (
	 * {@link #getMediaType()}) as well as some (optional) suffixed parameters
	 * (as of {@link #keySet()} and {@link #get(Object)}, mutable via
	 * {@link #put(Object, Object)}) all being concatenated in an HTTP valid
	 * manner, such as: "application/json;charset=UTF-8".
	 * 
	 * @return The HTTP header Media-Type as of the IANA specification.
	 */
	@Override
	public String toHttpMediaType() {
		String theResult = null;
		final MediaType theMediaType = getMediaType();
		if ( theMediaType != null ) {
			theResult = theMediaType.toHttpMediaType( this );
		}
		return theResult;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPut( Collection<?> aPathElements, String aValue ) {
		put( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPut( Object[] aPathElements, String aValue ) {
		put( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPut( Relation<String, String> aProperty ) {
		put( aProperty );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPut( String aKey, String aValue ) {
		put( aKey, aValue );
		return this;
	}

	//	/**
	//	 * {@inheritDoc}
	//	 */
	//	@Override
	//	public ContentType withPut( Object aPath, String aValue ) {
	//		put( toPath( aPath ), aValue );
	//		return this;
	//	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPut( Property aProperty ) {
		put( aProperty );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPut( String[] aKey, String aValue ) {
		put( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutBoolean( Collection<?> aPathElements, Boolean aValue ) {
		putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutBoolean( Object aKey, Boolean aValue ) {
		putBoolean( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutBoolean( Object[] aPathElements, Boolean aValue ) {
		putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutBoolean( String aKey, Boolean aValue ) {
		putBoolean( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutBoolean( String[] aPathElements, Boolean aValue ) {
		putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutByte( Collection<?> aPathElements, Byte aValue ) {
		putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutByte( Object aKey, Byte aValue ) {
		putByte( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutByte( Object[] aPathElements, Byte aValue ) {
		putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutByte( String aKey, Byte aValue ) {
		putByte( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutByte( String[] aPathElements, Byte aValue ) {
		putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutChar( Collection<?> aPathElements, Character aValue ) {
		putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutChar( Object aKey, Character aValue ) {
		putChar( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutChar( Object[] aPathElements, Character aValue ) {
		putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutChar( String aKey, Character aValue ) {
		putChar( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutChar( String[] aPathElements, Character aValue ) {
		putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> ContentType withPutClass( Collection<?> aPathElements, Class<C> aValue ) {
		putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> ContentType withPutClass( Object aKey, Class<C> aValue ) {
		putClass( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> ContentType withPutClass( Object[] aPathElements, Class<C> aValue ) {
		putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> ContentType withPutClass( String aKey, Class<C> aValue ) {
		putClass( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> ContentType withPutClass( String[] aPathElements, Class<C> aValue ) {
		putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutDouble( Collection<?> aPathElements, Double aValue ) {
		putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutDouble( Object aKey, Double aValue ) {
		putDouble( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutDouble( Object[] aPathElements, Double aValue ) {
		putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutDouble( String aKey, Double aValue ) {
		putDouble( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutDouble( String[] aPathElements, Double aValue ) {
		putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> ContentType withPutEnum( Collection<?> aPathElements, E aValue ) {
		putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> ContentType withPutEnum( Object aKey, E aValue ) {
		putEnum( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> ContentType withPutEnum( Object[] aPathElements, E aValue ) {
		putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> ContentType withPutEnum( String aKey, E aValue ) {
		putEnum( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> ContentType withPutEnum( String[] aPathElements, E aValue ) {
		putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutFloat( Collection<?> aPathElements, Float aValue ) {
		putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutFloat( Object aKey, Float aValue ) {
		putFloat( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutFloat( Object[] aPathElements, Float aValue ) {
		putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutFloat( String aKey, Float aValue ) {
		putFloat( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutFloat( String[] aPathElements, Float aValue ) {
		putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutInt( Collection<?> aPathElements, Integer aValue ) {
		putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutInt( Object aKey, Integer aValue ) {
		putInt( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutInt( Object[] aPathElements, Integer aValue ) {
		putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutInt( String aKey, Integer aValue ) {
		putInt( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutInt( String[] aPathElements, Integer aValue ) {
		putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutLong( Collection<?> aPathElements, Long aValue ) {
		putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutLong( Object aKey, Long aValue ) {
		putLong( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutLong( Object[] aPathElements, Long aValue ) {
		putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutLong( String aKey, Long aValue ) {
		putLong( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutLong( String[] aPathElements, Long aValue ) {
		putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutShort( Collection<?> aPathElements, Short aValue ) {
		putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutShort( Object aKey, Short aValue ) {
		putShort( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutShort( Object[] aPathElements, Short aValue ) {
		putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutShort( String aKey, Short aValue ) {
		putShort( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutShort( String[] aPathElements, Short aValue ) {
		putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutString( Collection<?> aPathElements, String aValue ) {
		putString( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutString( Object aKey, String aValue ) {
		putString( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutString( Object[] aPathElements, String aValue ) {
		putString( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutString( String aKey, String aValue ) {
		putString( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutString( String[] aPathElements, String aValue ) {
		putString( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsert( Object aObj ) {
		insert( aObj );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsert( PathMap<String> aFrom ) {
		insert( aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertBetween( Collection<?> aToPathElements, PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertBetween( Object aToPath, Object aFrom, Object aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertBetween( Object aToPath, PathMap<String> aFrom, Object aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertBetween( Object[] aToPathElements, PathMap<String> aFrom, Object[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertBetween( String aToPath, Object aFrom, String aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertBetween( String aToPath, PathMap<String> aFrom, String aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertBetween( String[] aToPathElements, PathMap<String> aFrom, String[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertFrom( Object aFrom, Collection<?> aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertFrom( Object aFrom, Object aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertFrom( Object aFrom, Object... aFromPathElements ) {
		withInsertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertFrom( Object aFrom, String aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertFrom( Object aFrom, String... aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertFrom( PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertFrom( PathMap<String> aFrom, Object aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertFrom( PathMap<String> aFrom, Object... aFromPathElements ) {
		withInsertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertFrom( PathMap<String> aFrom, String aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertFrom( PathMap<String> aFrom, String... aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertTo( Collection<?> aToPathElements, Object aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertTo( Collection<?> aToPathElements, PathMap<String> aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertTo( Object aToPath, Object aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertTo( Object aToPath, PathMap<String> aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertTo( Object[] aToPathElements, Object aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertTo( Object[] aToPathElements, PathMap<String> aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertTo( String aToPath, Object aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertTo( String aToPath, PathMap<String> aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertTo( String[] aToPathElements, Object aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withInsertTo( String[] aToPathElements, PathMap<String> aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMerge( Object aObj ) {
		merge( aObj );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMerge( PathMap<String> aFrom ) {
		merge( aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeBetween( Collection<?> aToPathElements, PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeBetween( Object aToPath, Object aFrom, Object aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeBetween( Object aToPath, PathMap<String> aFrom, Object aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeBetween( Object[] aToPathElements, PathMap<String> aFrom, Object[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFromPathElements, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeBetween( String aToPath, Object aFrom, String aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeBetween( String aToPath, PathMap<String> aFrom, String aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeBetween( String[] aToPathElements, PathMap<String> aFrom, String[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeFrom( Object aFrom, Collection<?> aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeFrom( Object aFrom, Object aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeFrom( Object aFrom, Object... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeFrom( Object aFrom, String aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeFrom( Object aFrom, String... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeFrom( PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeFrom( PathMap<String> aFrom, Object aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeFrom( PathMap<String> aFrom, Object... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeFrom( PathMap<String> aFrom, String aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeFrom( PathMap<String> aFrom, String... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeTo( Collection<?> aToPathElements, Object aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeTo( Collection<?> aToPathElements, PathMap<String> aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeTo( Object aToPath, Object aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeTo( Object aToPath, PathMap<String> aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeTo( Object[] aToPathElements, Object aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeTo( Object[] aToPathElements, PathMap<String> aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeTo( String aToPath, Object aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeTo( String aToPath, PathMap<String> aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeTo( String[] aToPathElements, Object aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withMergeTo( String[] aToPathElements, PathMap<String> aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutDirAt( Collection<?> aPathElements, int aIndex, Object aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutDirAt( Collection<?> aPathElements, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutDirAt( int aIndex, Object aDir ) {
		putDirAt( aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutDirAt( int aIndex, PathMap<String> aDir ) {
		putDirAt( aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutDirAt( Object aPath, int aIndex, Object aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutDirAt( Object aPath, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutDirAt( Object[] aPathElements, int aIndex, Object aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutDirAt( Object[] aPathElements, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutDirAt( String aPath, int aIndex, Object aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutDirAt( String aPath, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutDirAt( String[] aPathElements, int aIndex, Object aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withPutDirAt( String[] aPathElements, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withRemoveFrom( Collection<?> aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withRemoveFrom( Object aPath ) {
		removeFrom( aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withRemoveFrom( Object... aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withRemoveFrom( String aPath ) {
		removeFrom( aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withRemoveFrom( String... aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContentType withRemovePaths( String... aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MediaType getMediaType() {
		return _mediaType;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( _mediaType == null ) ? 0 : _mediaType.hashCode() );
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( getClass() != obj.getClass() ) {
			return false;
		}
		final ContentType other = (ContentType) obj;
		if ( _mediaType != other._mediaType ) {
			return false;
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return _mediaType.toHttpMediaType();
	}
}
