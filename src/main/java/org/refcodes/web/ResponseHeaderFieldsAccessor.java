// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides an accessor for an response Header-Fields property.
 */
public interface ResponseHeaderFieldsAccessor {

	/**
	 * Retrieves the response Header-Fields from the response Header-Fields
	 * property.
	 * 
	 * @return The response Header-Fields stored by the response Header-Fields
	 *         property.
	 */
	ResponseHeaderFields getResponseHeaderFields();

	/**
	 * Provides a mutator for an response Header-Fields property.
	 */
	public interface ResponseHeaderFieldsMutator {

		/**
		 * Sets the response Header-Fields for the response Header-Fields
		 * property.
		 * 
		 * @param aResponseHeaderFields The response Header-Fields to be stored
		 *        by the Header-Fields property.
		 */
		void setResponseHeaderFields( ResponseHeaderFields aResponseHeaderFields );
	}

	/**
	 * Provides a mutator for an response Header-Fields property.
	 * 
	 * @param <B> The builder which implements the
	 *        {@link ResponseHeaderFieldsBuilder}.
	 */
	public interface ResponseHeaderFieldsBuilder<B extends ResponseHeaderFieldsBuilder<?>> {

		/**
		 * Sets the response Header-Fields to use and returns this builder as of
		 * the Builder-Pattern.
		 * 
		 * @param aResponseHeaderFields The response Header-Fields to be stored
		 *        by the Header-Fields property.
		 * 
		 * @return This {@link ResponseHeaderFieldsBuilder} instance to continue
		 *         configuration.
		 */
		B withResponseHeaderFields( ResponseHeaderFields aResponseHeaderFields );
	}

	/**
	 * Provides an response Header-Fields property.
	 */
	public interface ResponseHeaderFieldsProperty extends ResponseHeaderFieldsAccessor, ResponseHeaderFieldsMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link ResponseHeaderFields} (setter) as of
		 * {@link #setResponseHeaderFields(ResponseHeaderFields)} and returns
		 * the very same value (getter).
		 * 
		 * @param aResponseHeaderFields The {@link ResponseHeaderFields} to set
		 *        (via {@link #setResponseHeaderFields(ResponseHeaderFields)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ResponseHeaderFields letResponseHeaderFields( ResponseHeaderFields aResponseHeaderFields ) {
			setResponseHeaderFields( aResponseHeaderFields );
			return aResponseHeaderFields;
		}
	}
}
