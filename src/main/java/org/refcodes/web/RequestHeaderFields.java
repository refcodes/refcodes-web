// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import java.util.List;
import java.util.Map;

/**
 * The {@link RequestHeaderFields} reflect the structure of a HTTP-Header and
 * may be used to represent a HTTP-Header.
 */
public class RequestHeaderFields extends AbstractHeaderFields<RequestCookie, RequestHeaderFields> {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new request Header-Fields impl.
	 */
	public RequestHeaderFields() {}

	/**
	 * Instantiates a new request Header-Fields impl.
	 *
	 * @param aFromHeaderFields the from Header-Fields
	 */
	public RequestHeaderFields( Map<String, List<String>> aFromHeaderFields ) {
		super( aFromHeaderFields );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RequestCookie addCookie( String aHttpCookie ) {
		return addCookie( new RequestCookie() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getCookieFieldName() {
		return HeaderField.COOKIE.getName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected RequestCookie createCookie( String aCookieName, String aValue ) {
		return new RequestCookie( aCookieName, aValue );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected RequestCookie createCookie( String aHttpCookie ) {
		return new RequestCookie( aHttpCookie );
	}
}
