// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import org.refcodes.mixin.Validatable;

/**
 * The Interface AuthTypeCredentials.
 *
 * @param <T> The implementing type to be e.g. returned by according builder
 *        methods and Super-Type of V.
 * @param <V> The lest common super-type of T which can be used to validate
 *        credentials.
 */
public interface AuthTypeCredentials<T extends AuthTypeCredentials<T, V>, V extends Validatable<V>> extends Validatable<V>, AuthTypeAccessor {

	/**
	 * Creates a {@link HeaderField#AUTHORIZATION} HTTP Header-Field value from
	 * the {@link AuthTypeCredentials} instance.
	 * 
	 * @return The according HTTP Header-Field's value.
	 */
	String toHttpAuthorization();

	/**
	 * Initializes this {@link AuthTypeCredentials} instance from the
	 * {@link HeaderField#AUTHORIZATION} Header-Fields's value.
	 * 
	 * @param aHttpAuthorization The {@link HeaderField#AUTHORIZATION} header
	 *        field's value
	 * 
	 * @throws IllegalArgumentException The value does not conform the the HTTP
	 *         Authorization Header-Field's Basic-Authentication format.
	 */
	void fromHttpAuthorization( String aHttpAuthorization );

	/**
	 * Initializes this {@link AuthTypeCredentials} instance from the
	 * {@link HeaderField#AUTHORIZATION} Header-Fields's value.
	 *
	 * @param aHttpAuthorization The {@link HeaderField#AUTHORIZATION} header
	 *        field's value
	 * 
	 * @return the t
	 * 
	 * @throws IllegalArgumentException The value does not conform the the HTTP
	 *         Authorization Header-Field's Basic-Authentication format.
	 */
	T withHttpAuthorization( String aHttpAuthorization );

	/**
	 * Validates the provided {@link BasicCredentials} with this
	 * {@link BasicAuthCredentials} via {@link #isValid(Object)}. In case the
	 * provided {@link BasicCredentials} are not valid, then a
	 * {@link ForbiddenException} is thrown.
	 * 
	 * @param aCredentials The {@link BasicCredentials} to be verified.
	 * 
	 * @throws ForbiddenException thrown in case the provided
	 *         {@link BasicCredentials} do not match.
	 */
	void validate( V aCredentials ) throws ForbiddenException;
}
