// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * The Class UnassignedStatusCodeException.
 */
public class UnassignedStatusCodeException extends HttpStatusException {

	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new according exception.
	 * 
	 * @param aMessage The aMessage describing this exception.
	 * @param aHttpStatusCode The HTTP-Status-Code in question.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public UnassignedStatusCodeException( String aMessage, int aHttpStatusCode, String aErrorCode ) {
		super( aMessage, HttpStatusCode.toHttpStatusCode( aHttpStatusCode ), aErrorCode );
	}

	/**
	 * Instantiates a new according exception.
	 * 
	 * @param aMessage The aMessage describing this exception.
	 * @param aHttpStatusCode The HTTP-Status-Code in question.
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public UnassignedStatusCodeException( String aMessage, int aHttpStatusCode, Throwable aCause, String aErrorCode ) {
		super( aMessage, HttpStatusCode.toHttpStatusCode( aHttpStatusCode ), aCause, aErrorCode );
	}

	/**
	 * Instantiates a new according exception.
	 * 
	 * @param aMessage The aMessage describing this exception.
	 * @param aHttpStatusCode The HTTP-Status-Code in question.
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 */
	public UnassignedStatusCodeException( String aMessage, int aHttpStatusCode, Throwable aCause ) {
		super( aMessage, HttpStatusCode.toHttpStatusCode( aHttpStatusCode ), aCause );
	}

	/**
	 * Instantiates a new according exception.
	 * 
	 * @param aMessage The aMessage describing this exception.
	 * @param aHttpStatusCode The HTTP-Status-Code in question.
	 */
	public UnassignedStatusCodeException( String aMessage, int aHttpStatusCode ) {
		super( aMessage, HttpStatusCode.toHttpStatusCode( aHttpStatusCode ) );
	}

	/**
	 * Instantiates a new according exception.
	 * 
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 * @param aHttpStatusCode The HTTP-Status-Code in question.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public UnassignedStatusCodeException( Throwable aCause, int aHttpStatusCode, String aErrorCode ) {
		super( HttpStatusCode.toHttpStatusCode( aHttpStatusCode ), aCause, aErrorCode );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aHttpStatusCode The HTTP-Status-Code in question.
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 */
	public UnassignedStatusCodeException( int aHttpStatusCode, Throwable aCause ) {
		super( HttpStatusCode.toHttpStatusCode( aHttpStatusCode ), aCause );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aMessage The aMessage describing this exception.
	 * @param aUrl The URL involved in this exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public UnassignedStatusCodeException( String aMessage, int aHttpStatusCode, Url aUrl, String aErrorCode ) {
		super( aMessage, HttpStatusCode.toHttpStatusCode( aHttpStatusCode ), aUrl, aErrorCode );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aMessage The aMessage describing this exception.
	 * @param aUrl The URL involved in this exception.
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public UnassignedStatusCodeException( String aMessage, int aHttpStatusCode, Url aUrl, Throwable aCause, String aErrorCode ) {
		super( aMessage, HttpStatusCode.toHttpStatusCode( aHttpStatusCode ), aUrl, aCause, aErrorCode );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aMessage The aMessage describing this exception.
	 * @param aUrl The URL involved in this exception.
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 */
	public UnassignedStatusCodeException( String aMessage, int aHttpStatusCode, Url aUrl, Throwable aCause ) {
		super( aMessage, HttpStatusCode.toHttpStatusCode( aHttpStatusCode ), aUrl, aCause );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aMessage The aMessage describing this exception.
	 * @param aUrl The URL involved in this exception.
	 */
	public UnassignedStatusCodeException( String aMessage, int aHttpStatusCode, Url aUrl ) {
		super( aMessage, HttpStatusCode.toHttpStatusCode( aHttpStatusCode ), aUrl );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aUrl The URL involved in this exception.
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public UnassignedStatusCodeException( int aHttpStatusCode, Url aUrl, Throwable aCause, String aErrorCode ) {
		super( HttpStatusCode.toHttpStatusCode( aHttpStatusCode ), aUrl, aCause, aErrorCode );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aUrl The URL involved in this exception.
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 */
	public UnassignedStatusCodeException( int aHttpStatusCode, Url aUrl, Throwable aCause ) {
		super( HttpStatusCode.toHttpStatusCode( aHttpStatusCode ), aUrl, aCause );
	}
}
