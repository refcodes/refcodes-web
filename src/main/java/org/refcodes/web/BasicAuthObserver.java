// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import java.net.InetSocketAddress;

/**
 * An observer being notified by incoming HTTP Basic-Authentication requests
 * which might have been registered via
 * {@link BasicAuthObservable#onBasicAuthRequest(BasicAuthObserver)} possibly
 * using lambda syntax.
 */
@FunctionalInterface
public interface BasicAuthObserver {

	/**
	 * Invoked upon an incoming HTTP Basic-Authentication requests. The user
	 * name and the password (in case being provided by the client) may be
	 * accepted or rejected by returning
	 * {@link BasicAuthResponse#BASIC_AUTH_SUCCESS} of
	 * {@link BasicAuthResponse#BASIC_AUTH_FAILURE}.
	 * 
	 * @param aLocalAddress The {@link InetSocketAddress} of the receiver of the
	 *        request.
	 * @param aRemoteAddress The remote client's {@link InetSocketAddress}.
	 * @param aHttpMethod The HTTP-Request method involved in the basic
	 *        authentication request.
	 * @param aLocator The locator of the request.
	 * @param aCredentials The name for which to grant access.
	 * @param aRealm The realm to which this request belongs.
	 * 
	 * @return {@link BasicAuthResponse#BASIC_AUTH_SUCCESS} in case HTTP basic
	 *         authentication (user and password) is accepted,
	 *         {@link BasicAuthResponse#BASIC_AUTH_FAILURE} if HTTP basic
	 *         authentication (user and password) is rejected.
	 */
	BasicAuthResponse onBasicAuthRequest( InetSocketAddress aLocalAddress, InetSocketAddress aRemoteAddress, HttpMethod aHttpMethod, String aLocator, BasicAuthCredentials aCredentials, String aRealm );

}
