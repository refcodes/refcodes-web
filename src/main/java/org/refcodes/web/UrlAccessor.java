// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides an accessor for a {@link Url} property..
 */
public interface UrlAccessor {

	/**
	 * Retrieves the {@link Url} from the {@link Url} property.
	 * 
	 * @return The Url stored by the {@link Url} property.
	 */
	Url getUrl();

	/**
	 * Provides a mutator for a {@link Url} property.
	 */
	public interface UrlMutator {
		/**
		 * Sets the Url for the {@link Url} property.
		 * 
		 * @param aUrl The Url to be stored by the {@link Url} property.
		 */
		void setUrl( Url aUrl );
	}

	/**
	 * Provides a builder method for a {@link Url} property returning the
	 * builder for applying multiple build operations.
	 */
	public interface UrlBuilder {

		/**
		 * Sets the Url for the {@link Url} property.
		 * 
		 * @param aUrl The Url to be stored by the {@link Url} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		UrlBuilder withUrl( Url aUrl );
	}

	/**
	 * Provides a {@link Url} property.
	 */
	public interface UrlProperty extends UrlAccessor, UrlMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Url} (setter) as
		 * of {@link #setUrl(Url)} and returns the very same value (getter).
		 * 
		 * @param aUrl The {@link Url} to set (via {@link #setUrl(Url)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Url letUrl( Url aUrl ) {
			setUrl( aUrl );
			return aUrl;
		}
	}
}
