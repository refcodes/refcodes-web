// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides an accessor for a {@link BearerAuthCredentials} property.
 */
public interface BearerAuthCredentialsAccessor {

	/**
	 * Retrieves the {@link BearerAuthCredentials} from the bearer auth
	 * credentials property (or null if there are none such credentials).
	 * 
	 * @return The {@link BearerAuthCredentials} stored by the bearer auth
	 *         credentials property (or null if there are none such
	 *         credentials).
	 */
	BearerAuthCredentials getBearerAuthCredentials();

	/**
	 * Provides a mutator for a {@link BearerAuthCredentials} property.
	 */
	public interface BearerAuthCredentialsMutator {

		/**
		 * Sets the {@link BearerAuthCredentials} for the bearer auth
		 * credentials property.
		 * 
		 * @param aBearerAuthCredentials The {@link BearerAuthCredentials} to be
		 *        stored by the {@link BearerAuthCredentials} property.
		 */
		void setBearerAuthCredentials( BearerAuthCredentials aBearerAuthCredentials );

		/**
		 * Sets the {@link BearerAuthCredentials} from the token for the bearer
		 * auth credentials property.
		 * 
		 * @param aBearerAuthCredentials The password to be stored by the
		 *        {@link BearerAuthCredentials} property.
		 */
		default void setBearerAuthCredentials( String aBearerAuthCredentials ) {
			setBearerAuthCredentials( new BearerAuthCredentials( aBearerAuthCredentials ) );
		}
	}

	/**
	 * Provides a builder method for a {@link BearerAuthCredentials} property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface BearerAuthCredentialsBuilder<B extends BearerAuthCredentialsBuilder<B>> {

		/**
		 * Sets the {@link BearerAuthCredentials} for the bearer auth
		 * credentials property.
		 * 
		 * @param aBearerAuthCredentials The {@link BearerAuthCredentials} to be
		 *        stored by the {@link BearerAuthCredentials} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withBearerAuthCredentials( BearerAuthCredentials aBearerAuthCredentials );

		/**
		 * Sets the {@link BearerAuthCredentials} from the token for the bearer
		 * auth credentials property.
		 * 
		 * @param aBearerAuthCredentials The password to be stored by the
		 *        {@link BearerAuthCredentials} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withBearerAuthCredentials( String aBearerAuthCredentials );
	}

	/**
	 * Provides a {@link BearerAuthCredentials} property.
	 */
	public interface BearerAuthCredentialsProperty extends BearerAuthCredentialsAccessor, BearerAuthCredentialsMutator {

		/**
		 * Sets the {@link BearerAuthCredentials} for the bearer auth
		 * credentials property.
		 * 
		 * @param aBearerAuthCredentials The {@link BearerAuthCredentials} to be
		 *        stored by the {@link BearerAuthCredentials} property.
		 * 
		 * @return The previously set {@link BearerAuthCredentials} or null if
		 *         none have been set.
		 */
		default BearerAuthCredentials letBearerAuthCredentials( BearerAuthCredentials aBearerAuthCredentials ) {
			setBearerAuthCredentials( aBearerAuthCredentials );
			return aBearerAuthCredentials;
		}

		/**
		 * Sets the {@link BearerAuthCredentials} from the token for the bearer
		 * auth credentials property.
		 * 
		 * @param aBearerAuthCredentials The password to be stored by the
		 *        {@link BearerAuthCredentials} property.
		 * 
		 * @return The previously set {@link BearerAuthCredentials} or null if
		 *         none have been set.
		 */
		default String letBearerAuthCredentials( String aBearerAuthCredentials ) {
			setBearerAuthCredentials( aBearerAuthCredentials );
			return aBearerAuthCredentials;
		}
	}
}
