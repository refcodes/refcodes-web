// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import java.net.MalformedURLException;
import java.net.URL;

import org.refcodes.data.Scheme;

/**
 * Declarative syntactic sugar which may be statically imported in order to
 * allow declarative definitions for {@link Url} ({@link UrlBuilder}) creation
 * and tweaking: <code>import static org.refcodes.web.UrlSugar.*;</code>
 */
public class UrlSugar {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates an empty {@link UrlBuilder}, make sure to set required attributes
	 * for a valid URL.
	 * 
	 * @return The accordingly constructed {@link UrlBuilder} instance.
	 */
	public UrlBuilder toUrl() {
		return new UrlBuilder();
	}

	/**
	 * Constructs an {@link UrlBuilder} from the provided URL {@link String}.
	 * 
	 * @param aUrl The URL {@link String} to be parsed.
	 * 
	 * @return The accordingly constructed {@link UrlBuilder} instance.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	public UrlBuilder toUrl( String aUrl ) throws MalformedURLException {
		return new UrlBuilder( aUrl );
	}

	/**
	 * Constructs an {@link UrlBuilder} from the provided {@link URL} instance.
	 * 
	 * @param aURL The {@link URL} to be used.
	 * 
	 * @return The accordingly constructed {@link UrlBuilder} instance.
	 */
	public UrlBuilder toUrl( URL aURL ) {
		return new UrlBuilder( aURL );
	}

	/**
	 * Constructs an {@link UrlBuilder} with the common attributes.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * 
	 * @return The accordingly constructed {@link UrlBuilder} instance.
	 */
	public UrlBuilder toUrl( Scheme aScheme, String aHost ) {
		return new UrlBuilder( aScheme, aHost );
	}

	/**
	 * Constructs an {@link UrlBuilder} with the common attributes.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * 
	 * @return The accordingly constructed {@link UrlBuilder} instance.
	 */
	public UrlBuilder toUrl( Scheme aScheme, String aHost, int aPort ) {
		return new UrlBuilder( aScheme, aHost, aPort );
	}

	/**
	 * Constructs an {@link UrlBuilder} with the common attributes.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The accordingly constructed {@link UrlBuilder} instance.
	 */
	public UrlBuilder toUrl( Scheme aScheme, String aHost, int aPort, String aPath ) {
		return new UrlBuilder( aScheme, aHost, aPort, aPath );
	}

	/**
	 * Constructs an {@link UrlBuilder} with the common attributes.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The accordingly constructed {@link UrlBuilder} instance.
	 */
	public UrlBuilder toUrl( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields ) {
		return new UrlBuilder( aScheme, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * Constructs an {@link UrlBuilder} with the common attributes.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aFragment The fragment to be set.
	 * 
	 * @return The accordingly constructed {@link UrlBuilder} instance.
	 */
	public UrlBuilder toUrl( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, String aFragment ) {
		return new UrlBuilder( aScheme, aHost, aPort, aPath, aQueryFields, aFragment );
	}

	/**
	 * Constructs an {@link UrlBuilder} with the common attributes.
	 * 
	 * @param aProtocol The protocol {@link String} (e.g. "http://" or
	 *        "https://") to be used for the destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * 
	 * @return The accordingly constructed {@link UrlBuilder} instance.
	 */
	public UrlBuilder toUrl( String aProtocol, String aHost ) {
		return new UrlBuilder( aProtocol, aHost );
	}

	/**
	 * Constructs an {@link UrlBuilder} with the common attributes.
	 * 
	 * @param aProtocol The protocol {@link String} (e.g. "http://" or
	 *        "https://") to be used for the destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * 
	 * @return The accordingly constructed {@link UrlBuilder} instance.
	 */
	public UrlBuilder toUrl( String aProtocol, String aHost, int aPort ) {
		return new UrlBuilder( aProtocol, aHost, aPort );
	}

	/**
	 * Constructs an {@link UrlBuilder} with the common attributes.
	 * 
	 * @param aProtocol The protocol {@link String} (e.g. "http://" or
	 *        "https://") to be used for the destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The accordingly constructed {@link UrlBuilder} instance.
	 */
	public UrlBuilder toUrl( String aProtocol, String aHost, int aPort, String aPath ) {
		return new UrlBuilder( aProtocol, aHost, aPort, aPath );
	}

	/**
	 * Constructs an {@link UrlBuilder} with the common attributes.
	 * 
	 * @param aProtocol The protocol {@link String} (e.g. "http://" or
	 *        "https://") to be used for the destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The accordingly constructed {@link UrlBuilder} instance.
	 */
	public UrlBuilder toUrl( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields ) {
		return new UrlBuilder( aProtocol, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * Constructs an {@link UrlBuilder} with the common attributes.
	 * 
	 * @param aProtocol The protocol {@link String} (e.g. "http://" or
	 *        "https://") to be used for the destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPort The port to be used when connecting to the host.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aFragment The fragment to be set.
	 * 
	 * @return The accordingly constructed {@link UrlBuilder} instance.
	 */
	public UrlBuilder toUrl( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, String aFragment ) {
		return new UrlBuilder( aProtocol, aHost, aPort, aPath, aQueryFields, aFragment );
	}

	/**
	 * Constructs an {@link UrlBuilder} with the common attributes.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The accordingly constructed {@link UrlBuilder} instance.
	 */
	public UrlBuilder toUrl( Scheme aScheme, String aHost, String aPath ) {
		return new UrlBuilder( aScheme, aHost, aPath );
	}

	/**
	 * Constructs an {@link UrlBuilder} with the common attributes.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The accordingly constructed {@link UrlBuilder} instance.
	 */
	public UrlBuilder toUrl( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields ) {
		return new UrlBuilder( aScheme, aHost, aPath, aQueryFields );
	}

	/**
	 * Constructs an {@link UrlBuilder} with the common attributes.
	 * 
	 * @param aScheme The {@link Scheme} (e.g. HTTP or HTTPS) to be used for the
	 *        destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aFragment The fragment to be set.
	 * 
	 * @return The accordingly constructed {@link UrlBuilder} instance.
	 */
	public UrlBuilder toUrl( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, String aFragment ) {
		return new UrlBuilder( aScheme, aHost, aPath, aQueryFields, aFragment );
	}

	/**
	 * Constructs an {@link UrlBuilder} with the common attributes.
	 * 
	 * @param aProtocol The protocol {@link String} (e.g. "http://" or
	 *        "https://") to be used for the destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * 
	 * @return The accordingly constructed {@link UrlBuilder} instance.
	 */
	public UrlBuilder toUrl( String aProtocol, String aHost, String aPath ) {
		return new UrlBuilder( aProtocol, aHost, aPath );
	}

	/**
	 * Constructs an {@link UrlBuilder} with the common attributes.
	 * 
	 * @param aProtocol The protocol {@link String} (e.g. "http://" or
	 *        "https://") to be used for the destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * 
	 * @return The accordingly constructed {@link UrlBuilder} instance.
	 */
	public UrlBuilder toUrl( String aProtocol, String aHost, String aPath, FormFields aQueryFields ) {
		return new UrlBuilder( aProtocol, aHost, aPath, aQueryFields );
	}

	/**
	 * Constructs an {@link UrlBuilder} with the common attributes.
	 * 
	 * @param aProtocol The protocol {@link String} (e.g. "http://" or
	 *        "https://") to be used for the destination URL.
	 * @param aHost The host to which the destination URL is to point to.
	 * @param aPath The path on the host to which the base destination URL is to
	 *        point to.
	 * @param aQueryFields The Query-Fields to be used for the HTTP
	 *        Query-String.
	 * @param aFragment The fragment to be set.
	 * 
	 * @return The accordingly constructed {@link UrlBuilder} instance.
	 */
	public UrlBuilder toUrl( String aProtocol, String aHost, String aPath, FormFields aQueryFields, String aFragment ) {
		return new UrlBuilder( aProtocol, aHost, aPath, aQueryFields, aFragment );
	}
}
