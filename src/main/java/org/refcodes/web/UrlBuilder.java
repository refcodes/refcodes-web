// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import java.net.MalformedURLException;
import java.net.URL;

import org.refcodes.data.Scheme;
import org.refcodes.mixin.CredentialsAccessor.CredentialsBuilder;
import org.refcodes.mixin.CredentialsAccessor.CredentialsProperty;
import org.refcodes.mixin.PathAccessor.PathBuilder;
import org.refcodes.mixin.PathAccessor.PathProperty;
import org.refcodes.mixin.PortAccessor.PortBuilder;
import org.refcodes.mixin.PortAccessor.PortProperty;
import org.refcodes.net.IpAddressAccessor.IpAddressBuilder;
import org.refcodes.net.IpAddressAccessor.IpAddressProperty;
import org.refcodes.web.FragmentAccessor.FragmentBuilder;
import org.refcodes.web.FragmentAccessor.FragmentProperty;
import org.refcodes.web.HostAccessor.HostBuilder;
import org.refcodes.web.HostAccessor.HostProperty;
import org.refcodes.web.QueryFieldsAccessor.QueryFieldsBuilder;
import org.refcodes.web.QueryFieldsAccessor.QueryFieldsProperty;
import org.refcodes.web.SchemeAccessor.SchemeBuilder;
import org.refcodes.web.SchemeAccessor.SchemeProperty;

/**
 * The {@link UrlBuilder} extends an immutable {@link Url} with mutable
 * functionality.
 */
public class UrlBuilder extends Url implements SchemeProperty, SchemeBuilder<UrlBuilder>, HostProperty, HostBuilder<UrlBuilder>, IpAddressProperty, IpAddressBuilder<UrlBuilder>, PortProperty, PortBuilder<UrlBuilder>, PathProperty, PathBuilder<UrlBuilder>, QueryFieldsProperty, QueryFieldsBuilder<UrlBuilder>, FragmentProperty, FragmentBuilder<UrlBuilder>, CredentialsProperty, CredentialsBuilder<UrlBuilder> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder() {}

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder( Url aUrl ) {
		super( aUrl );
	}

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder( String aUrl ) throws MalformedURLException {
		super( aUrl );
	}

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder( String aUrl, FormFields aQueryFields ) throws MalformedURLException {
		super( aUrl, aQueryFields );
	}

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder( String aUrl, FormFields aQueryFields, String aFragment ) throws MalformedURLException {
		super( aUrl, aQueryFields, aFragment );
	}

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder( URL aURL ) {
		super( aURL );
	}

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder( Scheme aScheme, String aHost ) {
		super( aScheme, aHost );
	}

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder( Scheme aScheme, String aHost, int aPort ) {
		super( aScheme, aHost, aPort );
	}

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields, String aFragment ) {
		super( aScheme, aHost, aPort, aPath, aQueryFields, aFragment );
	}

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder( Scheme aScheme, String aHost, int aPort, String aPath, FormFields aQueryFields ) {
		super( aScheme, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder( Scheme aScheme, String aHost, int aPort, String aPath ) {
		super( aScheme, aHost, aPort, aPath );
	}

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields, String aFragment ) {
		super( aScheme, aHost, aPath, aQueryFields, aFragment );
	}

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder( Scheme aScheme, String aHost, String aPath, FormFields aQueryFields ) {
		super( aScheme, aHost, aPath, aQueryFields );
	}

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder( Scheme aScheme, String aHost, String aPath ) {
		super( aScheme, aHost, aPath );
	}

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields, String aFragment ) {
		super( aProtocol, aHost, aPort, aPath, aQueryFields, aFragment );
	}

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder( String aProtocol, String aHost, int aPort, String aPath, FormFields aQueryFields ) {
		super( aProtocol, aHost, aPort, aPath, aQueryFields );
	}

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder( String aProtocol, String aHost, int aPort, String aPath ) {
		super( aProtocol, aHost, aPort, aPath );
	}

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder( String aProtocol, String aHost, int aPort ) {
		super( aProtocol, aHost, aPort );
	}

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder( String aProtocol, String aHost, String aPath, FormFields aQueryFields, String aFragment ) {
		super( aProtocol, aHost, aPath, aQueryFields, aFragment );
	}

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder( String aProtocol, String aHost, String aPath, FormFields aQueryFields ) {
		super( aProtocol, aHost, aPath, aQueryFields );
	}

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder( String aProtocol, String aHost, String aPath ) {
		super( aProtocol, aHost, aPath );
	}

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder( String aProtocol, String aHost ) {
		super( aProtocol, aHost );
	}

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder( Url aUrl, String... aPaths ) {
		super( aUrl, aPaths );
	}

	/**
	 * {@inheritDoc}
	 */
	public UrlBuilder( Url aUrl, Url aOtherUrl ) {
		super( aUrl, aOtherUrl );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Uses the given {@link String} to set the {@link Url}'s state.
	 * 
	 * @param aUrl The URL from which to determine the state.
	 * 
	 * @throws MalformedURLException in case the provided URL is considered
	 *         being malformed.
	 */
	@Override
	public void fromUrl( String aUrl ) throws MalformedURLException {
		super.fromUrl( aUrl );
	}

	/**
	 * Uses the given {@link URL} to set the {@link Url}'s state.
	 * 
	 * @param aUrl The {@link URL} from which to determine the state.
	 */
	@Override
	public void fromURL( URL aUrl ) {
		super.fromURL( aUrl );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UrlBuilder withScheme( Scheme aScheme ) {
		setScheme( aScheme );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UrlBuilder withProtocol( String aProtocol ) {
		setProtocol( aProtocol );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UrlBuilder withHost( String aHost ) {
		setHost( aHost );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UrlBuilder withIpAddress( int[] aIpAddress ) {
		setIpAddress( aIpAddress );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UrlBuilder withCidrNotation( String aCidrNotation ) {
		fromCidrNotation( aCidrNotation );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UrlBuilder withPort( int aPort ) {
		setPort( aPort );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UrlBuilder withPath( String aPath ) {
		setPath( aPath );
		return this;
	}

	/**
	 * Appends a path element to the path.
	 *
	 * @param aPathElement The path element to be appended.
	 */
	public void appendToPath( String aPathElement ) {
		// Normalize path |-->
		String thePath = getPath();
		if ( thePath != null ) {
			while ( thePath.length() != 0 && thePath.endsWith( "/" ) ) {
				thePath = thePath.substring( 0, thePath.length() - 1 );
			}
		}
		else {
			thePath = "";
		}
		// Normalize path <--|

		// Normalize path element |-->
		if ( aPathElement != null ) {
			while ( aPathElement.endsWith( "/" ) ) {
				aPathElement = aPathElement.substring( 0, aPathElement.length() - 1 );
			}
			while ( aPathElement.startsWith( "/" ) ) {
				aPathElement = aPathElement.substring( 1, aPathElement.length() );
			}
		}
		else {
			aPathElement = "";
		}
		// Normalize path element <--|

		if ( aPathElement.length() != 0 ) {
			thePath = thePath + "/" + aPathElement;
		}
		setPath( thePath.isEmpty() ? "/" : thePath );
	}

	/**
	 * Appends multiple path elements to the path, automatically adding the path
	 * separator "/" as required.
	 *
	 * @param aPathElements The path elements to be appended.
	 */
	public void appendToPath( String... aPathElements ) {
		// Normalize path |-->
		String thePath = getPath();
		if ( thePath != null ) {
			while ( thePath.length() != 0 && thePath.endsWith( "/" ) ) {
				thePath = thePath.substring( 0, thePath.length() - 1 );
			}
		}
		else {
			thePath = "";
		}
		// Normalize path <--|

		// Normalize + append path elements |-->
		String ePathElement;
		for ( String aPathElement : aPathElements ) {
			ePathElement = aPathElement;
			if ( ePathElement != null ) {
				while ( ePathElement.endsWith( "/" ) ) {
					ePathElement = ePathElement.substring( 0, ePathElement.length() - 1 );
				}
				while ( ePathElement.startsWith( "/" ) ) {
					ePathElement = ePathElement.substring( 1, ePathElement.length() );
				}
			}
			else {
				ePathElement = "";
			}
			if ( ePathElement.length() != 0 ) {
				thePath = thePath + "/" + ePathElement;
			}
		}
		// Normalize + append path element <--|

		setPath( thePath.isEmpty() ? "/" : thePath );
	}

	/**
	 * Appends a path element to the path as of the Builder-Pattern.
	 *
	 * @param aPath The path element to be appended.
	 * 
	 * @return This {@link UrlBuilder} instance to continue configuration.
	 */
	public UrlBuilder withAppendToPath( String aPath ) {
		appendToPath( aPath );
		return this;
	}

	/**
	 * Appends multiple path elements to the path, automatically adding the path
	 * separator "/" as required as of the Builder-Pattern.
	 *
	 * @param aPathElements The path elements to be appended.
	 * 
	 * @return This {@link UrlBuilder} instance to continue configuration.
	 */
	public UrlBuilder withAppendToPath( String... aPathElements ) {
		appendToPath( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UrlBuilder withQueryFields( FormFields aQueryFields ) {
		setQueryFields( aQueryFields );
		return this;
	}

	/**
	 * Adds a value to the list of values associated with the given Query-Field
	 * (key).
	 *
	 * @param aField The Query-Field (key) of which's list of values a value is
	 *        to be added.
	 * @param aValue The value to be added to the list of values associated to
	 *        the given Query-Field (key).
	 */
	public void addToQueryFields( String aField, String aValue ) {
		FormFields theQueryFields = getQueryFields();
		if ( theQueryFields == null ) {
			synchronized ( this ) {
				if ( theQueryFields == null ) {
					theQueryFields = new FormFields();
					setQueryFields( theQueryFields );
				}
			}
		}
		theQueryFields.addTo( aField, aValue );
	}

	/**
	 * Adds values to the list of values associated with the given Query-Field
	 * (key).
	 *
	 * @param aField The Query-Field (key) of which's list of values the values
	 *        are to be added.
	 * @param aValues The values to be added to the list of values associated to
	 *        the given Query-Field (key).
	 */
	public void addToQueryFields( String aField, String... aValues ) {
		FormFields theQueryFields = getQueryFields();
		if ( theQueryFields == null ) {
			synchronized ( this ) {
				if ( theQueryFields == null ) {
					theQueryFields = new FormFields();
					setQueryFields( theQueryFields );
				}
			}
		}
		theQueryFields.addTo( aField, aValues );
	}

	/**
	 * Adds a value to the list of values associated with the given Query-Field
	 * (key) and returns this builder as of the Builder-Pattern.
	 *
	 * @param aField The Query-Field (key) of which's list of values a value is
	 *        to be added.
	 * @param aValue The value to be added to the list of values associated to
	 *        the given Query-Field (key).
	 * 
	 * @return This {@link UrlBuilder} instance to continue configuration.
	 */
	public UrlBuilder withAddToQueryFields( String aField, String aValue ) {
		addToQueryFields( aField, aValue );
		return this;
	}

	/**
	 * Adds values to the list of values associated with the given Query-Field
	 * (key) and returns this builder as of the Builder-Pattern.
	 *
	 * @param aField The Query-Field (key) of which's list of values the values
	 *        are to be added.
	 * @param aValues The values to be added to the list of values associated to
	 *        the given Query-Field (key).
	 * 
	 * @return This {@link UrlBuilder} instance to continue configuration.
	 */
	public UrlBuilder withAddToQueryFields( String aField, String... aValues ) {
		addToQueryFields( aField, aValues );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UrlBuilder withFragment( String aFragment ) {
		setFragment( aFragment );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UrlBuilder withIdentity( String aIdentity ) {
		setIdentity( aIdentity );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UrlBuilder withSecret( String aSecret ) {
		setSecret( aSecret );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setScheme( Scheme aScheme ) {
		super.setScheme( aScheme );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setProtocol( String aProtocol ) {
		super.setProtocol( aProtocol );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHost( String aHost ) {
		super.setHost( aHost );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setIpAddress( int[] aIpAddress ) {
		super.setIpAddress( aIpAddress );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPort( int aPort ) {
		super.setPort( aPort );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPath( String aPath ) {
		super.setPath( aPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setQueryFields( FormFields aQueryFields ) {
		_queryFields = aQueryFields;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFragment( String aFragment ) {
		_fragment = aFragment;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setIdentity( String aIdentity ) {
		_identity = aIdentity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setSecret( String aSecret ) {
		_secret = aSecret;
	}

	@Override
	public String toString() {
		return toHttpUrl();
	}
}