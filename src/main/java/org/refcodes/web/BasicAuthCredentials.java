// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

import org.refcodes.data.Encoding;

/**
 * The {@link BasicAuthCredentials} defines a type for representing a
 * Basic-Authentication credentials Header-Field. Parse the Header-Field via
 * {@link #fromHttpAuthorization(String)} and feed it with the header's
 * {@link HeaderField#AUTHORIZATION} field's value. To create the according
 * Header-Field's value from the {@link BasicAuthCredentials} type, call
 * {@link #toHttpAuthorization()}.
 */
public class BasicAuthCredentials extends BasicCredentials implements AuthTypeCredentials<BasicAuthCredentials, BasicCredentials> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final char DELIMITER_BASIC_AUTH = ' ';
	public static final char DELIMITER_CREDENTIALS = ':';

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link BasicAuthCredentials} instance.
	 */
	public BasicAuthCredentials() {}

	/**
	 * Instantiates a new {@link BasicAuthCredentials} instance.
	 *
	 * @param aIdentity the user name
	 * @param aSecret the secret
	 */
	public BasicAuthCredentials( String aIdentity, String aSecret ) {
		super( aIdentity, aSecret );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Validates the provided {@link BasicCredentials} with this
	 * {@link BasicAuthCredentials} via {@link #isValid(BasicCredentials)}. In
	 * case the provided {@link BasicCredentials} are not valid, then a
	 * {@link ForbiddenException} is thrown.
	 * 
	 * @param aCredentials The {@link BasicCredentials} to be verified.
	 * 
	 * @throws ForbiddenException thrown in case the provided
	 *         {@link BasicCredentials} do not match.
	 */
	@Override
	public void validate( BasicCredentials aCredentials ) throws ForbiddenException {
		if ( !isValid( aCredentials ) ) {
			throw new ForbiddenException( "Access denied as of unsuccessful authentification!" );
		}
	}

	/**
	 * Validates the provided user-name and secret with this
	 * {@link BasicAuthCredentials} via {@link #isValid(String, String)}. In
	 * case the provided credentials are not valid, then a
	 * {@link ForbiddenException} is thrown.
	 * 
	 * @param aUserName The user-name part to be tested if it fits with the this
	 *        {@link BasicCredentials} instance.
	 * @param aSecret The secret part to be tested if it fits with the this
	 *        {@link BasicCredentials} instance.
	 * 
	 * @throws ForbiddenException thrown in case the provided
	 *         {@link BasicCredentials} do not match.
	 */
	public void validate( String aUserName, String aSecret ) throws ForbiddenException {
		if ( !isValid( aUserName, aSecret ) ) {
			throw new ForbiddenException( "Access denied as of unsuccessfull authentification!" );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BasicAuthCredentials withHttpAuthorization( String aHttpAuthorization ) {
		fromHttpAuthorization( aHttpAuthorization );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AuthType getAuthType() {
		return AuthType.BASIC;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getIdentity() {
		return _identity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getSecret() {
		return _secret;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( _secret == null ) ? 0 : _secret.hashCode() );
		result = prime * result + ( ( _identity == null ) ? 0 : _identity.hashCode() );
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( !( obj instanceof BasicAuthCredentials other ) ) {
			return false;
		}
		if ( _secret == null ) {
			if ( other._secret != null ) {
				return false;
			}
		}
		else if ( !_secret.equals( other._secret ) ) {
			return false;
		}
		if ( _identity == null ) {
			if ( other._identity != null ) {
				return false;
			}
		}
		else if ( !_identity.equalsIgnoreCase( other._identity ) ) {
			return false;
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "<" + _identity + "> with secret <...>";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void fromHttpAuthorization( String aHttpAuthorization ) {
		if ( aHttpAuthorization == null ) {
			throw new IllegalArgumentException( "The provided HTTP authorization value does not conform to the format expected for Basic-Authentication." );
		}
		int theMarker = aHttpAuthorization.indexOf( DELIMITER_BASIC_AUTH );
		if ( theMarker == -1 || !aHttpAuthorization.substring( 0, theMarker ).equals( AuthType.BASIC.getName() ) ) {
			throw new IllegalArgumentException( "The provided HTTP authorization value does not conform to the format expected for Basic-Authentication." );
		}
		final byte[] theCredentialChars = Base64.getDecoder().decode( aHttpAuthorization.substring( theMarker + 1 ) );
		String theCredentialsText;
		try {
			theCredentialsText = new String( theCredentialChars, Encoding.UTF_8.getCode() );
		}
		catch ( UnsupportedEncodingException e ) {
			theCredentialsText = new String( theCredentialChars );
		}
		theMarker = theCredentialsText.indexOf( DELIMITER_CREDENTIALS );
		_identity = theCredentialsText.substring( 0, theMarker );
		_secret = theCredentialsText.substring( theMarker + 1 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toHttpAuthorization() {
		String theCredentials = getIdentity() + DELIMITER_CREDENTIALS + getSecret();
		try {
			theCredentials = Base64.getEncoder().encodeToString( theCredentials.getBytes( Encoding.UTF_8.getCode() ) );
		}
		catch ( UnsupportedEncodingException e ) {
			theCredentials = Base64.getEncoder().encodeToString( theCredentials.getBytes() );
		}
		theCredentials = AuthType.BASIC.getName() + DELIMITER_BASIC_AUTH + theCredentials;
		return theCredentials;
	}
}
