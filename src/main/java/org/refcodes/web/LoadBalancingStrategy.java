// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Some simple load balancing strategies.
 */
public enum LoadBalancingStrategy {

	/**
	 * Succeeding requests are issued to succeeding servers, having used the
	 * last on in the list of applicable servers, then the procedure is
	 * continued with the first on in the list.
	 */
	ROUND_ROBIN,
	/**
	 * An server for a request is chosen at random from a list of applicable
	 * servers.
	 */
	RANDOM,
	/**
	 * Some custom strategy is being used.
	 */
	RANDOM_STICKY,
	/**
	 * Some custom server from the list of applicable servers is used for the
	 * request and all succeeding request..
	 */
	CUSTOM,
	/**
	 * No strategy is used. The first one in the list of applicable servers may
	 * be used.
	 */
	NONE

}
