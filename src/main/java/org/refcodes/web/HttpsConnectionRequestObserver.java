// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import java.net.InetSocketAddress;

import javax.net.ssl.SSLParameters;

/**
 * An observer being notified by incoming HTTPS requests which might have been
 * registered via
 * {@link HttpsConnectionRequestObservable#onConnectionRequest(HttpsConnectionRequestObserver)}
 * possibly using lambda syntax.
 */
@FunctionalInterface
public interface HttpsConnectionRequestObserver {

	/**
	 * Invoked upon an incoming HTTPS request. The {@link SSLParameters} can be
	 * modified according to the remote address issuing the request.
	 * 
	 * @param aLocalAddress The {@link InetSocketAddress} of the receiver of the
	 *        request.
	 * @param aRemoteAddress The remote client's {@link InetSocketAddress}.
	 * @param aHttpsParams The {@link SSLParameters} which may be modified in
	 *        accordance to the remote address.
	 */
	void onHttpsConnectionRequest( InetSocketAddress aLocalAddress, InetSocketAddress aRemoteAddress, SSLParameters aHttpsParams );

}
