package org.refcodes.web;

/**
 * Implementation of the {@link HttpRequest} interface.
 */
public abstract class AbstractHttpRequest implements HttpRequest {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected HttpMethod _httpMethod;
	protected Url _url;
	protected RequestHeaderFields _headerFields;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link HttpRequest} with all required attributes.
	 * 
	 * @param aHttpMethod The {@link HttpMethod} with which the request has been
	 *        sent.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 */
	public AbstractHttpRequest( HttpMethod aHttpMethod, Url aUrl ) {
		this( aHttpMethod, aUrl, new RequestHeaderFields() );
	}

	/**
	 * Constructs a {@link HttpRequest} with all required attributes.
	 * 
	 * @param aHttpMethod The {@link HttpMethod} with which the request has been
	 *        sent.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aHeaderFields The {@link RequestHeaderFields} sent by the request.
	 */
	public AbstractHttpRequest( HttpMethod aHttpMethod, Url aUrl, RequestHeaderFields aHeaderFields ) {
		if ( aUrl == null ) {
			throw new IllegalArgumentException( "The provided URL argument must not be null!" );
		}
		_httpMethod = aHttpMethod;
		_url = aUrl;
		_headerFields = aHeaderFields != null ? aHeaderFields : new RequestHeaderFields();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpMethod getHttpMethod() {
		return _httpMethod;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Url getUrl() {
		return _url;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RequestHeaderFields getHeaderFields() {
		return _headerFields;
	}
}
