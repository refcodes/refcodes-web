// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides an accessor for a Media-Top-Level-Type property.
 */
public interface TopLevelTypeAccessor {

	/**
	 * Retrieves the Media-Top-Level-Type from the Media-Top-Level-Type
	 * property.
	 * 
	 * @return The Media-Top-Level-Type stored by the Media-Top-Level-Type
	 *         property.
	 */
	TopLevelType getTopLevelType();

	/**
	 * Provides a mutator for a Media-Top-Level-Type property.
	 */
	public interface TopLevelTypeMutator {

		/**
		 * Sets the Media-Top-Level-Type for the Media-Top-Level-Type property.
		 * 
		 * @param aTopLevelType The Media-Top-Level-Type to be stored by the
		 *        Media-Top-Level-Type property.
		 */
		void setTopLevelType( TopLevelType aTopLevelType );
	}

	/**
	 * Provides a builder method for a Media-Top-Level-Type property returning
	 * the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface TopLevelTypeBuilder<B extends TopLevelTypeBuilder<B>> {

		/**
		 * Sets the Media-Top-Level-Type for the Media-Top-Level-Type property.
		 * 
		 * @param aTopLevelType The Media-Top-Level-Type to be stored by the
		 *        Media-Top-Level-Type property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withTopLevelType( TopLevelType aTopLevelType );
	}

	/**
	 * Provides a Media-Top-Level-Type property.
	 */
	public interface TopLevelTypeProperty extends TopLevelTypeAccessor, TopLevelTypeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link TopLevelType}
		 * (setter) as of {@link #setTopLevelType(TopLevelType)} and returns the
		 * very same value (getter).
		 * 
		 * @param aTopLevelType The {@link TopLevelType} to set (via
		 *        {@link #setTopLevelType(TopLevelType)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default TopLevelType letTopLevelType( TopLevelType aTopLevelType ) {
			setTopLevelType( aTopLevelType );
			return aTopLevelType;
		}
	}
}
