// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides an accessor for a {@link BasicAuthCredentials} property.
 */
public interface BasicAuthCredentialsAccessor {

	/**
	 * Retrieves the {@link BasicAuthCredentials} from the Basic-Authentication
	 * credentials property (or null if there are none such credentials).
	 * 
	 * @return The {@link BasicAuthCredentials} stored by the basic
	 *         authentication credentials property (or null if there are none
	 *         such credentials).
	 */
	BasicAuthCredentials getBasicAuthCredentials();

	/**
	 * Provides a mutator for a {@link BasicAuthCredentials} property.
	 */
	public interface BasicAuthCredentialsMutator {

		/**
		 * Sets the {@link BasicAuthCredentials} for the Basic-Authentication
		 * credentials property.
		 * 
		 * @param aBasicAuthCredentials The {@link BasicAuthCredentials} to be
		 *        stored by the {@link BasicAuthCredentials} property.
		 */
		void setBasicAuthCredentials( BasicAuthCredentials aBasicAuthCredentials );

		/**
		 * Sets the {@link BasicAuthCredentials} from the user name and the
		 * secret for the Basic-Authentication credentials property.
		 * 
		 * @param aUserName The user name to be stored by the
		 *        {@link BasicAuthCredentials} property.
		 * @param aSecret The password to be stored by the
		 *        {@link BasicAuthCredentials} property.
		 */
		default void setBasicAuthCredentials( String aUserName, String aSecret ) {
			setBasicAuthCredentials( new BasicAuthCredentials( aUserName, aSecret ) );
		}
	}

	/**
	 * Provides a builder method for a {@link BasicAuthCredentials} property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface BasicAuthCredentialsBuilder<B extends BasicAuthCredentialsBuilder<B>> {

		/**
		 * Sets the {@link BasicAuthCredentials} for the Basic-Authentication
		 * credentials property.
		 * 
		 * @param aBasicAuthCredentials The {@link BasicAuthCredentials} to be
		 *        stored by the {@link BasicAuthCredentials} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withBasicAuthCredentials( BasicAuthCredentials aBasicAuthCredentials );

		/**
		 * Sets the {@link BasicAuthCredentials} from the user name and the
		 * secret for the Basic-Authentication credentials property.
		 * 
		 * @param aUserName The user name to be stored by the
		 *        {@link BasicAuthCredentials} property.
		 * @param aSecret The password to be stored by the
		 *        {@link BasicAuthCredentials} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withBasicAuthCredentials( String aUserName, String aSecret );
	}

	/**
	 * Provides a {@link BasicAuthCredentials} property.
	 */
	public interface BasicAuthCredentialsProperty extends BasicAuthCredentialsAccessor, BasicAuthCredentialsMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs:Sets the {@link BasicAuthCredentials}
		 * for the Basic-Authentication credentials property.
		 * 
		 * @param aBasicAuthCredentials The {@link BasicAuthCredentials} to be
		 *        stored by the {@link BasicAuthCredentials} property.
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default BasicAuthCredentials letBasicAuthCredentials( BasicAuthCredentials aBasicAuthCredentials ) {
			setBasicAuthCredentials( aBasicAuthCredentials );
			return aBasicAuthCredentials;
		}

		/**
		 * This method stores and passes through the given arguments, which is
		 * very useful for builder APIs:Sets the {@link BasicAuthCredentials}
		 * from the user name and the secret for the Basic-Authentication
		 * credentials property.
		 * 
		 * @param aUserName The user name to be stored by the
		 *        {@link BasicAuthCredentials} property.
		 * @param aSecret The password to be stored by the
		 *        {@link BasicAuthCredentials} property.
		 * 
		 * @return Returns the values passed (combined to an instance of
		 *         {@link BasicAuthCredentials}) for it to be used in conclusive
		 *         processing steps.
		 */
		default BasicAuthCredentials letBasicAuthCredentials( String aUserName, String aSecret ) {
			final BasicAuthCredentials theBasicAuthCredentials = new BasicAuthCredentials( aUserName, aSecret );
			setBasicAuthCredentials( theBasicAuthCredentials );
			return theBasicAuthCredentials;
		}
	}
}
