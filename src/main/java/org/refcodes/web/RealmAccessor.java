// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides an accessor for a realm property. See RFC 1945 at
 * "https://tools.ietf.org/html/rfc1945#section-11" and RFC 2617 at
 * "https://tools.ietf.org/html/rfc2617#page-3": "... The realm attribute
 * (case-insensitive) is required for all authentication schemes which issue a
 * challenge. The realm value (case-sensitive), in combination with the
 * canonical root URL of the server being accessed, defines the protection
 * space. These realms allow the protected resources on a server to be
 * partitioned into a set of protection spaces, each with its own authentication
 * scheme and/or authorization database. The realm value is a string, generally
 * assigned by the origin server, which may have additional semantics specific
 * to the authentication scheme ..."
 */
public interface RealmAccessor {

	/**
	 * Retrieves the realm from the realm property. See RFC 1945 at
	 * "https://tools.ietf.org/html/rfc1945#section-11" and RFC 2617 at
	 * "https://tools.ietf.org/html/rfc2617#page-3".
	 * 
	 * @return The realm stored by the realm property.
	 */
	String getRealm();

	/**
	 * Provides a mutator for a realm property.
	 */
	public interface RealmMutator {

		/**
		 * Sets the realm for the realm property. See RFC 1945 at
		 * "https://tools.ietf.org/html/rfc1945#section-11" and RFC 2617 at
		 * "https://tools.ietf.org/html/rfc2617#page-3".
		 * 
		 * @param aRealm The realm to be stored by the realm property.
		 */
		void setRealm( String aRealm );
	}

	/**
	 * Provides a builder method for a realm property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface RealmBuilder<B extends RealmBuilder<B>> {

		/**
		 * Sets the realm for the realm property. See RFC 1945 at
		 * "https://tools.ietf.org/html/rfc1945#section-11" and RFC 2617 at
		 * "https://tools.ietf.org/html/rfc2617#page-3".
		 * 
		 * @param aRealm The realm to be stored by the realm property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withRealm( String aRealm );
	}

	/**
	 * Provides a realm property. See RFC 1945 at
	 * "https://tools.ietf.org/html/rfc1945#section-11" and RFC 2617 at
	 * "https://tools.ietf.org/html/rfc2617#page-3".
	 */
	public interface RealmProperty extends RealmAccessor, RealmMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setRealm(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aRealm The {@link String} to set (via
		 *        {@link #setRealm(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letRealm( String aRealm ) {
			setRealm( aRealm );
			return aRealm;
		}
	}
}
