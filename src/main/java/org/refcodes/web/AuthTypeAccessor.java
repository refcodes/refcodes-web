// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides an accessor for a {@link AuthType} property.
 */
public interface AuthTypeAccessor {

	/**
	 * Retrieves the {@link AuthType} from the Authorization-Type property (or
	 * null if there are none such credentials).
	 * 
	 * @return The {@link AuthType} stored by the Basic-Authentication
	 *         credentials property (or null if there are none such
	 *         credentials).
	 */
	AuthType getAuthType();

	/**
	 * Provides a mutator for a {@link AuthType} property.
	 */
	public interface AuthTypeMutator {

		/**
		 * Sets the {@link AuthType} for the Authorization-Type property.
		 * 
		 * @param aAuthType The {@link AuthType} to be stored by the
		 *        {@link AuthType} property.
		 */
		void setAuthType( AuthType aAuthType );

		/**
		 * Sets the {@link AuthType} from the bearer and token for the
		 * Authorization-Type property.
		 * 
		 * @param aBearer The bearer to be stored by the {@link AuthType}
		 *        property.
		 */
		default void setAuthType( String aBearer ) {
			setAuthType( AuthType.fromName( aBearer ) );
		}
	}

	/**
	 * Provides a builder method for a {@link AuthType} property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface AuthTypeBuilder<B extends AuthTypeBuilder<B>> {

		/**
		 * Sets the {@link AuthType} for the Authorization-Type property.
		 * 
		 * @param aAuthType The {@link AuthType} to be stored by the
		 *        {@link AuthType} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withAuthType( AuthType aAuthType );

		/**
		 * Sets the {@link AuthType} from the bearer and token for the
		 * Authorization-Type property.
		 * 
		 * @param aBearer The bearer to be stored by the {@link AuthType}
		 *        property.
		 * @param aToken The token to be stored by the {@link AuthType}
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withAuthType( String aBearer, String aToken );
	}

	/**
	 * Provides a {@link AuthType} property.
	 */
	public interface AuthTypeProperty extends AuthTypeAccessor, AuthTypeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link AuthType}
		 * (setter) as of {@link #setAuthType(AuthType)} and returns the very
		 * same value (getter).
		 * 
		 * @param aAuthType The {@link AuthType} to set (via
		 *        {@link #setAuthType(AuthType)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default AuthType letAuthType( AuthType aAuthType ) {
			setAuthType( aAuthType );
			return aAuthType;
		}
	}
}
