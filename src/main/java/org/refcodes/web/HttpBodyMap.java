// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import java.util.Collection;
import java.util.Map;
import java.util.regex.Pattern;

import org.refcodes.data.Delimiter;
import org.refcodes.struct.CanonicalMapBuilderImpl;
import org.refcodes.struct.PathMap;
import org.refcodes.struct.Property;
import org.refcodes.struct.Relation;

/**
 * The {@link HttpBodyMap} type is a "dynamic" type in terms of it's structure
 * being represented by paths'. When a data structure is parsed e.g. from JSON
 * or XML, then the data structure's internal structure and values are reflected
 * by according paths's and values in the {@link HttpBodyMap}. The
 * {@link HttpBodyMap} is actually a https://www.metacodes.proization of the
 * {@link PathMap} using {@link String} objects as values and a slash ("/") as
 * path delimiter. Using the {@link HttpBodyMap} it is convenient to navigate or
 * address elements from unknown or dynamic data structures. The other way round
 * the {@link HttpBodyMap} may be used to construct dynamic data types by adding
 * the according paths's and values for marshaling into e.g. JSON or XML. Think
 * of an {@link HttpBodyMap} as a flattened JSON or XML structure. The
 * {@link HttpClientResponse}, {@link HttpServerRequest},
 * {@link HttpServerResponse} as well as the {@link HttpClientResponse}
 * implementations directly support the {@link HttpBodyMap}, given that the
 * {@link MediaTypeFactory} being used supports marshaling and unmarshaling from
 * and to nested {@link Map} instances. The {@link HttpBodyMap} acts as a view
 * of the nested {@link Map} structures, which are somehow inconvenient to
 * navigate through. The {@link HttpClientRequest#setRequest(Object)}
 * implementation supports the {@link HttpBodyMap} to marshal an
 * {@link HttpBodyMap} into an HTTP Request-Body. The
 * {@link HttpClientResponse#getResponse(Class)} implementation supports the
 * {@link HttpBodyMap} to unmarshal an HTTP Response-Body into an
 * {@link HttpBodyMap}. The {@link HttpServerRequest#getRequest(Class)}
 * implementation supports the {@link HttpBodyMap} to unmarshal an HTTP
 * Request-Body into an {@link HttpBodyMap}. The
 * {@link HttpServerResponse#setResponse(Object)} implementation supports the
 * {@link HttpBodyMap} to marshal an {@link HttpBodyMap} into an HTTP
 * Request-Body.
 */
public class HttpBodyMap extends CanonicalMapBuilderImpl {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String STATUS_CODE_PATH = "/status/code";
	public static final String STATUS_ALIAS_PATH = "/status/alias";
	public static final String STATUS_MESSAGE_PATH = "/status/aMessage";
	public static final String STATUS_EXCEPTION_PATH = "/status/exception";
	public static final String STATUS_TIMESTAMP_PATH = "/status/timestamp";

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Create an empty {@link HttpBodyMap} instance using the public path
	 * delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 */
	public HttpBodyMap() {}

	/**
	 * Create a {@link HttpBodyMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the public path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 */
	public HttpBodyMap( Object aObj ) {
		super( aObj );
	}

	/**
	 * Create a {@link HttpBodyMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the public path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 */
	public HttpBodyMap( String aToPath, Object aObj ) {
		super( aToPath, aObj );
	}

	/**
	 * Creates a {@link HttpBodyMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the public path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public HttpBodyMap( Object aObj, String aFromPath ) {
		super( aObj, aFromPath );
	}

	/**
	 * Creates a {@link HttpBodyMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the public path delimiter "/"
	 * ({@link Delimiter#PATH} for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public HttpBodyMap( String aToPath, Object aObj, String aFromPath ) {
		super( aToPath, aObj, aFromPath );
	}

	/**
	 * Instantiates a new http body map impl.
	 *
	 * @param aPathMap the path map
	 */
	private HttpBodyMap( PathMap<String> aPathMap ) {
		for ( String ePath : aPathMap.paths() ) {
			put( ePath, aPathMap.get( ePath ) );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/// **
	//  * {@inheritDoc}
	//  */
	// @Override
	// public Object toDataStructure( String aFromPath ) {
	//	// TODO 2024-05-27 - Re-think marshaling and unmarshaling Map to Array types.
	//	// return TypeUtility.toUnwrappedMap( super.toDataStructure( aFromPath ) );
	//	return super.toDataStructure( aFromPath );
	// }

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap retrieveFrom( String aFromPath ) {
		return new HttpBodyMap( super.retrieveFrom( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap retrieveTo( String aToPath ) {
		return new HttpBodyMap( super.retrieveTo( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap retrieveBetween( String aFromPath, String aToPath ) {
		return new HttpBodyMap( super.retrieveBetween( aFromPath, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPut( String aKey, String aValue ) {
		put( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap getDirAt( int aIndex ) {
		return getDirAt( getRootPath(), aIndex );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap getDirAt( String aPath, int aIndex ) {
		return retrieveFrom( aPath, aIndex );
	}

	/**
	 * Retrieves the public path's ("status/alias") status alias.
	 * 
	 * @return The previously set value for the status alias public path.
	 */
	public String getStatusAlias() {
		return get( STATUS_ALIAS_PATH );
	}

	/**
	 * Retrieves the public path's ("status/code") status code.
	 * 
	 * @return The previously set value for the status code public path.
	 */
	public HttpStatusCode getStatusCode() {
		return HttpStatusCode.toHttpStatusCode( getInt( STATUS_CODE_PATH ) );
	}

	/**
	 * Retrieves the public path's ("status/exception") status exception.
	 * 
	 * @return The previously set value for the status exception public path.
	 */
	public Class<?> getStatusException() {
		try {
			return Class.forName( get( STATUS_EXCEPTION_PATH ) );
		}
		catch ( ClassNotFoundException e ) {
			return null;
		}
	}

	/**
	 * Retrieves the public path's ("status/aMessage") status aMessage.
	 * 
	 * @return The previously set value for the status aMessage public path.
	 */
	public String getStatusMessage() {
		return get( STATUS_MESSAGE_PATH );
	}

	/**
	 * Retrieves the public path's ("status/Time-Stamp") status Time-Stamp.
	 * 
	 * @return The previously set value for the status Time-Stamp public path.
	 */
	public Long getStatusTimeStamp() {
		return getLong( STATUS_TIMESTAMP_PATH );
	}

	/**
	 * Determines whether a public path's ("status/alias") status alias exists.
	 * 
	 * @return True in case there is a valid value for the status alias default
	 *         path.
	 */
	public boolean hasStatusAlias() {
		try {
			return getStatusAlias() != null;
		}
		catch ( Exception e ) {
			return false;
		}
	}

	/**
	 * Determines whether a public path's ("status/code") status code exists.
	 * 
	 * @return True in case there is a valid value for the status code default
	 *         path.
	 */
	public boolean hasStatusCode() {
		try {
			return getStatusCode() != null;
		}
		catch ( Exception e ) {
			return false;
		}
	}

	/**
	 * Determines whether a public path's ("status/exception") status exception
	 * exists.
	 * 
	 * @return True in case there is a valid value for the status exception
	 *         public path.
	 */
	public boolean hasStatusException() {
		try {
			return Class.forName( get( STATUS_EXCEPTION_PATH ) ) != null;
		}
		catch ( Exception e ) {
			return false;
		}
	}

	/**
	 * Determines whether a public path's ("status/aMessage") status aMessage
	 * exists.
	 * 
	 * @return True in case there is a valid value for the status aMessage
	 *         public path.
	 */
	public boolean hasStatusMessage() {
		return containsKey( STATUS_MESSAGE_PATH );
	}

	/**
	 * Determines whether a public path's ("status/Time-Stamp") status
	 * Time-Stamp in milliseconds exists.
	 * 
	 * @return True in case there is a valid value for the status Time-Stamp
	 *         public path.
	 */
	public boolean hasStatusTimeStamp() {
		try {
			return getStatusTimeStamp() != null;
		}
		catch ( Exception e ) {
			return false;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap putDirAt( Collection<?> aPathElements, int aIndex, Object aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap putDirAt( Collection<?> aPathElements, int aIndex, PathMap<String> aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap putDirAt( int aIndex, Object aDir ) {
		return putDirAt( getRootPath(), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap putDirAt( int aIndex, PathMap<String> aDir ) {
		return putDirAt( getRootPath(), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap putDirAt( Object aPath, int aIndex, Object aDir ) {
		return putDirAt( toPath( aPath ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap putDirAt( Object aPath, int aIndex, PathMap<String> aDir ) {
		return putDirAt( toPath( aPath ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap putDirAt( Object[] aPathElements, int aIndex, Object aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap putDirAt( Object[] aPathElements, int aIndex, PathMap<String> aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap putDirAt( String aPath, int aIndex, Object aDir ) {
		final HttpBodyMap theReplaced = removeDirAt( aPath, aIndex );
		insertTo( toPath( aPath, aIndex ), aDir );
		return theReplaced;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap putDirAt( String aPath, int aIndex, PathMap<String> aDir ) {
		final HttpBodyMap theReplaced = removeDirAt( aPath, aIndex );
		insertTo( toPath( aPath, aIndex ), aDir );
		return theReplaced;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap putDirAt( String[] aPathElements, int aIndex, Object aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap putDirAt( String[] aPathElements, int aIndex, PathMap<String> aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * Puts a public path's ("status/alias") status alias.
	 * 
	 * @param aStatusCode The {@link HttpStatusCode}, which's verbose alias is
	 *        to be set.
	 * 
	 * @return The previously set value for the status alias public path.
	 */
	public String putStatusAlias( HttpStatusCode aStatusCode ) {
		return put( STATUS_ALIAS_PATH, aStatusCode.toVerbose() );
	}

	/**
	 * Puts a public path's ("status/alias") status alias.
	 * 
	 * @param aStatusAlias The status alias value to be set.
	 * 
	 * @return The previously set value for the status alias public path.
	 */
	public String putStatusAlias( String aStatusAlias ) {
		return put( STATUS_ALIAS_PATH, aStatusAlias );
	}

	/**
	 * Puts a public path's ("status/code") status code.
	 * 
	 * @param aStatusCode The {@link HttpStatusCode} to be set.
	 * 
	 * @return The previously set value for the status code public path.
	 */
	public String putStatusCode( HttpStatusCode aStatusCode ) {
		return putInt( STATUS_CODE_PATH, aStatusCode.getStatusCode() );
	}

	/**
	 * Puts a public path's ("status/code") status code.
	 * 
	 * @param aStatusCode The status code value to be set.
	 * 
	 * @return The previously set value for the status code public path.
	 */
	public String putStatusCode( int aStatusCode ) {
		return putInt( STATUS_CODE_PATH, aStatusCode );
	}

	/**
	 * Puts a public path's ("status/exception") status exception.
	 * 
	 * @param aStatusException The status exception value to be set.
	 * 
	 * @return The previously set value for the status exception public path.
	 */
	public String putStatusException( Class<Exception> aStatusException ) {
		return put( STATUS_EXCEPTION_PATH, aStatusException.getName() );
	}

	/**
	 * Puts a public path's ("status/exception") status exception.
	 * 
	 * @param aStatusException The status exception value to be set.
	 * 
	 * @return The previously set value for the status exception public path.
	 */
	public String putStatusException( Exception aStatusException ) {
		return put( STATUS_EXCEPTION_PATH, aStatusException.getClass().getName() );
	}

	/**
	 * Puts a public path's ("status/aMessage") status aMessage.
	 * 
	 * @param aStatusMessage The status aMessage value to be set.
	 * 
	 * @return The previously set value for the status aMessage public path.
	 */
	public String putStatusMessage( String aStatusMessage ) {
		return put( STATUS_MESSAGE_PATH, aStatusMessage );
	}

	/**
	 * Puts a public path's ("status/Time-Stamp") status Time-Stamp to the
	 * current time in milliseconds as of {@link System#currentTimeMillis()}
	 * 
	 * @return The previously set value for the status Time-Stamp public path.
	 */
	public String putStatusTimeStamp() {
		return putLong( STATUS_TIMESTAMP_PATH, System.currentTimeMillis() );
	}

	/**
	 * Puts a public path's ("status/Time-Stamp") status Time-Stamp in
	 * milliseconds.
	 * 
	 * @param aStatusTimeStamp The status Time-Stamp value to be set.
	 * 
	 * @return The previously set value for the status Time-Stamp public path.
	 */
	public String putStatusTimeStamp( long aStatusTimeStamp ) {
		return putLong( STATUS_TIMESTAMP_PATH, aStatusTimeStamp );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap query( Collection<?> aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap query( Object... aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap query( String aPathQuery ) {
		return new HttpBodyMap( super.query( aPathQuery ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap query( Pattern aRegExp ) {
		return new HttpBodyMap( super.query( aRegExp ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap query( String... aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HttpBodyMap queryBetween( Collection<?> aFromPath, Collection<?> aPathQuery, Collection<?> aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HttpBodyMap queryBetween( Object aFromPath, Object aPathQuery, Object aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HttpBodyMap queryBetween( Object[] aFromPath, Object[] aPathQuery, Object[] aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap queryBetween( String aFromPath, String aPathQuery, String aToPath ) {
		return new HttpBodyMap( super.queryBetween( aFromPath, aPathQuery, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap queryBetween( String aFromPath, Pattern aRegExp, String aToPath ) {
		return new HttpBodyMap( super.queryBetween( aFromPath, aRegExp, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HttpBodyMap queryBetween( String[] aFromPath, String[] aPathQuery, String[] aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HttpBodyMap queryFrom( Collection<?> aPathQuery, Collection<?> aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HttpBodyMap queryFrom( Object aPathQuery, Object aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HttpBodyMap queryFrom( Object[] aPathQuery, Object[] aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap queryFrom( String aPathQuery, String aFromPath ) {
		return new HttpBodyMap( super.queryFrom( aPathQuery, aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap queryFrom( Pattern aRegExp, String aFromPath ) {
		return new HttpBodyMap( super.queryFrom( aRegExp, aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HttpBodyMap queryFrom( String[] aPathQuery, String[] aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HttpBodyMap queryTo( Collection<?> aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HttpBodyMap queryTo( Object aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HttpBodyMap queryTo( Object[] aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap queryTo( String aPathQuery, String aToPath ) {
		return new HttpBodyMap( super.queryTo( aPathQuery, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap queryTo( Pattern aRegExp, String aToPath ) {
		return new HttpBodyMap( super.queryTo( aRegExp, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HttpBodyMap queryTo( String[] aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap removePaths( Collection<?> aPaths ) {
		return new HttpBodyMap( super.removePaths( aPaths ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap removePaths( String... aPaths ) {
		return new HttpBodyMap( super.removePaths( aPaths ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap removeAll( String... aPathQueryElements ) {
		return new HttpBodyMap( super.removeAll( aPathQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap removeAll( Object... aPathQueryElements ) {
		return new HttpBodyMap( super.removeAll( aPathQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap removeAll( Collection<?> aPathQueryElements ) {
		return new HttpBodyMap( super.removeAll( aPathQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap removeAll( String aPathQuery ) {
		return new HttpBodyMap( super.removeAll( aPathQuery ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap removeAll( Pattern aRegExp ) {
		return new HttpBodyMap( super.removeAll( aRegExp ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap removeAll( Object aPathQuery ) {
		return new HttpBodyMap( super.removeAll( aPathQuery ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap removeDirAt( int aIndex ) {
		return new HttpBodyMap( super.removeDirAt( aIndex ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap removeDirAt( Object aPath, int aIndex ) {
		return new HttpBodyMap( super.removeDirAt( aPath, aIndex ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap removeDirAt( Object[] aPathElements, int aIndex ) {
		return new HttpBodyMap( super.removeDirAt( aPathElements, aIndex ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap removeDirAt( String aPath, int aIndex ) {
		return new HttpBodyMap( super.removeDirAt( aPath, aIndex ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap removeDirAt( String[] aPathElements, int aIndex ) {
		return new HttpBodyMap( super.removeDirAt( aPathElements, aIndex ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap removeFrom( Object... aPathElements ) {
		return new HttpBodyMap( super.removeFrom( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap removeFrom( Object aPath ) {
		return new HttpBodyMap( super.removeFrom( aPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap removeFrom( String aPath ) {
		return new HttpBodyMap( super.removeFrom( aPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap removeFrom( String... aPathElements ) {
		return new HttpBodyMap( super.removeFrom( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HttpBodyMap retrieveBetween( Collection<?> aFromPath, Collection<?> aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HttpBodyMap retrieveBetween( Object aFromPath, Object aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HttpBodyMap retrieveBetween( Object[] aFromPath, Object[] aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HttpBodyMap retrieveBetween( String[] aFromPath, String[] aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HttpBodyMap retrieveFrom( Collection<?> aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap retrieveFrom( Object aParentPath ) {
		return retrieveFrom( toPath( aParentPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap retrieveFrom( Object... aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap retrieveFrom( String... aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HttpBodyMap retrieveTo( Collection<?> aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap retrieveTo( Object aToPath ) {
		return retrieveTo( toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap retrieveTo( Object... aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap retrieveTo( String... aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	/**
	 * Puts a public path's ("status/alias") status alias.
	 * 
	 * @param aStatusCode The {@link HttpStatusCode}, which's verbose alias is
	 *        to be set.
	 * 
	 * @return The previously set value for the status alias public path.
	 */
	public HttpBodyMap withPutStatusAlias( HttpStatusCode aStatusCode ) {
		put( STATUS_ALIAS_PATH, aStatusCode.toVerbose() );
		return this;
	}

	/**
	 * Puts a public path's ("status/alias") status alias.
	 * 
	 * @param aStatusAlias The status alias value to be set.
	 * 
	 * @return The previously set value for the status alias public path.
	 */
	public HttpBodyMap withPutStatusAlias( String aStatusAlias ) {
		put( STATUS_ALIAS_PATH, aStatusAlias );
		return this;
	}

	/**
	 * Puts a public path's ("status/code") status code.
	 * 
	 * @param aStatusCode The {@link HttpStatusCode} to be set.
	 * 
	 * @return The previously set value for the status code public path.
	 */
	public HttpBodyMap withPutStatusCode( HttpStatusCode aStatusCode ) {
		putInt( STATUS_CODE_PATH, aStatusCode.getStatusCode() );
		return this;
	}

	/**
	 * Puts a public path's ("status/code") status code.
	 * 
	 * @param aStatusCode The status code value to be set.
	 * 
	 * @return The previously set value for the status code public path.
	 */
	public HttpBodyMap withPutStatusCode( int aStatusCode ) {
		putInt( STATUS_CODE_PATH, aStatusCode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPut( Collection<?> aPathElements, String aValue ) {
		put( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPut( Object[] aPathElements, String aValue ) {
		put( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPut( Relation<String, String> aProperty ) {
		put( aProperty );
		return this;
	}

	//	/**
	//	 * {@inheritDoc}
	//	 */
	//	@Override
	//	public HttpBodyMap withPut( Object aPath, String aValue ) {
	//		put( toPath( aPath ), aValue );
	//		return this;
	//	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPut( Property aProperty ) {
		put( aProperty );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPut( String[] aKey, String aValue ) {
		put( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutBoolean( Collection<?> aPathElements, Boolean aValue ) {
		putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutBoolean( Object aKey, Boolean aValue ) {
		putBoolean( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutBoolean( Object[] aPathElements, Boolean aValue ) {
		putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutBoolean( String aKey, Boolean aValue ) {
		putBoolean( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutBoolean( String[] aPathElements, Boolean aValue ) {
		putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutByte( Collection<?> aPathElements, Byte aValue ) {
		putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutByte( Object aKey, Byte aValue ) {
		putByte( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutByte( Object[] aPathElements, Byte aValue ) {
		putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutByte( String aKey, Byte aValue ) {
		putByte( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutByte( String[] aPathElements, Byte aValue ) {
		putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutChar( Collection<?> aPathElements, Character aValue ) {
		putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutChar( Object aKey, Character aValue ) {
		putChar( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutChar( Object[] aPathElements, Character aValue ) {
		putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutChar( String aKey, Character aValue ) {
		putChar( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutChar( String[] aPathElements, Character aValue ) {
		putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> HttpBodyMap withPutClass( Collection<?> aPathElements, Class<C> aValue ) {
		putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> HttpBodyMap withPutClass( Object aKey, Class<C> aValue ) {
		putClass( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> HttpBodyMap withPutClass( Object[] aPathElements, Class<C> aValue ) {
		putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> HttpBodyMap withPutClass( String aKey, Class<C> aValue ) {
		putClass( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> HttpBodyMap withPutClass( String[] aPathElements, Class<C> aValue ) {
		putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutDouble( Collection<?> aPathElements, Double aValue ) {
		putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutDouble( Object aKey, Double aValue ) {
		putDouble( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutDouble( Object[] aPathElements, Double aValue ) {
		putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutDouble( String aKey, Double aValue ) {
		putDouble( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutDouble( String[] aPathElements, Double aValue ) {
		putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> HttpBodyMap withPutEnum( Collection<?> aPathElements, E aValue ) {
		putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> HttpBodyMap withPutEnum( Object aKey, E aValue ) {
		putEnum( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> HttpBodyMap withPutEnum( Object[] aPathElements, E aValue ) {
		putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> HttpBodyMap withPutEnum( String aKey, E aValue ) {
		putEnum( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> HttpBodyMap withPutEnum( String[] aPathElements, E aValue ) {
		putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutFloat( Collection<?> aPathElements, Float aValue ) {
		putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutFloat( Object aKey, Float aValue ) {
		putFloat( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutFloat( Object[] aPathElements, Float aValue ) {
		putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutFloat( String aKey, Float aValue ) {
		putFloat( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutFloat( String[] aPathElements, Float aValue ) {
		putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutInt( Collection<?> aPathElements, Integer aValue ) {
		putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutInt( Object aKey, Integer aValue ) {
		putInt( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutInt( Object[] aPathElements, Integer aValue ) {
		putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutInt( String aKey, Integer aValue ) {
		putInt( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutInt( String[] aPathElements, Integer aValue ) {
		putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutLong( Collection<?> aPathElements, Long aValue ) {
		putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutLong( Object aKey, Long aValue ) {
		putLong( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutLong( Object[] aPathElements, Long aValue ) {
		putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutLong( String aKey, Long aValue ) {
		putLong( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutLong( String[] aPathElements, Long aValue ) {
		putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutShort( Collection<?> aPathElements, Short aValue ) {
		putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutShort( Object aKey, Short aValue ) {
		putShort( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutShort( Object[] aPathElements, Short aValue ) {
		putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutShort( String aKey, Short aValue ) {
		putShort( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutShort( String[] aPathElements, Short aValue ) {
		putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutString( Collection<?> aPathElements, String aValue ) {
		putString( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutString( Object aKey, String aValue ) {
		putString( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutString( Object[] aPathElements, String aValue ) {
		putString( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutString( String aKey, String aValue ) {
		putString( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutString( String[] aPathElements, String aValue ) {
		putString( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsert( Object aObj ) {
		insert( aObj );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsert( PathMap<String> aFrom ) {
		insert( aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertBetween( Collection<?> aToPathElements, PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertBetween( Object aToPath, Object aFrom, Object aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertBetween( Object aToPath, PathMap<String> aFrom, Object aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertBetween( Object[] aToPathElements, PathMap<String> aFrom, Object[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertBetween( String aToPath, Object aFrom, String aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertBetween( String aToPath, PathMap<String> aFrom, String aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertBetween( String[] aToPathElements, PathMap<String> aFrom, String[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertFrom( Object aFrom, Collection<?> aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertFrom( Object aFrom, Object aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertFrom( Object aFrom, Object... aFromPathElements ) {
		withInsertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertFrom( Object aFrom, String aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertFrom( Object aFrom, String... aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertFrom( PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertFrom( PathMap<String> aFrom, Object aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertFrom( PathMap<String> aFrom, Object... aFromPathElements ) {
		withInsertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertFrom( PathMap<String> aFrom, String aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertFrom( PathMap<String> aFrom, String... aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertTo( Collection<?> aToPathElements, Object aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertTo( Collection<?> aToPathElements, PathMap<String> aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertTo( Object aToPath, Object aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertTo( Object aToPath, PathMap<String> aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertTo( Object[] aToPathElements, Object aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertTo( Object[] aToPathElements, PathMap<String> aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertTo( String aToPath, Object aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertTo( String aToPath, PathMap<String> aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertTo( String[] aToPathElements, Object aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withInsertTo( String[] aToPathElements, PathMap<String> aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMerge( Object aObj ) {
		merge( aObj );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMerge( PathMap<String> aFrom ) {
		merge( aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeBetween( Collection<?> aToPathElements, PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeBetween( Object aToPath, Object aFrom, Object aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeBetween( Object aToPath, PathMap<String> aFrom, Object aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeBetween( Object[] aToPathElements, PathMap<String> aFrom, Object[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFromPathElements, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeBetween( String aToPath, Object aFrom, String aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeBetween( String aToPath, PathMap<String> aFrom, String aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeBetween( String[] aToPathElements, PathMap<String> aFrom, String[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeFrom( Object aFrom, Collection<?> aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeFrom( Object aFrom, Object aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeFrom( Object aFrom, Object... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeFrom( Object aFrom, String aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeFrom( Object aFrom, String... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeFrom( PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeFrom( PathMap<String> aFrom, Object aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeFrom( PathMap<String> aFrom, Object... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeFrom( PathMap<String> aFrom, String aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeFrom( PathMap<String> aFrom, String... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeTo( Collection<?> aToPathElements, Object aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeTo( Collection<?> aToPathElements, PathMap<String> aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeTo( Object aToPath, Object aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeTo( Object aToPath, PathMap<String> aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeTo( Object[] aToPathElements, Object aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeTo( Object[] aToPathElements, PathMap<String> aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeTo( String aToPath, Object aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeTo( String aToPath, PathMap<String> aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeTo( String[] aToPathElements, Object aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withMergeTo( String[] aToPathElements, PathMap<String> aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutDirAt( Collection<?> aPathElements, int aIndex, Object aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutDirAt( Collection<?> aPathElements, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutDirAt( int aIndex, Object aDir ) {
		putDirAt( aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutDirAt( int aIndex, PathMap<String> aDir ) {
		putDirAt( aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutDirAt( Object aPath, int aIndex, Object aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutDirAt( Object aPath, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutDirAt( Object[] aPathElements, int aIndex, Object aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutDirAt( Object[] aPathElements, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutDirAt( String aPath, int aIndex, Object aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutDirAt( String aPath, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutDirAt( String[] aPathElements, int aIndex, Object aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withPutDirAt( String[] aPathElements, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withRemoveFrom( Collection<?> aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withRemoveFrom( Object aPath ) {
		removeFrom( aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withRemoveFrom( Object... aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withRemoveFrom( String aPath ) {
		removeFrom( aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withRemoveFrom( String... aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpBodyMap withRemovePaths( String... aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}
}
