// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides an accessor for a OAuth token property.
 */
public interface OauthTokenAccessor {

	/**
	 * Retrieves the OAuth token from the OAuth token property
	 * 
	 * @return The OAuth token stored by the OAuth token property.
	 */
	OauthToken getOauthToken();

	/**
	 * Provides a mutator for a OAuth token property.
	 */
	public interface OauthTokenMutator {

		/**
		 * Sets the OAuth token for the OAuth token property.
		 * 
		 * @param aOauthToken The OAuth token to be stored by the OAuth token
		 *        property.
		 */
		void setOauthToken( OauthToken aOauthToken );
	}

	/**
	 * Provides a builder method for a OAuth token property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface OauthTokenBuilder<B extends OauthTokenBuilder<B>> {

		/**
		 * Sets the OAuth token for the OAuth token property
		 * 
		 * @param aOauthToken The OAuth token to be stored by the OAuth token
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withOAuthToken( OauthToken aOauthToken );
	}

	/**
	 * Provides a OAuth token property.
	 */
	public interface OauthTokenProperty extends OauthTokenAccessor, OauthTokenMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link OauthToken}
		 * (setter) as of {@link #setOauthToken(OauthToken)} and returns the
		 * very same value (getter).
		 * 
		 * @param aOauthToken The {@link OauthToken} to set (via
		 *        {@link #setOauthToken(OauthToken)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default OauthToken letOauthToken( OauthToken aOauthToken ) {
			setOauthToken( aOauthToken );
			return aOauthToken;
		}
	}
}
