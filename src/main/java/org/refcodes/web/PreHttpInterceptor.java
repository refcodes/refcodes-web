// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * An {@link PreHttpInterceptor} definition for pre-processing
 * {@link HttpRequest} as well as {@link HttpResponse} instances.
 */
@FunctionalInterface
public interface PreHttpInterceptor<REQ extends HttpRequest, RES extends HttpResponse> {

	/**
	 * Invoked to pre-process a {@link HttpRequest} alongside a
	 * {@link HttpResponse}.
	 *
	 * @param aRequest The {@link HttpRequest} to pre-process.
	 * @param aResponse The {@link HttpResponse} to pre-process.
	 */
	void preIntercept( REQ aRequest, RES aResponse );
}
