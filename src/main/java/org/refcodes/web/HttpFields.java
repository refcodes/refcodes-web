// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.refcodes.data.Delimiter;

/**
 * Various key/value "collections" we run across when we develop HTTP based
 * applications may have more then one value for one key. Such "collections" in
 * this package are the {@link HeaderFields} as well as the {@link FormFields}.
 * This interface represents this kind of data structure providing some common
 * convenience methods.
 *
 * @param <B> The generic type of the builder to be returned upon invoking
 *        builder methods.
 */
public interface HttpFields<B extends HttpFields<B>> extends Map<String, List<String>> {

	/**
	 * Adds all fields found in the provided {@link HttpFields} instance.
	 * 
	 * @param aHttpFields The {@link HttpFields} instances which's fields are to
	 *        be added.
	 */
	default void addAll( HttpFields<?> aHttpFields ) {
		for ( String eKey : aHttpFields.keySet() ) {
			put( eKey, aHttpFields.get( eKey ) );
		}
	}

	/**
	 * Returns the first Header-Field value in the list of values associated
	 * with the given Header-Field.
	 * 
	 * @param aField The Header-Field (key) of which's values the first value is
	 *        to be retrieved.
	 * 
	 * @return The first value in the list of values associated to the given
	 *         Header-Field (key).
	 */
	default String getFirst( String aField ) {
		final List<String> theValues = get( aField );
		if ( theValues != null ) {
			synchronized ( this ) {
				if ( theValues.size() > 0 ) {
					return theValues.get( 0 );
				}
			}
		}
		return null;
	}

	/**
	 * Same as {@link #getFirst(String)} though using the provided enumeration's
	 * {@link Enum#toString()} method to determine the key.
	 *
	 * @param aField the field
	 * 
	 * @return the first
	 * 
	 * @see #getFirst(String)
	 */
	default String getFirst( Enum<?> aField ) {
		return getFirst( aField.toString() );
	}

	/**
	 * Sets a single value for the Header-Field (key). Any values previously
	 * associated to the given Header-Field (key) are lost.
	 *
	 * @param aField The Header-Field for which to set a single value.
	 * @param aValue The single value to be set for the Header-Field (key).
	 * 
	 * @return the list
	 */
	default List<String> put( String aField, String aValue ) {
		if ( aValue == null ) {
			return remove( aField );
		}
		final List<String> theValues = new ArrayList<>();
		theValues.add( aValue );
		return put( aField, theValues );
	}

	/**
	 * Same as {@link #put(String, String)} though using the provided
	 * enumeration's {@link Enum#toString()} method to determine the key.
	 *
	 * @param aField the field
	 * @param aValue the value
	 * 
	 * @return the list
	 * 
	 * @see #put(String, String)
	 */
	default List<String> put( Enum<?> aField, String aValue ) {
		return put( aField.toString(), aValue );
	}

	/**
	 * Performs a #set(String, String) on multiple values.
	 *
	 * @param aField The Header-Field for which to set the values.
	 * @param aValues The values to be set for the Header-Field (key).
	 * 
	 * @return the list
	 */
	default List<String> put( String aField, String... aValues ) {
		if ( aValues != null ) {
			final List<String> theValues = new ArrayList<>();
			for ( String eValue : aValues ) {
				if ( eValue != null && eValue.length() != 0 ) {
					theValues.add( eValue );
				}
			}
			if ( theValues.size() > 0 ) {
				return put( aField, theValues );
			}
		}
		return remove( aField );
	}

	/**
	 * Same as {@link #put(String, String ... )} though using the provided
	 * enumeration's {@link Enum#toString()} method to determine the key.
	 *
	 * @param aField the field
	 * @param aValues the values
	 * 
	 * @return the list
	 * 
	 * @see #put(String, String ...)
	 */
	default List<String> put( Enum<?> aField, String... aValues ) {
		return put( aField.toString(), aValues );
	}

	/**
	 * Builder method for the {@link #put(String, String)} method.
	 * 
	 * @param aField The Header-Field for which to set a single value.
	 * @param aValue The single value to be set for the Header-Field (key).
	 * 
	 * @return This {@link HttpFields} instance to continue building up the
	 *         Header-Fields.
	 */
	@SuppressWarnings("unchecked")
	default B withPut( String aField, String aValue ) {
		put( aField, aValue );
		return (B) this;
	}

	/**
	 * Same as {@link #withPut(String, String)} though using the provided
	 * enumeration's {@link Enum#toString()} method to determine the key.
	 *
	 * @param aField the field
	 * @param aValue the value
	 * 
	 * @return the b
	 * 
	 * @see #withPut(String, String)
	 */
	default B withPut( Enum<?> aField, String aValue ) {
		return withPut( aField.toString(), aValue );
	}

	/**
	 * Builder method for the {@link #put(Object, Object)} method.
	 * 
	 * @param aField The Header-Field for which to set the values.
	 * @param aValues The values to be set for the Header-Field (key).
	 * 
	 * @return This {@link HttpFields} instance to continue building up the
	 *         Header-Fields.
	 */
	@SuppressWarnings("unchecked")
	default B withPut( String aField, List<String> aValues ) {
		put( aField, aValues );
		return (B) this;
	}

	/**
	 * Same as {@link #withPut(String, List)} though using the provided
	 * enumeration's {@link Enum#toString()} method to determine the key.
	 *
	 * @param aField the field
	 * @param aValues the values
	 * 
	 * @return the b
	 * 
	 * @see #withPut(String, List)
	 */
	default B withPut( Enum<?> aField, List<String> aValues ) {
		return withPut( aField.toString(), aValues );
	}

	/**
	 * Builder method for the {@link #put(String, String...)} method.
	 * 
	 * @param aField The Header-Field for which to set the values.
	 * @param aValues The values to be set for the Header-Field (key).
	 * 
	 * @return This {@link HttpFields} instance to continue building up the
	 *         Header-Fields.
	 */
	@SuppressWarnings("unchecked")
	default B withPut( String aField, String... aValues ) {
		put( aField, aValues );
		return (B) this;
	}

	/**
	 * Same as {@link #withPut(String, String ...)} though using the provided
	 * enumeration's {@link Enum#toString()} method to determine the key.
	 *
	 * @param aField the field
	 * @param aValues the values
	 * 
	 * @return the b
	 * 
	 * @see #withPut(String, String ...)
	 */
	default B withPut( Enum<?> aField, String... aValues ) {
		return withPut( aField.toString(), aValues );
	}

	/**
	 * Adds a value to the list of values associated with the given Header-Field
	 * (key).
	 *
	 * @param aField The Header-Field (key) of which's list of values a value is
	 *        to be added.
	 * @param aValue The value to be added to the list of values associated to
	 *        the given Header-Field (key).
	 */
	default void addTo( String aField, String aValue ) {
		if ( aValue == null ) {
			throw new IllegalArgumentException( "You must not add a <" + aValue + "> value to a Header-Field <" + aField + ">." );
		}
		List<String> theValues = get( aField );
		if ( theValues == null ) {
			synchronized ( this ) {
				theValues = get( aField );
				if ( theValues == null ) {
					theValues = new ArrayList<>();
					put( aField, theValues );
				}
			}
		}
		theValues.add( aValue );
	}

	/**
	 * Same as {@link #addTo(String, String)} though using the provided
	 * enumeration's {@link Enum#toString()} method to determine the key.
	 *
	 * @param aField the field
	 * @param aValue the value
	 * 
	 * @see #addTo(String, String)
	 */
	default void addTo( Enum<?> aField, String aValue ) {
		addTo( aField.toString(), aValue );
	}

	/**
	 * Adds values to the list of values associated with the given Header-Field
	 * (key).
	 *
	 * @param aField The Header-Field (key) of which's list of values the values
	 *        are to be added.
	 * @param aValues The values to be added to the list of values associated to
	 *        the given Header-Field (key).
	 */
	default void addTo( String aField, String... aValues ) {
		if ( aValues == null || aValues.length == 0 ) {
			throw new IllegalArgumentException( "You must not add a <" + aValues + "> value to a Header-Field <" + aField + ">." );
		}
		synchronized ( this ) {
			List<String> theValues = get( aField );
			if ( theValues == null ) {
				theValues = get( aField );
				if ( theValues == null ) {
					theValues = new ArrayList<>();
				}
			}
			for ( String eValue : aValues ) {
				if ( eValue != null && eValue.length() != 0 ) {
					theValues.add( eValue );
				}
			}
			if ( theValues.size() > 0 ) {
				put( aField, theValues );
			}
		}
	}

	/**
	 * Same as {@link #addTo(String, String ...)} though using the provided
	 * enumeration's {@link Enum#toString()} method to determine the key.
	 *
	 * @param aField the field
	 * @param aValues the values
	 * 
	 * @see #addTo(String, String ...)
	 */
	default void addTo( Enum<?> aField, String... aValues ) {
		addTo( aField.toString(), aValues );
	}

	/**
	 * Adds values to the list of values associated with the given Header-Field
	 * (key),.
	 *
	 * @param aField The Header-Field (key) of which's list of values the values
	 *        are to be added.
	 * @param aValues The values to be added to the list of values associated to
	 *        the given Header-Field (key).
	 */
	default void addTo( String aField, List<String> aValues ) {
		if ( aValues == null ) {
			throw new IllegalArgumentException( "You must not add a <" + aValues + "> value to a Header-Field <" + aField + ">." );
		}
		synchronized ( this ) {
			List<String> theValues = get( aField );
			if ( theValues == null ) {
				theValues = new ArrayList<>();
			}
			for ( String eValue : aValues ) {
				if ( eValue != null && eValue.length() != 0 ) {
					theValues.add( eValue );
				}
			}
			if ( theValues.size() > 0 ) {
				put( aField, theValues );
			}
		}
	}

	/**
	 * Same as {@link #addTo(String, List)} though using the provided
	 * enumeration's {@link Enum#toString()} method to determine the key.
	 *
	 * @param aField the field
	 * @param aValues the values
	 * 
	 * @see #addTo(String, List)
	 */
	default void addTo( Enum<?> aField, List<String> aValues ) {
		addTo( aField.toString(), aValues );
	}

	/**
	 * Builder method for the {@link #addTo(String, String)} method.
	 * 
	 * @param aField The Header-Field (key) of which's list of values a value is
	 *        to be added.
	 * @param aValue The value to be added to the list of values associated to
	 *        the given Header-Field (key).
	 * 
	 * @return This {@link HttpFields} instance to continue building up the
	 *         Header-Fields.
	 */
	@SuppressWarnings("unchecked")
	default B withAddTo( String aField, String aValue ) {
		addTo( aField, aValue );
		return (B) this;
	}

	/**
	 * Same as {@link #withAddTo(String, String)} though using the provided
	 * enumeration's {@link Enum#toString()} method to determine the key.
	 *
	 * @param aField the field
	 * @param aValue the value
	 * 
	 * @return the b
	 * 
	 * @see #withAddTo(String, String)
	 */
	default B withAddTo( Enum<?> aField, String aValue ) {
		return withAddTo( aField.toString(), aValue );
	}

	/**
	 * Builder method for the {@link #addTo(String, String...)} method.
	 * 
	 * @param aField The Header-Field (key) of which's list of values the values
	 *        are to be added.
	 * @param aValues The values to be added to the list of values associated to
	 *        the given Header-Field (key).
	 * 
	 * @return This {@link HttpFields} instance to continue building up the
	 *         Header-Fields.
	 */
	@SuppressWarnings("unchecked")
	default B withAddTo( String aField, String... aValues ) {
		put( aField, aValues );
		return (B) this;
	}

	/**
	 * Same as {@link #withAddTo(String, String ...)} though using the provided
	 * enumeration's {@link Enum#toString()} method to determine the key.
	 *
	 * @param aField the field
	 * @param aValues the values
	 * 
	 * @return the b
	 * 
	 * @see #withAddTo(String, String ...)
	 */
	default B withAddTo( Enum<?> aField, String... aValues ) {
		return withAddTo( aField.toString(), aValues );
	}

	/**
	 * Builder method for the {@link #addTo(String, List)} method.
	 * 
	 * @param aField The Header-Field (key) of which's list of values the values
	 *        are to be added.
	 * @param aValues The values to be added to the list of values associated to
	 *        the given Header-Field (key).
	 * 
	 * @return This {@link HttpFields} instance to continue building up the
	 *         Header-Fields.
	 */
	@SuppressWarnings("unchecked")
	default B withAddTo( String aField, List<String> aValues ) {
		put( aField, aValues );
		return (B) this;
	}

	/**
	 * Same as {@link #withAddTo(String, List)} though using the provided
	 * enumeration's {@link Enum#toString()} method to determine the key.
	 *
	 * @param aField the field
	 * @param aValues the values
	 * 
	 * @return the b
	 * 
	 * @see #addTo(String, List)
	 */
	default B withAddTo( Enum<?> aField, List<String> aValues ) {
		return withAddTo( aField.toString(), aValues );
	}

	/**
	 * This method returns a single {@link String} (instead of returning a
	 * {@link List} of {@link String} instances when calling
	 * {@link #get(Object)}) where the elements of the filed are separated by
	 * the {@link Delimiter#HTTP_HEADER_ELEMENTS} and concatenated as a single
	 * {@link String}.
	 * 
	 * @param aKey The field's key for which's elements to retrieve a comma
	 *        separated {@link String}
	 * 
	 * @return The comma separated {@link String} of the according elements or
	 *         null if none such field was found.
	 */
	default String toField( String aKey ) {
		final List<String> theValues = get( aKey );
		if ( theValues != null && theValues.size() != 0 ) {
			String theField = "";
			final Iterator<String> e = theValues.iterator();
			while ( e.hasNext() ) {
				theField += e.next();
				if ( e.hasNext() ) {
					theField += Delimiter.HTTP_HEADER_ELEMENTS.getChar();
				}
			}
			return theField;
		}
		return null;
	}

	/**
	 * Same as {@link #toField(String)} though using the provided enumeration's
	 * {@link Enum#toString()} method to determine the key.
	 *
	 * @param aField the field
	 * 
	 * @return the string
	 * 
	 * @see #toField(String)
	 */
	default String toField( Enum<?> aField ) {
		return toField( aField.toString() );
	}
}