// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * The {@link HttpServerInterceptable} provides base functionality for working
 * with {@link HttpServerInterceptor} instances using the
 * {@link PreHttpServerInterceptable} as well as the
 * {@link PostHttpServerInterceptable} definitions.
 */
public interface HttpServerInterceptable extends PreHttpServerInterceptable, PostHttpServerInterceptable {

	/**
	 * Tests whether the given {@link HttpServerInterceptor} instance has been
	 * added.
	 * 
	 * @param aInterceptor The {@link HttpServerInterceptor} instance for which
	 *        to test if it has been added.
	 * 
	 * @return True in case {@link #hasPreHttpInterceptor(PreHttpInterceptor)}
	 *         and {@link #hasPostHttpInterceptor(PostHttpInterceptor)} return
	 *         true.
	 */
	default boolean hasHttpInterceptor( HttpServerInterceptor aInterceptor ) {
		return hasPreHttpInterceptor( aInterceptor ) && hasPostHttpInterceptor( aInterceptor );
	}

	/**
	 * Adds the given {@link HttpServerInterceptor} instance. The
	 * {@link HttpServerInterceptor} instance itself acts as the handle which is
	 * used when removing the given {@link HttpServerInterceptor} instance
	 * later.
	 * 
	 * @param aInterceptor The {@link HttpServerInterceptor} instance which is
	 *        to be added.
	 * 
	 * @return True in case {@link #addPreHttpInterceptor(PreHttpInterceptor)}
	 *         or {@link #addPostHttpInterceptor(PostHttpInterceptor)} return
	 *         true.
	 */
	default boolean addHttpInterceptor( HttpServerInterceptor aInterceptor ) {
		return addPreHttpInterceptor( aInterceptor ) | addPostHttpInterceptor( aInterceptor );
	}

	/**
	 * Removes the {@link HttpServerInterceptor} instance. In case the
	 * {@link HttpServerInterceptor} instance has not been added before, then
	 * false is returned.
	 * 
	 * @param aInterceptor The {@link HttpServerInterceptor} instance which is
	 *        to be removed.
	 * 
	 * @return True in case
	 *         {@link #removePreHttpInterceptor(PreHttpInterceptor)} or
	 *         {@link #removePostHttpInterceptor(PostHttpInterceptor)} return
	 *         true.
	 */
	default boolean removeHttpInterceptor( HttpServerInterceptor aInterceptor ) {
		return removePreHttpInterceptor( aInterceptor ) | removePostHttpInterceptor( aInterceptor );
	}
}