package org.refcodes.web;

import org.refcodes.mixin.KeyAccessor;

/**
 * The Enum CookieAttribute.
 */
public enum CookieAttribute implements KeyAccessor<String> {

	SECURE("SECURE"), PATH("PATH"), DOMAIN("DOMAIN"), EXPIRES("EXPIRES"), HTTPONLY("HTTPONLY"), VERSION("VERSION"),

	/**
	 * Internet Explorer (ie6, ie7, and ie8) does not support "MAX-AGE" , while
	 * (mostly) all browsers support {@link #EXPIRES}.
	 */
	MAX_AGE("MAX-AGE");

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _key;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new cookie attribute.
	 *
	 * @param aKey the key
	 */
	private CookieAttribute( String aKey ) {
		_key = aKey;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getKey() {
		return _key;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * From key.
	 *
	 * @param aKey the key
	 * 
	 * @return the cookie attribute
	 */
	public static CookieAttribute fromKey( String aKey ) {
		if ( aKey != null ) {
			aKey = aKey.trim();
		}
		for ( CookieAttribute eCookieKey : values() ) {
			if ( eCookieKey.getKey().equalsIgnoreCase( aKey ) ) {
				return eCookieKey;
			}
		}
		return null;
	}
}
