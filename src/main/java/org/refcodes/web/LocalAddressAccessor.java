// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import java.net.InetSocketAddress;

/**
 * Provides an accessor for a local address property.
 */
public interface LocalAddressAccessor {

	/**
	 * Retrieves the local address from the local address property.
	 * 
	 * @return The local address stored by the local address property.
	 */
	InetSocketAddress getLocalAddress();

	/**
	 * Provides a mutator for a local address property.
	 */
	public interface LocalAddressMutator {

		/**
		 * Sets the local address for the local address property.
		 * 
		 * @param aLocalAddress The local address to be stored by the local
		 *        address property.
		 */
		void setLocalAddress( InetSocketAddress aLocalAddress );
	}

	/**
	 * Provides a builder method for a local address property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface LocalAddressBuilder<B extends LocalAddressBuilder<B>> {

		/**
		 * Sets the local address for the local address property.
		 * 
		 * @param aLocalAddress The local address to be stored by the local
		 *        address property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withLocalAddress( InetSocketAddress aLocalAddress );
	}

	/**
	 * Provides a local address property.
	 */
	public interface LocalAddressProperty extends LocalAddressAccessor, LocalAddressMutator {

		/**
		 * This method stores and passes through the given
		 * {@link InetSocketAddress}, which is very useful for builder APIs:
		 * Sets the given value (setter) as of
		 * {@link #setLocalAddress(InetSocketAddress)} and returns the very same
		 * value (getter).
		 * 
		 * @param aLocalAddress The {@link InetSocketAddress} to set (via
		 *        {@link #setLocalAddress(InetSocketAddress)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default InetSocketAddress letLocalAddress( InetSocketAddress aLocalAddress ) {
			setLocalAddress( aLocalAddress );
			return aLocalAddress;
		}
	}
}
