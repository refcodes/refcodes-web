// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.stream.Collectors;

import org.refcodes.exception.MarshalException;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.factory.MarshalFactory.MarshalFactoryComposite;
import org.refcodes.factory.UnmarshalFactory.UnmarshalFactoryComposite;
import org.refcodes.struct.PathMap;

/**
 * The {@link MediaTypeFactory} creates instances from an HTTP body and creates
 * an HTTP body from given instances according to the {@link MediaType} (as of
 * {@link #getMediaTypes()}) for which the {@link MediaTypeFactory} is
 * responsible.
 */
public interface MediaTypeFactory extends UnmarshalFactoryComposite<String, InputStream>, MarshalFactoryComposite<String, InputStream>, MediaTypesAccessor {

	String VALUE_SELECTOR = "value";
	String ARRAY_SELECTOR = "array";
	String COLLECTION_SELECTOR = "collection";
	String MAP_SELECTOR = "map";
	String TYPE_ANNOTATION = PathMap.ANNOTATOR + "type";

	/**
	 * {@inheritDoc}
	 */
	@Override
	default <T> T fromMarshaled( InputStream aContentInputStream, Class<T> aType ) throws UnmarshalException {
		final BufferedReader theBuffer = new BufferedReader( new InputStreamReader( aContentInputStream ) );
		final String theHttpBody = theBuffer.lines().collect( Collectors.joining( "\n" ) );
		return toUnmarshaled( theHttpBody, aType );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default Object fromMarshaled( InputStream aContentInputStream, Class<?>... aTypes ) throws UnmarshalException {
		final BufferedReader theBuffer = new BufferedReader( new InputStreamReader( aContentInputStream ) );
		final String theHttpBody = theBuffer.lines().collect( Collectors.joining( "\n" ) );
		return toUnmarshaled( theHttpBody, aTypes );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default Object fromMarshaled( InputStream aContentInputStream, Map<String, String> aProperties, Class<?>... aTypes ) throws UnmarshalException {
		final BufferedReader theBuffer = new BufferedReader( new InputStreamReader( aContentInputStream ) );
		final String theHttpBody = theBuffer.lines().collect( Collectors.joining( "\n" ) );
		return toUnmarshaled( theHttpBody, aProperties, aTypes );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default <T> T fromMarshaled( InputStream aContentInputStream, Class<T> aType, Map<String, String> aProperties ) throws UnmarshalException {
		final BufferedReader theBuffer = new BufferedReader( new InputStreamReader( aContentInputStream ) );
		final String theHttpBody = theBuffer.lines().collect( Collectors.joining( "\n" ) );
		return toUnmarshaled( theHttpBody, aType, aProperties );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default <SRC> InputStream fromUnmarshaled( SRC aContext ) throws MarshalException {
		return new ByteArrayInputStream( toMarshaled( aContext ).getBytes( StandardCharsets.UTF_8 ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default <SRC> InputStream fromUnmarshaled( SRC aContext, Map<String, String> aProperties ) throws MarshalException {
		return new ByteArrayInputStream( toMarshaled( aContext, aProperties ).getBytes( StandardCharsets.UTF_8 ) );
	}
}
