// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides an accessor for a redirect depth property. The redirect depth
 * provides the count of HTTP-Request and HTTP-Response cycles where the
 * response represents a redirect as of
 * {@link HttpStatusCode#isRedirectStatus()}. A value of -1 represents the
 * default behavior, e.g. using {@link java.net.HttpURLConnection}'s redirection
 * means.
 */
public interface RedirectDepthAccessor {

	/**
	 * Retrieves the redirect depth from the redirect depth property.
	 * 
	 * @return The redirect depth stored by the redirect depth property.
	 */
	int getRedirectDepth();

	/**
	 * Provides a mutator for a redirect depth property.
	 */
	public interface RedirectDepthMutator {

		/**
		 * Sets the redirect depth for the redirect depth property.
		 * 
		 * @param aRedirectDepth The redirect depth to be stored by the redirect
		 *        depth property.
		 */
		void setRedirectDepth( int aRedirectDepth );
	}

	/**
	 * Provides a builder method for a redirect depth property returning the
	 * builder for applying multiple build operations. The redirect depth
	 * provides the count of HTTP-Request and HTTP-Response cycles where the
	 * response represents a redirect as of
	 * {@link HttpStatusCode#isRedirectStatus()}. A value of -1 represents the
	 * default behavior, e.g. using {@link java.net.HttpURLConnection}'s
	 * redirection means.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface RedirectDepthBuilder<B extends RedirectDepthBuilder<B>> {

		/**
		 * Sets the redirect depth for the redirect depth property.
		 * 
		 * @param aRedirectDepth The redirect depth to be stored by the redirect
		 *        depth property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withRedirectDepth( int aRedirectDepth );
	}

	/**
	 * Provides a redirect depth property. The redirect depth provides the count
	 * of HTTP-Request and HTTP-Response cycles where the response represents a
	 * redirect as of {@link HttpStatusCode#isRedirectStatus()}. A value of -1
	 * represents the default behavior, e.g. using
	 * {@link java.net.HttpURLConnection}'s redirection means.
	 */
	public interface RedirectDepthProperty extends RedirectDepthAccessor, RedirectDepthMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setRedirectDepth(int)} and returns the very same value
		 * (getter).
		 * 
		 * @param aRedirectDepth The integer to set (via
		 *        {@link #setRedirectDepth(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letRedirectDepth( int aRedirectDepth ) {
			setRedirectDepth( aRedirectDepth );
			return aRedirectDepth;
		}
	}
}
