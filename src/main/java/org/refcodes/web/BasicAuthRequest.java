// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import org.refcodes.mixin.Dumpable;

/**
 * Defines a {@link BasicAuthRequest} describes a Baisc-Auth request as of HTTP.
 * See also "https://en.wikipedia.org/wiki/Basic_access_authentication".
 */
public class BasicAuthRequest implements BasicAuthCredentialsAccessor, HttpMethodAccessor, UrlAccessor, Dumpable {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final BasicAuthCredentials _credentials;
	private final HttpMethod _httpMethod;
	private final Url _url;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link BasicAuthRequest} instance with required attributes.
	 * 
	 * @param aHttpMethod The {@link HttpMethod} with which the request has been
	 *        sent.
	 * @param aUrl The {@link Url} from which to take the URL specific data.
	 * @param aCredentials The credentials retrieved from the request.
	 */
	public BasicAuthRequest( HttpMethod aHttpMethod, Url aUrl, BasicAuthCredentials aCredentials ) {
		_httpMethod = aHttpMethod;
		_url = aUrl;
		_credentials = aCredentials;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BasicAuthCredentials getBasicAuthCredentials() {
		return _credentials;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpMethod getHttpMethod() {
		return _httpMethod;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Url getUrl() {
		return _url;
	}
}
