// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * The {@link PostHttpInterceptable} provides base functionality for working
 * with {@link PostHttpInterceptor} instances.
 *
 * @param <I> The type of {@link PostHttpInterceptor} to be managed.
 */
public interface PostHttpInterceptable<I extends PostHttpInterceptor<?, ?>> {

	/**
	 * Tests whether the given {@link PostHttpInterceptor} instance has been
	 * added.
	 * 
	 * @param aPostInterceptor The {@link PostHttpInterceptor} instance for
	 *        which to test if it has been added.
	 * 
	 * @return True if the given {@link PostHttpInterceptor} instance has been
	 *         added already.
	 */
	boolean hasPostHttpInterceptor( I aPostInterceptor );

	/**
	 * Adds the given {@link PostHttpInterceptor} instance. The
	 * {@link PostHttpInterceptor} instance itself acts as the handle which is
	 * used when removing the given {@link PostHttpInterceptor} instance later.
	 * 
	 * @param aPostInterceptor The {@link PostHttpInterceptor} instance which is
	 *        to be added.
	 * 
	 * @return True if the {@link PostHttpInterceptor} instance has been added
	 *         successfully. If the {@link PostHttpInterceptor} instance has
	 *         already been added, false is returned.
	 */
	boolean addPostHttpInterceptor( I aPostInterceptor );

	/**
	 * Removes the {@link PostHttpInterceptor} instance. In case the
	 * {@link PostHttpInterceptor} instance has not been added before, then
	 * false is returned.
	 * 
	 * @param aPostInterceptor The {@link PostHttpInterceptor} instance which is
	 *        to be removed.
	 * 
	 * @return True if the {@link PostHttpInterceptor} instance has been removed
	 *         successfully. If there was none such {@link PostHttpInterceptor}
	 *         instance or if the {@link PostHttpInterceptor} instance has
	 *         already been removed, then false is returned.
	 */
	boolean removePostHttpInterceptor( I aPostInterceptor );

}