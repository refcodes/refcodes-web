// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import java.util.List;

import org.refcodes.web.UrlAccessor.UrlBuilder;
import org.refcodes.web.UrlAccessor.UrlMutator;

/**
 * The Interface HttpRequestBuilder.
 *
 * @param <B> the generic type
 */
public interface HttpRequestBuilder<B extends HttpRequestBuilder<B>> extends HttpRequest, UrlMutator, UrlBuilder {

	/**
	 * Builder method for the {@link #getHeaderFields()} method
	 * {@link RequestHeaderFields#addTo(String, String)}.
	 * 
	 * @param aField The Header-Field (key) of which's list of values a value is
	 *        to be added.
	 * @param aValue The value to be added to the list of values associated to
	 *        the given Header-Field (key).
	 * 
	 * @return This {@link HttpRequestBuilder} instance to continue building up
	 *         the Header-Fields.
	 */
	@SuppressWarnings("unchecked")
	default B withAddToHeaderFields( String aField, String aValue ) {
		getHeaderFields().put( aField, aValue );
		return (B) this;
	}

	/**
	 * Builder method for the {@link #getHeaderFields()} method
	 * {@link RequestHeaderFields#addTo(String, String...)}.
	 * 
	 * @param aField The Header-Field (key) of which's list of values the values
	 *        are to be added.
	 * @param aValues The values to be added to the list of values associated to
	 *        the given Header-Field (key).
	 * 
	 * @return This {@link HttpRequestBuilder} instance to continue building up
	 *         the Header-Fields.
	 */
	@SuppressWarnings("unchecked")
	default B withAddToHeaderFields( String aField, String... aValues ) {
		getHeaderFields().put( aField, aValues );
		return (B) this;
	}

	/**
	 * Builder method for the {@link #getHeaderFields()} method
	 * {@link RequestHeaderFields#addTo(String, List)}.
	 * 
	 * @param aField The Header-Field (key) of which's list of values the values
	 *        are to be added.
	 * @param aValues The values to be added to the list of values associated to
	 *        the given Header-Field (key).
	 * 
	 * @return This {@link HttpRequestBuilder} instance to continue building up
	 *         the Header-Fields.
	 */
	@SuppressWarnings("unchecked")
	default B withAddToHeaderFields( String aField, List<String> aValues ) {
		getHeaderFields().put( aField, aValues );
		return (B) this;
	}

	/**
	 * With add to Header-Fields.
	 *
	 * @param aHeaderField the Header-Field
	 * @param aValues the values
	 * 
	 * @return the b
	 * 
	 * @see #withAddToHeaderFields(String, String...)
	 */
	default B withAddToHeaderFields( HeaderField aHeaderField, String... aValues ) {
		return withAddToHeaderFields( aHeaderField.getName(), aValues );
	}

	/**
	 * With add to Header-Fields.
	 *
	 * @param aHeaderField the Header-Field
	 * @param aValues the values
	 * 
	 * @return the b
	 * 
	 * @see #withAddToHeaderFields(String, List)
	 */
	default B withAddToHeaderFields( HeaderField aHeaderField, List<String> aValues ) {
		return withAddToHeaderFields( aHeaderField.getName(), aValues );
	}

	/**
	 * Builder method for the {@link HttpRequestBuilder#getUrl()}'s
	 * {@link Url#getQueryFields()} method
	 * {@link FormFields#addTo(String, String)}.
	 * 
	 * @param aField The Header-Field (key) of which's list of values a value is
	 *        to be added.
	 * @param aValue The value to be added to the list of values associated to
	 *        the given Header-Field (key).
	 * 
	 * @return This {@link HttpRequestBuilder} instance to continue building up
	 *         the Header-Fields.
	 */
	@SuppressWarnings("unchecked")
	default B withAddToQueryFields( String aField, String aValue ) {
		getUrl().getQueryFields().addTo( aField, aValue );
		return (B) this;
	}

	/**
	 * Builder method for the {@link HttpRequestBuilder#getUrl()}'s
	 * {@link Url#getQueryFields()} method
	 * {@link FormFields#addTo(String, String...)}.
	 * 
	 * @param aField The Header-Field (key) of which's list of values the values
	 *        are to be added.
	 * @param aValues The values to be added to the list of values associated to
	 *        the given Header-Field (key).
	 * 
	 * @return This {@link HttpRequestBuilder} instance to continue building up
	 *         the Header-Fields.
	 */
	@SuppressWarnings("unchecked")
	default B withAddToQueryFields( String aField, String... aValues ) {
		getUrl().getQueryFields().put( aField, aValues );
		return (B) this;
	}

	/**
	 * Builder method for the {@link HttpRequestBuilder#getUrl()}'s
	 * {@link Url#getQueryFields()} method
	 * {@link FormFields#addTo(String, List)}.
	 * 
	 * @param aField The Header-Field (key) of which's list of values the values
	 *        are to be added.
	 * @param aValues The values to be added to the list of values associated to
	 *        the given Header-Field (key).
	 * 
	 * @return This {@link HttpRequestBuilder} instance to continue building up
	 *         the Header-Fields.
	 */
	@SuppressWarnings("unchecked")
	default B withAddToQueryFields( String aField, List<String> aValues ) {
		getUrl().getQueryFields().put( aField, aValues );
		return (B) this;
	}

	/**
	 * This is a convenience method for easily instantiating the according
	 * builder.
	 * 
	 * @return an instance (using a default implementation) of this builder
	 */
	//	static HttpRequestBuilder build() {
	//		return new HttpRequestBuilderImpl();
	//	}
}
