// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * The Class TemporaryRedirectRuntimeException.
 */
public class TemporaryRedirectRuntimeException extends HttpStatusRuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aMessage The aMessage describing this exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public TemporaryRedirectRuntimeException( String aMessage, String aErrorCode ) {
		super( aMessage, HttpStatusCode.TEMPORARY_REDIRECT, aErrorCode );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aMessage The aMessage describing this exception.
	 * @param aCause The {@link Throwable} ({@link RuntimeException}) causing
	 *        this exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public TemporaryRedirectRuntimeException( String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, HttpStatusCode.TEMPORARY_REDIRECT, aCause, aErrorCode );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aMessage The aMessage describing this exception.
	 * @param aCause The {@link Throwable} ({@link RuntimeException}) causing
	 *        this exception.
	 */
	public TemporaryRedirectRuntimeException( String aMessage, Throwable aCause ) {
		super( aMessage, HttpStatusCode.TEMPORARY_REDIRECT, aCause );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aMessage The aMessage describing this exception.
	 */
	public TemporaryRedirectRuntimeException( String aMessage ) {
		super( aMessage, HttpStatusCode.TEMPORARY_REDIRECT );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aCause The {@link Throwable} ({@link RuntimeException}) causing
	 *        this exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public TemporaryRedirectRuntimeException( Throwable aCause, String aErrorCode ) {
		super( HttpStatusCode.TEMPORARY_REDIRECT, aCause, aErrorCode );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aCause The {@link Throwable} ({@link RuntimeException}) causing
	 *        this exception.
	 */
	public TemporaryRedirectRuntimeException( Throwable aCause ) {
		super( HttpStatusCode.TEMPORARY_REDIRECT, aCause );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aMessage The aMessage describing this exception.
	 * @param aUrl The URL involved in this exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public TemporaryRedirectRuntimeException( String aMessage, Url aUrl, String aErrorCode ) {
		super( aMessage, HttpStatusCode.TEMPORARY_REDIRECT, aUrl, aErrorCode );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aMessage The aMessage describing this exception.
	 * @param aUrl The URL involved in this exception.
	 * @param aCause The {@link Throwable} ({@link RuntimeException}) causing
	 *        this exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public TemporaryRedirectRuntimeException( String aMessage, Url aUrl, Throwable aCause, String aErrorCode ) {
		super( aMessage, HttpStatusCode.TEMPORARY_REDIRECT, aUrl, aCause, aErrorCode );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aMessage The aMessage describing this exception.
	 * @param aUrl The URL involved in this exception.
	 * @param aCause The {@link Throwable} ({@link RuntimeException}) causing
	 *        this exception.
	 */
	public TemporaryRedirectRuntimeException( String aMessage, Url aUrl, Throwable aCause ) {
		super( aMessage, HttpStatusCode.TEMPORARY_REDIRECT, aUrl, aCause );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aMessage The aMessage describing this exception.
	 * @param aUrl The URL involved in this exception.
	 */
	public TemporaryRedirectRuntimeException( String aMessage, Url aUrl ) {
		super( aMessage, HttpStatusCode.TEMPORARY_REDIRECT, aUrl );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aUrl The URL involved in this exception.
	 * @param aCause The {@link Throwable} ({@link RuntimeException}) causing
	 *        this exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public TemporaryRedirectRuntimeException( Url aUrl, Throwable aCause, String aErrorCode ) {
		super( HttpStatusCode.TEMPORARY_REDIRECT, aUrl, aCause, aErrorCode );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aUrl The URL involved in this exception.
	 * @param aCause The {@link Throwable} ({@link RuntimeException}) causing
	 *        this exception.
	 */
	public TemporaryRedirectRuntimeException( Url aUrl, Throwable aCause ) {
		super( HttpStatusCode.TEMPORARY_REDIRECT, aUrl, aCause );
	}
}
