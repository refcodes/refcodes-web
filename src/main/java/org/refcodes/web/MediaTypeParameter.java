// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;
///////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
///////////////////////////////////////////////////////////////////////////////

import org.refcodes.data.MarshalParameter;
import org.refcodes.mixin.NameAccessor;

/**
 * Known or in this scope used {@link MediaType} parameters.
 */
public enum MediaTypeParameter implements NameAccessor {

	CHARSET(MarshalParameter.CHARSET.getName()),

	BASE_URL(MarshalParameter.BASE_URL.getName());

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _parameterName;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new media type parameter.
	 *
	 * @param aParameterName the parameter name
	 */
	private MediaTypeParameter( String aParameterName ) {
		_parameterName = aParameterName;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	@Override
	public String getName() {
		return _parameterName;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns that {@link MediaTypeParameter} represented by the given HTTP
	 * Media-Type parameter.
	 * 
	 * @param aHttpMediaTypeParameter The HTTP Media-Type for which to determine
	 *        the {@link MediaTypeParameter}.
	 * 
	 * @return The determined {@link MediaTypeParameter} or null if none was
	 *         determinable.
	 */
	public static MediaTypeParameter fromHttpMediaTypeParameter( String aHttpMediaTypeParameter ) {
		for ( MediaTypeParameter eMediaTypeParameter : values() ) {
			if ( aHttpMediaTypeParameter.equalsIgnoreCase( eMediaTypeParameter.getName() ) ) {
				return eMediaTypeParameter;
			}
		}
		return null;
	}
}
