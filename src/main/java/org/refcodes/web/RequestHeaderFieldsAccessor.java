// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides an accessor for an request Header-Fields property.
 */
public interface RequestHeaderFieldsAccessor {

	/**
	 * Retrieves the request Header-Fields from the request Header-Fields
	 * property.
	 * 
	 * @return The request Header-Fields stored by the request Header-Fields
	 *         property.
	 */
	RequestHeaderFields getRequestHeaderFields();

	/**
	 * Provides a mutator for an request Header-Fields property.
	 */
	public interface RequestHeaderFieldsMutator {

		/**
		 * Sets the request Header-Fields for the request Header-Fields
		 * property.
		 * 
		 * @param aRequestHeaderFields The request Header-Fields to be stored by
		 *        the Header-Fields property.
		 */
		void setRequestHeaderFields( RequestHeaderFields aRequestHeaderFields );
	}

	/**
	 * Provides a mutator for an request Header-Fields property.
	 * 
	 * @param <B> The builder which implements the
	 *        {@link RequestHeaderFieldsBuilder}.
	 */
	public interface RequestHeaderFieldsBuilder<B extends RequestHeaderFieldsBuilder<?>> {

		/**
		 * Sets the request Header-Fields to use and returns this builder as of
		 * the Builder-Pattern.
		 * 
		 * @param aRequestHeaderFields The request Header-Fields to be stored by
		 *        the Header-Fields property.
		 * 
		 * @return This {@link RequestHeaderFieldsBuilder} instance to continue
		 *         configuration.
		 */
		B withRequestHeaderFields( RequestHeaderFields aRequestHeaderFields );
	}

	/**
	 * Provides an request Header-Fields property.
	 */
	public interface RequestHeaderFieldsProperty extends RequestHeaderFieldsAccessor, RequestHeaderFieldsMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link RequestHeaderFields} (setter) as of
		 * {@link #setRequestHeaderFields(RequestHeaderFields)} and returns the
		 * very same value (getter).
		 * 
		 * @param aRequestHeaderFields The {@link RequestHeaderFields} to set
		 *        (via {@link #setRequestHeaderFields(RequestHeaderFields)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default RequestHeaderFields letRequestHeaderFields( RequestHeaderFields aRequestHeaderFields ) {
			setRequestHeaderFields( aRequestHeaderFields );
			return aRequestHeaderFields;
		}
	}
}
