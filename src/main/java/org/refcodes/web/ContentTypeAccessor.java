// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides an accessor for a Content-Type property.
 */
public interface ContentTypeAccessor {

	/**
	 * Retrieves the Content-Type from the Content-Type property.
	 * 
	 * @return The Content-Type stored by the Content-Type property.
	 */
	ContentType getContentType();

	/**
	 * A provider interface provides a "toSomething(?)" method which converts a
	 * given instance into something else. The {@link ContentTypeProvider}
	 * converts an implementing instance's state into a {@link ContentType}
	 * instance.
	 */
	public interface ContentTypeProvider {

		/**
		 * Returns the {@link ContentType} instance from the implementing
		 * instance.
		 * 
		 * @return The according {@link ContentType} instance represented by the
		 *         implementing instance.
		 */
		ContentType toContentType();

	}

	/**
	 * Provides a mutator for a Content-Type property.
	 */
	public interface ContentTypeMutator {

		/**
		 * Sets the Content-Type for the Content-Type property.
		 * 
		 * @param aContentType The Content-Type to be stored by the Content-Type
		 *        property.
		 */
		void setContentType( ContentType aContentType );
	}

	/**
	 * Provides a builder method for a Content-Type property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ContentTypeBuilder<B extends ContentTypeBuilder<B>> {

		/**
		 * Sets the Content-Type for the Content-Type property.
		 * 
		 * @param aContentType The Content-Type to be stored by the Content-Type
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withContentType( ContentType aContentType );
	}

	/**
	 * Provides a Content-Type property.
	 */
	public interface ContentTypeProperty extends ContentTypeAccessor, ContentTypeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link ContentType}
		 * (setter) as of {@link #setContentType(ContentType)} and returns the
		 * very same value (getter).
		 * 
		 * @param aContentType The {@link ContentType} to set (via
		 *        {@link #setContentType(ContentType)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ContentType letContentType( ContentType aContentType ) {
			setContentType( aContentType );
			return aContentType;
		}
	}
}
