// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import org.refcodes.mixin.StatusCodeAccessor;

/**
 * Only the Success 2xx codes as of (from) the {@link HttpStatusCode}
 * enumeration.
 */
public enum HttpSuccessCode implements StatusCodeAccessor<Integer> {

	// /////////////////////////////////////////////////////////////////////////
	// 2XX SUCCESS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * 200 OK (HTTP/1.0 - RFC 1945)
	 */
	OK(200),

	/**
	 * 201 Created (HTTP/1.0 - RFC 1945)
	 */
	CREATED(201),

	/**
	 * 202 Accepted (HTTP/1.0 - RFC 1945)
	 */
	ACCEPTED(202),

	/**
	 * 203 Non Authoritative Information (HTTP/1.1 - RFC 2616)
	 */
	NON_AUTHORITATIVE_INFORMATION(203),

	/**
	 * 204 No Content (HTTP/1.0 - RFC 1945)
	 */
	NO_CONTENT(204),

	/**
	 * 205 Reset Content (HTTP/1.1 - RFC 2616)
	 */
	RESET_CONTENT(205),

	/**
	 * 206 Partial Content (HTTP/1.1 - RFC 2616)
	 */
	PARTIAL_CONTENT(206),

	/**
	 * 207 Multi-Status (WebDAV - RFC 2518) or 207 Partial Update OK (HTTP/1.1 -
	 * draft-ietf-http-v11-spec-rev-01?)
	 */
	MULTI_STATUS(207);

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Integer _statusCode;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new http success code.
	 *
	 * @param aStatusCode the status code
	 */
	private HttpSuccessCode( Integer aStatusCode ) {
		_statusCode = aStatusCode;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer getStatusCode() {
		return _statusCode;
	}

	/**
	 * Determines the HTTP success code from the given value by evaluating the
	 * {@link #getStatusCode()} property.
	 * 
	 * @param aHttpStatusCode The code from which to get the
	 *        {@link HttpStatusCode} element.
	 * 
	 * @return The according {@link HttpStatusCode} element or null if none was
	 *         found.
	 */
	public static HttpSuccessCode toHttpSuccessCode( int aHttpStatusCode ) {
		for ( HttpSuccessCode eCode : values() ) {
			if ( eCode.getStatusCode().equals( aHttpStatusCode ) ) {
				return eCode;
			}
		}
		return null;
	}

	/**
	 * Determines the HTTP success code from the given HTTP Status-Code property
	 * by evaluating the {@link #getStatusCode()} property.
	 * 
	 * @param aHttpStatusCode The code property from which
	 *        {@link StatusCodeAccessor#getStatusCode()} to get the
	 *        {@link HttpStatusCode} element.
	 * 
	 * @return The according {@link HttpStatusCode} element or null if none was
	 *         found.
	 */
	public static HttpSuccessCode toHttpSuccessCode( StatusCodeAccessor<Integer> aHttpStatusCode ) {
		for ( HttpSuccessCode eCode : values() ) {
			if ( eCode.getStatusCode().equals( aHttpStatusCode.getStatusCode() ) ) {
				return eCode;
			}
		}
		return null;
	}
}
