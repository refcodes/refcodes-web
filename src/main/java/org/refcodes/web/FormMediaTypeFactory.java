package org.refcodes.web;

import java.util.List;
import java.util.Map;

import org.refcodes.exception.MarshalException;
import org.refcodes.exception.UnmarshalException;

/**
 * Implements the {@link MediaTypeFactory} for Media-Type
 * "application/x-www-form-urlencoded" (
 * {@link MediaType#APPLICATION_X_WWW_FORM_URLENCODED}). CAUTION: This
 * implementation can only marshal objects of (sub-)type {@link Map} of generic
 * type {@link String} (key) and {@link List} containing {@link String}
 * instances (value) as argument. Also supported is the derived
 * {@link FormFields} type.
 */
public class FormMediaTypeFactory implements MediaTypeFactory {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MediaType[] getMediaTypes() {
		return new MediaType[] { MediaType.APPLICATION_X_WWW_FORM_URLENCODED };
	}

	/**
	 * This method expects an instance of (sub-)type {@link Map} of generic type
	 * {@link String} (key) and {@link List} containing {@link String} instances
	 * (value) as argument. Also supported is the derived {@link FormFields}
	 * type. {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String toMarshaled( Object aObject ) throws MarshalException {
		try {
			if ( aObject instanceof FormFields ) {
				return ( (FormFields) aObject ).toBodyFormFields();
			}
			else if ( aObject instanceof Map<?, ?> ) {
				final FormFields theFormFields = new FormFields();
				theFormFields.putAll( (Map<String, List<String>>) aObject );
				return theFormFields.toBodyFormFields();
			}
			throw new MarshalException( "Cannot marshal HTTP Form-Fields from <" + aObject + "> to type  <" + String.class.getName() + "> as this factory can only marshal HTTP Form-Fields of (sub-)type \"Map<String, List<String>\"." );

		}
		catch ( Exception e ) {
			throw new MarshalException( "A problem occurred marshaling an object of type <" + aObject.getClass().getName() + ">.", e );
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T toUnmarshaled( String aHttpBody, Class<T> aType ) throws UnmarshalException {
		try {
			if ( FormFields.class.equals( aType ) ) {
				return aHttpBody != null ? (T) new FormFields( aHttpBody ) : null;
			}
			if ( FormFields.class.isAssignableFrom( aType ) ) {
				final FormFields theFormFields = (FormFields) aType.getConstructor().newInstance();
				if ( aHttpBody != null && aHttpBody.length() != 0 ) {
					theFormFields.fromBodyFormFields( aHttpBody );
				}
				return (T) theFormFields;
			}
			throw new UnmarshalException( "Cannot unmarshal HTTP Form-Fields \"" + aHttpBody + "\" to type  <" + aType.getName() + "> as this factory only supports the type <" + FormFields.class.getName() + ">!" );
		}
		catch ( Exception e ) {
			throw new UnmarshalException( "Unable to unmarshal the HTTP body \"" + aHttpBody + "\" to type  <" + aType.getName() + ">!", e );
		}
	}
}
