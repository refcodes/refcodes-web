// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Thrown in case HTTP basic authorization is required but no required
 * {@link HeaderField#WWW_AUTHENTICATE} data was found, e.g.
 * {@link BasicAuthCredentials} was null.
 */
public class BasicAuthRequiredException extends UnauthorizedException {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public BasicAuthRequiredException( String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public BasicAuthRequiredException( String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public BasicAuthRequiredException( String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public BasicAuthRequiredException( String aMessage ) {
		super( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	public BasicAuthRequiredException( Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public BasicAuthRequiredException( Throwable aCause ) {
		super( aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public BasicAuthRequiredException( String aMessage, Url aUrl, String aErrorCode ) {
		super( aMessage, aUrl, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public BasicAuthRequiredException( String aMessage, Url aUrl, Throwable aCause, String aErrorCode ) {
		super( aMessage, aUrl, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public BasicAuthRequiredException( String aMessage, Url aUrl, Throwable aCause ) {
		super( aMessage, aUrl, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public BasicAuthRequiredException( String aMessage, Url aUrl ) {
		super( aMessage, aUrl );
	}

	/**
	 * {@inheritDoc}
	 */
	public BasicAuthRequiredException( Url aUrl, Throwable aCause, String aErrorCode ) {
		super( aUrl, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public BasicAuthRequiredException( Url aUrl, Throwable aCause ) {
		super( aUrl, aCause );
	}
}
