// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import java.io.InputStream;

import org.refcodes.web.ContentTypeAccessor.ContentTypeProvider;

/**
 * Provides an accessor for a HTTP {@link InputStream} body property.
 */
public interface HttpInputStreamAccessor {

	/**
	 * Retrieves the {@link InputStream} from the HTTP {@link InputStream}
	 * property.
	 * 
	 * @return The {@link InputStream} stored by the {@link InputStream}
	 *         property.
	 */
	InputStream getHttpInputStream();

	/**
	 * A provider interface provides a "toSomething(?)" method which converts a
	 * given instance into something else. The {@link ContentTypeProvider}
	 * converts an implementing instance's state into a {@link ContentType}
	 * instance.
	 * 
	 * @param <EXC> The exception type which may be thrown upon converting to
	 *        the desired type.
	 */
	public interface HttpInputStreamProvider<EXC extends Exception> {

		/**
		 * Returns the {@link InputStream} instance representing the HTTP body
		 * from the implementing instance.
		 * 
		 * @return The according {@link InputStream} instance represented by the
		 *         implementing instance.
		 * 
		 * @throws EXC Thrown in case providing the type as {@link InputStream}
		 *         failed.
		 */
		InputStream toHttpInputStream() throws EXC;

	}

	/**
	 * Provides a mutator for a {@link InputStream} property.
	 */
	public interface HttpInputStreamMutator {

		/**
		 * Sets the {@link InputStream} for the HTTP {@link InputStream}
		 * property.
		 * 
		 * @param aInputStream The {@link InputStream} to be stored by the HTTP
		 *        {@link InputStream} property.
		 */
		void setHttpInputStream( InputStream aInputStream );
	}

	/**
	 * Provides a builder body for a HTTP {@link InputStream} property returning
	 * the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface HttpInputStreamBuilder<B extends HttpInputStreamBuilder<B>> {

		/**
		 * Sets the {@link InputStream} for the HTTP {@link InputStream}
		 * property.
		 * 
		 * @param aInputStream The {@link InputStream} to be stored by the
		 *        {@link InputStream} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withHttpInputStream( InputStream aInputStream );
	}

	/**
	 * Provides a HTTP {@link InputStream} property.
	 */
	public interface HttpInputStreamProperty extends HttpInputStreamAccessor, HttpInputStreamMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link InputStream}
		 * (setter) as of {@link #setHttpInputStream(InputStream)} and returns
		 * the very same value (getter).
		 * 
		 * @param aInputStream The {@link InputStream} to set (via
		 *        {@link #setHttpInputStream(InputStream)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default InputStream letHttpInputStream( InputStream aInputStream ) {
			setHttpInputStream( aInputStream );
			return aInputStream;
		}
	}
}
