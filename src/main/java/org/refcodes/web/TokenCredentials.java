// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import org.refcodes.mixin.TokenAccessor;
import org.refcodes.mixin.Validatable;

/**
 * The Interface TokenCredentials.
 */
public interface TokenCredentials extends TokenAccessor, Validatable<TokenCredentials> {

	/**
	 * Validates the provided {@link TokenCredentials} against this
	 * {@link TokenCredentials} user-name and secret. If this
	 * {@link TokenCredentials} instance's user-name or password are null, then
	 * false is returned.
	 * 
	 * @param aCredentials The {@link TokenCredentials} to be tested if them fit
	 *        with the this {@link TokenCredentials} instance.
	 * 
	 * @return True if the {@link TokenCredentials} match with the this
	 *         {@link TokenCredentials} instance and this
	 *         {@link BasicAuthCredentials} instance's user-name and secret are
	 *         not null.
	 */
	@Override
	default boolean isValid( TokenCredentials aCredentials ) {
		if ( getToken() == null ) {
			return false;
		}
		return getToken().equals( aCredentials.getToken() );
	}

	/**
	 * Validates the provided user-name and secret against this
	 * {@link TokenCredentials} user-name and secret. If this
	 * {@link TokenCredentials} instance's user-name or password are null, then
	 * false is returned.
	 *
	 * @param aToken the token
	 * 
	 * @return True if the user-name and secret match with the this
	 *         {@link TokenCredentials} instance and this
	 *         {@link BasicAuthCredentials} instance's user-name and secret are
	 *         not null.
	 */
	default boolean isValid( String aToken ) {
		return getToken() == null ? false : getToken().equals( aToken );
	}
}
