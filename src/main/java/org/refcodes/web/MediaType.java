// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import java.util.ArrayList;
import java.util.List;

import org.refcodes.data.Delimiter;
import org.refcodes.mixin.NameAccessor;
import org.refcodes.properties.Properties;
import org.refcodes.web.ContentTypeAccessor.ContentTypeProvider;

/**
 * The {@link MediaType} is the Media-Sub-Type part of a HTTP Media-Type. As a
 * Media-Sub-Type can only exist with a Media-Top-Level-Type (see
 * {@link TopLevelType}), the according {@link TopLevelType} can be retrieved by
 * the {@link #getTopLevelType()} method. Calling {@link #toHttpMediaType()} you
 * can create a complete HTTP Media-Type (without the concrete header's use-case
 * specific parameters). Therefore we call the HTTP Media-Sub-Type in this
 * context {@link MediaType}. Using {@link #toContentType()} you may create a
 * Media-Type with the means to attach parameters as found in concrete
 * occurrences of the HTTP Media-Type in the HTTP headers. Given the HTTP
 * Media-Type "application/json", "application" is considered to be the
 * Media-Namespace (top level Media-Type) and "json" to be the Sub-Media-Type.
 * The method {@link #toHttpMediaType()} returns the full HTTP conforming
 * Media-Type. E.g. calling <code>toHttpMediaType()</code> on
 * {@link #APPLICATION_JSON} will return "application/json". This enumeration
 * contains the MIME-Types as registered at IANA's MIME-Type list found at
 * "http://www.iana.org/assignments/media-types/media-types.xhtml" Use the shell
 * script "grab-mediatypes.sh" to generate a new list of Media-Types from
 * iana.org. As of common speaking, the Sub-Media-Type is usually called
 * Media-Type. Therefore the Sub-Media-Types are gathered in the
 * {@link MediaType} enumeration to avoid misunderstanding.
 */
public enum MediaType implements TopLevelTypeAccessor, NameAccessor, ContentTypeProvider, HttpMediaType {

	APPLICATION_1D_INTERLEAVED_PARITYFEC(TopLevelType.APPLICATION, "1d-interleaved-parityfec"),

	APPLICATION_3GPDASH_QOE_REPORT_XML(TopLevelType.APPLICATION, "3gpdash-qoe-report+xml"),

	APPLICATION_3GPP_IMS_XML(TopLevelType.APPLICATION, "3gpp-ims+xml"),

	APPLICATION_A2L(TopLevelType.APPLICATION, "A2L"),

	APPLICATION_ACTIVEMESSAGE(TopLevelType.APPLICATION, "activemessage"),

	APPLICATION_ALTO_COSTMAP_JSON(TopLevelType.APPLICATION, "alto-costmap+json"),

	APPLICATION_ALTO_COSTMAPFILTER_JSON(TopLevelType.APPLICATION, "alto-costmapfilter+json"),

	APPLICATION_ALTO_DIRECTORY_JSON(TopLevelType.APPLICATION, "alto-directory+json"),

	APPLICATION_ALTO_ENDPOINTPROP_JSON(TopLevelType.APPLICATION, "alto-endpointprop+json"),

	APPLICATION_ALTO_ENDPOINTPROPPARAMS_JSON(TopLevelType.APPLICATION, "alto-endpointpropparams+json"),

	APPLICATION_ALTO_ENDPOINTCOST_JSON(TopLevelType.APPLICATION, "alto-endpointcost+json"),

	APPLICATION_ALTO_ENDPOINTCOSTPARAMS_JSON(TopLevelType.APPLICATION, "alto-endpointcostparams+json"),

	APPLICATION_ALTO_ERROR_JSON(TopLevelType.APPLICATION, "alto-error+json"),

	APPLICATION_ALTO_NETWORKMAPFILTER_JSON(TopLevelType.APPLICATION, "alto-networkmapfilter+json"),

	APPLICATION_ALTO_NETWORKMAP_JSON(TopLevelType.APPLICATION, "alto-networkmap+json"),

	APPLICATION_AML(TopLevelType.APPLICATION, "AML"),

	APPLICATION_ANDREW_INSET(TopLevelType.APPLICATION, "andrew-inset"),

	APPLICATION_APPLEFILE(TopLevelType.APPLICATION, "applefile"),

	APPLICATION_ATF(TopLevelType.APPLICATION, "ATF"),

	APPLICATION_ATFX(TopLevelType.APPLICATION, "ATFX"),

	APPLICATION_ATOM_XML(TopLevelType.APPLICATION, "atom+xml"),

	APPLICATION_ATOMCAT_XML(TopLevelType.APPLICATION, "atomcat+xml"),

	APPLICATION_ATOMDELETED_XML(TopLevelType.APPLICATION, "atomdeleted+xml"),

	APPLICATION_ATOMICMAIL(TopLevelType.APPLICATION, "atomicmail"),

	APPLICATION_ATOMSVC_XML(TopLevelType.APPLICATION, "atomsvc+xml"),

	APPLICATION_ATXML(TopLevelType.APPLICATION, "ATXML"),

	APPLICATION_AUTH_POLICY_XML(TopLevelType.APPLICATION, "auth-policy+xml"),

	APPLICATION_BACNET_XDD_ZIP(TopLevelType.APPLICATION, "bacnet-xdd+zip"),

	APPLICATION_BATCH_SMTP(TopLevelType.APPLICATION, "batch-SMTP"),

	APPLICATION_BEEP_XML(TopLevelType.APPLICATION, "beep+xml"),

	APPLICATION_CALENDAR_JSON(TopLevelType.APPLICATION, "calendar+json"),

	APPLICATION_CALENDAR_XML(TopLevelType.APPLICATION, "calendar+xml"),

	APPLICATION_CALL_COMPLETION(TopLevelType.APPLICATION, "call-completion"),

	APPLICATION_CALS_1840(TopLevelType.APPLICATION, "CALS-1840"),

	APPLICATION_CBOR(TopLevelType.APPLICATION, "cbor"),

	APPLICATION_CCMP_XML(TopLevelType.APPLICATION, "ccmp+xml"),

	APPLICATION_CCXML_XML(TopLevelType.APPLICATION, "ccxml+xml"),

	APPLICATION_CDFX_XML(TopLevelType.APPLICATION, "CDFX+XML"),

	APPLICATION_CDMI_CAPABILITY(TopLevelType.APPLICATION, "cdmi-capability"),

	APPLICATION_CDMI_CONTAINER(TopLevelType.APPLICATION, "cdmi-container"),

	APPLICATION_CDMI_DOMAIN(TopLevelType.APPLICATION, "cdmi-domain"),

	APPLICATION_CDMI_OBJECT(TopLevelType.APPLICATION, "cdmi-object"),

	APPLICATION_CDMI_QUEUE(TopLevelType.APPLICATION, "cdmi-queue"),

	APPLICATION_CDNI(TopLevelType.APPLICATION, "cdni"),

	APPLICATION_CEA(TopLevelType.APPLICATION, "CEA"),

	APPLICATION_CEA_2018_XML(TopLevelType.APPLICATION, "cea-2018+xml"),

	APPLICATION_CELLML_XML(TopLevelType.APPLICATION, "cellml+xml"),

	APPLICATION_CFW(TopLevelType.APPLICATION, "cfw"),

	APPLICATION_CLUE_INFO_XML(TopLevelType.APPLICATION, "clue_info+xml"),

	APPLICATION_CMS(TopLevelType.APPLICATION, "cms"),

	APPLICATION_CNRP_XML(TopLevelType.APPLICATION, "cnrp+xml"),

	APPLICATION_COAP_GROUP_JSON(TopLevelType.APPLICATION, "coap-group+json"),

	APPLICATION_COMMONGROUND(TopLevelType.APPLICATION, "commonground"),

	APPLICATION_CONFERENCE_INFO_XML(TopLevelType.APPLICATION, "conference-info+xml"),

	APPLICATION_CPL_XML(TopLevelType.APPLICATION, "cpl+xml"),

	APPLICATION_CSRATTRS(TopLevelType.APPLICATION, "csrattrs"),

	APPLICATION_CSTA_XML(TopLevelType.APPLICATION, "csta+xml"),

	APPLICATION_CSTADATA_XML(TopLevelType.APPLICATION, "CSTAdata+xml"),

	APPLICATION_CSVM_JSON(TopLevelType.APPLICATION, "csvm+json"),

	APPLICATION_CYBERCASH(TopLevelType.APPLICATION, "cybercash"),

	APPLICATION_DASH_XML(TopLevelType.APPLICATION, "dash+xml"),

	APPLICATION_DASHDELTA(TopLevelType.APPLICATION, "dashdelta"),

	APPLICATION_DAVMOUNT_XML(TopLevelType.APPLICATION, "davmount+xml"),

	APPLICATION_DCA_RFT(TopLevelType.APPLICATION, "dca-rft"),

	APPLICATION_DCD(TopLevelType.APPLICATION, "DCD"),

	APPLICATION_DEC_DX(TopLevelType.APPLICATION, "dec-dx"),

	APPLICATION_DIALOG_INFO_XML(TopLevelType.APPLICATION, "dialog-info+xml"),

	APPLICATION_DICOM(TopLevelType.APPLICATION, "dicom"),

	APPLICATION_DII(TopLevelType.APPLICATION, "DII"),

	APPLICATION_DIT(TopLevelType.APPLICATION, "DIT"),

	APPLICATION_DNS(TopLevelType.APPLICATION, "dns"),

	APPLICATION_DSKPP_XML(TopLevelType.APPLICATION, "dskpp+xml"),

	APPLICATION_DSSC_DER(TopLevelType.APPLICATION, "dssc+der"),

	APPLICATION_DSSC_XML(TopLevelType.APPLICATION, "dssc+xml"),

	APPLICATION_DVCS(TopLevelType.APPLICATION, "dvcs"),

	APPLICATION_ECMASCRIPT(TopLevelType.APPLICATION, "ecmascript"),

	APPLICATION_EDI_CONSENT(TopLevelType.APPLICATION, "EDI-consent"),

	APPLICATION_EDIFACT(TopLevelType.APPLICATION, "EDIFACT"),

	APPLICATION_EDI_X12(TopLevelType.APPLICATION, "EDI-X12"),

	APPLICATION_EFI(TopLevelType.APPLICATION, "efi"),

	APPLICATION_EMERGENCYCALLDATA_COMMENT_XML(TopLevelType.APPLICATION, "EmergencyCallData.Comment+xml"),

	APPLICATION_EMERGENCYCALLDATA_DEVICEINFO_XML(TopLevelType.APPLICATION, "EmergencyCallData.DeviceInfo+xml"),

	APPLICATION_EMERGENCYCALLDATA_PROVIDERINFO_XML(TopLevelType.APPLICATION, "EmergencyCallData.ProviderInfo+xml"),

	APPLICATION_EMERGENCYCALLDATA_SERVICEINFO_XML(TopLevelType.APPLICATION, "EmergencyCallData.ServiceInfo+xml"),

	APPLICATION_EMERGENCYCALLDATA_SUBSCRIBERINFO_XML(TopLevelType.APPLICATION, "EmergencyCallData.SubscriberInfo+xml"),

	APPLICATION_EMOTIONML_XML(TopLevelType.APPLICATION, "emotionml+xml"),

	APPLICATION_ENCAPRTP(TopLevelType.APPLICATION, "encaprtp"),

	APPLICATION_EPP_XML(TopLevelType.APPLICATION, "epp+xml"),

	APPLICATION_EPUB_ZIP(TopLevelType.APPLICATION, "epub+zip"),

	APPLICATION_ESHOP(TopLevelType.APPLICATION, "eshop"),

	APPLICATION_EXAMPLE(TopLevelType.APPLICATION, "example"),

	APPLICATION_FASTINFOSET(TopLevelType.APPLICATION, "fastinfoset"),

	APPLICATION_FASTSOAP(TopLevelType.APPLICATION, "fastsoap"),

	APPLICATION_FDT_XML(TopLevelType.APPLICATION, "fdt+xml"),

	APPLICATION_FITS(TopLevelType.APPLICATION, "fits"),

	APPLICATION_FONT_SFNT(TopLevelType.APPLICATION, "font-sfnt"),

	APPLICATION_FONT_TDPFR(TopLevelType.APPLICATION, "font-tdpfr"),

	APPLICATION_FONT_WOFF(TopLevelType.APPLICATION, "font-woff"),

	APPLICATION_FRAMEWORK_ATTRIBUTES_XML(TopLevelType.APPLICATION, "framework-attributes+xml"),

	APPLICATION_GEO_JSON(TopLevelType.APPLICATION, "geo+json"),

	APPLICATION_GZIP(TopLevelType.APPLICATION, "gzip"),

	APPLICATION_HAL_JSON(TopLevelType.APPLICATION, "hal+json"),

	APPLICATION_HAL_XML(TopLevelType.APPLICATION, "hal+xml"),

	APPLICATION_H224(TopLevelType.APPLICATION, "H224"),

	APPLICATION_HELD_XML(TopLevelType.APPLICATION, "held+xml"),

	APPLICATION_HTTP(TopLevelType.APPLICATION, "http"),

	APPLICATION_HYPERSTUDIO(TopLevelType.APPLICATION, "hyperstudio"),

	APPLICATION_IBE_KEY_REQUEST_XML(TopLevelType.APPLICATION, "ibe-key-request+xml"),

	APPLICATION_IBE_PKG_REPLY_XML(TopLevelType.APPLICATION, "ibe-pkg-reply+xml"),

	APPLICATION_IBE_PP_DATA(TopLevelType.APPLICATION, "ibe-pp-data"),

	APPLICATION_IGES(TopLevelType.APPLICATION, "iges"),

	APPLICATION_IM_ISCOMPOSING_XML(TopLevelType.APPLICATION, "im-iscomposing+xml"),

	APPLICATION_INDEX(TopLevelType.APPLICATION, "index"),

	APPLICATION_INDEX_CMD(TopLevelType.APPLICATION, "index.cmd"),

	APPLICATION_INDEX_OBJ(TopLevelType.APPLICATION, "index-obj"),

	APPLICATION_INDEX_RESPONSE(TopLevelType.APPLICATION, "index.response"),

	APPLICATION_INDEX_VND(TopLevelType.APPLICATION, "index.vnd"),

	APPLICATION_INKML_XML(TopLevelType.APPLICATION, "inkml+xml"),

	APPLICATION_IOTP(TopLevelType.APPLICATION, "IOTP"),

	APPLICATION_IPFIX(TopLevelType.APPLICATION, "ipfix"),

	APPLICATION_IPP(TopLevelType.APPLICATION, "ipp"),

	APPLICATION_ISUP(TopLevelType.APPLICATION, "ISUP"),

	APPLICATION_ITS_XML(TopLevelType.APPLICATION, "its+xml"),

	APPLICATION_JAVASCRIPT(TopLevelType.APPLICATION, "javascript"),

	APPLICATION_JOSE(TopLevelType.APPLICATION, "jose"),

	APPLICATION_JOSE_JSON(TopLevelType.APPLICATION, "jose+json"),

	APPLICATION_JRD_JSON(TopLevelType.APPLICATION, "jrd+json"),

	APPLICATION_JSON(TopLevelType.APPLICATION, "json"),

	APPLICATION_JSON_PATCH_JSON(TopLevelType.APPLICATION, "json-patch+json"),

	APPLICATION_JSON_SEQ(TopLevelType.APPLICATION, "json-seq"),

	APPLICATION_JWK_JSON(TopLevelType.APPLICATION, "jwk+json"),

	APPLICATION_JWK_SET_JSON(TopLevelType.APPLICATION, "jwk-set+json"),

	APPLICATION_JWT(TopLevelType.APPLICATION, "jwt"),

	APPLICATION_KPML_REQUEST_XML(TopLevelType.APPLICATION, "kpml-request+xml"),

	APPLICATION_KPML_RESPONSE_XML(TopLevelType.APPLICATION, "kpml-response+xml"),

	APPLICATION_LD_JSON(TopLevelType.APPLICATION, "ld+json"),

	APPLICATION_LGR_XML(TopLevelType.APPLICATION, "lgr+xml"),

	APPLICATION_LINK_FORMAT(TopLevelType.APPLICATION, "link-format"),

	APPLICATION_LOAD_CONTROL_XML(TopLevelType.APPLICATION, "load-control+xml"),

	APPLICATION_LOST_XML(TopLevelType.APPLICATION, "lost+xml"),

	APPLICATION_LOSTSYNC_XML(TopLevelType.APPLICATION, "lostsync+xml"),

	APPLICATION_LXF(TopLevelType.APPLICATION, "LXF"),

	APPLICATION_MAC_BINHEX40(TopLevelType.APPLICATION, "mac-binhex40"),

	APPLICATION_MACWRITEII(TopLevelType.APPLICATION, "macwriteii"),

	APPLICATION_MADS_XML(TopLevelType.APPLICATION, "mads+xml"),

	APPLICATION_MARC(TopLevelType.APPLICATION, "marc"),

	APPLICATION_MARCXML_XML(TopLevelType.APPLICATION, "marcxml+xml"),

	APPLICATION_MATHEMATICA(TopLevelType.APPLICATION, "mathematica"),

	APPLICATION_MBMS_ASSOCIATED_PROCEDURE_DESCRIPTION_XML(TopLevelType.APPLICATION, "mbms-associated-procedure-description+xml"),

	APPLICATION_MBMS_DEREGISTER_XML(TopLevelType.APPLICATION, "mbms-deregister+xml"),

	APPLICATION_MBMS_ENVELOPE_XML(TopLevelType.APPLICATION, "mbms-envelope+xml"),

	APPLICATION_MBMS_MSK_RESPONSE_XML(TopLevelType.APPLICATION, "mbms-msk-response+xml"),

	APPLICATION_MBMS_MSK_XML(TopLevelType.APPLICATION, "mbms-msk+xml"),

	APPLICATION_MBMS_PROTECTION_DESCRIPTION_XML(TopLevelType.APPLICATION, "mbms-protection-description+xml"),

	APPLICATION_MBMS_RECEPTION_REPORT_XML(TopLevelType.APPLICATION, "mbms-reception-report+xml"),

	APPLICATION_MBMS_REGISTER_RESPONSE_XML(TopLevelType.APPLICATION, "mbms-register-response+xml"),

	APPLICATION_MBMS_REGISTER_XML(TopLevelType.APPLICATION, "mbms-register+xml"),

	APPLICATION_MBMS_SCHEDULE_XML(TopLevelType.APPLICATION, "mbms-schedule+xml"),

	APPLICATION_MBMS_USER_SERVICE_DESCRIPTION_XML(TopLevelType.APPLICATION, "mbms-user-service-description+xml"),

	APPLICATION_MBOX(TopLevelType.APPLICATION, "mbox"),

	APPLICATION_MEDIA_CONTROL_XML(TopLevelType.APPLICATION, "media_control+xml"),

	APPLICATION_MEDIA_POLICY_DATASET_XML(TopLevelType.APPLICATION, "media-policy-dataset+xml"),

	APPLICATION_MEDIASERVERCONTROL_XML(TopLevelType.APPLICATION, "mediaservercontrol+xml"),

	APPLICATION_MERGE_PATCH_JSON(TopLevelType.APPLICATION, "merge-patch+json"),

	APPLICATION_METALINK4_XML(TopLevelType.APPLICATION, "metalink4+xml"),

	APPLICATION_METS_XML(TopLevelType.APPLICATION, "mets+xml"),

	APPLICATION_MF4(TopLevelType.APPLICATION, "MF4"),

	APPLICATION_MIKEY(TopLevelType.APPLICATION, "mikey"),

	APPLICATION_MODS_XML(TopLevelType.APPLICATION, "mods+xml"),

	APPLICATION_MOSS_KEYS(TopLevelType.APPLICATION, "moss-keys"),

	APPLICATION_MOSS_SIGNATURE(TopLevelType.APPLICATION, "moss-signature"),

	APPLICATION_MOSSKEY_DATA(TopLevelType.APPLICATION, "mosskey-data"),

	APPLICATION_MOSSKEY_REQUEST(TopLevelType.APPLICATION, "mosskey-request"),

	APPLICATION_MP21(TopLevelType.APPLICATION, "mp21"),

	APPLICATION_MP4(TopLevelType.APPLICATION, "mp4"),

	APPLICATION_MPEG4_GENERIC(TopLevelType.APPLICATION, "mpeg4-generic"),

	APPLICATION_MPEG4_IOD(TopLevelType.APPLICATION, "mpeg4-iod"),

	APPLICATION_MPEG4_IOD_XMT(TopLevelType.APPLICATION, "mpeg4-iod-xmt"),

	APPLICATION_MRB_CONSUMER_XML(TopLevelType.APPLICATION, "mrb-consumer+xml"),

	APPLICATION_MRB_PUBLISH_XML(TopLevelType.APPLICATION, "mrb-publish+xml"),

	APPLICATION_MSC_IVR_XML(TopLevelType.APPLICATION, "msc-ivr+xml"),

	APPLICATION_MSC_MIXER_XML(TopLevelType.APPLICATION, "msc-mixer+xml"),

	APPLICATION_MSWORD(TopLevelType.APPLICATION, "msword"),

	APPLICATION_MXF(TopLevelType.APPLICATION, "mxf"),

	APPLICATION_NASDATA(TopLevelType.APPLICATION, "nasdata"),

	APPLICATION_NEWS_CHECKGROUPS(TopLevelType.APPLICATION, "news-checkgroups"),

	APPLICATION_NEWS_GROUPINFO(TopLevelType.APPLICATION, "news-groupinfo"),

	APPLICATION_NEWS_TRANSMISSION(TopLevelType.APPLICATION, "news-transmission"),

	APPLICATION_NLSML_XML(TopLevelType.APPLICATION, "nlsml+xml"),

	APPLICATION_NSS(TopLevelType.APPLICATION, "nss"),

	APPLICATION_OCSP_REQUEST(TopLevelType.APPLICATION, "ocsp-request"),

	APPLICATION_OCSP_RESPONSE(TopLevelType.APPLICATION, "ocsp-response"),

	APPLICATION_OCTET_STREAM(TopLevelType.APPLICATION, "octet-stream"),

	APPLICATION_ODA(TopLevelType.APPLICATION, "ODA"),

	APPLICATION_ODX(TopLevelType.APPLICATION, "ODX"),

	APPLICATION_OEBPS_PACKAGE_XML(TopLevelType.APPLICATION, "oebps-package+xml"),

	APPLICATION_OGG(TopLevelType.APPLICATION, "ogg"),

	APPLICATION_OXPS(TopLevelType.APPLICATION, "oxps"),

	APPLICATION_P2P_OVERLAY_XML(TopLevelType.APPLICATION, "p2p-overlay+xml"),

	APPLICATION_PATCH_OPS_ERROR_XML(TopLevelType.APPLICATION, "patch-ops-error+xml"),

	APPLICATION_PDF(TopLevelType.APPLICATION, "pdf"),

	APPLICATION_PDX(TopLevelType.APPLICATION, "PDX"),

	APPLICATION_PGP_ENCRYPTED(TopLevelType.APPLICATION, "pgp-encrypted"),

	APPLICATION_PGP_SIGNATURE(TopLevelType.APPLICATION, "pgp-signature"),

	APPLICATION_PIDF_DIFF_XML(TopLevelType.APPLICATION, "pidf-diff+xml"),

	APPLICATION_PIDF_XML(TopLevelType.APPLICATION, "pidf+xml"),

	APPLICATION_PKCS10(TopLevelType.APPLICATION, "pkcs10"),

	APPLICATION_PKCS7_MIME(TopLevelType.APPLICATION, "pkcs7-mime"),

	APPLICATION_PKCS7_SIGNATURE(TopLevelType.APPLICATION, "pkcs7-signature"),

	APPLICATION_PKCS8(TopLevelType.APPLICATION, "pkcs8"),

	APPLICATION_PKCS12(TopLevelType.APPLICATION, "pkcs12"),

	APPLICATION_PKIX_ATTR_CERT(TopLevelType.APPLICATION, "pkix-attr-cert"),

	APPLICATION_PKIX_CERT(TopLevelType.APPLICATION, "pkix-cert"),

	APPLICATION_PKIX_CRL(TopLevelType.APPLICATION, "pkix-crl"),

	APPLICATION_PKIX_PKIPATH(TopLevelType.APPLICATION, "pkix-pkipath"),

	APPLICATION_PKIXCMP(TopLevelType.APPLICATION, "pkixcmp"),

	APPLICATION_PLS_XML(TopLevelType.APPLICATION, "pls+xml"),

	APPLICATION_POC_SETTINGS_XML(TopLevelType.APPLICATION, "poc-settings+xml"),

	APPLICATION_POSTSCRIPT(TopLevelType.APPLICATION, "postscript"),

	APPLICATION_PPSP_TRACKER_JSON(TopLevelType.APPLICATION, "ppsp-tracker+json"),

	APPLICATION_PROBLEM_JSON(TopLevelType.APPLICATION, "problem+json"),

	APPLICATION_PROBLEM_XML(TopLevelType.APPLICATION, "problem+xml"),

	APPLICATION_PROVENANCE_XML(TopLevelType.APPLICATION, "provenance+xml"),

	APPLICATION_PRS_ALVESTRAND_TITRAX_SHEET(TopLevelType.APPLICATION, "prs.alvestrand.titrax-sheet"),

	APPLICATION_PRS_CWW(TopLevelType.APPLICATION, "prs.cww"),

	APPLICATION_PRS_HPUB_ZIP(TopLevelType.APPLICATION, "prs.hpub+zip"),

	APPLICATION_PRS_NPREND(TopLevelType.APPLICATION, "prs.nprend"),

	APPLICATION_PRS_PLUCKER(TopLevelType.APPLICATION, "prs.plucker"),

	APPLICATION_PRS_RDF_XML_CRYPT(TopLevelType.APPLICATION, "prs.rdf-xml-crypt"),

	APPLICATION_PRS_XSF_XML(TopLevelType.APPLICATION, "prs.xsf+xml"),

	APPLICATION_PSKC_XML(TopLevelType.APPLICATION, "pskc+xml"),

	APPLICATION_RDF_XML(TopLevelType.APPLICATION, "rdf+xml"),

	APPLICATION_QSIG(TopLevelType.APPLICATION, "QSIG"),

	APPLICATION_RAPTORFEC(TopLevelType.APPLICATION, "raptorfec"),

	APPLICATION_RDAP_JSON(TopLevelType.APPLICATION, "rdap+json"),

	APPLICATION_REGINFO_XML(TopLevelType.APPLICATION, "reginfo+xml"),

	APPLICATION_RELAX_NG_COMPACT_SYNTAX(TopLevelType.APPLICATION, "relax-ng-compact-syntax"),

	APPLICATION_REMOTE_PRINTING(TopLevelType.APPLICATION, "remote-printing"),

	APPLICATION_REPUTON_JSON(TopLevelType.APPLICATION, "reputon+json"),

	APPLICATION_RESOURCE_LISTS_DIFF_XML(TopLevelType.APPLICATION, "resource-lists-diff+xml"),

	APPLICATION_RESOURCE_LISTS_XML(TopLevelType.APPLICATION, "resource-lists+xml"),

	APPLICATION_RFC_XML(TopLevelType.APPLICATION, "rfc+xml"),

	APPLICATION_RISCOS(TopLevelType.APPLICATION, "riscos"),

	APPLICATION_RLMI_XML(TopLevelType.APPLICATION, "rlmi+xml"),

	APPLICATION_RLS_SERVICES_XML(TopLevelType.APPLICATION, "rls-services+xml"),

	APPLICATION_RPKI_GHOSTBUSTERS(TopLevelType.APPLICATION, "rpki-ghostbusters"),

	APPLICATION_RPKI_MANIFEST(TopLevelType.APPLICATION, "rpki-manifest"),

	APPLICATION_RPKI_ROA(TopLevelType.APPLICATION, "rpki-roa"),

	APPLICATION_RPKI_UPDOWN(TopLevelType.APPLICATION, "rpki-updown"),

	APPLICATION_RTF(TopLevelType.APPLICATION, "rtf"),

	APPLICATION_RTPLOOPBACK(TopLevelType.APPLICATION, "rtploopback"),

	APPLICATION_RTX(TopLevelType.APPLICATION, "rtx"),

	APPLICATION_SAMLASSERTION_XML(TopLevelType.APPLICATION, "samlassertion+xml"),

	APPLICATION_SAMLMETADATA_XML(TopLevelType.APPLICATION, "samlmetadata+xml"),

	APPLICATION_SBML_XML(TopLevelType.APPLICATION, "sbml+xml"),

	APPLICATION_SCAIP_XML(TopLevelType.APPLICATION, "scaip+xml"),

	APPLICATION_SCHEMA_JSON(TopLevelType.APPLICATION, "schema+json"),

	APPLICATION_SCHEMA_XML(TopLevelType.APPLICATION, "schema+xml"),

	APPLICATION_SCIM_JSON(TopLevelType.APPLICATION, "scim+json"),

	APPLICATION_SCVP_CV_REQUEST(TopLevelType.APPLICATION, "scvp-cv-request"),

	APPLICATION_SCVP_CV_RESPONSE(TopLevelType.APPLICATION, "scvp-cv-response"),

	APPLICATION_SCVP_VP_REQUEST(TopLevelType.APPLICATION, "scvp-vp-request"),

	APPLICATION_SCVP_VP_RESPONSE(TopLevelType.APPLICATION, "scvp-vp-response"),

	APPLICATION_SDP(TopLevelType.APPLICATION, "sdp"),

	APPLICATION_SEP_EXI(TopLevelType.APPLICATION, "sep-exi"),

	APPLICATION_SEP_XML(TopLevelType.APPLICATION, "sep+xml"),

	APPLICATION_SESSION_INFO(TopLevelType.APPLICATION, "session-info"),

	APPLICATION_SET_PAYMENT(TopLevelType.APPLICATION, "set-payment"),

	APPLICATION_SET_PAYMENT_INITIATION(TopLevelType.APPLICATION, "set-payment-initiation"),

	APPLICATION_SET_REGISTRATION(TopLevelType.APPLICATION, "set-registration"),

	APPLICATION_SET_REGISTRATION_INITIATION(TopLevelType.APPLICATION, "set-registration-initiation"),

	APPLICATION_SGML(TopLevelType.APPLICATION, "SGML"),

	APPLICATION_SGML_OPEN_CATALOG(TopLevelType.APPLICATION, "sgml-open-catalog"),

	APPLICATION_SHF_XML(TopLevelType.APPLICATION, "shf+xml"),

	APPLICATION_SIEVE(TopLevelType.APPLICATION, "sieve"),

	APPLICATION_SIMPLE_FILTER_XML(TopLevelType.APPLICATION, "simple-filter+xml"),

	APPLICATION_SIMPLE_MESSAGE_SUMMARY(TopLevelType.APPLICATION, "simple-aMessage-summary"),

	APPLICATION_SIMPLESYMBOLCONTAINER(TopLevelType.APPLICATION, "simpleSymbolContainer"),

	APPLICATION_SLATE(TopLevelType.APPLICATION, "slate"),

	APPLICATION_SMIL(TopLevelType.APPLICATION, "smil"),

	APPLICATION_SMIL_XML(TopLevelType.APPLICATION, "smil+xml"),

	APPLICATION_SMPTE336M(TopLevelType.APPLICATION, "smpte336m"),

	APPLICATION_SOAP_FASTINFOSET(TopLevelType.APPLICATION, "soap+fastinfoset"),

	APPLICATION_SOAP_XML(TopLevelType.APPLICATION, "soap+xml"),

	APPLICATION_SPIRITS_EVENT_XML(TopLevelType.APPLICATION, "spirits-event+xml"),

	APPLICATION_SQL(TopLevelType.APPLICATION, "sql"),

	APPLICATION_SRGS(TopLevelType.APPLICATION, "srgs"),

	APPLICATION_SRGS_XML(TopLevelType.APPLICATION, "srgs+xml"),

	APPLICATION_SRU_XML(TopLevelType.APPLICATION, "sru+xml"),

	APPLICATION_SSML_XML(TopLevelType.APPLICATION, "ssml+xml"),

	APPLICATION_TAMP_APEX_UPDATE(TopLevelType.APPLICATION, "tamp-apex-update"),

	APPLICATION_TAMP_APEX_UPDATE_CONFIRM(TopLevelType.APPLICATION, "tamp-apex-update-confirm"),

	APPLICATION_TAMP_COMMUNITY_UPDATE(TopLevelType.APPLICATION, "tamp-community-update"),

	APPLICATION_TAMP_COMMUNITY_UPDATE_CONFIRM(TopLevelType.APPLICATION, "tamp-community-update-confirm"),

	APPLICATION_TAMP_ERROR(TopLevelType.APPLICATION, "tamp-error"),

	APPLICATION_TAMP_SEQUENCE_ADJUST(TopLevelType.APPLICATION, "tamp-sequence-adjust"),

	APPLICATION_TAMP_SEQUENCE_ADJUST_CONFIRM(TopLevelType.APPLICATION, "tamp-sequence-adjust-confirm"),

	APPLICATION_TAMP_STATUS_QUERY(TopLevelType.APPLICATION, "tamp-status-query"),

	APPLICATION_TAMP_STATUS_RESPONSE(TopLevelType.APPLICATION, "tamp-status-response"),

	APPLICATION_TAMP_UPDATE(TopLevelType.APPLICATION, "tamp-update"),

	APPLICATION_TAMP_UPDATE_CONFIRM(TopLevelType.APPLICATION, "tamp-update-confirm"),

	APPLICATION_TEI_XML(TopLevelType.APPLICATION, "tei+xml"),

	APPLICATION_THRAUD_XML(TopLevelType.APPLICATION, "thraud+xml"),

	APPLICATION_TIMESTAMP_QUERY(TopLevelType.APPLICATION, "timestamp-query"),

	APPLICATION_TIMESTAMP_REPLY(TopLevelType.APPLICATION, "timestamp-reply"),

	APPLICATION_TIMESTAMPED_DATA(TopLevelType.APPLICATION, "timestamped-data"),

	APPLICATION_TTML_XML(TopLevelType.APPLICATION, "ttml+xml"),

	APPLICATION_TVE_TRIGGER(TopLevelType.APPLICATION, "tve-trigger"),

	APPLICATION_ULPFEC(TopLevelType.APPLICATION, "ulpfec"),

	APPLICATION_URC_GRPSHEET_XML(TopLevelType.APPLICATION, "urc-grpsheet+xml"),

	APPLICATION_URC_RESSHEET_XML(TopLevelType.APPLICATION, "urc-ressheet+xml"),

	APPLICATION_URC_TARGETDESC_XML(TopLevelType.APPLICATION, "urc-targetdesc+xml"),

	APPLICATION_URC_UISOCKETDESC_XML(TopLevelType.APPLICATION, "urc-uisocketdesc+xml"),

	APPLICATION_VCARD_JSON(TopLevelType.APPLICATION, "vcard+json"),

	APPLICATION_VCARD_XML(TopLevelType.APPLICATION, "vcard+xml"),

	APPLICATION_VEMMI(TopLevelType.APPLICATION, "vemmi"),

	APPLICATION_VND_3GPP_ACCESS_TRANSFER_EVENTS_XML(TopLevelType.APPLICATION, "vnd.3gpp.access-transfer-events+xml"),

	APPLICATION_VND_3GPP_BSF_XML(TopLevelType.APPLICATION, "vnd.3gpp.bsf+xml"),

	APPLICATION_VND_3GPP_MID_CALL_XML(TopLevelType.APPLICATION, "vnd.3gpp.mid-call+xml"),

	APPLICATION_VND_3GPP_PIC_BW_LARGE(TopLevelType.APPLICATION, "vnd.3gpp.pic-bw-large"),

	APPLICATION_VND_3GPP_PIC_BW_SMALL(TopLevelType.APPLICATION, "vnd.3gpp.pic-bw-small"),

	APPLICATION_VND_3GPP_PIC_BW_VAR(TopLevelType.APPLICATION, "vnd.3gpp.pic-bw-var"),

	APPLICATION_VND_3GPP_PROSE_PC3CH_XML(TopLevelType.APPLICATION, "vnd.3gpp-prose-pc3ch+xml"),

	APPLICATION_VND_3GPP_PROSE_XML(TopLevelType.APPLICATION, "vnd.3gpp-prose+xml"),

	APPLICATION_VND_3GPP_SMS(TopLevelType.APPLICATION, "vnd.3gpp.sms"),

	APPLICATION_VND_3GPP_SMS_XML(TopLevelType.APPLICATION, "vnd.3gpp.sms+xml"),

	APPLICATION_VND_3GPP_SRVCC_EXT_XML(TopLevelType.APPLICATION, "vnd.3gpp.srvcc-ext+xml"),

	APPLICATION_VND_3GPP_SRVCC_INFO_XML(TopLevelType.APPLICATION, "vnd.3gpp.SRVCC-info+xml"),

	APPLICATION_VND_3GPP_STATE_AND_EVENT_INFO_XML(TopLevelType.APPLICATION, "vnd.3gpp.state-and-event-info+xml"),

	APPLICATION_VND_3GPP_USSD_XML(TopLevelType.APPLICATION, "vnd.3gpp.ussd+xml"),

	APPLICATION_VND_3GPP2_BCMCSINFO_XML(TopLevelType.APPLICATION, "vnd.3gpp2.bcmcsinfo+xml"),

	APPLICATION_VND_3GPP2_SMS(TopLevelType.APPLICATION, "vnd.3gpp2.sms"),

	APPLICATION_VND_3GPP2_TCAP(TopLevelType.APPLICATION, "vnd.3gpp2.tcap"),

	APPLICATION_VND_3LIGHTSSOFTWARE_IMAGESCAL(TopLevelType.APPLICATION, "vnd.3lightssoftware.imagescal"),

	APPLICATION_VND_3M_POST_IT_NOTES(TopLevelType.APPLICATION, "vnd.3M.Post-it-Notes"),

	APPLICATION_VND_ACCPAC_SIMPLY_ASO(TopLevelType.APPLICATION, "vnd.accpac.simply.aso"),

	APPLICATION_VND_ACCPAC_SIMPLY_IMP(TopLevelType.APPLICATION, "vnd.accpac.simply.imp"),

	APPLICATION_VND_ACUCOBOL(TopLevelType.APPLICATION, "vnd-acucobol"),

	APPLICATION_VND_ACUCORP(TopLevelType.APPLICATION, "vnd.acucorp"),

	APPLICATION_VND_ADOBE_FLASH_MOVIE(TopLevelType.APPLICATION, "vnd.adobe.flash-movie"),

	APPLICATION_VND_ADOBE_FORMSCENTRAL_FCDT(TopLevelType.APPLICATION, "vnd.adobe.formscentral.fcdt"),

	APPLICATION_VND_ADOBE_FXP(TopLevelType.APPLICATION, "vnd.adobe.fxp"),

	APPLICATION_VND_ADOBE_PARTIAL_UPLOAD(TopLevelType.APPLICATION, "vnd.adobe.partial-upload"),

	APPLICATION_VND_ADOBE_XDP_XML(TopLevelType.APPLICATION, "vnd.adobe.xdp+xml"),

	APPLICATION_VND_ADOBE_XFDF(TopLevelType.APPLICATION, "vnd.adobe.xfdf"),

	APPLICATION_VND_AETHER_IMP(TopLevelType.APPLICATION, "vnd.aether.imp"),

	APPLICATION_VND_AH_BARCODE(TopLevelType.APPLICATION, "vnd.ah-barcode"),

	APPLICATION_VND_AHEAD_SPACE(TopLevelType.APPLICATION, "vnd.ahead.space"),

	APPLICATION_VND_AIRZIP_FILESECURE_AZF(TopLevelType.APPLICATION, "vnd.airzip.filesecure.azf"),

	APPLICATION_VND_AIRZIP_FILESECURE_AZS(TopLevelType.APPLICATION, "vnd.airzip.filesecure.azs"),

	APPLICATION_VND_AMAZON_MOBI8_EBOOK(TopLevelType.APPLICATION, "vnd.amazon.mobi8-ebook"),

	APPLICATION_VND_AMERICANDYNAMICS_ACC(TopLevelType.APPLICATION, "vnd.americandynamics.acc"),

	APPLICATION_VND_AMIGA_AMI(TopLevelType.APPLICATION, "vnd.amiga.ami"),

	APPLICATION_VND_AMUNDSEN_MAZE_XML(TopLevelType.APPLICATION, "vnd.amundsen.maze+xml"),

	APPLICATION_VND_ANKI(TopLevelType.APPLICATION, "vnd.anki"),

	APPLICATION_VND_ANSER_WEB_CERTIFICATE_ISSUE_INITIATION(TopLevelType.APPLICATION, "vnd.anser-web-certificate-issue-initiation"),

	APPLICATION_VND_ANTIX_GAME_COMPONENT(TopLevelType.APPLICATION, "vnd.antix.game-component"),

	APPLICATION_VND_APACHE_THRIFT_BINARY(TopLevelType.APPLICATION, "vnd.apache.thrift.binary"),

	APPLICATION_VND_APACHE_THRIFT_COMPACT(TopLevelType.APPLICATION, "vnd.apache.thrift.compact"),

	APPLICATION_VND_APACHE_THRIFT_JSON(TopLevelType.APPLICATION, "vnd.apache.thrift.json"),

	APPLICATION_VND_API_JSON(TopLevelType.APPLICATION, "vnd.api+json"),

	APPLICATION_VND_APPLE_MPEGURL(TopLevelType.APPLICATION, "vnd.apple.mpegurl"),

	APPLICATION_VND_APPLE_INSTALLER_XML(TopLevelType.APPLICATION, "vnd.apple.installer+xml"),

	APPLICATION_VND_ARASTRA_SWI(TopLevelType.APPLICATION, "vnd.arastra.swi"),

	APPLICATION_VND_ARISTANETWORKS_SWI(TopLevelType.APPLICATION, "vnd.aristanetworks.swi"),

	APPLICATION_VND_ARTSQUARE(TopLevelType.APPLICATION, "vnd.artsquare"),

	APPLICATION_VND_ASTRAEA_SOFTWARE_IOTA(TopLevelType.APPLICATION, "vnd.astraea-software.iota"),

	APPLICATION_VND_AUDIOGRAPH(TopLevelType.APPLICATION, "vnd.audiograph"),

	APPLICATION_VND_AUTOPACKAGE(TopLevelType.APPLICATION, "vnd.autopackage"),

	APPLICATION_VND_AVISTAR_XML(TopLevelType.APPLICATION, "vnd.avistar+xml"),

	APPLICATION_VND_BALSAMIQ_BMML_XML(TopLevelType.APPLICATION, "vnd.balsamiq.bmml+xml"),

	APPLICATION_VND_BALSAMIQ_BMPR(TopLevelType.APPLICATION, "vnd.balsamiq.bmpr"),

	APPLICATION_VND_BEKITZUR_STECH_JSON(TopLevelType.APPLICATION, "vnd.bekitzur-stech+json"),

	APPLICATION_VND_BIOPAX_RDF_XML(TopLevelType.APPLICATION, "vnd.biopax.rdf+xml"),

	APPLICATION_VND_BLUEICE_MULTIPASS(TopLevelType.APPLICATION, "vnd.blueice.multipass"),

	APPLICATION_VND_BLUETOOTH_EP_OOB(TopLevelType.APPLICATION, "vnd.bluetooth.ep.oob"),

	APPLICATION_VND_BLUETOOTH_LE_OOB(TopLevelType.APPLICATION, "vnd.bluetooth.le.oob"),

	APPLICATION_VND_BMI(TopLevelType.APPLICATION, "vnd.bmi"),

	APPLICATION_VND_BUSINESSOBJECTS(TopLevelType.APPLICATION, "vnd.businessobjects"),

	APPLICATION_VND_CAB_JSCRIPT(TopLevelType.APPLICATION, "vnd.cab-jscript"),

	APPLICATION_VND_CANON_CPDL(TopLevelType.APPLICATION, "vnd.canon-cpdl"),

	APPLICATION_VND_CANON_LIPS(TopLevelType.APPLICATION, "vnd.canon-lips"),

	APPLICATION_VND_CENDIO_THINLINC_CLIENTCONF(TopLevelType.APPLICATION, "vnd.cendio.thinlinc.clientconf"),

	APPLICATION_VND_CENTURY_SYSTEMS_TCP_STREAM(TopLevelType.APPLICATION, "vnd.century-systems.tcp_stream"),

	APPLICATION_VND_CHEMDRAW_XML(TopLevelType.APPLICATION, "vnd.chemdraw+xml"),

	APPLICATION_VND_CHESS_PGN(TopLevelType.APPLICATION, "vnd.chess-pgn"),

	APPLICATION_VND_CHIPNUTS_KARAOKE_MMD(TopLevelType.APPLICATION, "vnd.chipnuts.karaoke-mmd"),

	APPLICATION_VND_CINDERELLA(TopLevelType.APPLICATION, "vnd.cinderella"),

	APPLICATION_VND_CIRPACK_ISDN_EXT(TopLevelType.APPLICATION, "vnd.cirpack.isdn-ext"),

	APPLICATION_VND_CITATIONSTYLES_STYLE_XML(TopLevelType.APPLICATION, "vnd.citationstyles.style+xml"),

	APPLICATION_VND_CLAYMORE(TopLevelType.APPLICATION, "vnd.claymore"),

	APPLICATION_VND_CLOANTO_RP9(TopLevelType.APPLICATION, "vnd.cloanto.rp9"),

	APPLICATION_VND_CLONK_C4GROUP(TopLevelType.APPLICATION, "vnd.clonk.c4group"),

	APPLICATION_VND_CLUETRUST_CARTOMOBILE_CONFIG(TopLevelType.APPLICATION, "vnd.cluetrust.cartomobile-config"),

	APPLICATION_VND_CLUETRUST_CARTOMOBILE_CONFIG_PKG(TopLevelType.APPLICATION, "vnd.cluetrust.cartomobile-config-pkg"),

	APPLICATION_VND_COFFEESCRIPT(TopLevelType.APPLICATION, "vnd.coffeescript"),

	APPLICATION_VND_COLLECTION_DOC_JSON(TopLevelType.APPLICATION, "vnd.collection.doc+json"),

	APPLICATION_VND_COLLECTION_JSON(TopLevelType.APPLICATION, "vnd.collection+json"),

	APPLICATION_VND_COLLECTION_NEXT_JSON(TopLevelType.APPLICATION, "vnd.collection.next+json"),

	APPLICATION_VND_COMICBOOK_ZIP(TopLevelType.APPLICATION, "vnd.comicbook+zip"),

	APPLICATION_VND_COMMERCE_BATTELLE(TopLevelType.APPLICATION, "vnd.commerce-battelle"),

	APPLICATION_VND_COMMONSPACE(TopLevelType.APPLICATION, "vnd.commonspace"),

	APPLICATION_VND_COREOS_IGNITION_JSON(TopLevelType.APPLICATION, "vnd.coreos.ignition+json"),

	APPLICATION_VND_COSMOCALLER(TopLevelType.APPLICATION, "vnd.cosmocaller"),

	APPLICATION_VND_CONTACT_CMSG(TopLevelType.APPLICATION, "vnd.contact.cmsg"),

	APPLICATION_VND_CRICK_CLICKER(TopLevelType.APPLICATION, "vnd.crick.clicker"),

	APPLICATION_VND_CRICK_CLICKER_KEYBOARD(TopLevelType.APPLICATION, "vnd.crick.clicker.keyboard"),

	APPLICATION_VND_CRICK_CLICKER_PALETTE(TopLevelType.APPLICATION, "vnd.crick.clicker.palette"),

	APPLICATION_VND_CRICK_CLICKER_TEMPLATE(TopLevelType.APPLICATION, "vnd.crick.clicker.template"),

	APPLICATION_VND_CRICK_CLICKER_WORDBANK(TopLevelType.APPLICATION, "vnd.crick.clicker.wordbank"),

	APPLICATION_VND_CRITICALTOOLS_WBS_XML(TopLevelType.APPLICATION, "vnd.criticaltools.wbs+xml"),

	APPLICATION_VND_CTC_POSML(TopLevelType.APPLICATION, "vnd.ctc-posml"),

	APPLICATION_VND_CTCT_WS_XML(TopLevelType.APPLICATION, "vnd.ctct.ws+xml"),

	APPLICATION_VND_CUPS_PDF(TopLevelType.APPLICATION, "vnd.cups-pdf"),

	APPLICATION_VND_CUPS_POSTSCRIPT(TopLevelType.APPLICATION, "vnd.cups-postscript"),

	APPLICATION_VND_CUPS_PPD(TopLevelType.APPLICATION, "vnd.cups-ppd"),

	APPLICATION_VND_CUPS_RASTER(TopLevelType.APPLICATION, "vnd.cups-raster"),

	APPLICATION_VND_CUPS_RAW(TopLevelType.APPLICATION, "vnd.cups-raw"),

	APPLICATION_VND_CURL(TopLevelType.APPLICATION, "vnd-curl"),

	APPLICATION_VND_CYAN_DEAN_ROOT_XML(TopLevelType.APPLICATION, "vnd.cyan.dean.root+xml"),

	APPLICATION_VND_CYBANK(TopLevelType.APPLICATION, "vnd.cybank"),

	APPLICATION_VND_D2L_COURSEPACKAGE1P0_ZIP(TopLevelType.APPLICATION, "vnd.d2l.coursepackage1p0+zip"),

	APPLICATION_VND_DART(TopLevelType.APPLICATION, "vnd-dart"),

	APPLICATION_VND_DATA_VISION_RDZ(TopLevelType.APPLICATION, "vnd.data-vision.rdz"),

	APPLICATION_VND_DEBIAN_BINARY_PACKAGE(TopLevelType.APPLICATION, "vnd.debian.binary-package"),

	APPLICATION_VND_DECE_DATA(TopLevelType.APPLICATION, "vnd.dece.data"),

	APPLICATION_VND_DECE_TTML_XML(TopLevelType.APPLICATION, "vnd.dece.ttml+xml"),

	APPLICATION_VND_DECE_UNSPECIFIED(TopLevelType.APPLICATION, "vnd.dece.unspecified"),

	APPLICATION_VND_DECE_ZIP(TopLevelType.APPLICATION, "vnd.dece-zip"),

	APPLICATION_VND_DENOVO_FCSELAYOUT_LINK(TopLevelType.APPLICATION, "vnd.denovo.fcselayout-link"),

	APPLICATION_VND_DESMUME_MOVIE(TopLevelType.APPLICATION, "vnd.desmume-movie"),

	APPLICATION_VND_DIR_BI_PLATE_DL_NOSUFFIX(TopLevelType.APPLICATION, "vnd.dir-bi.plate-dl-nosuffix"),

	APPLICATION_VND_DM_DELEGATION_XML(TopLevelType.APPLICATION, "vnd.dm.delegation+xml"),

	APPLICATION_VND_DNA(TopLevelType.APPLICATION, "vnd.dna"),

	APPLICATION_VND_DOCUMENT_JSON(TopLevelType.APPLICATION, "vnd.document+json"),

	APPLICATION_VND_DOLBY_MOBILE_1(TopLevelType.APPLICATION, "vnd.dolby.mobile.1"),

	APPLICATION_VND_DOLBY_MOBILE_2(TopLevelType.APPLICATION, "vnd.dolby.mobile.2"),

	APPLICATION_VND_DOREMIR_SCORECLOUD_BINARY_DOCUMENT(TopLevelType.APPLICATION, "vnd.doremir.scorecloud-binary-document"),

	APPLICATION_VND_DPGRAPH(TopLevelType.APPLICATION, "vnd.dpgraph"),

	APPLICATION_VND_DREAMFACTORY(TopLevelType.APPLICATION, "vnd.dreamfactory"),

	APPLICATION_VND_DRIVE_JSON(TopLevelType.APPLICATION, "vnd.drive+json"),

	APPLICATION_VND_DTG_LOCAL(TopLevelType.APPLICATION, "vnd.dtg.local"),

	APPLICATION_VND_DTG_LOCAL_FLASH(TopLevelType.APPLICATION, "vnd.dtg.local.flash"),

	APPLICATION_VND_DTG_LOCAL_HTML(TopLevelType.APPLICATION, "vnd.dtg.local-html"),

	APPLICATION_VND_DVB_AIT(TopLevelType.APPLICATION, "vnd.dvb.ait"),

	APPLICATION_VND_DVB_DVBJ(TopLevelType.APPLICATION, "vnd.dvb.dvbj"),

	APPLICATION_VND_DVB_ESGCONTAINER(TopLevelType.APPLICATION, "vnd.dvb.esgcontainer"),

	APPLICATION_VND_DVB_IPDCDFTNOTIFACCESS(TopLevelType.APPLICATION, "vnd.dvb.ipdcdftnotifaccess"),

	APPLICATION_VND_DVB_IPDCESGACCESS(TopLevelType.APPLICATION, "vnd.dvb.ipdcesgaccess"),

	APPLICATION_VND_DVB_IPDCESGACCESS2(TopLevelType.APPLICATION, "vnd.dvb.ipdcesgaccess2"),

	APPLICATION_VND_DVB_IPDCESGPDD(TopLevelType.APPLICATION, "vnd.dvb.ipdcesgpdd"),

	APPLICATION_VND_DVB_IPDCROAMING(TopLevelType.APPLICATION, "vnd.dvb.ipdcroaming"),

	APPLICATION_VND_DVB_IPTV_ALFEC_BASE(TopLevelType.APPLICATION, "vnd.dvb.iptv.alfec-base"),

	APPLICATION_VND_DVB_IPTV_ALFEC_ENHANCEMENT(TopLevelType.APPLICATION, "vnd.dvb.iptv.alfec-enhancement"),

	APPLICATION_VND_DVB_NOTIF_AGGREGATE_ROOT_XML(TopLevelType.APPLICATION, "vnd.dvb.notif-aggregate-root+xml"),

	APPLICATION_VND_DVB_NOTIF_CONTAINER_XML(TopLevelType.APPLICATION, "vnd.dvb.notif-container+xml"),

	APPLICATION_VND_DVB_NOTIF_GENERIC_XML(TopLevelType.APPLICATION, "vnd.dvb.notif-generic+xml"),

	APPLICATION_VND_DVB_NOTIF_IA_MSGLIST_XML(TopLevelType.APPLICATION, "vnd.dvb.notif-ia-msglist+xml"),

	APPLICATION_VND_DVB_NOTIF_IA_REGISTRATION_REQUEST_XML(TopLevelType.APPLICATION, "vnd.dvb.notif-ia-registration-request+xml"),

	APPLICATION_VND_DVB_NOTIF_IA_REGISTRATION_RESPONSE_XML(TopLevelType.APPLICATION, "vnd.dvb.notif-ia-registration-response+xml"),

	APPLICATION_VND_DVB_NOTIF_INIT_XML(TopLevelType.APPLICATION, "vnd.dvb.notif-init+xml"),

	APPLICATION_VND_DVB_PFR(TopLevelType.APPLICATION, "vnd.dvb.pfr"),

	APPLICATION_VND_DVB_SERVICE(TopLevelType.APPLICATION, "vnd.dvb_service"),

	APPLICATION_VND_DXR(TopLevelType.APPLICATION, "vnd-dxr"),

	APPLICATION_VND_DYNAGEO(TopLevelType.APPLICATION, "vnd.dynageo"),

	APPLICATION_VND_DZR(TopLevelType.APPLICATION, "vnd.dzr"),

	APPLICATION_VND_EASYKARAOKE_CDGDOWNLOAD(TopLevelType.APPLICATION, "vnd.easykaraoke.cdgdownload"),

	APPLICATION_VND_ECDIS_UPDATE(TopLevelType.APPLICATION, "vnd.ecdis-update"),

	APPLICATION_VND_ECOWIN_CHART(TopLevelType.APPLICATION, "vnd.ecowin.chart"),

	APPLICATION_VND_ECOWIN_FILEREQUEST(TopLevelType.APPLICATION, "vnd.ecowin.filerequest"),

	APPLICATION_VND_ECOWIN_FILEUPDATE(TopLevelType.APPLICATION, "vnd.ecowin.fileupdate"),

	APPLICATION_VND_ECOWIN_SERIES(TopLevelType.APPLICATION, "vnd.ecowin.series"),

	APPLICATION_VND_ECOWIN_SERIESREQUEST(TopLevelType.APPLICATION, "vnd.ecowin.seriesrequest"),

	APPLICATION_VND_ECOWIN_SERIESUPDATE(TopLevelType.APPLICATION, "vnd.ecowin.seriesupdate"),

	APPLICATION_VND_EMCLIENT_ACCESSREQUEST_XML(TopLevelType.APPLICATION, "vnd.emclient.accessrequest+xml"),

	APPLICATION_VND_ENLIVEN(TopLevelType.APPLICATION, "vnd.enliven"),

	APPLICATION_VND_ENPHASE_ENVOY(TopLevelType.APPLICATION, "vnd.enphase.envoy"),

	APPLICATION_VND_EPRINTS_DATA_XML(TopLevelType.APPLICATION, "vnd.eprints.data+xml"),

	APPLICATION_VND_EPSON_ESF(TopLevelType.APPLICATION, "vnd.epson.esf"),

	APPLICATION_VND_EPSON_MSF(TopLevelType.APPLICATION, "vnd.epson.msf"),

	APPLICATION_VND_EPSON_QUICKANIME(TopLevelType.APPLICATION, "vnd.epson.quickanime"),

	APPLICATION_VND_EPSON_SALT(TopLevelType.APPLICATION, "vnd.epson.salt"),

	APPLICATION_VND_EPSON_SSF(TopLevelType.APPLICATION, "vnd.epson.ssf"),

	APPLICATION_VND_ERICSSON_QUICKCALL(TopLevelType.APPLICATION, "vnd.ericsson.quickcall"),

	APPLICATION_VND_ESPASS_ESPASS_ZIP(TopLevelType.APPLICATION, "vnd.espass-espass+zip"),

	APPLICATION_VND_ESZIGNO3_XML(TopLevelType.APPLICATION, "vnd.eszigno3+xml"),

	APPLICATION_VND_ETSI_AOC_XML(TopLevelType.APPLICATION, "vnd.etsi.aoc+xml"),

	APPLICATION_VND_ETSI_ASIC_S_ZIP(TopLevelType.APPLICATION, "vnd.etsi.asic-s+zip"),

	APPLICATION_VND_ETSI_ASIC_E_ZIP(TopLevelType.APPLICATION, "vnd.etsi.asic-e+zip"),

	APPLICATION_VND_ETSI_CUG_XML(TopLevelType.APPLICATION, "vnd.etsi.cug+xml"),

	APPLICATION_VND_ETSI_IPTVCOMMAND_XML(TopLevelType.APPLICATION, "vnd.etsi.iptvcommand+xml"),

	APPLICATION_VND_ETSI_IPTVDISCOVERY_XML(TopLevelType.APPLICATION, "vnd.etsi.iptvdiscovery+xml"),

	APPLICATION_VND_ETSI_IPTVPROFILE_XML(TopLevelType.APPLICATION, "vnd.etsi.iptvprofile+xml"),

	APPLICATION_VND_ETSI_IPTVSAD_BC_XML(TopLevelType.APPLICATION, "vnd.etsi.iptvsad-bc+xml"),

	APPLICATION_VND_ETSI_IPTVSAD_COD_XML(TopLevelType.APPLICATION, "vnd.etsi.iptvsad-cod+xml"),

	APPLICATION_VND_ETSI_IPTVSAD_NPVR_XML(TopLevelType.APPLICATION, "vnd.etsi.iptvsad-npvr+xml"),

	APPLICATION_VND_ETSI_IPTVSERVICE_XML(TopLevelType.APPLICATION, "vnd.etsi.iptvservice+xml"),

	APPLICATION_VND_ETSI_IPTVSYNC_XML(TopLevelType.APPLICATION, "vnd.etsi.iptvsync+xml"),

	APPLICATION_VND_ETSI_IPTVUEPROFILE_XML(TopLevelType.APPLICATION, "vnd.etsi.iptvueprofile+xml"),

	APPLICATION_VND_ETSI_MCID_XML(TopLevelType.APPLICATION, "vnd.etsi.mcid+xml"),

	APPLICATION_VND_ETSI_MHEG5(TopLevelType.APPLICATION, "vnd.etsi.mheg5"),

	APPLICATION_VND_ETSI_OVERLOAD_CONTROL_POLICY_DATASET_XML(TopLevelType.APPLICATION, "vnd.etsi.overload-control-policy-dataset+xml"),

	APPLICATION_VND_ETSI_PSTN_XML(TopLevelType.APPLICATION, "vnd.etsi.pstn+xml"),

	APPLICATION_VND_ETSI_SCI_XML(TopLevelType.APPLICATION, "vnd.etsi.sci+xml"),

	APPLICATION_VND_ETSI_SIMSERVS_XML(TopLevelType.APPLICATION, "vnd.etsi.simservs+xml"),

	APPLICATION_VND_ETSI_TIMESTAMP_TOKEN(TopLevelType.APPLICATION, "vnd.etsi.timestamp-token"),

	APPLICATION_VND_ETSI_TSL_XML(TopLevelType.APPLICATION, "vnd.etsi.tsl+xml"),

	APPLICATION_VND_ETSI_TSL_DER(TopLevelType.APPLICATION, "vnd.etsi.tsl.der"),

	APPLICATION_VND_EUDORA_DATA(TopLevelType.APPLICATION, "vnd.eudora.data"),

	APPLICATION_VND_EZPIX_ALBUM(TopLevelType.APPLICATION, "vnd.ezpix-album"),

	APPLICATION_VND_EZPIX_PACKAGE(TopLevelType.APPLICATION, "vnd.ezpix-package"),

	APPLICATION_VND_F_SECURE_MOBILE(TopLevelType.APPLICATION, "vnd.f-secure.mobile"),

	APPLICATION_VND_FASTCOPY_DISK_IMAGE(TopLevelType.APPLICATION, "vnd.fastcopy-disk-image"),

	APPLICATION_VND_FDF(TopLevelType.APPLICATION, "vnd-fdf"),

	APPLICATION_VND_FDSN_MSEED(TopLevelType.APPLICATION, "vnd.fdsn.mseed"),

	APPLICATION_VND_FDSN_SEED(TopLevelType.APPLICATION, "vnd.fdsn.seed"),

	APPLICATION_VND_FFSNS(TopLevelType.APPLICATION, "vnd.ffsns"),

	APPLICATION_VND_FILMIT_ZFC(TopLevelType.APPLICATION, "vnd.filmit.zfc"),

	APPLICATION_VND_FINTS(TopLevelType.APPLICATION, "vnd.fints"),

	APPLICATION_VND_FIREMONKEYS_CLOUDCELL(TopLevelType.APPLICATION, "vnd.firemonkeys.cloudcell"),

	APPLICATION_VND_FLOGRAPHIT(TopLevelType.APPLICATION, "vnd.FloGraphIt"),

	APPLICATION_VND_FLUXTIME_CLIP(TopLevelType.APPLICATION, "vnd.fluxtime.clip"),

	APPLICATION_VND_FONT_FONTFORGE_SFD(TopLevelType.APPLICATION, "vnd.font-fontforge-sfd"),

	APPLICATION_VND_FRAMEMAKER(TopLevelType.APPLICATION, "vnd.framemaker"),

	APPLICATION_VND_FROGANS_FNC(TopLevelType.APPLICATION, "vnd.frogans.fnc"),

	APPLICATION_VND_FROGANS_LTF(TopLevelType.APPLICATION, "vnd.frogans.ltf"),

	APPLICATION_VND_FSC_WEBLAUNCH(TopLevelType.APPLICATION, "vnd.fsc.weblaunch"),

	APPLICATION_VND_FUJITSU_OASYS(TopLevelType.APPLICATION, "vnd.fujitsu.oasys"),

	APPLICATION_VND_FUJITSU_OASYS2(TopLevelType.APPLICATION, "vnd.fujitsu.oasys2"),

	APPLICATION_VND_FUJITSU_OASYS3(TopLevelType.APPLICATION, "vnd.fujitsu.oasys3"),

	APPLICATION_VND_FUJITSU_OASYSGP(TopLevelType.APPLICATION, "vnd.fujitsu.oasysgp"),

	APPLICATION_VND_FUJITSU_OASYSPRS(TopLevelType.APPLICATION, "vnd.fujitsu.oasysprs"),

	APPLICATION_VND_FUJIXEROX_ART4(TopLevelType.APPLICATION, "vnd.fujixerox.ART4"),

	APPLICATION_VND_FUJIXEROX_ART_EX(TopLevelType.APPLICATION, "vnd.fujixerox.ART-EX"),

	APPLICATION_VND_FUJIXEROX_DDD(TopLevelType.APPLICATION, "vnd.fujixerox.ddd"),

	APPLICATION_VND_FUJIXEROX_DOCUWORKS(TopLevelType.APPLICATION, "vnd.fujixerox.docuworks"),

	APPLICATION_VND_FUJIXEROX_DOCUWORKS_BINDER(TopLevelType.APPLICATION, "vnd.fujixerox.docuworks.binder"),

	APPLICATION_VND_FUJIXEROX_DOCUWORKS_CONTAINER(TopLevelType.APPLICATION, "vnd.fujixerox.docuworks.container"),

	APPLICATION_VND_FUJIXEROX_HBPL(TopLevelType.APPLICATION, "vnd.fujixerox.HBPL"),

	APPLICATION_VND_FUT_MISNET(TopLevelType.APPLICATION, "vnd.fut-misnet"),

	APPLICATION_VND_FUZZYSHEET(TopLevelType.APPLICATION, "vnd.fuzzysheet"),

	APPLICATION_VND_GENOMATIX_TUXEDO(TopLevelType.APPLICATION, "vnd.genomatix.tuxedo"),

	APPLICATION_VND_GEO_JSON(TopLevelType.APPLICATION, "vnd.geo+json"),

	APPLICATION_VND_GEOCUBE_XML(TopLevelType.APPLICATION, "vnd.geocube+xml"),

	APPLICATION_VND_GEOGEBRA_FILE(TopLevelType.APPLICATION, "vnd.geogebra.file"),

	APPLICATION_VND_GEOGEBRA_TOOL(TopLevelType.APPLICATION, "vnd.geogebra.tool"),

	APPLICATION_VND_GEOMETRY_EXPLORER(TopLevelType.APPLICATION, "vnd.geometry-explorer"),

	APPLICATION_VND_GEONEXT(TopLevelType.APPLICATION, "vnd.geonext"),

	APPLICATION_VND_GEOPLAN(TopLevelType.APPLICATION, "vnd.geoplan"),

	APPLICATION_VND_GEOSPACE(TopLevelType.APPLICATION, "vnd.geospace"),

	APPLICATION_VND_GERBER(TopLevelType.APPLICATION, "vnd.gerber"),

	APPLICATION_VND_GLOBALPLATFORM_CARD_CONTENT_MGT(TopLevelType.APPLICATION, "vnd.globalplatform.card-content-mgt"),

	APPLICATION_VND_GLOBALPLATFORM_CARD_CONTENT_MGT_RESPONSE(TopLevelType.APPLICATION, "vnd.globalplatform.card-content-mgt-response"),

	APPLICATION_VND_GMX(TopLevelType.APPLICATION, "vnd.gmx"),

	APPLICATION_VND_GOOGLE_EARTH_KML_XML(TopLevelType.APPLICATION, "vnd.google-earth.kml+xml"),

	APPLICATION_VND_GOOGLE_EARTH_KMZ(TopLevelType.APPLICATION, "vnd.google-earth.kmz"),

	APPLICATION_VND_GOV_SK_E_FORM_XML(TopLevelType.APPLICATION, "vnd.gov.sk.e-form+xml"),

	APPLICATION_VND_GOV_SK_E_FORM_ZIP(TopLevelType.APPLICATION, "vnd.gov.sk.e-form+zip"),

	APPLICATION_VND_GOV_SK_XMLDATACONTAINER_XML(TopLevelType.APPLICATION, "vnd.gov.sk.xmldatacontainer+xml"),

	APPLICATION_VND_GRAFEQ(TopLevelType.APPLICATION, "vnd.grafeq"),

	APPLICATION_VND_GRIDMP(TopLevelType.APPLICATION, "vnd.gridmp"),

	APPLICATION_VND_GROOVE_ACCOUNT(TopLevelType.APPLICATION, "vnd.groove-account"),

	APPLICATION_VND_GROOVE_HELP(TopLevelType.APPLICATION, "vnd.groove-help"),

	APPLICATION_VND_GROOVE_IDENTITY_MESSAGE(TopLevelType.APPLICATION, "vnd.groove-identity-aMessage"),

	APPLICATION_VND_GROOVE_INJECTOR(TopLevelType.APPLICATION, "vnd.groove-injector"),

	APPLICATION_VND_GROOVE_TOOL_MESSAGE(TopLevelType.APPLICATION, "vnd.groove-tool-aMessage"),

	APPLICATION_VND_GROOVE_TOOL_TEMPLATE(TopLevelType.APPLICATION, "vnd.groove-tool-template"),

	APPLICATION_VND_GROOVE_VCARD(TopLevelType.APPLICATION, "vnd.groove-vcard"),

	APPLICATION_VND_HAL_JSON(TopLevelType.APPLICATION, "vnd.hal+json"),

	APPLICATION_VND_HAL_XML(TopLevelType.APPLICATION, "vnd.hal+xml"),

	APPLICATION_VND_HANDHELD_ENTERTAINMENT_XML(TopLevelType.APPLICATION, "vnd.HandHeld-Entertainment+xml"),

	APPLICATION_VND_HBCI(TopLevelType.APPLICATION, "vnd.hbci"),

	APPLICATION_VND_HCL_BIREPORTS(TopLevelType.APPLICATION, "vnd.hcl-bireports"),

	APPLICATION_VND_HDT(TopLevelType.APPLICATION, "vnd.hdt"),

	APPLICATION_VND_HEROKU_JSON(TopLevelType.APPLICATION, "vnd.heroku+json"),

	APPLICATION_VND_HHE_LESSON_PLAYER(TopLevelType.APPLICATION, "vnd.hhe.lesson-player"),

	APPLICATION_VND_HP_HPGL(TopLevelType.APPLICATION, "vnd.hp-HPGL"),

	APPLICATION_VND_HP_HPID(TopLevelType.APPLICATION, "vnd.hp-hpid"),

	APPLICATION_VND_HP_HPS(TopLevelType.APPLICATION, "vnd.hp-hps"),

	APPLICATION_VND_HP_JLYT(TopLevelType.APPLICATION, "vnd.hp-jlyt"),

	APPLICATION_VND_HP_PCL(TopLevelType.APPLICATION, "vnd.hp-PCL"),

	APPLICATION_VND_HP_PCLXL(TopLevelType.APPLICATION, "vnd.hp-PCLXL"),

	APPLICATION_VND_HTTPHONE(TopLevelType.APPLICATION, "vnd.httphone"),

	APPLICATION_VND_HYDROSTATIX_SOF_DATA(TopLevelType.APPLICATION, "vnd.hydrostatix.sof-data"),

	APPLICATION_VND_HYPERDRIVE_JSON(TopLevelType.APPLICATION, "vnd.hyperdrive+json"),

	APPLICATION_VND_HZN_3D_CROSSWORD(TopLevelType.APPLICATION, "vnd.hzn-3d-crossword"),

	APPLICATION_VND_IBM_AFPLINEDATA(TopLevelType.APPLICATION, "vnd.ibm.afplinedata"),

	APPLICATION_VND_IBM_ELECTRONIC_MEDIA(TopLevelType.APPLICATION, "vnd.ibm.electronic-media"),

	APPLICATION_VND_IBM_MINIPAY(TopLevelType.APPLICATION, "vnd.ibm.MiniPay"),

	APPLICATION_VND_IBM_MODCAP(TopLevelType.APPLICATION, "vnd.ibm.modcap"),

	APPLICATION_VND_IBM_RIGHTS_MANAGEMENT(TopLevelType.APPLICATION, "vnd.ibm.rights-management"),

	APPLICATION_VND_IBM_SECURE_CONTAINER(TopLevelType.APPLICATION, "vnd.ibm.secure-container"),

	APPLICATION_VND_ICCPROFILE(TopLevelType.APPLICATION, "vnd.iccprofile"),

	APPLICATION_VND_IEEE_1905(TopLevelType.APPLICATION, "vnd.ieee.1905"),

	APPLICATION_VND_IGLOADER(TopLevelType.APPLICATION, "vnd.igloader"),

	APPLICATION_VND_IMMERVISION_IVP(TopLevelType.APPLICATION, "vnd.immervision-ivp"),

	APPLICATION_VND_IMMERVISION_IVU(TopLevelType.APPLICATION, "vnd.immervision-ivu"),

	APPLICATION_VND_IMS_IMSCCV1P1(TopLevelType.APPLICATION, "vnd.ims.imsccv1p1"),

	APPLICATION_VND_IMS_IMSCCV1P2(TopLevelType.APPLICATION, "vnd.ims.imsccv1p2"),

	APPLICATION_VND_IMS_IMSCCV1P3(TopLevelType.APPLICATION, "vnd.ims.imsccv1p3"),

	APPLICATION_VND_IMS_LIS_V2_RESULT_JSON(TopLevelType.APPLICATION, "vnd.ims.lis.v2.result+json"),

	APPLICATION_VND_IMS_LTI_V2_TOOLCONSUMERPROFILE_JSON(TopLevelType.APPLICATION, "vnd.ims.lti.v2.toolconsumerprofile+json"),

	APPLICATION_VND_IMS_LTI_V2_TOOLPROXY_ID_JSON(TopLevelType.APPLICATION, "vnd.ims.lti.v2.toolproxy.id+json"),

	APPLICATION_VND_IMS_LTI_V2_TOOLPROXY_JSON(TopLevelType.APPLICATION, "vnd.ims.lti.v2.toolproxy+json"),

	APPLICATION_VND_IMS_LTI_V2_TOOLSETTINGS_JSON(TopLevelType.APPLICATION, "vnd.ims.lti.v2.toolsettings+json"),

	APPLICATION_VND_IMS_LTI_V2_TOOLSETTINGS_SIMPLE_JSON(TopLevelType.APPLICATION, "vnd.ims.lti.v2.toolsettings.simple+json"),

	APPLICATION_VND_INFORMEDCONTROL_RMS_XML(TopLevelType.APPLICATION, "vnd.informedcontrol.rms+xml"),

	APPLICATION_VND_INFOTECH_PROJECT(TopLevelType.APPLICATION, "vnd.infotech.project"),

	APPLICATION_VND_INFOTECH_PROJECT_XML(TopLevelType.APPLICATION, "vnd.infotech.project+xml"),

	APPLICATION_VND_INFORMIX_VISIONARY(TopLevelType.APPLICATION, "vnd.informix-visionary"),

	APPLICATION_VND_INNOPATH_WAMP_NOTIFICATION(TopLevelType.APPLICATION, "vnd.innopath.wamp.notification"),

	APPLICATION_VND_INSORS_IGM(TopLevelType.APPLICATION, "vnd.insors.igm"),

	APPLICATION_VND_INTERCON_FORMNET(TopLevelType.APPLICATION, "vnd.intercon.formnet"),

	APPLICATION_VND_INTERGEO(TopLevelType.APPLICATION, "vnd.intergeo"),

	APPLICATION_VND_INTERTRUST_DIGIBOX(TopLevelType.APPLICATION, "vnd.intertrust.digibox"),

	APPLICATION_VND_INTERTRUST_NNCP(TopLevelType.APPLICATION, "vnd.intertrust.nncp"),

	APPLICATION_VND_INTU_QBO(TopLevelType.APPLICATION, "vnd.intu.qbo"),

	APPLICATION_VND_INTU_QFX(TopLevelType.APPLICATION, "vnd.intu.qfx"),

	APPLICATION_VND_IPTC_G2_CATALOGITEM_XML(TopLevelType.APPLICATION, "vnd.iptc.g2.catalogitem+xml"),

	APPLICATION_VND_IPTC_G2_CONCEPTITEM_XML(TopLevelType.APPLICATION, "vnd.iptc.g2.conceptitem+xml"),

	APPLICATION_VND_IPTC_G2_KNOWLEDGEITEM_XML(TopLevelType.APPLICATION, "vnd.iptc.g2.knowledgeitem+xml"),

	APPLICATION_VND_IPTC_G2_NEWSITEM_XML(TopLevelType.APPLICATION, "vnd.iptc.g2.newsitem+xml"),

	APPLICATION_VND_IPTC_G2_NEWSMESSAGE_XML(TopLevelType.APPLICATION, "vnd.iptc.g2.newsmessage+xml"),

	APPLICATION_VND_IPTC_G2_PACKAGEITEM_XML(TopLevelType.APPLICATION, "vnd.iptc.g2.packageitem+xml"),

	APPLICATION_VND_IPTC_G2_PLANNINGITEM_XML(TopLevelType.APPLICATION, "vnd.iptc.g2.planningitem+xml"),

	APPLICATION_VND_IPUNPLUGGED_RCPROFILE(TopLevelType.APPLICATION, "vnd.ipunplugged.rcprofile"),

	APPLICATION_VND_IREPOSITORY_PACKAGE_XML(TopLevelType.APPLICATION, "vnd.irepository.package+xml"),

	APPLICATION_VND_IS_XPR(TopLevelType.APPLICATION, "vnd.is-xpr"),

	APPLICATION_VND_ISAC_FCS(TopLevelType.APPLICATION, "vnd.isac.fcs"),

	APPLICATION_VND_JAM(TopLevelType.APPLICATION, "vnd.jam"),

	APPLICATION_VND_JAPANNET_DIRECTORY_SERVICE(TopLevelType.APPLICATION, "vnd.japannet-directory-service"),

	APPLICATION_VND_JAPANNET_JPNSTORE_WAKEUP(TopLevelType.APPLICATION, "vnd.japannet-jpnstore-wakeup"),

	APPLICATION_VND_JAPANNET_PAYMENT_WAKEUP(TopLevelType.APPLICATION, "vnd.japannet-payment-wakeup"),

	APPLICATION_VND_JAPANNET_REGISTRATION(TopLevelType.APPLICATION, "vnd.japannet-registration"),

	APPLICATION_VND_JAPANNET_REGISTRATION_WAKEUP(TopLevelType.APPLICATION, "vnd.japannet-registration-wakeup"),

	APPLICATION_VND_JAPANNET_SETSTORE_WAKEUP(TopLevelType.APPLICATION, "vnd.japannet-setstore-wakeup"),

	APPLICATION_VND_JAPANNET_VERIFICATION(TopLevelType.APPLICATION, "vnd.japannet-verification"),

	APPLICATION_VND_JAPANNET_VERIFICATION_WAKEUP(TopLevelType.APPLICATION, "vnd.japannet-verification-wakeup"),

	APPLICATION_VND_JCP_JAVAME_MIDLET_RMS(TopLevelType.APPLICATION, "vnd.jcp.javame.midlet-rms"),

	APPLICATION_VND_JISP(TopLevelType.APPLICATION, "vnd.jisp"),

	APPLICATION_VND_JOOST_JODA_ARCHIVE(TopLevelType.APPLICATION, "vnd.joost.joda-archive"),

	APPLICATION_VND_JSK_ISDN_NGN(TopLevelType.APPLICATION, "vnd.jsk.isdn-ngn"),

	APPLICATION_VND_KAHOOTZ(TopLevelType.APPLICATION, "vnd.kahootz"),

	APPLICATION_VND_KDE_KARBON(TopLevelType.APPLICATION, "vnd.kde.karbon"),

	APPLICATION_VND_KDE_KCHART(TopLevelType.APPLICATION, "vnd.kde.kchart"),

	APPLICATION_VND_KDE_KFORMULA(TopLevelType.APPLICATION, "vnd.kde.kformula"),

	APPLICATION_VND_KDE_KIVIO(TopLevelType.APPLICATION, "vnd.kde.kivio"),

	APPLICATION_VND_KDE_KONTOUR(TopLevelType.APPLICATION, "vnd.kde.kontour"),

	APPLICATION_VND_KDE_KPRESENTER(TopLevelType.APPLICATION, "vnd.kde.kpresenter"),

	APPLICATION_VND_KDE_KSPREAD(TopLevelType.APPLICATION, "vnd.kde.kspread"),

	APPLICATION_VND_KDE_KWORD(TopLevelType.APPLICATION, "vnd.kde.kword"),

	APPLICATION_VND_KENAMEAAPP(TopLevelType.APPLICATION, "vnd.kenameaapp"),

	APPLICATION_VND_KIDSPIRATION(TopLevelType.APPLICATION, "vnd.kidspiration"),

	APPLICATION_VND_KINAR(TopLevelType.APPLICATION, "vnd.Kinar"),

	APPLICATION_VND_KOAN(TopLevelType.APPLICATION, "vnd.koan"),

	APPLICATION_VND_KODAK_DESCRIPTOR(TopLevelType.APPLICATION, "vnd.kodak-descriptor"),

	APPLICATION_VND_LAS_LAS_XML(TopLevelType.APPLICATION, "vnd.las.las+xml"),

	APPLICATION_VND_LIBERTY_REQUEST_XML(TopLevelType.APPLICATION, "vnd.liberty-request+xml"),

	APPLICATION_VND_LLAMAGRAPHICS_LIFE_BALANCE_DESKTOP(TopLevelType.APPLICATION, "vnd.llamagraphics.life-balance.desktop"),

	APPLICATION_VND_LLAMAGRAPHICS_LIFE_BALANCE_EXCHANGE_XML(TopLevelType.APPLICATION, "vnd.llamagraphics.life-balance.exchange+xml"),

	APPLICATION_VND_LOTUS_1_2_3(TopLevelType.APPLICATION, "vnd.lotus-1-2-3"),

	APPLICATION_VND_LOTUS_APPROACH(TopLevelType.APPLICATION, "vnd.lotus-approach"),

	APPLICATION_VND_LOTUS_FREELANCE(TopLevelType.APPLICATION, "vnd.lotus-freelance"),

	APPLICATION_VND_LOTUS_NOTES(TopLevelType.APPLICATION, "vnd.lotus-notes"),

	APPLICATION_VND_LOTUS_ORGANIZER(TopLevelType.APPLICATION, "vnd.lotus-organizer"),

	APPLICATION_VND_LOTUS_SCREENCAM(TopLevelType.APPLICATION, "vnd.lotus-screencam"),

	APPLICATION_VND_LOTUS_WORDPRO(TopLevelType.APPLICATION, "vnd.lotus-wordpro"),

	APPLICATION_VND_MACPORTS_PORTPKG(TopLevelType.APPLICATION, "vnd.macports.portpkg"),

	// APPLICATION_VND_MACPORTS_PORTPKG( MediaType.APPLICATION, "vnd.macports.portpkg" ),
	APPLICATION_VND_MAPBOX_VECTOR_TILE(TopLevelType.APPLICATION, "vnd.mapbox-vector-tile"),

	APPLICATION_VND_MARLIN_DRM_ACTIONTOKEN_XML(TopLevelType.APPLICATION, "vnd.marlin.drm.actiontoken+xml"),

	APPLICATION_VND_MARLIN_DRM_CONFTOKEN_XML(TopLevelType.APPLICATION, "vnd.marlin.drm.conftoken+xml"),

	APPLICATION_VND_MARLIN_DRM_LICENSE_XML(TopLevelType.APPLICATION, "vnd.marlin.drm.license+xml"),

	APPLICATION_VND_MARLIN_DRM_MDCF(TopLevelType.APPLICATION, "vnd.marlin.drm.mdcf"),

	APPLICATION_VND_MASON_JSON(TopLevelType.APPLICATION, "vnd.mason+json"),

	APPLICATION_VND_MAXMIND_MAXMIND_DB(TopLevelType.APPLICATION, "vnd.maxmind.maxmind-db"),

	APPLICATION_VND_MCD(TopLevelType.APPLICATION, "vnd.mcd"),

	APPLICATION_VND_MEDCALCDATA(TopLevelType.APPLICATION, "vnd.medcalcdata"),

	APPLICATION_VND_MEDIASTATION_CDKEY(TopLevelType.APPLICATION, "vnd.mediastation.cdkey"),

	APPLICATION_VND_MERIDIAN_SLINGSHOT(TopLevelType.APPLICATION, "vnd.meridian-slingshot"),

	APPLICATION_VND_MFER(TopLevelType.APPLICATION, "vnd.MFER"),

	APPLICATION_VND_MFMP(TopLevelType.APPLICATION, "vnd.mfmp"),

	APPLICATION_VND_MICRO_JSON(TopLevelType.APPLICATION, "vnd.micro+json"),

	APPLICATION_VND_MICROGRAFX_FLO(TopLevelType.APPLICATION, "vnd.micrografx.flo"),

	APPLICATION_VND_MICROGRAFX_IGX(TopLevelType.APPLICATION, "vnd.micrografx-igx"),

	APPLICATION_VND_MICROSOFT_PORTABLE_EXECUTABLE(TopLevelType.APPLICATION, "vnd.microsoft.portable-executable"),

	APPLICATION_VND_MIELE_JSON(TopLevelType.APPLICATION, "vnd.miele+json"),

	APPLICATION_VND_MIF(TopLevelType.APPLICATION, "vnd-mif"),

	APPLICATION_VND_MINISOFT_HP3000_SAVE(TopLevelType.APPLICATION, "vnd.minisoft-hp3000-save"),

	APPLICATION_VND_MITSUBISHI_MISTY_GUARD_TRUSTWEB(TopLevelType.APPLICATION, "vnd.mitsubishi.misty-guard.trustweb"),

	APPLICATION_VND_MOBIUS_DAF(TopLevelType.APPLICATION, "vnd.Mobius.DAF"),

	APPLICATION_VND_MOBIUS_DIS(TopLevelType.APPLICATION, "vnd.Mobius.DIS"),

	APPLICATION_VND_MOBIUS_MBK(TopLevelType.APPLICATION, "vnd.Mobius.MBK"),

	APPLICATION_VND_MOBIUS_MQY(TopLevelType.APPLICATION, "vnd.Mobius.MQY"),

	APPLICATION_VND_MOBIUS_MSL(TopLevelType.APPLICATION, "vnd.Mobius.MSL"),

	APPLICATION_VND_MOBIUS_PLC(TopLevelType.APPLICATION, "vnd.Mobius.PLC"),

	APPLICATION_VND_MOBIUS_TXF(TopLevelType.APPLICATION, "vnd.Mobius.TXF"),

	APPLICATION_VND_MOPHUN_APPLICATION(TopLevelType.APPLICATION, "vnd.mophun.application"),

	APPLICATION_VND_MOPHUN_CERTIFICATE(TopLevelType.APPLICATION, "vnd.mophun.certificate"),

	APPLICATION_VND_MOTOROLA_FLEXSUITE(TopLevelType.APPLICATION, "vnd.motorola.flexsuite"),

	APPLICATION_VND_MOTOROLA_FLEXSUITE_ADSI(TopLevelType.APPLICATION, "vnd.motorola.flexsuite.adsi"),

	APPLICATION_VND_MOTOROLA_FLEXSUITE_FIS(TopLevelType.APPLICATION, "vnd.motorola.flexsuite.fis"),

	APPLICATION_VND_MOTOROLA_FLEXSUITE_GOTAP(TopLevelType.APPLICATION, "vnd.motorola.flexsuite.gotap"),

	APPLICATION_VND_MOTOROLA_FLEXSUITE_KMR(TopLevelType.APPLICATION, "vnd.motorola.flexsuite.kmr"),

	APPLICATION_VND_MOTOROLA_FLEXSUITE_TTC(TopLevelType.APPLICATION, "vnd.motorola.flexsuite.ttc"),

	APPLICATION_VND_MOTOROLA_FLEXSUITE_WEM(TopLevelType.APPLICATION, "vnd.motorola.flexsuite.wem"),

	APPLICATION_VND_MOTOROLA_IPRM(TopLevelType.APPLICATION, "vnd.motorola.iprm"),

	APPLICATION_VND_MOZILLA_XUL_XML(TopLevelType.APPLICATION, "vnd.mozilla.xul+xml"),

	APPLICATION_VND_MS_ARTGALRY(TopLevelType.APPLICATION, "vnd.ms-artgalry"),

	APPLICATION_VND_MS_ASF(TopLevelType.APPLICATION, "vnd.ms-asf"),

	APPLICATION_VND_MS_CAB_COMPRESSED(TopLevelType.APPLICATION, "vnd.ms-cab-compressed"),

	APPLICATION_VND_MS_3MFDOCUMENT(TopLevelType.APPLICATION, "vnd.ms-3mfdocument"),

	APPLICATION_VND_MS_EXCEL(TopLevelType.APPLICATION, "vnd.ms-excel"),

	APPLICATION_VND_MS_EXCEL_ADDIN_MACROENABLED_12(TopLevelType.APPLICATION, "vnd.ms-excel.addin.macroEnabled.12"),

	APPLICATION_VND_MS_EXCEL_SHEET_BINARY_MACROENABLED_12(TopLevelType.APPLICATION, "vnd.ms-excel.sheet.binary.macroEnabled.12"),

	APPLICATION_VND_MS_EXCEL_SHEET_MACROENABLED_12(TopLevelType.APPLICATION, "vnd.ms-excel.sheet.macroEnabled.12"),

	APPLICATION_VND_MS_EXCEL_TEMPLATE_MACROENABLED_12(TopLevelType.APPLICATION, "vnd.ms-excel.template.macroEnabled.12"),

	APPLICATION_VND_MS_FONTOBJECT(TopLevelType.APPLICATION, "vnd.ms-fontobject"),

	APPLICATION_VND_MS_HTMLHELP(TopLevelType.APPLICATION, "vnd.ms-htmlhelp"),

	APPLICATION_VND_MS_IMS(TopLevelType.APPLICATION, "vnd.ms-ims"),

	APPLICATION_VND_MS_LRM(TopLevelType.APPLICATION, "vnd.ms-lrm"),

	APPLICATION_VND_MS_OFFICE_ACTIVEX_XML(TopLevelType.APPLICATION, "vnd.ms-office.activeX+xml"),

	APPLICATION_VND_MS_OFFICETHEME(TopLevelType.APPLICATION, "vnd.ms-officetheme"),

	APPLICATION_VND_MS_PLAYREADY_INITIATOR_XML(TopLevelType.APPLICATION, "vnd.ms-playready.initiator+xml"),

	APPLICATION_VND_MS_POWERPOINT(TopLevelType.APPLICATION, "vnd.ms-powerpoint"),

	APPLICATION_VND_MS_POWERPOINT_ADDIN_MACROENABLED_12(TopLevelType.APPLICATION, "vnd.ms-powerpoint.addin.macroEnabled.12"),

	APPLICATION_VND_MS_POWERPOINT_PRESENTATION_MACROENABLED_12(TopLevelType.APPLICATION, "vnd.ms-powerpoint.presentation.macroEnabled.12"),

	APPLICATION_VND_MS_POWERPOINT_SLIDE_MACROENABLED_12(TopLevelType.APPLICATION, "vnd.ms-powerpoint.slide.macroEnabled.12"),

	APPLICATION_VND_MS_POWERPOINT_SLIDESHOW_MACROENABLED_12(TopLevelType.APPLICATION, "vnd.ms-powerpoint.slideshow.macroEnabled.12"),

	APPLICATION_VND_MS_POWERPOINT_TEMPLATE_MACROENABLED_12(TopLevelType.APPLICATION, "vnd.ms-powerpoint.template.macroEnabled.12"),

	APPLICATION_VND_MS_PRINTDEVICECAPABILITIES_XML(TopLevelType.APPLICATION, "vnd.ms-PrintDeviceCapabilities+xml"),

	APPLICATION_VND_MS_PRINTSCHEMATICKET_XML(TopLevelType.APPLICATION, "vnd.ms-PrintSchemaTicket+xml"),

	APPLICATION_VND_MS_PROJECT(TopLevelType.APPLICATION, "vnd.ms-project"),

	APPLICATION_VND_MS_TNEF(TopLevelType.APPLICATION, "vnd.ms-tnef"),

	APPLICATION_VND_MS_WINDOWS_DEVICEPAIRING(TopLevelType.APPLICATION, "vnd.ms-windows.devicepairing"),

	APPLICATION_VND_MS_WINDOWS_NWPRINTING_OOB(TopLevelType.APPLICATION, "vnd.ms-windows.nwprinting.oob"),

	APPLICATION_VND_MS_WINDOWS_PRINTERPAIRING(TopLevelType.APPLICATION, "vnd.ms-windows.printerpairing"),

	APPLICATION_VND_MS_WINDOWS_WSD_OOB(TopLevelType.APPLICATION, "vnd.ms-windows.wsd.oob"),

	APPLICATION_VND_MS_WMDRM_LIC_CHLG_REQ(TopLevelType.APPLICATION, "vnd.ms-wmdrm.lic-chlg-req"),

	APPLICATION_VND_MS_WMDRM_LIC_RESP(TopLevelType.APPLICATION, "vnd.ms-wmdrm.lic-resp"),

	APPLICATION_VND_MS_WMDRM_METER_CHLG_REQ(TopLevelType.APPLICATION, "vnd.ms-wmdrm.meter-chlg-req"),

	APPLICATION_VND_MS_WMDRM_METER_RESP(TopLevelType.APPLICATION, "vnd.ms-wmdrm.meter-resp"),

	APPLICATION_VND_MS_WORD_DOCUMENT_MACROENABLED_12(TopLevelType.APPLICATION, "vnd.ms-word.document.macroEnabled.12"),

	APPLICATION_VND_MS_WORD_TEMPLATE_MACROENABLED_12(TopLevelType.APPLICATION, "vnd.ms-word.template.macroEnabled.12"),

	APPLICATION_VND_MS_WORKS(TopLevelType.APPLICATION, "vnd.ms-works"),

	APPLICATION_VND_MS_WPL(TopLevelType.APPLICATION, "vnd.ms-wpl"),

	APPLICATION_VND_MS_XPSDOCUMENT(TopLevelType.APPLICATION, "vnd.ms-xpsdocument"),

	APPLICATION_VND_MSA_DISK_IMAGE(TopLevelType.APPLICATION, "vnd.msa-disk-image"),

	APPLICATION_VND_MSEQ(TopLevelType.APPLICATION, "vnd.mseq"),

	APPLICATION_VND_MSIGN(TopLevelType.APPLICATION, "vnd.msign"),

	APPLICATION_VND_MULTIAD_CREATOR(TopLevelType.APPLICATION, "vnd.multiad.creator"),

	APPLICATION_VND_MULTIAD_CREATOR_CIF(TopLevelType.APPLICATION, "vnd.multiad.creator.cif"),

	APPLICATION_VND_MUSICIAN(TopLevelType.APPLICATION, "vnd.musician"),

	APPLICATION_VND_MUSIC_NIFF(TopLevelType.APPLICATION, "vnd.music-niff"),

	APPLICATION_VND_MUVEE_STYLE(TopLevelType.APPLICATION, "vnd.muvee.style"),

	APPLICATION_VND_MYNFC(TopLevelType.APPLICATION, "vnd.mynfc"),

	APPLICATION_VND_NCD_CONTROL(TopLevelType.APPLICATION, "vnd.ncd.control"),

	APPLICATION_VND_NCD_REFERENCE(TopLevelType.APPLICATION, "vnd.ncd.reference"),

	APPLICATION_VND_NEARST_INV_JSON(TopLevelType.APPLICATION, "vnd.nearst.inv+json"),

	APPLICATION_VND_NERVANA(TopLevelType.APPLICATION, "vnd.nervana"),

	APPLICATION_VND_NETFPX(TopLevelType.APPLICATION, "vnd.netfpx"),

	APPLICATION_VND_NEUROLANGUAGE_NLU(TopLevelType.APPLICATION, "vnd.neurolanguage.nlu"),

	APPLICATION_VND_NINTENDO_SNES_ROM(TopLevelType.APPLICATION, "vnd.nintendo.snes.rom"),

	APPLICATION_VND_NINTENDO_NITRO_ROM(TopLevelType.APPLICATION, "vnd.nintendo.nitro.rom"),

	APPLICATION_VND_NITF(TopLevelType.APPLICATION, "vnd.nitf"),

	APPLICATION_VND_NOBLENET_DIRECTORY(TopLevelType.APPLICATION, "vnd.noblenet-directory"),

	APPLICATION_VND_NOBLENET_SEALER(TopLevelType.APPLICATION, "vnd.noblenet-sealer"),

	APPLICATION_VND_NOBLENET_WEB(TopLevelType.APPLICATION, "vnd.noblenet-web"),

	APPLICATION_VND_NOKIA_CATALOGS(TopLevelType.APPLICATION, "vnd.nokia.catalogs"),

	APPLICATION_VND_NOKIA_CONML_WBXML(TopLevelType.APPLICATION, "vnd.nokia.conml+wbxml"),

	APPLICATION_VND_NOKIA_CONML_XML(TopLevelType.APPLICATION, "vnd.nokia.conml+xml"),

	APPLICATION_VND_NOKIA_IPTV_CONFIG_XML(TopLevelType.APPLICATION, "vnd.nokia.iptv.config+xml"),

	APPLICATION_VND_NOKIA_ISDS_RADIO_PRESETS(TopLevelType.APPLICATION, "vnd.nokia.iSDS-radio-presets"),

	APPLICATION_VND_NOKIA_LANDMARK_WBXML(TopLevelType.APPLICATION, "vnd.nokia.landmark+wbxml"),

	APPLICATION_VND_NOKIA_LANDMARK_XML(TopLevelType.APPLICATION, "vnd.nokia.landmark+xml"),

	APPLICATION_VND_NOKIA_LANDMARKCOLLECTION_XML(TopLevelType.APPLICATION, "vnd.nokia.landmarkcollection+xml"),

	APPLICATION_VND_NOKIA_NCD(TopLevelType.APPLICATION, "vnd.nokia.ncd"),

	APPLICATION_VND_NOKIA_N_GAGE_AC_XML(TopLevelType.APPLICATION, "vnd.nokia.n-gage.ac+xml"),

	APPLICATION_VND_NOKIA_N_GAGE_DATA(TopLevelType.APPLICATION, "vnd.nokia.n-gage.data"),

	APPLICATION_VND_NOKIA_N_GAGE_SYMBIAN_INSTALL(TopLevelType.APPLICATION, "vnd.nokia.n-gage.symbian.install"),

	APPLICATION_VND_NOKIA_PCD_WBXML(TopLevelType.APPLICATION, "vnd.nokia.pcd+wbxml"),

	APPLICATION_VND_NOKIA_PCD_XML(TopLevelType.APPLICATION, "vnd.nokia.pcd+xml"),

	APPLICATION_VND_NOKIA_RADIO_PRESET(TopLevelType.APPLICATION, "vnd.nokia.radio-preset"),

	APPLICATION_VND_NOKIA_RADIO_PRESETS(TopLevelType.APPLICATION, "vnd.nokia.radio-presets"),

	APPLICATION_VND_NOVADIGM_EDM(TopLevelType.APPLICATION, "vnd.novadigm.EDM"),

	APPLICATION_VND_NOVADIGM_EDX(TopLevelType.APPLICATION, "vnd.novadigm.EDX"),

	APPLICATION_VND_NOVADIGM_EXT(TopLevelType.APPLICATION, "vnd.novadigm.EXT"),

	APPLICATION_VND_NTT_LOCAL_CONTENT_SHARE(TopLevelType.APPLICATION, "vnd.ntt-local.content-share"),

	APPLICATION_VND_NTT_LOCAL_FILE_TRANSFER(TopLevelType.APPLICATION, "vnd.ntt-local.file-transfer"),

	APPLICATION_VND_NTT_LOCAL_OGW_REMOTE_ACCESS(TopLevelType.APPLICATION, "vnd.ntt-local.ogw_remote-access"),

	APPLICATION_VND_NTT_LOCAL_SIP_TA_REMOTE(TopLevelType.APPLICATION, "vnd.ntt-local.sip-ta_remote"),

	APPLICATION_VND_NTT_LOCAL_SIP_TA_TCP_STREAM(TopLevelType.APPLICATION, "vnd.ntt-local.sip-ta_tcp_stream"),

	APPLICATION_VND_OASIS_OPENDOCUMENT_CHART(TopLevelType.APPLICATION, "vnd.oasis.opendocument.chart"),

	APPLICATION_VND_OASIS_OPENDOCUMENT_CHART_TEMPLATE(TopLevelType.APPLICATION, "vnd.oasis.opendocument.chart-template"),

	APPLICATION_VND_OASIS_OPENDOCUMENT_DATABASE(TopLevelType.APPLICATION, "vnd.oasis.opendocument.database"),

	APPLICATION_VND_OASIS_OPENDOCUMENT_FORMULA(TopLevelType.APPLICATION, "vnd.oasis.opendocument.formula"),

	APPLICATION_VND_OASIS_OPENDOCUMENT_FORMULA_TEMPLATE(TopLevelType.APPLICATION, "vnd.oasis.opendocument.formula-template"),

	APPLICATION_VND_OASIS_OPENDOCUMENT_GRAPHICS(TopLevelType.APPLICATION, "vnd.oasis.opendocument.graphics"),

	APPLICATION_VND_OASIS_OPENDOCUMENT_GRAPHICS_TEMPLATE(TopLevelType.APPLICATION, "vnd.oasis.opendocument.graphics-template"),

	APPLICATION_VND_OASIS_OPENDOCUMENT_IMAGE(TopLevelType.APPLICATION, "vnd.oasis.opendocument.image"),

	APPLICATION_VND_OASIS_OPENDOCUMENT_IMAGE_TEMPLATE(TopLevelType.APPLICATION, "vnd.oasis.opendocument.image-template"),

	APPLICATION_VND_OASIS_OPENDOCUMENT_PRESENTATION(TopLevelType.APPLICATION, "vnd.oasis.opendocument.presentation"),

	APPLICATION_VND_OASIS_OPENDOCUMENT_PRESENTATION_TEMPLATE(TopLevelType.APPLICATION, "vnd.oasis.opendocument.presentation-template"),

	APPLICATION_VND_OASIS_OPENDOCUMENT_SPREADSHEET(TopLevelType.APPLICATION, "vnd.oasis.opendocument.spreadsheet"),

	APPLICATION_VND_OASIS_OPENDOCUMENT_SPREADSHEET_TEMPLATE(TopLevelType.APPLICATION, "vnd.oasis.opendocument.spreadsheet-template"),

	APPLICATION_VND_OASIS_OPENDOCUMENT_TEXT(TopLevelType.APPLICATION, "vnd.oasis.opendocument.text"),

	APPLICATION_VND_OASIS_OPENDOCUMENT_TEXT_MASTER(TopLevelType.APPLICATION, "vnd.oasis.opendocument.text-master"),

	APPLICATION_VND_OASIS_OPENDOCUMENT_TEXT_TEMPLATE(TopLevelType.APPLICATION, "vnd.oasis.opendocument.text-template"),

	APPLICATION_VND_OASIS_OPENDOCUMENT_TEXT_WEB(TopLevelType.APPLICATION, "vnd.oasis.opendocument.text-web"),

	APPLICATION_VND_OBN(TopLevelType.APPLICATION, "vnd.obn"),

	APPLICATION_VND_OFTN_L10N_JSON(TopLevelType.APPLICATION, "vnd.oftn.l10n+json"),

	APPLICATION_VND_OIPF_CONTENTACCESSDOWNLOAD_XML(TopLevelType.APPLICATION, "vnd.oipf.contentaccessdownload+xml"),

	APPLICATION_VND_OIPF_CONTENTACCESSSTREAMING_XML(TopLevelType.APPLICATION, "vnd.oipf.contentaccessstreaming+xml"),

	APPLICATION_VND_OIPF_CSPG_HEXBINARY(TopLevelType.APPLICATION, "vnd.oipf.cspg-hexbinary"),

	APPLICATION_VND_OIPF_DAE_SVG_XML(TopLevelType.APPLICATION, "vnd.oipf.dae.svg+xml"),

	APPLICATION_VND_OIPF_DAE_XHTML_XML(TopLevelType.APPLICATION, "vnd.oipf.dae.xhtml+xml"),

	APPLICATION_VND_OIPF_MIPPVCONTROLMESSAGE_XML(TopLevelType.APPLICATION, "vnd.oipf.mippvcontrolmessage+xml"),

	APPLICATION_VND_OIPF_PAE_GEM(TopLevelType.APPLICATION, "vnd.oipf.pae.gem"),

	APPLICATION_VND_OIPF_SPDISCOVERY_XML(TopLevelType.APPLICATION, "vnd.oipf.spdiscovery+xml"),

	APPLICATION_VND_OIPF_SPDLIST_XML(TopLevelType.APPLICATION, "vnd.oipf.spdlist+xml"),

	APPLICATION_VND_OIPF_UEPROFILE_XML(TopLevelType.APPLICATION, "vnd.oipf.ueprofile+xml"),

	APPLICATION_VND_OIPF_USERPROFILE_XML(TopLevelType.APPLICATION, "vnd.oipf.userprofile+xml"),

	APPLICATION_VND_OLPC_SUGAR(TopLevelType.APPLICATION, "vnd.olpc-sugar"),

	APPLICATION_VND_OMA_BCAST_ASSOCIATED_PROCEDURE_PARAMETER_XML(TopLevelType.APPLICATION, "vnd.oma.bcast.associated-procedure-parameter+xml"),

	APPLICATION_VND_OMA_BCAST_DRM_TRIGGER_XML(TopLevelType.APPLICATION, "vnd.oma.bcast.drm-trigger+xml"),

	APPLICATION_VND_OMA_BCAST_IMD_XML(TopLevelType.APPLICATION, "vnd.oma.bcast.imd+xml"),

	APPLICATION_VND_OMA_BCAST_LTKM(TopLevelType.APPLICATION, "vnd.oma.bcast.ltkm"),

	APPLICATION_VND_OMA_BCAST_NOTIFICATION_XML(TopLevelType.APPLICATION, "vnd.oma.bcast.notification+xml"),

	APPLICATION_VND_OMA_BCAST_PROVISIONINGTRIGGER(TopLevelType.APPLICATION, "vnd.oma.bcast.provisioningtrigger"),

	APPLICATION_VND_OMA_BCAST_SGBOOT(TopLevelType.APPLICATION, "vnd.oma.bcast.sgboot"),

	APPLICATION_VND_OMA_BCAST_SGDD_XML(TopLevelType.APPLICATION, "vnd.oma.bcast.sgdd+xml"),

	APPLICATION_VND_OMA_BCAST_SGDU(TopLevelType.APPLICATION, "vnd.oma.bcast.sgdu"),

	APPLICATION_VND_OMA_BCAST_SIMPLE_SYMBOL_CONTAINER(TopLevelType.APPLICATION, "vnd.oma.bcast.simple-symbol-container"),

	APPLICATION_VND_OMA_BCAST_SMARTCARD_TRIGGER_XML(TopLevelType.APPLICATION, "vnd.oma.bcast.smartcard-trigger+xml"),

	APPLICATION_VND_OMA_BCAST_SPROV_XML(TopLevelType.APPLICATION, "vnd.oma.bcast.sprov+xml"),

	APPLICATION_VND_OMA_BCAST_STKM(TopLevelType.APPLICATION, "vnd.oma.bcast.stkm"),

	APPLICATION_VND_OMA_CAB_ADDRESS_BOOK_XML(TopLevelType.APPLICATION, "vnd.oma.cab-address-book+xml"),

	APPLICATION_VND_OMA_CAB_FEATURE_HANDLER_XML(TopLevelType.APPLICATION, "vnd.oma.cab-feature-handler+xml"),

	APPLICATION_VND_OMA_CAB_PCC_XML(TopLevelType.APPLICATION, "vnd.oma.cab-pcc+xml"),

	APPLICATION_VND_OMA_CAB_SUBS_INVITE_XML(TopLevelType.APPLICATION, "vnd.oma.cab-subs-invite+xml"),

	APPLICATION_VND_OMA_CAB_USER_PREFS_XML(TopLevelType.APPLICATION, "vnd.oma.cab-user-prefs+xml"),

	APPLICATION_VND_OMA_DCD(TopLevelType.APPLICATION, "vnd.oma.dcd"),

	APPLICATION_VND_OMA_DCDC(TopLevelType.APPLICATION, "vnd.oma.dcdc"),

	APPLICATION_VND_OMA_DD2_XML(TopLevelType.APPLICATION, "vnd.oma.dd2+xml"),

	APPLICATION_VND_OMA_DRM_RISD_XML(TopLevelType.APPLICATION, "vnd.oma.drm.risd+xml"),

	APPLICATION_VND_OMA_GROUP_USAGE_LIST_XML(TopLevelType.APPLICATION, "vnd.oma.group-usage-list+xml"),

	APPLICATION_VND_OMA_LWM2M_JSON(TopLevelType.APPLICATION, "vnd.oma.lwm2m+json"),

	APPLICATION_VND_OMA_LWM2M_TLV(TopLevelType.APPLICATION, "vnd.oma.lwm2m+tlv"),

	APPLICATION_VND_OMA_PAL_XML(TopLevelType.APPLICATION, "vnd.oma.pal+xml"),

	APPLICATION_VND_OMA_POC_DETAILED_PROGRESS_REPORT_XML(TopLevelType.APPLICATION, "vnd.oma.poc.detailed-progress-report+xml"),

	APPLICATION_VND_OMA_POC_FINAL_REPORT_XML(TopLevelType.APPLICATION, "vnd.oma.poc.final-report+xml"),

	APPLICATION_VND_OMA_POC_GROUPS_XML(TopLevelType.APPLICATION, "vnd.oma.poc.groups+xml"),

	APPLICATION_VND_OMA_POC_INVOCATION_DESCRIPTOR_XML(TopLevelType.APPLICATION, "vnd.oma.poc.invocation-descriptor+xml"),

	APPLICATION_VND_OMA_POC_OPTIMIZED_PROGRESS_REPORT_XML(TopLevelType.APPLICATION, "vnd.oma.poc.optimized-progress-report+xml"),

	APPLICATION_VND_OMA_PUSH(TopLevelType.APPLICATION, "vnd.oma.push"),

	APPLICATION_VND_OMA_SCIDM_MESSAGES_XML(TopLevelType.APPLICATION, "vnd.oma.scidm.messages+xml"),

	APPLICATION_VND_OMA_XCAP_DIRECTORY_XML(TopLevelType.APPLICATION, "vnd.oma.xcap-directory+xml"),

	APPLICATION_VND_OMADS_EMAIL_XML(TopLevelType.APPLICATION, "vnd.omads-email+xml"),

	APPLICATION_VND_OMADS_FILE_XML(TopLevelType.APPLICATION, "vnd.omads-file+xml"),

	APPLICATION_VND_OMADS_FOLDER_XML(TopLevelType.APPLICATION, "vnd.omads-folder+xml"),

	APPLICATION_VND_OMALOC_SUPL_INIT(TopLevelType.APPLICATION, "vnd.omaloc-supl-init"),

	APPLICATION_VND_OMA_SCWS_CONFIG(TopLevelType.APPLICATION, "vnd.oma-scws-config"),

	APPLICATION_VND_OMA_SCWS_HTTP_REQUEST(TopLevelType.APPLICATION, "vnd.oma-scws-http-request"),

	APPLICATION_VND_OMA_SCWS_HTTP_RESPONSE(TopLevelType.APPLICATION, "vnd.oma-scws-http-response"),

	APPLICATION_VND_ONEPAGER(TopLevelType.APPLICATION, "vnd.onepager"),

	APPLICATION_VND_OPENBLOX_GAME_BINARY(TopLevelType.APPLICATION, "vnd.openblox.game-binary"),

	APPLICATION_VND_OPENBLOX_GAME_XML(TopLevelType.APPLICATION, "vnd.openblox.game+xml"),

	APPLICATION_VND_OPENEYE_OEB(TopLevelType.APPLICATION, "vnd.openeye.oeb"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_CUSTOM_PROPERTIES_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.custom-properties+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_CUSTOMXMLPROPERTIES_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.customXmlProperties+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_DRAWING_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.drawing+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_DRAWINGML_CHART_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.drawingml.chart+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_DRAWINGML_CHARTSHAPES_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.drawingml.chartshapes+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_DRAWINGML_DIAGRAMCOLORS_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.drawingml.diagramColors+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_DRAWINGML_DIAGRAMDATA_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.drawingml.diagramData+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_DRAWINGML_DIAGRAMLAYOUT_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.drawingml.diagramLayout+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_DRAWINGML_DIAGRAMSTYLE_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.drawingml.diagramStyle+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_EXTENDED_PROPERTIES_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.extended-properties+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_COMMENTAUTHORS_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.presentationml.commentAuthors+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_COMMENTS_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.presentationml.comments+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_HANDOUTMASTER_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.presentationml.handoutMaster+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_NOTESMASTER_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.presentationml.notesMaster+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_NOTESSLIDE_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.presentationml.notesSlide+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_PRESENTATION(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.presentationml.presentation"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_PRESENTATION_MAIN_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.presentationml.presentation.main+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_PRESPROPS_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.presentationml.presProps+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_SLIDE(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.presentationml.slide"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_SLIDE_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.presentationml.slide+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_SLIDELAYOUT_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.presentationml.slideLayout+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_SLIDEMASTER_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.presentationml.slideMaster+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_SLIDESHOW(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.presentationml.slideshow"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_SLIDESHOW_MAIN_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.presentationml.slideshow.main+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_SLIDEUPDATEINFO_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.presentationml.slideUpdateInfo+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_TABLESTYLES_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.presentationml.tableStyles+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_TAGS_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.presentationml.tags+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_TEMPLATE(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.presentationml-template"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_TEMPLATE_MAIN_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.presentationml.template.main+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_VIEWPROPS_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.presentationml.viewProps+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_CALCCHAIN_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml.calcChain+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_CHARTSHEET_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml.chartsheet+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_COMMENTS_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml.comments+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_CONNECTIONS_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml.connections+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_DIALOGSHEET_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml.dialogsheet+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_EXTERNALLINK_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml.externalLink+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_PIVOTCACHEDEFINITION_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml.pivotCacheDefinition+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_PIVOTCACHERECORDS_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml.pivotCacheRecords+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_PIVOTTABLE_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml.pivotTable+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_QUERYTABLE_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml.queryTable+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_REVISIONHEADERS_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml.revisionHeaders+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_REVISIONLOG_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml.revisionLog+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_SHAREDSTRINGS_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml.sharedStrings+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_SHEET(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml.sheet"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_SHEET_MAIN_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_SHEETMETADATA_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml.sheetMetadata+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_STYLES_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml.styles+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_TABLE_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml.table+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_TABLESINGLECELLS_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml.tableSingleCells+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_TEMPLATE(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml-template"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_TEMPLATE_MAIN_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml.template.main+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_USERNAMES_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml.userNames+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_VOLATILEDEPENDENCIES_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml.volatileDependencies+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_WORKSHEET_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_THEME_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.theme+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_THEMEOVERRIDE_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.themeOverride+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_VMLDRAWING(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.vmlDrawing"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_COMMENTS_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.wordprocessingml.comments+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_DOCUMENT(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.wordprocessingml.document"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_DOCUMENT_GLOSSARY_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.wordprocessingml.document.glossary+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_DOCUMENT_MAIN_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_ENDNOTES_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.wordprocessingml.endnotes+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_FONTTABLE_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.wordprocessingml.fontTable+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_FOOTER_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.wordprocessingml.footer+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_FOOTNOTES_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.wordprocessingml.footnotes+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_NUMBERING_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.wordprocessingml.numbering+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_SETTINGS_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.wordprocessingml.settings+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_STYLES_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.wordprocessingml.styles+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_TEMPLATE(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.wordprocessingml-template"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_TEMPLATE_MAIN_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.wordprocessingml.template.main+xml"),

	APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_WEBSETTINGS_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-officedocument.wordprocessingml.webSettings+xml"),

	APPLICATION_VND_OPENXMLFORMATS_PACKAGE_CORE_PROPERTIES_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-package.core-properties+xml"),

	APPLICATION_VND_OPENXMLFORMATS_PACKAGE_DIGITAL_SIGNATURE_XMLSIGNATURE_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-package.digital-signature-xmlsignature+xml"),

	APPLICATION_VND_OPENXMLFORMATS_PACKAGE_RELATIONSHIPS_XML(TopLevelType.APPLICATION, "vnd.openxmlformats-package.relationships+xml"),

	APPLICATION_VND_ORACLE_RESOURCE_JSON(TopLevelType.APPLICATION, "vnd.oracle.resource+json"),

	APPLICATION_VND_ORANGE_INDATA(TopLevelType.APPLICATION, "vnd.orange.indata"),

	APPLICATION_VND_OSA_NETDEPLOY(TopLevelType.APPLICATION, "vnd.osa.netdeploy"),

	APPLICATION_VND_OSGEO_MAPGUIDE_PACKAGE(TopLevelType.APPLICATION, "vnd.osgeo.mapguide.package"),

	APPLICATION_VND_OSGI_BUNDLE(TopLevelType.APPLICATION, "vnd.osgi.bundle"),

	APPLICATION_VND_OSGI_DP(TopLevelType.APPLICATION, "vnd.osgi.dp"),

	APPLICATION_VND_OSGI_SUBSYSTEM(TopLevelType.APPLICATION, "vnd.osgi.subsystem"),

	APPLICATION_VND_OTPS_CT_KIP_XML(TopLevelType.APPLICATION, "vnd.otps.ct-kip+xml"),

	APPLICATION_VND_OXLI_COUNTGRAPH(TopLevelType.APPLICATION, "vnd.oxli.countgraph"),

	APPLICATION_VND_PAGERDUTY_JSON(TopLevelType.APPLICATION, "vnd.pagerduty+json"),

	APPLICATION_VND_PALM(TopLevelType.APPLICATION, "vnd.palm"),

	APPLICATION_VND_PANOPLY(TopLevelType.APPLICATION, "vnd.panoply"),

	APPLICATION_VND_PAOS_XML(TopLevelType.APPLICATION, "vnd.paos+xml"),

	APPLICATION_VND_PAWAAFILE(TopLevelType.APPLICATION, "vnd.pawaafile"),

	APPLICATION_VND_PCOS(TopLevelType.APPLICATION, "vnd.pcos"),

	APPLICATION_VND_PG_FORMAT(TopLevelType.APPLICATION, "vnd.pg.format"),

	APPLICATION_VND_PG_OSASLI(TopLevelType.APPLICATION, "vnd.pg.osasli"),

	APPLICATION_VND_PIACCESS_APPLICATION_LICENCE(TopLevelType.APPLICATION, "vnd.piaccess.application-licence"),

	APPLICATION_VND_PICSEL(TopLevelType.APPLICATION, "vnd.picsel"),

	APPLICATION_VND_PMI_WIDGET(TopLevelType.APPLICATION, "vnd.pmi.widget"),

	APPLICATION_VND_POC_GROUP_ADVERTISEMENT_XML(TopLevelType.APPLICATION, "vnd.poc.group-advertisement+xml"),

	APPLICATION_VND_POCKETLEARN(TopLevelType.APPLICATION, "vnd.pocketlearn"),

	APPLICATION_VND_POWERBUILDER6(TopLevelType.APPLICATION, "vnd.powerbuilder6"),

	APPLICATION_VND_POWERBUILDER6_S(TopLevelType.APPLICATION, "vnd.powerbuilder6-s"),

	APPLICATION_VND_POWERBUILDER7(TopLevelType.APPLICATION, "vnd.powerbuilder7"),

	APPLICATION_VND_POWERBUILDER75(TopLevelType.APPLICATION, "vnd.powerbuilder75"),

	APPLICATION_VND_POWERBUILDER75_S(TopLevelType.APPLICATION, "vnd.powerbuilder75-s"),

	APPLICATION_VND_POWERBUILDER7_S(TopLevelType.APPLICATION, "vnd.powerbuilder7-s"),

	APPLICATION_VND_PREMINET(TopLevelType.APPLICATION, "vnd.preminet"),

	APPLICATION_VND_PREVIEWSYSTEMS_BOX(TopLevelType.APPLICATION, "vnd.previewsystems.box"),

	APPLICATION_VND_PROTEUS_MAGAZINE(TopLevelType.APPLICATION, "vnd.proteus.magazine"),

	APPLICATION_VND_PUBLISHARE_DELTA_TREE(TopLevelType.APPLICATION, "vnd.publishare-delta-tree"),

	APPLICATION_VND_PVI_PTID1(TopLevelType.APPLICATION, "vnd.pvi.ptid1"),

	APPLICATION_VND_PWG_MULTIPLEXED(TopLevelType.APPLICATION, "vnd.pwg-multiplexed"),

	APPLICATION_VND_PWG_XHTML_PRINT_XML(TopLevelType.APPLICATION, "vnd.pwg-xhtml-print+xml"),

	APPLICATION_VND_QUALCOMM_BREW_APP_RES(TopLevelType.APPLICATION, "vnd.qualcomm.brew-app-res"),

	APPLICATION_VND_QUARANTAINENET(TopLevelType.APPLICATION, "vnd.quarantainenet"),

	APPLICATION_VND_QUARK_QUARKXPRESS(TopLevelType.APPLICATION, "vnd.Quark.QuarkXPress"),

	APPLICATION_VND_QUOBJECT_QUOXDOCUMENT(TopLevelType.APPLICATION, "vnd.quobject-quoxdocument"),

	APPLICATION_VND_RADISYS_MOML_XML(TopLevelType.APPLICATION, "vnd.radisys.moml+xml"),

	APPLICATION_VND_RADISYS_MSML_AUDIT_CONF_XML(TopLevelType.APPLICATION, "vnd.radisys.msml-audit-conf+xml"),

	APPLICATION_VND_RADISYS_MSML_AUDIT_CONN_XML(TopLevelType.APPLICATION, "vnd.radisys.msml-audit-conn+xml"),

	APPLICATION_VND_RADISYS_MSML_AUDIT_DIALOG_XML(TopLevelType.APPLICATION, "vnd.radisys.msml-audit-dialog+xml"),

	APPLICATION_VND_RADISYS_MSML_AUDIT_STREAM_XML(TopLevelType.APPLICATION, "vnd.radisys.msml-audit-stream+xml"),

	APPLICATION_VND_RADISYS_MSML_AUDIT_XML(TopLevelType.APPLICATION, "vnd.radisys.msml-audit+xml"),

	APPLICATION_VND_RADISYS_MSML_CONF_XML(TopLevelType.APPLICATION, "vnd.radisys.msml-conf+xml"),

	APPLICATION_VND_RADISYS_MSML_DIALOG_BASE_XML(TopLevelType.APPLICATION, "vnd.radisys.msml-dialog-base+xml"),

	APPLICATION_VND_RADISYS_MSML_DIALOG_FAX_DETECT_XML(TopLevelType.APPLICATION, "vnd.radisys.msml-dialog-fax-detect+xml"),

	APPLICATION_VND_RADISYS_MSML_DIALOG_FAX_SENDRECV_XML(TopLevelType.APPLICATION, "vnd.radisys.msml-dialog-fax-sendrecv+xml"),

	APPLICATION_VND_RADISYS_MSML_DIALOG_GROUP_XML(TopLevelType.APPLICATION, "vnd.radisys.msml-dialog-group+xml"),

	APPLICATION_VND_RADISYS_MSML_DIALOG_SPEECH_XML(TopLevelType.APPLICATION, "vnd.radisys.msml-dialog-speech+xml"),

	APPLICATION_VND_RADISYS_MSML_DIALOG_TRANSFORM_XML(TopLevelType.APPLICATION, "vnd.radisys.msml-dialog-transform+xml"),

	APPLICATION_VND_RADISYS_MSML_DIALOG_XML(TopLevelType.APPLICATION, "vnd.radisys.msml-dialog+xml"),

	APPLICATION_VND_RADISYS_MSML_XML(TopLevelType.APPLICATION, "vnd.radisys.msml+xml"),

	APPLICATION_VND_RAINSTOR_DATA(TopLevelType.APPLICATION, "vnd.rainstor.data"),

	APPLICATION_VND_RAPID(TopLevelType.APPLICATION, "vnd.rapid"),

	APPLICATION_VND_RAR(TopLevelType.APPLICATION, "vnd.rar"),

	APPLICATION_VND_REALVNC_BED(TopLevelType.APPLICATION, "vnd.realvnc.bed"),

	APPLICATION_VND_RECORDARE_MUSICXML(TopLevelType.APPLICATION, "vnd.recordare.musicxml"),

	APPLICATION_VND_RECORDARE_MUSICXML_XML(TopLevelType.APPLICATION, "vnd.recordare.musicxml+xml"),

	APPLICATION_VND_RENLEARN_RLPRINT(TopLevelType.APPLICATION, "vnd.renlearn.rlprint"),

	APPLICATION_VND_RIG_CRYPTONOTE(TopLevelType.APPLICATION, "vnd.rig.cryptonote"),

	APPLICATION_VND_ROUTE66_LINK66_XML(TopLevelType.APPLICATION, "vnd.route66.link66+xml"),

	APPLICATION_VND_RS_274X(TopLevelType.APPLICATION, "vnd.rs-274x"),

	APPLICATION_VND_RUCKUS_DOWNLOAD(TopLevelType.APPLICATION, "vnd.ruckus.download"),

	APPLICATION_VND_S3SMS(TopLevelType.APPLICATION, "vnd.s3sms"),

	APPLICATION_VND_SAILINGTRACKER_TRACK(TopLevelType.APPLICATION, "vnd.sailingtracker.track"),

	APPLICATION_VND_SBM_CID(TopLevelType.APPLICATION, "vnd.sbm.cid"),

	APPLICATION_VND_SBM_MID2(TopLevelType.APPLICATION, "vnd.sbm.mid2"),

	APPLICATION_VND_SCRIBUS(TopLevelType.APPLICATION, "vnd.scribus"),

	APPLICATION_VND_SEALED_3DF(TopLevelType.APPLICATION, "vnd.sealed.3df"),

	APPLICATION_VND_SEALED_CSF(TopLevelType.APPLICATION, "vnd.sealed.csf"),

	APPLICATION_VND_SEALED_DOC(TopLevelType.APPLICATION, "vnd.sealed-doc"),

	APPLICATION_VND_SEALED_EML(TopLevelType.APPLICATION, "vnd.sealed-eml"),

	APPLICATION_VND_SEALED_MHT(TopLevelType.APPLICATION, "vnd.sealed-mht"),

	APPLICATION_VND_SEALED_NET(TopLevelType.APPLICATION, "vnd.sealed.net"),

	APPLICATION_VND_SEALED_PPT(TopLevelType.APPLICATION, "vnd.sealed-ppt"),

	APPLICATION_VND_SEALED_TIFF(TopLevelType.APPLICATION, "vnd.sealed-tiff"),

	APPLICATION_VND_SEALED_XLS(TopLevelType.APPLICATION, "vnd.sealed-xls"),

	APPLICATION_VND_SEALEDMEDIA_SOFTSEAL_HTML(TopLevelType.APPLICATION, "vnd.sealedmedia.softseal-html"),

	APPLICATION_VND_SEALEDMEDIA_SOFTSEAL_PDF(TopLevelType.APPLICATION, "vnd.sealedmedia.softseal-pdf"),

	APPLICATION_VND_SEEMAIL(TopLevelType.APPLICATION, "vnd.seemail"),

	APPLICATION_VND_SEMA(TopLevelType.APPLICATION, "vnd-sema"),

	APPLICATION_VND_SEMD(TopLevelType.APPLICATION, "vnd.semd"),

	APPLICATION_VND_SEMF(TopLevelType.APPLICATION, "vnd.semf"),

	APPLICATION_VND_SHANA_INFORMED_FORMDATA(TopLevelType.APPLICATION, "vnd.shana.informed.formdata"),

	APPLICATION_VND_SHANA_INFORMED_FORMTEMPLATE(TopLevelType.APPLICATION, "vnd.shana.informed.formtemplate"),

	APPLICATION_VND_SHANA_INFORMED_INTERCHANGE(TopLevelType.APPLICATION, "vnd.shana.informed.interchange"),

	APPLICATION_VND_SHANA_INFORMED_PACKAGE(TopLevelType.APPLICATION, "vnd.shana.informed.package"),

	APPLICATION_VND_SIMTECH_MINDMAPPER(TopLevelType.APPLICATION, "vnd.SimTech-MindMapper"),

	APPLICATION_VND_SIREN_JSON(TopLevelType.APPLICATION, "vnd.siren+json"),

	APPLICATION_VND_SMAF(TopLevelType.APPLICATION, "vnd.smaf"),

	APPLICATION_VND_SMART_NOTEBOOK(TopLevelType.APPLICATION, "vnd.smart.notebook"),

	APPLICATION_VND_SMART_TEACHER(TopLevelType.APPLICATION, "vnd.smart.teacher"),

	APPLICATION_VND_SOFTWARE602_FILLER_FORM_XML(TopLevelType.APPLICATION, "vnd.software602.filler.form+xml"),

	APPLICATION_VND_SOFTWARE602_FILLER_FORM_XML_ZIP(TopLevelType.APPLICATION, "vnd.software602.filler.form-xml-zip"),

	APPLICATION_VND_SOLENT_SDKM_XML(TopLevelType.APPLICATION, "vnd.solent.sdkm+xml"),

	APPLICATION_VND_SPOTFIRE_DXP(TopLevelType.APPLICATION, "vnd.spotfire.dxp"),

	APPLICATION_VND_SPOTFIRE_SFS(TopLevelType.APPLICATION, "vnd.spotfire.sfs"),

	APPLICATION_VND_SSS_COD(TopLevelType.APPLICATION, "vnd.sss-cod"),

	APPLICATION_VND_SSS_DTF(TopLevelType.APPLICATION, "vnd.sss-dtf"),

	APPLICATION_VND_SSS_NTF(TopLevelType.APPLICATION, "vnd.sss-ntf"),

	APPLICATION_VND_STEPMANIA_PACKAGE(TopLevelType.APPLICATION, "vnd.stepmania.package"),

	APPLICATION_VND_STEPMANIA_STEPCHART(TopLevelType.APPLICATION, "vnd.stepmania.stepchart"),

	APPLICATION_VND_STREET_STREAM(TopLevelType.APPLICATION, "vnd.street-stream"),

	APPLICATION_VND_SUN_WADL_XML(TopLevelType.APPLICATION, "vnd.sun.wadl+xml"),

	APPLICATION_VND_SUS_CALENDAR(TopLevelType.APPLICATION, "vnd.sus-calendar"),

	APPLICATION_VND_SVD(TopLevelType.APPLICATION, "vnd.svd"),

	APPLICATION_VND_SWIFTVIEW_ICS(TopLevelType.APPLICATION, "vnd.swiftview-ics"),

	APPLICATION_VND_SYNCML_DM_NOTIFICATION(TopLevelType.APPLICATION, "vnd.syncml.dm.notification"),

	APPLICATION_VND_SYNCML_DMDDF_XML(TopLevelType.APPLICATION, "vnd.syncml.dmddf+xml"),

	APPLICATION_VND_SYNCML_DMTNDS_WBXML(TopLevelType.APPLICATION, "vnd.syncml.dmtnds+wbxml"),

	APPLICATION_VND_SYNCML_DMTNDS_XML(TopLevelType.APPLICATION, "vnd.syncml.dmtnds+xml"),

	APPLICATION_VND_SYNCML_DMDDF_WBXML(TopLevelType.APPLICATION, "vnd.syncml.dmddf+wbxml"),

	APPLICATION_VND_SYNCML_DM_WBXML(TopLevelType.APPLICATION, "vnd.syncml.dm+wbxml"),

	APPLICATION_VND_SYNCML_DM_XML(TopLevelType.APPLICATION, "vnd.syncml.dm+xml"),

	APPLICATION_VND_SYNCML_DS_NOTIFICATION(TopLevelType.APPLICATION, "vnd.syncml.ds.notification"),

	APPLICATION_VND_SYNCML_XML(TopLevelType.APPLICATION, "vnd.syncml+xml"),

	APPLICATION_VND_TAO_INTENT_MODULE_ARCHIVE(TopLevelType.APPLICATION, "vnd.tao.intent-module-archive"),

	APPLICATION_VND_TCPDUMP_PCAP(TopLevelType.APPLICATION, "vnd.tcpdump.pcap"),

	APPLICATION_VND_TML(TopLevelType.APPLICATION, "vnd.tml"),

	APPLICATION_VND_TMD_MEDIAFLEX_API_XML(TopLevelType.APPLICATION, "vnd.tmd.mediaflex.api+xml"),

	APPLICATION_VND_TMOBILE_LIVETV(TopLevelType.APPLICATION, "vnd.tmobile-livetv"),

	APPLICATION_VND_TRID_TPT(TopLevelType.APPLICATION, "vnd.trid.tpt"),

	APPLICATION_VND_TRISCAPE_MXS(TopLevelType.APPLICATION, "vnd.triscape.mxs"),

	APPLICATION_VND_TRUEAPP(TopLevelType.APPLICATION, "vnd.trueapp"),

	APPLICATION_VND_TRUEDOC(TopLevelType.APPLICATION, "vnd.truedoc"),

	APPLICATION_VND_UBISOFT_WEBPLAYER(TopLevelType.APPLICATION, "vnd.ubisoft.webplayer"),

	APPLICATION_VND_UFDL(TopLevelType.APPLICATION, "vnd.ufdl"),

	APPLICATION_VND_UIQ_THEME(TopLevelType.APPLICATION, "vnd.uiq.theme"),

	APPLICATION_VND_UMAJIN(TopLevelType.APPLICATION, "vnd.umajin"),

	APPLICATION_VND_UNITY(TopLevelType.APPLICATION, "vnd.unity"),

	APPLICATION_VND_UOML_XML(TopLevelType.APPLICATION, "vnd.uoml+xml"),

	APPLICATION_VND_UPLANET_ALERT(TopLevelType.APPLICATION, "vnd.uplanet.alert"),

	APPLICATION_VND_UPLANET_ALERT_WBXML(TopLevelType.APPLICATION, "vnd.uplanet.alert-wbxml"),

	APPLICATION_VND_UPLANET_BEARER_CHOICE(TopLevelType.APPLICATION, "vnd.uplanet.bearer-choice"),

	APPLICATION_VND_UPLANET_BEARER_CHOICE_WBXML(TopLevelType.APPLICATION, "vnd.uplanet.bearer-choice-wbxml"),

	APPLICATION_VND_UPLANET_CACHEOP(TopLevelType.APPLICATION, "vnd.uplanet.cacheop"),

	APPLICATION_VND_UPLANET_CACHEOP_WBXML(TopLevelType.APPLICATION, "vnd.uplanet.cacheop-wbxml"),

	APPLICATION_VND_UPLANET_CHANNEL(TopLevelType.APPLICATION, "vnd.uplanet.channel"),

	APPLICATION_VND_UPLANET_CHANNEL_WBXML(TopLevelType.APPLICATION, "vnd.uplanet.channel-wbxml"),

	APPLICATION_VND_UPLANET_LIST(TopLevelType.APPLICATION, "vnd.uplanet.list"),

	APPLICATION_VND_UPLANET_LISTCMD(TopLevelType.APPLICATION, "vnd.uplanet.listcmd"),

	APPLICATION_VND_UPLANET_LISTCMD_WBXML(TopLevelType.APPLICATION, "vnd.uplanet.listcmd-wbxml"),

	APPLICATION_VND_UPLANET_LIST_WBXML(TopLevelType.APPLICATION, "vnd.uplanet.list-wbxml"),

	APPLICATION_VND_URI_MAP(TopLevelType.APPLICATION, "vnd.uri-map"),

	APPLICATION_VND_UPLANET_SIGNAL(TopLevelType.APPLICATION, "vnd.uplanet.signal"),

	APPLICATION_VND_VALVE_SOURCE_MATERIAL(TopLevelType.APPLICATION, "vnd.valve.source.material"),

	APPLICATION_VND_VCX(TopLevelType.APPLICATION, "vnd.vcx"),

	APPLICATION_VND_VD_STUDY(TopLevelType.APPLICATION, "vnd.vd-study"),

	APPLICATION_VND_VECTORWORKS(TopLevelType.APPLICATION, "vnd.vectorworks"),

	APPLICATION_VND_VEL_JSON(TopLevelType.APPLICATION, "vnd.vel+json"),

	APPLICATION_VND_VERIMATRIX_VCAS(TopLevelType.APPLICATION, "vnd.verimatrix.vcas"),

	APPLICATION_VND_VIDSOFT_VIDCONFERENCE(TopLevelType.APPLICATION, "vnd.vidsoft.vidconference"),

	APPLICATION_VND_VISIO(TopLevelType.APPLICATION, "vnd.visio"),

	APPLICATION_VND_VISIONARY(TopLevelType.APPLICATION, "vnd.visionary"),

	APPLICATION_VND_VIVIDENCE_SCRIPTFILE(TopLevelType.APPLICATION, "vnd.vividence.scriptfile"),

	APPLICATION_VND_VSF(TopLevelType.APPLICATION, "vnd.vsf"),

	APPLICATION_VND_WAP_SIC(TopLevelType.APPLICATION, "vnd.wap.sic"),

	APPLICATION_VND_WAP_SLC(TopLevelType.APPLICATION, "vnd.wap-slc"),

	APPLICATION_VND_WAP_WBXML(TopLevelType.APPLICATION, "vnd.wap-wbxml"),

	APPLICATION_VND_WAP_WMLC(TopLevelType.APPLICATION, "vnd-wap-wmlc"),

	APPLICATION_VND_WAP_WMLSCRIPTC(TopLevelType.APPLICATION, "vnd.wap.wmlscriptc"),

	APPLICATION_VND_WEBTURBO(TopLevelType.APPLICATION, "vnd.webturbo"),

	APPLICATION_VND_WFA_P2P(TopLevelType.APPLICATION, "vnd.wfa.p2p"),

	APPLICATION_VND_WFA_WSC(TopLevelType.APPLICATION, "vnd.wfa.wsc"),

	APPLICATION_VND_WINDOWS_DEVICEPAIRING(TopLevelType.APPLICATION, "vnd.windows.devicepairing"),

	APPLICATION_VND_WMC(TopLevelType.APPLICATION, "vnd.wmc"),

	APPLICATION_VND_WMF_BOOTSTRAP(TopLevelType.APPLICATION, "vnd.wmf.bootstrap"),

	APPLICATION_VND_WOLFRAM_MATHEMATICA(TopLevelType.APPLICATION, "vnd.wolfram.mathematica"),

	APPLICATION_VND_WOLFRAM_MATHEMATICA_PACKAGE(TopLevelType.APPLICATION, "vnd.wolfram.mathematica.package"),

	APPLICATION_VND_WOLFRAM_PLAYER(TopLevelType.APPLICATION, "vnd.wolfram.player"),

	APPLICATION_VND_WORDPERFECT(TopLevelType.APPLICATION, "vnd.wordperfect"),

	APPLICATION_VND_WQD(TopLevelType.APPLICATION, "vnd.wqd"),

	APPLICATION_VND_WRQ_HP3000_LABELLED(TopLevelType.APPLICATION, "vnd.wrq-hp3000-labelled"),

	APPLICATION_VND_WT_STF(TopLevelType.APPLICATION, "vnd.wt.stf"),

	APPLICATION_VND_WV_CSP_XML(TopLevelType.APPLICATION, "vnd.wv.csp+xml"),

	APPLICATION_VND_WV_CSP_WBXML(TopLevelType.APPLICATION, "vnd.wv.csp+wbxml"),

	APPLICATION_VND_WV_SSP_XML(TopLevelType.APPLICATION, "vnd.wv.ssp+xml"),

	APPLICATION_VND_XACML_JSON(TopLevelType.APPLICATION, "vnd.xacml+json"),

	APPLICATION_VND_XARA(TopLevelType.APPLICATION, "vnd.xara"),

	APPLICATION_VND_XFDL(TopLevelType.APPLICATION, "vnd.xfdl"),

	APPLICATION_VND_XFDL_WEBFORM(TopLevelType.APPLICATION, "vnd.xfdl.webform"),

	APPLICATION_VND_XMI_XML(TopLevelType.APPLICATION, "vnd.xmi+xml"),

	APPLICATION_VND_XMPIE_CPKG(TopLevelType.APPLICATION, "vnd.xmpie.cpkg"),

	APPLICATION_VND_XMPIE_DPKG(TopLevelType.APPLICATION, "vnd.xmpie.dpkg"),

	APPLICATION_VND_XMPIE_PLAN(TopLevelType.APPLICATION, "vnd.xmpie.plan"),

	APPLICATION_VND_XMPIE_PPKG(TopLevelType.APPLICATION, "vnd.xmpie.ppkg"),

	APPLICATION_VND_XMPIE_XLIM(TopLevelType.APPLICATION, "vnd.xmpie.xlim"),

	APPLICATION_VND_YAMAHA_HV_DIC(TopLevelType.APPLICATION, "vnd.yamaha.hv-dic"),

	APPLICATION_VND_YAMAHA_HV_SCRIPT(TopLevelType.APPLICATION, "vnd.yamaha.hv-script"),

	APPLICATION_VND_YAMAHA_HV_VOICE(TopLevelType.APPLICATION, "vnd.yamaha.hv-voice"),

	APPLICATION_VND_YAMAHA_OPENSCOREFORMAT_OSFPVG_XML(TopLevelType.APPLICATION, "vnd.yamaha.openscoreformat.osfpvg+xml"),

	APPLICATION_VND_YAMAHA_OPENSCOREFORMAT(TopLevelType.APPLICATION, "vnd.yamaha.openscoreformat"),

	APPLICATION_VND_YAMAHA_REMOTE_SETUP(TopLevelType.APPLICATION, "vnd.yamaha.remote-setup"),

	APPLICATION_VND_YAMAHA_SMAF_AUDIO(TopLevelType.APPLICATION, "vnd.yamaha.smaf-audio"),

	APPLICATION_VND_YAMAHA_SMAF_PHRASE(TopLevelType.APPLICATION, "vnd.yamaha.smaf-phrase"),

	APPLICATION_VND_YAMAHA_THROUGH_NGN(TopLevelType.APPLICATION, "vnd.yamaha.through-ngn"),

	APPLICATION_VND_YAMAHA_TUNNEL_UDPENCAP(TopLevelType.APPLICATION, "vnd.yamaha.tunnel-udpencap"),

	APPLICATION_VND_YAOWEME(TopLevelType.APPLICATION, "vnd.yaoweme"),

	APPLICATION_VND_YELLOWRIVER_CUSTOM_MENU(TopLevelType.APPLICATION, "vnd.yellowriver-custom-menu"),

	APPLICATION_VND_ZUL(TopLevelType.APPLICATION, "vnd.zul"),

	APPLICATION_VND_ZZAZZ_DECK_XML(TopLevelType.APPLICATION, "vnd.zzazz.deck+xml"),

	APPLICATION_VOICEXML_XML(TopLevelType.APPLICATION, "voicexml+xml"),

	APPLICATION_VQ_RTCPXR(TopLevelType.APPLICATION, "vq-rtcpxr"),

	APPLICATION_WATCHERINFO_XML(TopLevelType.APPLICATION, "watcherinfo+xml"),

	APPLICATION_WHOISPP_QUERY(TopLevelType.APPLICATION, "whoispp-query"),

	APPLICATION_WHOISPP_RESPONSE(TopLevelType.APPLICATION, "whoispp-response"),

	APPLICATION_WITA(TopLevelType.APPLICATION, "wita"),

	APPLICATION_WORDPERFECT5_1(TopLevelType.APPLICATION, "wordperfect5.1"),

	APPLICATION_WSDL_XML(TopLevelType.APPLICATION, "wsdl+xml"),

	APPLICATION_WSPOLICY_XML(TopLevelType.APPLICATION, "wspolicy+xml"),

	APPLICATION_X_WWW_FORM_URLENCODED(TopLevelType.APPLICATION, "x-www-form-urlencoded"),

	APPLICATION_X400_BP(TopLevelType.APPLICATION, "x400-bp"),

	APPLICATION_XACML_XML(TopLevelType.APPLICATION, "xacml+xml"),

	APPLICATION_XCAP_ATT_XML(TopLevelType.APPLICATION, "xcap-att+xml"),

	APPLICATION_XCAP_CAPS_XML(TopLevelType.APPLICATION, "xcap-caps+xml"),

	APPLICATION_XCAP_DIFF_XML(TopLevelType.APPLICATION, "xcap-diff+xml"),

	APPLICATION_XCAP_EL_XML(TopLevelType.APPLICATION, "xcap-el+xml"),

	APPLICATION_XCAP_ERROR_XML(TopLevelType.APPLICATION, "xcap-error+xml"),

	APPLICATION_XCAP_NS_XML(TopLevelType.APPLICATION, "xcap-ns+xml"),

	APPLICATION_XCON_CONFERENCE_INFO_DIFF_XML(TopLevelType.APPLICATION, "xcon-conference-info-diff+xml"),

	APPLICATION_XCON_CONFERENCE_INFO_XML(TopLevelType.APPLICATION, "xcon-conference-info+xml"),

	APPLICATION_XENC_XML(TopLevelType.APPLICATION, "xenc+xml"),

	APPLICATION_XHTML_XML(TopLevelType.APPLICATION, "xhtml+xml"),

	APPLICATION_XML(TopLevelType.APPLICATION, "xml"),

	APPLICATION_XML_DTD(TopLevelType.APPLICATION, "xml-dtd"),

	APPLICATION_XML_EXTERNAL_PARSED_ENTITY(TopLevelType.APPLICATION, "xml-external-parsed-entity"),

	APPLICATION_XML_PATCH_XML(TopLevelType.APPLICATION, "xml-patch+xml"),

	APPLICATION_XMPP_XML(TopLevelType.APPLICATION, "xmpp+xml"),

	APPLICATION_XOP_XML(TopLevelType.APPLICATION, "xop+xml"),

	APPLICATION_XV_XML(TopLevelType.APPLICATION, "xv+xml"),

	APPLICATION_YAML(TopLevelType.APPLICATION, "x-yaml"),

	APPLICATION_YANG(TopLevelType.APPLICATION, "yang"),

	APPLICATION_YIN_XML(TopLevelType.APPLICATION, "yin+xml"),

	APPLICATION_ZIP(TopLevelType.APPLICATION, "zip"),

	APPLICATION_ZLIB(TopLevelType.APPLICATION, "zlib"),

	AUDIO_1D_INTERLEAVED_PARITYFEC(TopLevelType.AUDIO, "1d-interleaved-parityfec"),

	AUDIO_32KADPCM(TopLevelType.AUDIO, "32kadpcm"),

	AUDIO_3GPP(TopLevelType.AUDIO, "3gpp"),

	AUDIO_3GPP2(TopLevelType.AUDIO, "3gpp2"),

	AUDIO_AC3(TopLevelType.AUDIO, "ac3"),

	AUDIO_AMR(TopLevelType.AUDIO, "AMR"),

	AUDIO_AMR_WB(TopLevelType.AUDIO, "AMR-WB"),

	AUDIO_AMR_WB_(TopLevelType.AUDIO, "amr-wb+"),

	AUDIO_APTX(TopLevelType.AUDIO, "aptx"),

	AUDIO_ASC(TopLevelType.AUDIO, "asc"),

	AUDIO_ATRAC_ADVANCED_LOSSLESS(TopLevelType.AUDIO, "ATRAC-ADVANCED-LOSSLESS"),

	AUDIO_ATRAC_X(TopLevelType.AUDIO, "ATRAC-X"),

	AUDIO_ATRAC3(TopLevelType.AUDIO, "ATRAC3"),

	AUDIO_BASIC(TopLevelType.AUDIO, "basic"),

	AUDIO_BV16(TopLevelType.AUDIO, "BV16"),

	AUDIO_BV32(TopLevelType.AUDIO, "BV32"),

	AUDIO_CLEARMODE(TopLevelType.AUDIO, "clearmode"),

	AUDIO_CN(TopLevelType.AUDIO, "CN"),

	AUDIO_DAT12(TopLevelType.AUDIO, "DAT12"),

	AUDIO_DLS(TopLevelType.AUDIO, "dls"),

	AUDIO_DSR_ES201108(TopLevelType.AUDIO, "dsr-es201108"),

	AUDIO_DSR_ES202050(TopLevelType.AUDIO, "dsr-es202050"),

	AUDIO_DSR_ES202211(TopLevelType.AUDIO, "dsr-es202211"),

	AUDIO_DSR_ES202212(TopLevelType.AUDIO, "dsr-es202212"),

	AUDIO_DV(TopLevelType.AUDIO, "DV"),

	AUDIO_DVI4(TopLevelType.AUDIO, "DVI4"),

	AUDIO_EAC3(TopLevelType.AUDIO, "eac3"),

	AUDIO_ENCAPRTP(TopLevelType.AUDIO, "encaprtp"),

	AUDIO_EVRC(TopLevelType.AUDIO, "EVRC"),

	AUDIO_EVRC_QCP(TopLevelType.AUDIO, "EVRC-QCP"),

	AUDIO_EVRC0(TopLevelType.AUDIO, "EVRC0"),

	AUDIO_EVRC1(TopLevelType.AUDIO, "EVRC1"),

	AUDIO_EVRCB(TopLevelType.AUDIO, "EVRCB"),

	AUDIO_EVRCB0(TopLevelType.AUDIO, "EVRCB0"),

	AUDIO_EVRCB1(TopLevelType.AUDIO, "EVRCB1"),

	AUDIO_EVRCNW(TopLevelType.AUDIO, "EVRCNW"),

	AUDIO_EVRCNW0(TopLevelType.AUDIO, "EVRCNW0"),

	AUDIO_EVRCNW1(TopLevelType.AUDIO, "EVRCNW1"),

	AUDIO_EVRCWB(TopLevelType.AUDIO, "EVRCWB"),

	AUDIO_EVRCWB0(TopLevelType.AUDIO, "EVRCWB0"),

	AUDIO_EVRCWB1(TopLevelType.AUDIO, "EVRCWB1"),

	AUDIO_EVS(TopLevelType.AUDIO, "EVS"),

	AUDIO_EXAMPLE(TopLevelType.AUDIO, "example"),

	AUDIO_FWDRED(TopLevelType.AUDIO, "fwdred"),

	AUDIO_G711_0(TopLevelType.AUDIO, "G711-0"),

	AUDIO_G719(TopLevelType.AUDIO, "G719"),

	AUDIO_G7221(TopLevelType.AUDIO, "G7221"),

	AUDIO_G722(TopLevelType.AUDIO, "G722"),

	AUDIO_G723(TopLevelType.AUDIO, "G723"),

	AUDIO_G726_16(TopLevelType.AUDIO, "G726-16"),

	AUDIO_G726_24(TopLevelType.AUDIO, "G726-24"),

	AUDIO_G726_32(TopLevelType.AUDIO, "G726-32"),

	AUDIO_G726_40(TopLevelType.AUDIO, "G726-40"),

	AUDIO_G728(TopLevelType.AUDIO, "G728"),

	AUDIO_G729(TopLevelType.AUDIO, "G729"),

	AUDIO_G729D(TopLevelType.AUDIO, "G729D"),

	AUDIO_G729E(TopLevelType.AUDIO, "G729E"),

	AUDIO_GSM(TopLevelType.AUDIO, "GSM"),

	AUDIO_GSM_EFR(TopLevelType.AUDIO, "GSM-EFR"),

	AUDIO_GSM_HR_08(TopLevelType.AUDIO, "GSM-HR-08"),

	AUDIO_ILBC(TopLevelType.AUDIO, "iLBC"),

	AUDIO_IP_MR_V2_5(TopLevelType.AUDIO, "ip-mr_v2.5"),

	AUDIO_L8(TopLevelType.AUDIO, "L8"),

	AUDIO_L16(TopLevelType.AUDIO, "L16"),

	AUDIO_L20(TopLevelType.AUDIO, "L20"),

	AUDIO_L24(TopLevelType.AUDIO, "L24"),

	AUDIO_LPC(TopLevelType.AUDIO, "LPC"),

	AUDIO_MOBILE_XMF(TopLevelType.AUDIO, "mobile-xmf"),

	AUDIO_MPA(TopLevelType.AUDIO, "MPA"),

	AUDIO_MP4(TopLevelType.AUDIO, "mp4"),

	AUDIO_MP4A_LATM(TopLevelType.AUDIO, "MP4A-LATM"),

	AUDIO_MPA_ROBUST(TopLevelType.AUDIO, "mpa-robust"),

	AUDIO_MPEG(TopLevelType.AUDIO, "mpeg"),

	AUDIO_MPEG4_GENERIC(TopLevelType.AUDIO, "mpeg4-generic"),

	AUDIO_OGG(TopLevelType.AUDIO, "ogg"),

	AUDIO_OPUS(TopLevelType.AUDIO, "opus"),

	AUDIO_PCMA(TopLevelType.AUDIO, "PCMA"),

	AUDIO_PCMA_WB(TopLevelType.AUDIO, "PCMA-WB"),

	AUDIO_PCMU(TopLevelType.AUDIO, "PCMU"),

	AUDIO_PCMU_WB(TopLevelType.AUDIO, "PCMU-WB"),

	AUDIO_PRS_SID(TopLevelType.AUDIO, "prs.sid"),

	AUDIO_RAPTORFEC(TopLevelType.AUDIO, "raptorfec"),

	AUDIO_RED(TopLevelType.AUDIO, "RED"),

	AUDIO_RTP_ENC_AESCM128(TopLevelType.AUDIO, "rtp-enc-aescm128"),

	AUDIO_RTPLOOPBACK(TopLevelType.AUDIO, "rtploopback"),

	AUDIO_RTP_MIDI(TopLevelType.AUDIO, "rtp-midi"),

	AUDIO_RTX(TopLevelType.AUDIO, "rtx"),

	AUDIO_SMV(TopLevelType.AUDIO, "SMV"),

	AUDIO_SMV0(TopLevelType.AUDIO, "SMV0"),

	AUDIO_SMV_QCP(TopLevelType.AUDIO, "SMV-QCP"),

	AUDIO_SP_MIDI(TopLevelType.AUDIO, "sp-midi"),

	AUDIO_SPEEX(TopLevelType.AUDIO, "speex"),

	AUDIO_T140C(TopLevelType.AUDIO, "t140c"),

	AUDIO_T38(TopLevelType.AUDIO, "t38"),

	AUDIO_TELEPHONE_EVENT(TopLevelType.AUDIO, "telephone-event"),

	AUDIO_TONE(TopLevelType.AUDIO, "tone"),

	AUDIO_UEMCLIP(TopLevelType.AUDIO, "UEMCLIP"),

	AUDIO_ULPFEC(TopLevelType.AUDIO, "ulpfec"),

	AUDIO_VDVI(TopLevelType.AUDIO, "VDVI"),

	AUDIO_VMR_WB(TopLevelType.AUDIO, "VMR-WB"),

	AUDIO_VND_3GPP_IUFP(TopLevelType.AUDIO, "vnd.3gpp.iufp"),

	AUDIO_VND_4SB(TopLevelType.AUDIO, "vnd.4SB"),

	AUDIO_VND_AUDIOKOZ(TopLevelType.AUDIO, "vnd.audiokoz"),

	AUDIO_VND_CELP(TopLevelType.AUDIO, "vnd.CELP"),

	AUDIO_VND_CISCO_NSE(TopLevelType.AUDIO, "vnd.cisco.nse"),

	AUDIO_VND_CMLES_RADIO_EVENTS(TopLevelType.AUDIO, "vnd.cmles.radio-events"),

	AUDIO_VND_CNS_ANP1(TopLevelType.AUDIO, "vnd.cns.anp1"),

	AUDIO_VND_CNS_INF1(TopLevelType.AUDIO, "vnd.cns.inf1"),

	AUDIO_VND_DECE_AUDIO(TopLevelType.AUDIO, "vnd.dece.audio"),

	AUDIO_VND_DIGITAL_WINDS(TopLevelType.AUDIO, "vnd.digital-winds"),

	AUDIO_VND_DLNA_ADTS(TopLevelType.AUDIO, "vnd.dlna.adts"),

	AUDIO_VND_DOLBY_HEAAC_1(TopLevelType.AUDIO, "vnd.dolby.heaac.1"),

	AUDIO_VND_DOLBY_HEAAC_2(TopLevelType.AUDIO, "vnd.dolby.heaac.2"),

	AUDIO_VND_DOLBY_MLP(TopLevelType.AUDIO, "vnd.dolby.mlp"),

	AUDIO_VND_DOLBY_MPS(TopLevelType.AUDIO, "vnd.dolby.mps"),

	AUDIO_VND_DOLBY_PL2(TopLevelType.AUDIO, "vnd.dolby.pl2"),

	AUDIO_VND_DOLBY_PL2X(TopLevelType.AUDIO, "vnd.dolby.pl2x"),

	AUDIO_VND_DOLBY_PL2Z(TopLevelType.AUDIO, "vnd.dolby.pl2z"),

	AUDIO_VND_DOLBY_PULSE_1(TopLevelType.AUDIO, "vnd.dolby.pulse.1"),

	AUDIO_VND_DRA(TopLevelType.AUDIO, "vnd.dra"),

	AUDIO_VND_DTS(TopLevelType.AUDIO, "vnd.dts"),

	AUDIO_VND_DTS_HD(TopLevelType.AUDIO, "vnd.dts.hd"),

	AUDIO_VND_DVB_FILE(TopLevelType.AUDIO, "vnd.dvb.file"),

	AUDIO_VND_EVERAD_PLJ(TopLevelType.AUDIO, "vnd.everad.plj"),

	AUDIO_VND_HNS_AUDIO(TopLevelType.AUDIO, "vnd.hns.audio"),

	AUDIO_VND_LUCENT_VOICE(TopLevelType.AUDIO, "vnd.lucent.voice"),

	AUDIO_VND_MS_PLAYREADY_MEDIA_PYA(TopLevelType.AUDIO, "vnd.ms-playready.media.pya"),

	AUDIO_VND_NOKIA_MOBILE_XMF(TopLevelType.AUDIO, "vnd.nokia.mobile-xmf"),

	AUDIO_VND_NORTEL_VBK(TopLevelType.AUDIO, "vnd.nortel.vbk"),

	AUDIO_VND_NUERA_ECELP4800(TopLevelType.AUDIO, "vnd.nuera.ecelp4800"),

	AUDIO_VND_NUERA_ECELP7470(TopLevelType.AUDIO, "vnd.nuera.ecelp7470"),

	AUDIO_VND_NUERA_ECELP9600(TopLevelType.AUDIO, "vnd.nuera.ecelp9600"),

	AUDIO_VND_OCTEL_SBC(TopLevelType.AUDIO, "vnd.octel.sbc"),

	AUDIO_VND_QCELP(TopLevelType.AUDIO, "vnd.qcelp"),

	AUDIO_VND_RHETOREX_32KADPCM(TopLevelType.AUDIO, "vnd.rhetorex.32kadpcm"),

	AUDIO_VND_RIP(TopLevelType.AUDIO, "vnd.rip"),

	AUDIO_VND_SEALEDMEDIA_SOFTSEAL_MPEG(TopLevelType.AUDIO, "vnd.sealedmedia.softseal-mpeg"),

	AUDIO_VND_VMX_CVSD(TopLevelType.AUDIO, "vnd.vmx.cvsd"),

	AUDIO_VORBIS(TopLevelType.AUDIO, "vorbis"),

	AUDIO_VORBIS_CONFIG(TopLevelType.AUDIO, "vorbis-config"),

	IMAGE_BMP(TopLevelType.IMAGE, "bmp"),

	IMAGE_CGM(TopLevelType.IMAGE, "cgm"),

	IMAGE_DICOM_RLE(TopLevelType.IMAGE, "dicom-rle"),

	IMAGE_EMF(TopLevelType.IMAGE, "emf"),

	IMAGE_EXAMPLE(TopLevelType.IMAGE, "example"),

	IMAGE_FITS(TopLevelType.IMAGE, "fits"),

	IMAGE_G3FAX(TopLevelType.IMAGE, "g3fax"),

	IMAGE_JLS(TopLevelType.IMAGE, "jls"),

	IMAGE_JP2(TopLevelType.IMAGE, "jp2"),

	IMAGE_JPM(TopLevelType.IMAGE, "jpm"),

	IMAGE_JPX(TopLevelType.IMAGE, "jpx"),

	IMAGE_NAPLPS(TopLevelType.IMAGE, "naplps"),

	IMAGE_PNG(TopLevelType.IMAGE, "png"),

	IMAGE_PRS_BTIF(TopLevelType.IMAGE, "prs.btif"),

	IMAGE_PRS_PTI(TopLevelType.IMAGE, "prs.pti"),

	IMAGE_PWG_RASTER(TopLevelType.IMAGE, "pwg-raster"),

	IMAGE_T38(TopLevelType.IMAGE, "t38"),

	IMAGE_TIFF(TopLevelType.IMAGE, "tiff"),

	IMAGE_TIFF_FX(TopLevelType.IMAGE, "tiff-fx"),

	IMAGE_VND_ADOBE_PHOTOSHOP(TopLevelType.IMAGE, "vnd.adobe.photoshop"),

	IMAGE_VND_AIRZIP_ACCELERATOR_AZV(TopLevelType.IMAGE, "vnd.airzip.accelerator.azv"),

	IMAGE_VND_CNS_INF2(TopLevelType.IMAGE, "vnd.cns.inf2"),

	IMAGE_VND_DECE_GRAPHIC(TopLevelType.IMAGE, "vnd.dece.graphic"),

	IMAGE_VND_DJVU(TopLevelType.IMAGE, "vnd-djvu"),

	IMAGE_VND_DWG(TopLevelType.IMAGE, "vnd.dwg"),

	IMAGE_VND_DXF(TopLevelType.IMAGE, "vnd.dxf"),

	IMAGE_VND_DVB_SUBTITLE(TopLevelType.IMAGE, "vnd.dvb.subtitle"),

	IMAGE_VND_FASTBIDSHEET(TopLevelType.IMAGE, "vnd.fastbidsheet"),

	IMAGE_VND_FPX(TopLevelType.IMAGE, "vnd.fpx"),

	IMAGE_VND_FST(TopLevelType.IMAGE, "vnd.fst"),

	IMAGE_VND_FUJIXEROX_EDMICS_MMR(TopLevelType.IMAGE, "vnd.fujixerox.edmics-mmr"),

	IMAGE_VND_FUJIXEROX_EDMICS_RLC(TopLevelType.IMAGE, "vnd.fujixerox.edmics-rlc"),

	IMAGE_VND_GLOBALGRAPHICS_PGB(TopLevelType.IMAGE, "vnd.globalgraphics.pgb"),

	IMAGE_VND_MICROSOFT_ICON(TopLevelType.IMAGE, "vnd.microsoft.icon"),

	IMAGE_VND_MIX(TopLevelType.IMAGE, "vnd.mix"),

	IMAGE_VND_MS_MODI(TopLevelType.IMAGE, "vnd.ms-modi"),

	IMAGE_VND_MOZILLA_APNG(TopLevelType.IMAGE, "vnd.mozilla.apng"),

	IMAGE_VND_NET_FPX(TopLevelType.IMAGE, "vnd.net-fpx"),

	IMAGE_VND_RADIANCE(TopLevelType.IMAGE, "vnd.radiance"),

	IMAGE_VND_SEALED_PNG(TopLevelType.IMAGE, "vnd.sealed-png"),

	IMAGE_VND_SEALEDMEDIA_SOFTSEAL_GIF(TopLevelType.IMAGE, "vnd.sealedmedia.softseal-gif"),

	IMAGE_VND_SEALEDMEDIA_SOFTSEAL_JPG(TopLevelType.IMAGE, "vnd.sealedmedia.softseal-jpg"),

	IMAGE_VND_SVF(TopLevelType.IMAGE, "vnd-svf"),

	IMAGE_VND_TENCENT_TAP(TopLevelType.IMAGE, "vnd.tencent.tap"),

	IMAGE_VND_VALVE_SOURCE_TEXTURE(TopLevelType.IMAGE, "vnd.valve.source.texture"),

	IMAGE_VND_WAP_WBMP(TopLevelType.IMAGE, "vnd-wap-wbmp"),

	IMAGE_VND_XIFF(TopLevelType.IMAGE, "vnd.xiff"),

	IMAGE_VND_ZBRUSH_PCX(TopLevelType.IMAGE, "vnd.zbrush.pcx"),

	IMAGE_WMF(TopLevelType.IMAGE, "wmf"),
	// IMAGE_EMF( MediaType.IMAGE, "emf" ),
	// IMAGE_WMF( MediaType.IMAGE, "wmf" ),
	MESSAGE_CPIM(TopLevelType.MESSAGE, "CPIM"),

	MESSAGE_DELIVERY_STATUS(TopLevelType.MESSAGE, "delivery-status"),

	MESSAGE_DISPOSITION_NOTIFICATION(TopLevelType.MESSAGE, "disposition-notification"),

	MESSAGE_EXAMPLE(TopLevelType.MESSAGE, "example"),

	MESSAGE_FEEDBACK_REPORT(TopLevelType.MESSAGE, "feedback-report"),

	MESSAGE_GLOBAL(TopLevelType.MESSAGE, "global"),

	MESSAGE_GLOBAL_DELIVERY_STATUS(TopLevelType.MESSAGE, "global-delivery-status"),

	MESSAGE_GLOBAL_DISPOSITION_NOTIFICATION(TopLevelType.MESSAGE, "global-disposition-notification"),

	MESSAGE_GLOBAL_HEADERS(TopLevelType.MESSAGE, "global-headers"),

	MESSAGE_HTTP(TopLevelType.MESSAGE, "http"),

	MESSAGE_IMDN_XML(TopLevelType.MESSAGE, "imdn+xml"),

	MESSAGE_NEWS(TopLevelType.MESSAGE, "news"),

	MESSAGE_S_HTTP(TopLevelType.MESSAGE, "s-http"),

	MESSAGE_SIP(TopLevelType.MESSAGE, "sip"),

	MESSAGE_SIPFRAG(TopLevelType.MESSAGE, "sipfrag"),

	MESSAGE_TRACKING_STATUS(TopLevelType.MESSAGE, "tracking-status"),

	MESSAGE_VND_SI_SIMP(TopLevelType.MESSAGE, "vnd.si.simp"),

	MESSAGE_VND_WFA_WSC(TopLevelType.MESSAGE, "vnd.wfa.wsc"),

	MODEL_EXAMPLE(TopLevelType.MODEL, "example"),

	MODEL_GLTF_JSON(TopLevelType.MODEL, "gltf+json"),

	MODEL_IGES(TopLevelType.MODEL, "iges"),

	MODEL_VND_COLLADA_XML(TopLevelType.MODEL, "vnd.collada+xml"),

	MODEL_VND_DWF(TopLevelType.MODEL, "vnd-dwf"),

	MODEL_VND_FLATLAND_3DML(TopLevelType.MODEL, "vnd.flatland.3dml"),

	MODEL_VND_GDL(TopLevelType.MODEL, "vnd.gdl"),

	MODEL_VND_GS_GDL(TopLevelType.MODEL, "vnd.gs-gdl"),

	MODEL_VND_GTW(TopLevelType.MODEL, "vnd.gtw"),

	MODEL_VND_MOML_XML(TopLevelType.MODEL, "vnd.moml+xml"),

	MODEL_VND_MTS(TopLevelType.MODEL, "vnd.mts"),

	MODEL_VND_OPENGEX(TopLevelType.MODEL, "vnd.opengex"),

	MODEL_VND_PARASOLID_TRANSMIT_BINARY(TopLevelType.MODEL, "vnd.parasolid.transmit-binary"),

	MODEL_VND_PARASOLID_TRANSMIT_TEXT(TopLevelType.MODEL, "vnd.parasolid.transmit-text"),

	MODEL_VND_ROSETTE_ANNOTATED_DATA_MODEL(TopLevelType.MODEL, "vnd.rosette.annotated-data-model"),

	MODEL_VND_VALVE_SOURCE_COMPILED_MAP(TopLevelType.MODEL, "vnd.valve.source.compiled-map"),

	MODEL_VND_VTU(TopLevelType.MODEL, "vnd.vtu"),

	MODEL_X3D_VRML(TopLevelType.MODEL, "x3d-vrml"),

	MODEL_X3D_FASTINFOSET(TopLevelType.MODEL, "x3d+fastinfoset"),

	MODEL_X3D_XML(TopLevelType.MODEL, "x3d+xml"),

	MULTIPART_APPLEDOUBLE(TopLevelType.MULTIPART, "appledouble"),

	MULTIPART_BYTERANGES(TopLevelType.MULTIPART, "byteranges"),

	MULTIPART_ENCRYPTED(TopLevelType.MULTIPART, "encrypted"),

	MULTIPART_EXAMPLE(TopLevelType.MULTIPART, "example"),

	MULTIPART_FORM_DATA(TopLevelType.MULTIPART, "form-data"),

	MULTIPART_HEADER_SET(TopLevelType.MULTIPART, "header-set"),

	MULTIPART_RELATED(TopLevelType.MULTIPART, "related"),

	MULTIPART_REPORT(TopLevelType.MULTIPART, "report"),

	MULTIPART_SIGNED(TopLevelType.MULTIPART, "signed"),

	MULTIPART_VOICE_MESSAGE(TopLevelType.MULTIPART, "voice-aMessage"),

	MULTIPART_X_MIXED_REPLACE(TopLevelType.MULTIPART, "x-mixed-replace"),

	TEXT_1D_INTERLEAVED_PARITYFEC(TopLevelType.TEXT, "1d-interleaved-parityfec"),

	TEXT_CACHE_MANIFEST(TopLevelType.TEXT, "cache-manifest"),

	TEXT_CALENDAR(TopLevelType.TEXT, "calendar"),

	TEXT_CSS(TopLevelType.TEXT, "css"),

	TEXT_CSV(TopLevelType.TEXT, "csv"),

	TEXT_CSV_SCHEMA(TopLevelType.TEXT, "csv-schema"),

	TEXT_DIRECTORY(TopLevelType.TEXT, "directory"),

	TEXT_DNS(TopLevelType.TEXT, "dns"),

	TEXT_ECMASCRIPT(TopLevelType.TEXT, "ecmascript"),

	TEXT_ENCAPRTP(TopLevelType.TEXT, "encaprtp"),

	TEXT_EXAMPLE(TopLevelType.TEXT, "example"),

	TEXT_FWDRED(TopLevelType.TEXT, "fwdred"),

	TEXT_GRAMMAR_REF_LIST(TopLevelType.TEXT, "grammar-ref-list"),

	TEXT_HTML(TopLevelType.TEXT, "html"),

	TEXT_JAVASCRIPT(TopLevelType.TEXT, "javascript"),

	TEXT_JCR_CND(TopLevelType.TEXT, "jcr-cnd"),

	TEXT_MARKDOWN(TopLevelType.TEXT, "markdown"),

	TEXT_MIZAR(TopLevelType.TEXT, "mizar"),

	TEXT_N3(TopLevelType.TEXT, "n3"),

	TEXT_PARAMETERS(TopLevelType.TEXT, "parameters"),

	TEXT_PLAIN(TopLevelType.TEXT, "plain"),

	TEXT_PROVENANCE_NOTATION(TopLevelType.TEXT, "provenance-notation"),

	TEXT_PRS_FALLENSTEIN_RST(TopLevelType.TEXT, "prs.fallenstein.rst"),

	TEXT_PRS_LINES_TAG(TopLevelType.TEXT, "prs.lines.tag"),

	TEXT_PRS_PROP_LOGIC(TopLevelType.TEXT, "prs.prop.logic"),

	TEXT_RAPTORFEC(TopLevelType.TEXT, "raptorfec"),

	TEXT_RED(TopLevelType.TEXT, "RED"),

	TEXT_RFC822_HEADERS(TopLevelType.TEXT, "rfc822-headers"),

	TEXT_RTF(TopLevelType.TEXT, "rtf"),

	TEXT_RTP_ENC_AESCM128(TopLevelType.TEXT, "rtp-enc-aescm128"),

	TEXT_RTPLOOPBACK(TopLevelType.TEXT, "rtploopback"),

	TEXT_RTX(TopLevelType.TEXT, "rtx"),

	TEXT_SGML(TopLevelType.TEXT, "SGML"),

	TEXT_T140(TopLevelType.TEXT, "t140"),

	TEXT_TAB_SEPARATED_VALUES(TopLevelType.TEXT, "tab-separated-values"),

	TEXT_TROFF(TopLevelType.TEXT, "troff"),

	TEXT_TURTLE(TopLevelType.TEXT, "turtle"),

	TEXT_ULPFEC(TopLevelType.TEXT, "ulpfec"),

	TEXT_URI_LIST(TopLevelType.TEXT, "uri-list"),

	TEXT_VCARD(TopLevelType.TEXT, "vcard"),

	TEXT_VND_A(TopLevelType.TEXT, "vnd-a"),

	TEXT_VND_ABC(TopLevelType.TEXT, "vnd.abc"),

	TEXT_VND_ASCII_ART(TopLevelType.TEXT, "vnd.ascii-art"),

	TEXT_VND_CURL(TopLevelType.TEXT, "vnd-curl"),

	TEXT_VND_DEBIAN_COPYRIGHT(TopLevelType.TEXT, "vnd.debian.copyright"),

	TEXT_VND_DMCLIENTSCRIPT(TopLevelType.TEXT, "vnd.DMClientScript"),

	TEXT_VND_DVB_SUBTITLE(TopLevelType.TEXT, "vnd.dvb.subtitle"),

	TEXT_VND_ESMERTEC_THEME_DESCRIPTOR(TopLevelType.TEXT, "vnd.esmertec.theme-descriptor"),

	TEXT_VND_FLY(TopLevelType.TEXT, "vnd.fly"),

	TEXT_VND_FMI_FLEXSTOR(TopLevelType.TEXT, "vnd.fmi.flexstor"),

	TEXT_VND_GRAPHVIZ(TopLevelType.TEXT, "vnd.graphviz"),

	TEXT_VND_IN3D_3DML(TopLevelType.TEXT, "vnd.in3d.3dml"),

	TEXT_VND_IN3D_SPOT(TopLevelType.TEXT, "vnd.in3d.spot"),

	TEXT_VND_IPTC_NEWSML(TopLevelType.TEXT, "vnd.IPTC.NewsML"),

	TEXT_VND_IPTC_NITF(TopLevelType.TEXT, "vnd.IPTC.NITF"),

	TEXT_VND_LATEX_Z(TopLevelType.TEXT, "vnd.latex-z"),

	TEXT_VND_MOTOROLA_REFLEX(TopLevelType.TEXT, "vnd.motorola.reflex"),

	TEXT_VND_MS_MEDIAPACKAGE(TopLevelType.TEXT, "vnd.ms-mediapackage"),

	TEXT_VND_NET2PHONE_COMMCENTER_COMMAND(TopLevelType.TEXT, "vnd.net2phone.commcenter.command"),

	TEXT_VND_RADISYS_MSML_BASIC_LAYOUT(TopLevelType.TEXT, "vnd.radisys.msml-basic-layout"),

	TEXT_VND_SI_URICATALOGUE(TopLevelType.TEXT, "vnd.si.uricatalogue"),

	TEXT_VND_SUN_J2ME_APP_DESCRIPTOR(TopLevelType.TEXT, "vnd.sun.j2me.app-descriptor"),

	TEXT_VND_TROLLTECH_LINGUIST(TopLevelType.TEXT, "vnd.trolltech.linguist"),

	TEXT_VND_WAP_SI(TopLevelType.TEXT, "vnd.wap.si"),

	TEXT_VND_WAP_SL(TopLevelType.TEXT, "vnd.wap.sl"),

	TEXT_VND_WAP_WML(TopLevelType.TEXT, "vnd.wap-wml"),

	TEXT_VND_WAP_WMLSCRIPT(TopLevelType.TEXT, "vnd.wap.wmlscript"),

	TEXT_XML(TopLevelType.TEXT, "xml"),

	TEXT_XML_EXTERNAL_PARSED_ENTITY(TopLevelType.TEXT, "xml-external-parsed-entity"),

	TEXT_YAML(TopLevelType.TEXT, "x-yaml"),

	VIDEO_1D_INTERLEAVED_PARITYFEC(TopLevelType.VIDEO, "1d-interleaved-parityfec"),

	VIDEO_3GPP(TopLevelType.VIDEO, "3gpp"),

	VIDEO_3GPP2(TopLevelType.VIDEO, "3gpp2"),

	VIDEO_3GPP_TT(TopLevelType.VIDEO, "3gpp-tt"),

	VIDEO_BMPEG(TopLevelType.VIDEO, "BMPEG"),

	VIDEO_BT656(TopLevelType.VIDEO, "BT656"),

	VIDEO_CELB(TopLevelType.VIDEO, "CelB"),

	VIDEO_DV(TopLevelType.VIDEO, "DV"),

	VIDEO_ENCAPRTP(TopLevelType.VIDEO, "encaprtp"),

	VIDEO_EXAMPLE(TopLevelType.VIDEO, "example"),

	VIDEO_H261(TopLevelType.VIDEO, "H261"),

	VIDEO_H263(TopLevelType.VIDEO, "H263"),

	VIDEO_H263_1998(TopLevelType.VIDEO, "H263-1998"),

	VIDEO_H263_2000(TopLevelType.VIDEO, "H263-2000"),

	VIDEO_H264(TopLevelType.VIDEO, "H264"),

	VIDEO_H264_RCDO(TopLevelType.VIDEO, "H264-RCDO"),

	VIDEO_H264_SVC(TopLevelType.VIDEO, "H264-SVC"),

	VIDEO_H265(TopLevelType.VIDEO, "H265"),

	VIDEO_ISO_SEGMENT(TopLevelType.VIDEO, "iso.segment"),

	VIDEO_JPEG(TopLevelType.VIDEO, "JPEG"),

	VIDEO_JPEG2000(TopLevelType.VIDEO, "jpeg2000"),

	VIDEO_MJ2(TopLevelType.VIDEO, "mj2"),

	VIDEO_MP1S(TopLevelType.VIDEO, "MP1S"),

	VIDEO_MP2P(TopLevelType.VIDEO, "MP2P"),

	VIDEO_MP2T(TopLevelType.VIDEO, "MP2T"),

	VIDEO_MP4(TopLevelType.VIDEO, "mp4"),

	VIDEO_MP4V_ES(TopLevelType.VIDEO, "MP4V-ES"),

	VIDEO_MPV(TopLevelType.VIDEO, "MPV"),

	VIDEO_MPEG4_GENERIC(TopLevelType.VIDEO, "mpeg4-generic"),

	VIDEO_NV(TopLevelType.VIDEO, "nv"),

	VIDEO_OGG(TopLevelType.VIDEO, "ogg"),

	VIDEO_POINTER(TopLevelType.VIDEO, "pointer"),

	VIDEO_QUICKTIME(TopLevelType.VIDEO, "quicktime"),

	VIDEO_RAPTORFEC(TopLevelType.VIDEO, "raptorfec"),

	VIDEO_RTP_ENC_AESCM128(TopLevelType.VIDEO, "rtp-enc-aescm128"),

	VIDEO_RTPLOOPBACK(TopLevelType.VIDEO, "rtploopback"),

	VIDEO_RTX(TopLevelType.VIDEO, "rtx"),

	VIDEO_SMPTE292M(TopLevelType.VIDEO, "SMPTE292M"),

	VIDEO_ULPFEC(TopLevelType.VIDEO, "ulpfec"),

	VIDEO_VC1(TopLevelType.VIDEO, "vc1"),

	VIDEO_VND_CCTV(TopLevelType.VIDEO, "vnd.CCTV"),

	VIDEO_VND_DECE_HD(TopLevelType.VIDEO, "vnd.dece.hd"),

	VIDEO_VND_DECE_MOBILE(TopLevelType.VIDEO, "vnd.dece.mobile"),

	VIDEO_VND_DECE_MP4(TopLevelType.VIDEO, "vnd.dece-mp4"),

	VIDEO_VND_DECE_PD(TopLevelType.VIDEO, "vnd.dece.pd"),

	VIDEO_VND_DECE_SD(TopLevelType.VIDEO, "vnd.dece.sd"),

	VIDEO_VND_DECE_VIDEO(TopLevelType.VIDEO, "vnd.dece.video"),

	VIDEO_VND_DIRECTV_MPEG(TopLevelType.VIDEO, "vnd.directv-mpeg"),

	VIDEO_VND_DIRECTV_MPEG_TTS(TopLevelType.VIDEO, "vnd.directv.mpeg-tts"),

	VIDEO_VND_DLNA_MPEG_TTS(TopLevelType.VIDEO, "vnd.dlna.mpeg-tts"),

	VIDEO_VND_DVB_FILE(TopLevelType.VIDEO, "vnd.dvb.file"),

	VIDEO_VND_FVT(TopLevelType.VIDEO, "vnd.fvt"),

	VIDEO_VND_HNS_VIDEO(TopLevelType.VIDEO, "vnd.hns.video"),

	VIDEO_VND_IPTVFORUM_1DPARITYFEC_1010(TopLevelType.VIDEO, "vnd.iptvforum.1dparityfec-1010"),

	VIDEO_VND_IPTVFORUM_1DPARITYFEC_2005(TopLevelType.VIDEO, "vnd.iptvforum.1dparityfec-2005"),

	VIDEO_VND_IPTVFORUM_2DPARITYFEC_1010(TopLevelType.VIDEO, "vnd.iptvforum.2dparityfec-1010"),

	VIDEO_VND_IPTVFORUM_2DPARITYFEC_2005(TopLevelType.VIDEO, "vnd.iptvforum.2dparityfec-2005"),

	VIDEO_VND_IPTVFORUM_TTSAVC(TopLevelType.VIDEO, "vnd.iptvforum.ttsavc"),

	VIDEO_VND_IPTVFORUM_TTSMPEG2(TopLevelType.VIDEO, "vnd.iptvforum.ttsmpeg2"),

	VIDEO_VND_MOTOROLA_VIDEO(TopLevelType.VIDEO, "vnd.motorola.video"),

	VIDEO_VND_MOTOROLA_VIDEOP(TopLevelType.VIDEO, "vnd.motorola.videop"),

	VIDEO_VND_MPEGURL(TopLevelType.VIDEO, "vnd-mpegurl"),

	VIDEO_VND_MS_PLAYREADY_MEDIA_PYV(TopLevelType.VIDEO, "vnd.ms-playready.media.pyv"),

	VIDEO_VND_NOKIA_INTERLEAVED_MULTIMEDIA(TopLevelType.VIDEO, "vnd.nokia.interleaved-multimedia"),

	VIDEO_VND_NOKIA_VIDEOVOIP(TopLevelType.VIDEO, "vnd.nokia.videovoip"),

	VIDEO_VND_OBJECTVIDEO(TopLevelType.VIDEO, "vnd.objectvideo"),

	VIDEO_VND_RADGAMETTOOLS_BINK(TopLevelType.VIDEO, "vnd.radgamettools.bink"),

	VIDEO_VND_RADGAMETTOOLS_SMACKER(TopLevelType.VIDEO, "vnd.radgamettools.smacker"),

	VIDEO_VND_SEALED_MPEG1(TopLevelType.VIDEO, "vnd.sealed.mpeg1"),

	VIDEO_VND_SEALED_MPEG4(TopLevelType.VIDEO, "vnd.sealed.mpeg4"),

	VIDEO_VND_SEALED_SWF(TopLevelType.VIDEO, "vnd.sealed-swf"),

	VIDEO_VND_SEALEDMEDIA_SOFTSEAL_MOV(TopLevelType.VIDEO, "vnd.sealedmedia.softseal-mov"),

	VIDEO_VND_UVVU_MP4(TopLevelType.VIDEO, "vnd.uvvu-mp4"),

	VIDEO_VND_VIVO(TopLevelType.VIDEO, "vnd-vivo"),

	VIDEO_VP8(TopLevelType.VIDEO, "VP8"),

	WILDCARD_WILDCARD(TopLevelType.WILDCARD, "*");

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private TopLevelType _topLevelType;

	private String _mediaTypeName;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new media type.
	 *
	 * @param aTopLevelType the top level type
	 * @param aMediaSubType the media sub type
	 */
	private MediaType( TopLevelType aTopLevelType, String aMediaSubType ) {
		_topLevelType = aTopLevelType;
		_mediaTypeName = aMediaSubType;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TopLevelType getTopLevelType() {
		return _topLevelType;
	}

	/**
	 * Returns the sub-type part of the {@link MediaType}. The
	 * {@link TopLevelType} portion is not prepended! Use
	 * {@link #toHttpMediaType()} to construct a valid Media-Type descriptor
	 * consisting of {@link TopLevelType} and {@link MediaType}.
	 * 
	 * @return The name of the {@link MediaType} without the
	 *         {@link TopLevelType} portion!
	 */
	@Override
	public String getName() {
		return _mediaTypeName;
	}

	/**
	 * Returns the Media-Type / MIME-Type as expected by the HTTP protocol. This
	 * means that the {@link TopLevelType}'s name with the {@link MediaType}'s
	 * name is concatenated in an HTTP valid manner such as: "application/json".
	 * 
	 * @return The Media-Type as of the IANA specification.
	 */
	@Override
	public String toHttpMediaType() {
		return _topLevelType.getName() + Delimiter.PATH.getChar() + _mediaTypeName;
	}

	/**
	 * Similar to {@link #toHttpMediaType()} with the provided parameters
	 * suffixed in an HTTP heder field's synatx.
	 * 
	 * @param aParameters The parameters to be suffixed to the HTTP Media-Type
	 *        field.
	 * 
	 * @return The Media-Type including the parameters (if any) as of the IANA
	 *         specification.
	 */
	public String toHttpMediaType( Properties aParameters ) {
		String theResult = toHttpMediaType();
		String eValue;
		for ( String eParam : aParameters.keySet() ) {
			theResult += Delimiter.MEDIA_TYPE_PARAMETERS.getChar() + eParam;
			eValue = aParameters.get( eParam );
			if ( eValue != null ) {
				theResult += Delimiter.PROPERTY.getChar() + eValue;
			}
		}
		return theResult;
	}

	/**
	 * Returns a {@link ContentType} instance from the available data. E.g. the
	 * Media-Top-Level-Type and the Media-Subtype. {@inheritDoc}
	 */
	@Override
	public ContentType toContentType() {
		return new ContentType( this );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns all herein defined {@link MediaType} elements belonging to the
	 * given {@link TopLevelType}.
	 * 
	 * @param aTopLevelType The top level Media-Type for which to get all herein
	 *        known {@link MediaType} elements.
	 * 
	 * @return All {@link MediaType} elements belonging to the given
	 *         {@link TopLevelType}.
	 */
	public static MediaType[] fromTopLevelType( TopLevelType aTopLevelType ) {
		final List<MediaType> theMediaTypes = new ArrayList<>();
		for ( MediaType eTopLevelMediaType : values() ) {
			if ( eTopLevelMediaType.getTopLevelType() == aTopLevelType ) {
				theMediaTypes.add( eTopLevelMediaType );
			}
		}
		return theMediaTypes.toArray( new MediaType[theMediaTypes.size()] );
	}

	/**
	 * Retrieves the {@link MediaType} representing the provided HTTP Media-Type
	 * (ignoring the case).
	 * 
	 * @param aHttpMediaSubtype The HTTP Media-Type for which to determine the
	 *        {@link MediaType}.
	 * 
	 * @return The {@link MediaType} determined or null if none such was found.
	 */
	public static MediaType fromHttpMediaSubtype( String aHttpMediaSubtype ) {
		if ( aHttpMediaSubtype == null || aHttpMediaSubtype.isEmpty() ) {
			return null;
		}
		final int indexOf = aHttpMediaSubtype.indexOf( Delimiter.MEDIA_TYPE_PARAMETERS.getChar() );
		if ( indexOf != -1 ) {
			aHttpMediaSubtype = aHttpMediaSubtype.substring( 0, indexOf );
		}
		aHttpMediaSubtype = aHttpMediaSubtype.trim();
		for ( MediaType eHttpMediaSubtype : values() ) {
			if ( eHttpMediaSubtype.toHttpMediaType().equalsIgnoreCase( aHttpMediaSubtype ) ) {
				return eHttpMediaSubtype;
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return toHttpMediaType();
	}
}
