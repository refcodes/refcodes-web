// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides access to a load balancing strategy property as of
 * {@link LoadBalancingStrategy}.
 */
public interface LoadBalancingStrategyAccessor {

	/**
	 * Retrieves the load balancing strategy from the load balancing strategy
	 * property.
	 * 
	 * @return The load balancing strategy stored by the load balancing strategy
	 *         property.
	 */
	LoadBalancingStrategy getLoadBalancingStrategy();

	/**
	 * Extends the {@link LoadBalancingStrategyAccessor} with a setter method.
	 */
	public interface LoadBalancingStrategyMutator {

		/**
		 * Sets the load balancing strategy for the load balancing strategy
		 * property.
		 * 
		 * @param aStrategy The load balancing strategy to be stored by the load
		 *        balancing strategy property.
		 */
		void setLoadBalancingStrategy( LoadBalancingStrategy aStrategy );
	}

	/**
	 * Provides a builder method for a load balancing strategy property
	 * returning the builder for applying multiple build operations.
	 *
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface LoadBalancingStrategyBuilder<B extends LoadBalancingStrategyBuilder<B>> {

		/**
		 * Sets the load balancing strategy for the load balancing strategy
		 * property.
		 * 
		 * @param aStrategy The load balancing strategy to be stored by the load
		 *        balancing strategy property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withLoadBalancingStrategy( LoadBalancingStrategy aStrategy );
	}

	/**
	 * Extends the {@link LoadBalancingStrategyAccessor} with a setter method.
	 */
	public interface LoadBalancingStrategyProperty extends LoadBalancingStrategyAccessor, LoadBalancingStrategyMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link LoadBalancingStrategy} (setter) as of
		 * {@link #setLoadBalancingStrategy(LoadBalancingStrategy)} and returns
		 * the very same value (getter).
		 * 
		 * @param aLoadBalancingStrategy The {@link LoadBalancingStrategy} to
		 *        set (via
		 *        {@link #setLoadBalancingStrategy(LoadBalancingStrategy)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default LoadBalancingStrategy letLoadBalancingStrategy( LoadBalancingStrategy aLoadBalancingStrategy ) {
			setLoadBalancingStrategy( aLoadBalancingStrategy );
			return aLoadBalancingStrategy;
		}
	}
}
