// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides access to a HTTP Status-Code property including all known HTTP HTTP
 * Status-Codes (success as well as error codes) as of {@link HttpStatusCode}.
 */
public interface HttpStatusCodeAccessor {

	/**
	 * Retrieves the HTTP Status-Code from the HTTP Status-Code property.
	 * 
	 * @return The HTTP Status-Code stored by the HTTP Status-Code property.
	 */
	HttpStatusCode getHttpStatusCode();

	/**
	 * Extends the {@link HttpStatusCodeAccessor} with a setter method.
	 */
	public interface HttpStatusCodeMutator {

		/**
		 * Sets the HTTP Status-Code for the HTTP Status-Code property.
		 * 
		 * @param aStatusCode The HTTP Status-Code to be stored by the HTTP HTTP
		 *        Status-Code property.
		 */
		void setHttpStatusCode( HttpStatusCode aStatusCode );
	}

	/**
	 * Provides a builder method for a HTTP Status-Code property returning the
	 * builder for applying multiple build operations.
	 *
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface HttpStatusCodeBuilder<B extends HttpStatusCodeBuilder<B>> {

		/**
		 * Sets the HTTP Status-Code for the HTTP Status-Code property.
		 * 
		 * @param aStatusCode The HTTP Status-Code to be stored by the HTTP HTTP
		 *        Status-Code property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withHttpStatusCode( HttpStatusCode aStatusCode );
	}

	/**
	 * Extends the {@link HttpStatusCodeAccessor} with a setter method.
	 */
	public interface HttpStatusCodeProperty extends HttpStatusCodeAccessor, HttpStatusCodeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link HttpStatusCode}
		 * (setter) as of {@link #setHttpStatusCode(HttpStatusCode)} and returns
		 * the very same value (getter).
		 * 
		 * @param aHttpStatusCode The {@link HttpStatusCode} to set (via
		 *        {@link #setHttpStatusCode(HttpStatusCode)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default HttpStatusCode letHttpStatusCode( HttpStatusCode aHttpStatusCode ) {
			setHttpStatusCode( aHttpStatusCode );
			return aHttpStatusCode;
		}
	}
}
