// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * Provides an accessor for a {@link HeaderField} property.
 */
public interface HeaderFieldAccessor {

	/**
	 * Retrieves the {@link HeaderField} from the {@link HeaderField} property.
	 * 
	 * @return The {@link HeaderField} stored by the {@link HeaderField}
	 *         property.
	 */
	HeaderField getHeaderField();

	/**
	 * Provides a mutator for a {@link HeaderField} property.
	 */
	public interface HeaderFieldMutator {

		/**
		 * Sets the {@link HeaderField} for the {@link HeaderField} property.
		 * 
		 * @param aHeaderField The {@link HeaderField} to be stored by the
		 *        {@link HeaderField} property.
		 */
		void setHeaderField( HeaderField aHeaderField );
	}

	/**
	 * Provides a builder method for a {@link HeaderField} property returning
	 * the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface HeaderFieldBuilder<B extends HeaderFieldBuilder<B>> {

		/**
		 * Sets the {@link HeaderField} for the {@link HeaderField} property.
		 * 
		 * @param aHeaderField The {@link HeaderField} to be stored by the
		 *        {@link HeaderField} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withHeaderField( HeaderField aHeaderField );
	}

	/**
	 * Provides a {@link HeaderField} property.
	 */
	public interface HeaderFieldProperty extends HeaderFieldAccessor, HeaderFieldMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link HeaderField}
		 * (setter) as of {@link #setHeaderField(HeaderField)} and returns the
		 * very same value (getter).
		 * 
		 * @param aHeaderField The {@link HeaderField} to set (via
		 *        {@link #setHeaderField(HeaderField)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default HeaderField letHeaderField( HeaderField aHeaderField ) {
			setHeaderField( aHeaderField );
			return aHeaderField;
		}
	}
}
