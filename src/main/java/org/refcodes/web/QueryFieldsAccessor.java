// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

import org.refcodes.web.FormFieldsAccessor.FormFieldsBuilder;

/**
 * Provides an accessor for a request Query-Fields property.
 */
public interface QueryFieldsAccessor {

	/**
	 * Retrieves the request Query-Fields from the request Query-Fields
	 * property.
	 * 
	 * @return The request Query-Fields stored by the request Query-Fields
	 *         property.
	 */
	FormFields getQueryFields();

	/**
	 * Provides a mutator for a request Query-Fields property.
	 */
	public interface QueryFieldsMutator {

		/**
		 * Sets the request Query-Fields for the request Query-Fields property.
		 * 
		 * @param aQueryFields The request Query-Fields to be stored by the form
		 *        fields property.
		 */
		void setQueryFields( FormFields aQueryFields );
	}

	/**
	 * Provides a mutator for a request Query-Fields property.
	 * 
	 * @param <B> The builder which implements the {@link FormFieldsBuilder}.
	 */
	public interface QueryFieldsBuilder<B extends QueryFieldsBuilder<?>> {

		/**
		 * Sets the request Query-Fields to use and returns this builder as of
		 * the Builder-Pattern.
		 * 
		 * @param aQueryFields The request Query-Fields to be stored by the form
		 *        fields property.
		 * 
		 * @return This {@link FormFieldsBuilder} instance to continue
		 *         configuration.
		 */
		B withQueryFields( FormFields aQueryFields );
	}

	/**
	 * Provides a request Query-Fields property.
	 */
	public interface QueryFieldsProperty extends QueryFieldsAccessor, QueryFieldsMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link FormFields}
		 * (setter) as of {@link #setQueryFields(FormFields)} and returns the
		 * very same value (getter).
		 * 
		 * @param aQueryFields The {@link FormFields} to set (via
		 *        {@link #setQueryFields(FormFields)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default FormFields letQueryFields( FormFields aQueryFields ) {
			setQueryFields( aQueryFields );
			return aQueryFields;
		}
	}
}
