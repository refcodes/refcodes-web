// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.web;

/**
 * The Interface MediaTypeFactoryLookup.
 *
 * @author steiner
 */
public interface MediaTypeFactoryLookup {

	/**
	 * Retrieves the {@link MediaTypeFactory} related to the given
	 * {@link MediaType} or null if there is none such {@link MediaTypeFactory}.
	 *
	 * @param aMediaType the media type
	 * 
	 * @return The according {@link MediaTypeFactory} or null if there is none
	 *         such.
	 */
	MediaTypeFactory toMediaTypeFactory( MediaType aMediaType );

	/**
	 * Determines whether there is a {@link MediaTypeFactory} for the given
	 * {@link MediaType}.
	 *
	 * @param aMediaType the media type
	 * 
	 * @return True in case there is a {@link MediaTypeFactory} responsible for
	 *         the given {@link MediaType}, else false.
	 */
	default boolean hasMediaTypeFactory( MediaType aMediaType ) {
		return toMediaTypeFactory( aMediaType ) != null;
	}

	/**
	 * Returns the currently supported {@link MediaType}s as of the
	 * {@link MediaTypeFactory} instances being added and in the order of them
	 * being added. The order is important as the first {@link MediaType} found
	 * in the returned array may be used as default {@link MediaType} e.g. when
	 * no Content-Type has been specified.
	 * 
	 * @return The currently supported {@link MediaType}s in the order of the
	 *         {@link MediaTypeFactory} instance having been added.
	 */
	MediaType[] getFactoryMediaTypes();

	/**
	 * Adds functionality to modify the {@link MediaTypeFactoryLookup} e.g.
	 * adding additional {@link MediaTypeFactory} instances by using
	 * {@link #addMediaTypeFactory(MediaTypeFactory)}.
	 */
	public interface MutableMediaTypeFactoryLookup extends MediaTypeFactoryLookup {

		/**
		 * Registers a {@link MediaTypeFactory} for marshaling and unmarshaling
		 * a HTTP-Request's body of the according {@link MediaType}. A
		 * {@link MediaTypeFactory} is responsible for exactly one
		 * {@link MediaType} , declared via the
		 * {@link MediaTypeFactory#getMediaTypes()} method. .
		 * 
		 * @param aMediaTypeFactory The {@link MediaTypeFactory} to be
		 *        registered.
		 * 
		 * @return True in case the {@link MediaTypeFactory} has successfully
		 *         been registered, false in case there has already a
		 *         {@link MediaTypeFactory} been registered which is responsible
		 *         for the provided {@link MediaTypeFactory}'s {@link MediaType}
		 *         .
		 */
		boolean addMediaTypeFactory( MediaTypeFactory aMediaTypeFactory );
	}
}