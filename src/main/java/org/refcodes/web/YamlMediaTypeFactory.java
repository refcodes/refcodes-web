package org.refcodes.web;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.refcodes.exception.MarshalException;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.struct.TypeUtility;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

/**
 * Implements the {@link MediaTypeFactory} for Media-Type "application/YAML" (
 * {@link MediaType#APPLICATION_YAML}). CAUTION: This implementation uses field
 * resolution instead of getter/setter property resolution as of the used
 * marshaling API.
 */
public class YamlMediaTypeFactory implements MediaTypeFactory {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final MediaType[] MEDIA_TYPES = new MediaType[] { MediaType.APPLICATION_YAML, MediaType.TEXT_YAML };

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final ObjectMapper _mapper = new ObjectMapper( new YAMLFactory() );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link YamlMediaTypeFactory}.
	 */
	public YamlMediaTypeFactory() {
		_mapper.configure( DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MediaType[] getMediaTypes() {
		return MEDIA_TYPES;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toMarshaled( Object aObject ) throws MarshalException {
		// if ( aObject instanceof PathMap<?> ) {
		//	aObject = ((PathMap<?>) aObject).toDataStructure();
		// }
		try {
			// String theYAML = _gson.toYAML( aObject );
			return _mapper.writeValueAsString( aObject );
		}
		catch ( Exception e ) {
			throw new MarshalException( "A problem occurred marshaling an object of type <" + aObject.getClass().getName() + ">.", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> T toUnmarshaled( String aHttpBody, Class<T> aType ) throws UnmarshalException {
		try {
			if ( aHttpBody == null ) {
				return null;
			}
			else if ( aHttpBody.isEmpty() ) {
				return aType.getConstructor().newInstance();
			}
			return _mapper.readValue( aHttpBody, aType );
		}
		catch ( JsonMappingException | JsonParseException e ) {
			if ( aType.isArray() ) {
				try {
					final Map<?, ?> theMap = _mapper.readValue( aHttpBody, Map.class );
					return TypeUtility.toArrayType( theMap, aType );
				}
				catch ( JsonProcessingException e2 ) {
					throw new UnmarshalException( "Unable to unmarshal the HTTP body \"" + aHttpBody + "\" to type  <" + aType.getName() + "> at column <" + e.getLocation().getColumnNr() + ">!", e );
				}
			}
			else if ( Map.class.isAssignableFrom( aType ) ) {
				try {
					final Map<Object, Object> theMap;
					if ( Map.class.equals( aType ) ) {
						theMap = new HashMap<>();
					}
					else {
						theMap = (Map<Object, Object>) aType.getDeclaredConstructor().newInstance();
					}
					final Object[] theArray = _mapper.readValue( aHttpBody, Object[].class );
					if ( theArray != null ) {
						for ( int i = 0; i < theArray.length; i++ ) {
							if ( theArray[i] != null ) {
								theMap.put( Integer.toString( i ), theArray[i] );
							}
						}
					}
					return (T) theMap;

				}
				catch ( JsonProcessingException | IllegalAccessException | InstantiationException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e2 ) {
					throw new UnmarshalException( "Unable to unmarshal the HTTP body \"" + aHttpBody + "\" to type  <" + aType.getName() + "> at column <" + e.getLocation().getColumnNr() + ">!", e );
				}
			}
			throw new UnmarshalException( "Unable to unmarshal the HTTP body \"" + aHttpBody + "\" to type  <" + aType.getName() + "> at column <" + e.getLocation().getColumnNr() + ">!", e );
		}
		catch ( Exception e ) {
			throw new UnmarshalException( "Unable to unmarshal the HTTP body \"" + aHttpBody + "\" to type  <" + aType.getName() + ">!", e );
		}
	}
}