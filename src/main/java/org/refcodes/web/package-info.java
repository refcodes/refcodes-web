// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

/**
 * Use the types and definitions of this artifact when you are tired of defining
 * your own networking based basics, your own HTTP-status-code enumerations
 * ({@link org.refcodes.web.HttpStatusCode} or exceptions representing erroneous
 * HTTP state, or if you are tired of cookie handling, Header-Field processing
 * ... and so on:
 * <p>
 * This artifact provides various types such as {@link org.refcodes.web.Cookie},
 * {@link org.refcodes.web.FormFields}, {@link org.refcodes.web.HeaderFields} or
 * {@link org.refcodes.web.HttpMethod}, which enable you to use such HTTP based
 * structures in a native Java way while having a tool at hand to bridge back
 * into the HTTP protocol world. HTTP related error codes are provided as
 * exception types such as {@link org.refcodes.web.NotFoundException} (404) or
 * `InternalServerErrorException` (500).
 * <p>
 * Please note that the {@link org.refcodes.web.HttpBodyMap} is most useful when
 * you have to handle dynamic data structures. The
 * {@link org.refcodes.web.HttpBodyMap} type is a "dynamic" type in terms of
 * it's structure being represented by paths'. When a data structure is parsed
 * e.g. from JSON or XML, then the data structure's internal structure and
 * values are reflected by according paths's and values in the
 * {@link org.refcodes.web.HttpBodyMap}. The
 * {@link org.refcodes.web.HttpBodyMap} is a kind of flat (no nested maps)
 * representation of the according (nested) data structures. Implementations of
 * the {@link org.refcodes.web.HttpClientRequest},
 * {@link org.refcodes.web.HttpClientResponse},
 * {@link org.refcodes.web.HttpServerRequest} as well as
 * {@link org.refcodes.web.HttpServerResponse} types support the
 * {@link org.refcodes.web.HttpBodyMap}.
 * <p>
 * As another example, the {@link org.refcodes.web.BasicAuthCredentials} make it
 * ease to parse or create Basic-Auth Header-Fields.
 * <p>
 * Other interesting types and classes to mention are the
 * {@link org.refcodes.data.Port} enumeration, defining quite a few default
 * ports or the {@link org.refcodes.web.MediaTypeFactory} interface, which's
 * implementations make marshaling and unmarshaling of your types from and to
 * JSON, XML (and so on) an easy task.
 * <p>
 * Predefined MIME-Types are bundled in the {@link org.refcodes.web.MediaType}
 * enumeration, as of the information provided by the
 * [`IANA`](http://www.iana.org/assignments/media-types/media-types.xhtml),
 * effective in October 2016.
 * <p>
 * See the [Javadoc](https://www.javadoc.io/doc/org.refcodes/refcodes-web) for a
 * full overview on this artifact.
 */
package org.refcodes.web;