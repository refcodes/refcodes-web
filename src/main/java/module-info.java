module org.refcodes.web {
	requires org.refcodes.factory;
	requires org.refcodes.textual;
	requires transitive org.refcodes.properties;
	requires transitive org.refcodes.data;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.io;
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.net;
	requires transitive org.refcodes.security;
	requires transitive org.refcodes.struct;
	requires transitive org.refcodes.struct.ext.factory;
	requires transitive org.refcodes.time;
	requires com.fasterxml.jackson.databind;
	requires com.fasterxml.jackson.dataformat.yaml;

	exports org.refcodes.web;
}
