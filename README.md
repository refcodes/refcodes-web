# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***This artifact provides web (HTTP) related definitions and types being used by [`REFCODES.ORG`](http://www.refcodes.org/refcodes) web related functionality and artifacts.***

## How do I get set up? ##

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:

```
<dependencies>
	...
	<dependency>
		<artifactId>refcodes-web</artifactId>
		<groupId>org.refcodes</groupId>
		<version>3.0.0</version>
	</dependency>
	...
</dependencies>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-web). Read the artifact's javadoc at [javadoc.io](http://www.javadoc.io/doc/org.refcodes/refcodes-web).

## How do I get started? ##

Use the types and definitions of this artifact when you are tired of defining your own networking based basics, your own HTTP-status-code enumerations (`HttpStatusCode`) or exceptions representing erroneous HTTP state, or if you are tired of cookie handling, header field processing ... and so on:

This artifact provides various types such as `Cookie`, `FormFields`, `HeaderFields` or `HttpMethod`, which enable you to use such HTTP based structures in a native *Java* way while having a tool at hand to bridge back into the HTTP protocol world. HTTP related error codes are provided as  exception types such as `NotFoundException` (404) or `InternalServerErrorException` (500).

Please note that the `HttpBodyMap` is most useful when you have to handle dynamic data structures. The `HttpBodyMap` type is a "dynamic" type in terms of it's structure being represented by paths'. When a data structure is parsed e.g. from JSON or XML, then the data structure's internal structure and values are reflected by according paths's and values in the `HttpBodyMap`. The `HttpBodyMap` is a kind of flat (no nested maps) representation of the according (nested) data structures. Implementations of the `HttpClientRequest`, `HttpClientResponse`, `HttpServerRequest` as well as `HttpServerResponse` types support the `HttpBodyMap`.

As another example, the `BasicAuthCredentials` make it ease to parse or create `Basic-Auth` header fields.

Other interesting types and classes to mention are the `Port` enumeration, defining quite a few default ports or the `MediaTypefactory` interface, which's implementations make marshaling and unmarshaling of your types from and to JSON, XML (and so on) an easy task.

Predefined MIME-Types are bundled in the `MediaType` enumeration, as of the information provided by the [`IANA`](http://www.iana.org/assignments/media-types/media-types.xhtml), effective  in October 2016.

See the [Javadoc](https://www.javadoc.io/doc/org.refcodes/refcodes-web) for a full overview on this artifact.

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/refcodes/refcodes-web/issues)
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.
